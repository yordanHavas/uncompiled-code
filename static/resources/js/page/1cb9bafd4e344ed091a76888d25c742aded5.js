
/* -------------------
BEGIN  
Sitecore Resource: f2cf054d-931d-4751-97ea-29d578d2cb4e
   -------------------*/
function enterSite(){
    if($("#accessCode-bottom").length > 0) {
        var e = $("#accessCode-bottom").val().trim();
        if (e && e.length > 2 ) {
            localStorage.setItem("din", e);
            window.location.replace("patientenlogin/homepage/");
             var now = new Date();
            var time = now.getTime();
            //var expireTime = time + (1000 * 60 * 60 * 24 * expireDays);
            var expireTime = time + (1000 * 60 * 20);
            now.setTime(expireTime);
            //document.cookie = name + "=" + value + "; expires=" + now.toGMTString() + "; path=/";
            document.cookie = "BatchLogin=True"+"; expires=" + now.toGMTString() + ";path=/";
        }
        else{
            if (localStorage.getItem("din")) localStorage.removeItem("din");
            $("#accessCodeError").show();
        }
    } 
}

function enterSiteTop(){
    var e = $("#accessCode-top").val().trim();
    if (e && e.length > 2 ) {
        localStorage.setItem("din", e);
        window.location.replace("patientenlogin/homepage/");
         var now = new Date();
        var time = now.getTime();
        //var expireTime = time + (1000 * 60 * 60 * 24 * expireDays);
        var expireTime = time + (1000 * 60 * 20);
        now.setTime(expireTime);
        //document.cookie = name + "=" + value + "; expires=" + now.toGMTString() + "; path=/";
        document.cookie = "BatchLogin=True"+"; expires=" + now.toGMTString() + ";path=/";
    }
    else{
        if (localStorage.getItem("din")) localStorage.removeItem("din");
        $("#accessCodeError").show();
    }
       
}

$(function(){
    if($(".form-bottom").length > 0) {
        $(".form-bottom").submit(function(e){
            e.preventDefault();
            enterSite();
            
        });    
        $("#accessCodeError").hide();
    }
    
});

$(function(){
    $(".form-top").submit(function(e){
        enterSiteTop();
        e.preventDefault();
        
        
    });    
    $("#accessCodeError").hide();
});

$(document).ready(function() {
    
    $("#submit-bottom").attr('disabled', 'disabled');
    $("#accessCode-bottom").keyup(function(e) {
        
        // To Disable Submit Button
        $("#submit-bottom").attr('disabled', 'disabled');
        
        // Validating Fields
        var chargeNumber = $(this).val().toUpperCase();
        var filter = new RegExp('^[0-9]{1}[A-Za-z]{1}[A-Za-z0-9]{3}[A-Za-z]{1}$');
        
        var chargeNumberList =  [   
            "9L455F", "9L003K", "9L816H", "0L127K", "0L127K", "0L342C", "0L741C", "1LU01F",
            "9L748K", "0L440C", "0L132W", "0L254C", "0L254M",
            "9L175C", "9L806F", "0L440J", "0L330A", "0L440J", "0L440Z",
            "9L426V", "9L352A", "9L352A", "9L003H", "0L002F", "0LU05H", "0LU16A", "0L342H", "0L419A", "0L419C", "0L555C", "0L496C", "0L218H", "1L137A",
            "9F004A", "0F010A", "0F010B", "0F030A", "0F030B", "1F068A", "1F068B",  
            "9F007A", "9F007A", "0F015A", "0F016A", "0F027A", "0F033A", "0F033B", "1F055A", "1F058A", "1F069A", "1F087A",
            "9F008A", "0F015A", "0F022A", "0F016A", "0F022B", "0F027A", "0F033A", "1F056A", "1F055A", "1F057A", "1F067A", "1F069A", "1F087A"                                   
        ];

        if (!(chargeNumber == "")) {
            if ( chargeNumberList.includes(chargeNumber) || chargeNumberList.indexOf(chargeNumber) !== -1) {                
                // To Enable Submit Button
                $("#submit-bottom").removeAttr('disabled');
            }
            else if (filter.test(chargeNumber) || filter2.test(chargeNumber)) {
                // To Enable Submit Button
                $("#submit-bottom").removeAttr('disabled');
            }
        } else {
            // To Disable Submit Button
            $("#submit-bottom").attr('disabled', 'disabled');
        }

    });
    
    $("#submit-top").attr('disabled', 'disabled');
    $("#accessCode-top").keyup(function(e) {
        
        // To Disable Submit Button
        $("#submit-top").attr('disabled', 'disabled');
        
        // Validating Fields
        var chargeNumber = $(this).val().toUpperCase();
        var filter = new RegExp('^[0-9]{1}[A-Za-z]{1}[A-Za-z0-9]{3}[A-Za-z]{1}$');
        
        var chargeNumberList =  [   
            "9L455F", "9L003K", "9L816H", "0L127K", "0L127K", "0L342C", "0L741C", "1LU01F",
            "9L748K", "0L440C", "0L132W", "0L254C", "0L254M",
            "9L175C", "9L806F", "0L440J", "0L330A", "0L440J", "0L440Z",
            "9L426V", "9L352A", "9L352A", "9L003H", "0L002F", "0LU05H", "0LU16A", "0L342H", "0L419A", "0L419C", "0L555C", "0L496C", "0L218H", "1L137A",
            "9F004A", "0F010A", "0F010B", "0F030A", "0F030B", "1F068A", "1F068B",  
            "9F007A", "9F007A", "0F015A", "0F016A", "0F027A", "0F033A", "0F033B", "1F055A", "1F058A", "1F069A", "1F087A",
            "9F008A", "0F015A", "0F022A", "0F016A", "0F022B", "0F027A", "0F033A", "1F056A", "1F055A", "1F057A", "1F067A", "1F069A", "1F087A"                                   
        ];

        if (!(chargeNumber == "")) {
            if ( chargeNumberList.includes(chargeNumber) || chargeNumberList.indexOf(chargeNumber) !== -1) {                
                // To Enable Submit Button
                $("#submit-top").removeAttr('disabled');
            }
            else if (filter.test(chargeNumber) || filter2.test(chargeNumber)) {
                // To Enable Submit Button
                $("#submit-top").removeAttr('disabled');
            }
        } else {
            // To Disable Submit Button
            $("#submit-top").attr('disabled', 'disabled');
        }

    });

    $('#btnLogoutbtn').click(function(e) {
	        e.preventDefault();
	        userLogout();
	        
	        console.log(document.referrer)
		})
		
		var delete_cookie = function(name) {
	    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;';
	};
});

function userLogout() {
    $.ajax({
        type: "POST",
        url: "/api/UserValidateApi/UserLogOut/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
			})
		.done(function (response) {
		    console.log(response);
		    eraseCookie('User');
		   eraseCookie('BatchLogin');
		   	delete_cookie('BatchLogin');
		   		delete_cookie('User');
		   	 //window.location ="/asthma-fachkreise";
		   	 
		   	/*if(getCookie('User') ==""){
		   	     window.location ="/asthma-fachkreise";
		   	}
		   	else if(getCookie('BatchLogin') == ""){
		   	    window.location ="/asthma-patienten";
		   	}*/
		   	
		 	if(document.referrer.includes('/asthma-fachkreise')){
                window.location ="asthma-fachkreise.html";
            }
            else if (document.referrer.includes('/asthma-patienten')){
                window.location ="asthma-patienten.html";
            }
            else if (document.referrer.includes('/dupixent-patient')){
                 window.location ="index.html"; 
            }
            else if (document.referrer.includes('/patientenlogin')){
            	console.log(2);
                 window.location ="index.html"; 
            }
		})
		.fail(function (e) {
			console.log(e);
			console.log("error");
		});
    
}

/* -------------------
BEGIN  
Sitecore Resource: d7f791f8-73d3-4807-acdc-c2dcbdcac9d7
   -------------------*/
$(document).ready(function () {
    $(".burger_menu").click(function () {
        if ($('.menu_list').css('display') === 'none') {
            $('.submenu_list').css('display', 'none');
            $('.list_btn_submenu').css('marginBottom', '0');
            $('.menu_list').css('display', 'block');
        }
        else {
            $('.menu_list').css('display', 'none');
        }
    });

    $("#header_submenu").click(function () {
        if ($('.submenu_list').css('display') === 'none') {
            $('.list_btn_submenu').css('marginBottom', '194px');
            $('.submenu_list').css('display', 'block');
        }
        else {
            $('.list_btn_submenu').css('marginBottom', '0');
            $('.submenu_list').css('display', 'none');
        }
    });
});


/* -------------------
BEGIN  
Sitecore Resource: 5cd2bd06-9576-4c40-8cfa-d8d9d59794e6
   -------------------*/
$(document).ready(function () {
    if(window.innerWidth <= 1024){
        $(".show_anna_info").click(function () { 
            $(".karin_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".anna_info").css('display', 'block');
        });
        $(".show_petra_info").click(function () { 
            $(".karin_info").css('display', 'none');
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'block');  
        });
        $(".show_karin_info").click(function () { 
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".karin_info").css('display', 'block');
        });
    }
    else {
         $(".show_anna_info").hover(function () { 
            $(".karin_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".anna_info").css('display', 'block');
        },
        function () {
            $(".anna_info").css('display', 'none');
        });
        $(".show_petra_info").hover(function () { 
            $(".karin_info").css('display', 'none');
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'block');  
        },
        function () {
            $(".petra_info").css('display', 'none');
        });
        $(".show_karin_info").hover(function () { 
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".karin_info").css('display', 'block');
        },
        function () {
            $(".karin_info").css('display', 'none');
        });
    }
});

$(document).ready(function () {
    $('#patient-login').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.patient_login_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.patient_login_popup_section').css('top', scrollTopPosition + 'px');
    });
    
    $('#patient-login_m').click(function(e){
        e.preventDefault();
        $('.patient_login_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.patient_login_popup_section').css('top', scrollTopPosition + 'px');
    });

    $('#close_modal_pl').click(function(e){
        $('.patient_login_popup_section').hide(300);
    });


    $('.patient_login_popup').click(function(e) {
        event.stopPropagation();
    });
});
$(document).ready(function () {
    $('#service-kontakt').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.service_kontakt_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.service_kontakt_popup_section').css('top', scrollTopPosition + 'px');
    });
    
    $('#service-kontakt_m').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.service_kontakt_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.service_kontakt_popup_section').css('top', scrollTopPosition + 'px');
    });

    $('#close_modal_sk').click(function(e){
        $('.service_kontakt_popup_section').hide(300);
    });

    $("body").on('click', function(e) {
        var classList = e.target.classList;
        var externalLink = 0;

        for(var cn in classList) {
            if(classList[cn] == "external-link-new" || classList[cn] == "instagram-image" || classList[cn] == "thinner_p" || classList[cn] == "thinner_h") {
                externalLink = -1;
            }
        }

        if(e.target.id != 'patient-login' && e.target.id != 'patient-login_m' && e.target.id != "service-kontakt" && e.target.id != "service-kontakt_m" && e.target.id != "expertenfinder" && e.target.id != "expertenfinder_m" && externalLink == 0) {
            $('.section-modal').hide(300);
        }
    });

    $('.service_kontakt_popup').click(function(e) {
        event.stopPropagation();
    });
});
$(document).ready(function () {
    $('#expertenfinder').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.expert_find_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.expert_find_popup_section').css('top', scrollTopPosition + 'px');
    });
    
    $('#expertenfinder_m').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.expert_find_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.expert_find_popup_section').css('top', scrollTopPosition + 'px');
    });

    $('#close_modal_ef').click(function(e){
        $('.expert_find_popup_section').hide(300);
    });

    $('.expert_find_popup').click(function(e) {
        event.stopPropagation();
    });
});

$(document).ready(function () {
    $('#close_modal_im').click(function(e){
        $('.info_modal_section').hide(300);
    });

    $('.info_modal_section').find('.orange_link_btn').click(function(e) {
        e.preventDefault();
        $('.info_modal_section').hide(300);
    });
});

$(document).ready(function() {
    $('.external-link-new').click(function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('.info_modal_section').find('.orange_bg_btn_with_border').attr('href', link);
        
       
        $('.info_modal_section').find('.orange_bg_btn_with_border').attr('target', '_blank');
        

        //$('.section-modal').hide(300);
        $('.info_modal_section').show(300);
        
        var scrollTopPosition = $(window).scrollTop();
           
        $('.info_modal_section').css('top', scrollTopPosition + 'px');
    });
});
