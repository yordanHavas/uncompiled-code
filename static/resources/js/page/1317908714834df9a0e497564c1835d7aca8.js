
/* -------------------
BEGIN how-dupixent-works 
Sitecore Resource: fc619cb7-c78e-4f62-83cb-271c8b5bd12c
   -------------------*/
$(function(){
    var indicator = $('.indicator');
    
    $('#type-carousel').on('slide.bs.carousel', function (e) {
        var active = $(e.relatedTarget);
        updateIndicator(active);
    }).on('slid.bs.carousel', function (e) {
        var active = $('.item.active');
        
        if(active.index() === 0) {
            setTimeout(function(){
                $('#type-carousel').carousel('pause');
            })
        }
    });
    
    $('.type-label').on('click', function(){
        $('#type-carousel').carousel('pause');
        //console.log('pause!');
    })
    
    function updateIndicator(active){
        //console.log('translateY(' + active.position().top + 'px)');
        
        indicator.css({
            height: active.outerHeight(),
            transform: 'translateY(' + active.position().top + 'px)'
        });
    }
    
    updateIndicator($('.item.active'));
})
