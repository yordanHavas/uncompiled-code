
/* -------------------
BEGIN  
Sitecore Resource: d7f791f8-73d3-4807-acdc-c2dcbdcac9d7
   -------------------*/
$(document).ready(function () {
    $(".burger_menu").click(function () {
        if ($('.menu_list').css('display') === 'none') {
            $('.submenu_list').css('display', 'none');
            $('.list_btn_submenu').css('marginBottom', '0');
            $('.menu_list').css('display', 'block');
        }
        else {
            $('.menu_list').css('display', 'none');
        }
    });

    $("#header_submenu").click(function () {
        if ($('.submenu_list').css('display') === 'none') {
            $('.list_btn_submenu').css('marginBottom', '194px');
            $('.submenu_list').css('display', 'block');
        }
        else {
            $('.list_btn_submenu').css('marginBottom', '0');
            $('.submenu_list').css('display', 'none');
        }
    });
});


/* -------------------
BEGIN  
Sitecore Resource: f2cf054d-931d-4751-97ea-29d578d2cb4e
   -------------------*/
function enterSite(){
    if($("#accessCode-bottom").length > 0) {
        var e = $("#accessCode-bottom").val().trim();
        if (e && e.length > 2 ) {
            localStorage.setItem("din", e);
            window.location.replace("/patientenlogin/homepage/");
             var now = new Date();
            var time = now.getTime();
            //var expireTime = time + (1000 * 60 * 60 * 24 * expireDays);
            var expireTime = time + (1000 * 60 * 20);
            now.setTime(expireTime);
            //document.cookie = name + "=" + value + "; expires=" + now.toGMTString() + "; path=/";
            document.cookie = "BatchLogin=True"+"; expires=" + now.toGMTString() + ";path=/";
        }
        else{
            if (localStorage.getItem("din")) localStorage.removeItem("din");
            $("#accessCodeError").show();
        }
    } 
}

function enterSiteTop(){
    var e = $("#accessCode-top").val().trim();
    if (e && e.length > 2 ) {
        localStorage.setItem("din", e);
        window.location.replace("/patientenlogin/homepage/");
         var now = new Date();
        var time = now.getTime();
        //var expireTime = time + (1000 * 60 * 60 * 24 * expireDays);
        var expireTime = time + (1000 * 60 * 20);
        now.setTime(expireTime);
        //document.cookie = name + "=" + value + "; expires=" + now.toGMTString() + "; path=/";
        document.cookie = "BatchLogin=True"+"; expires=" + now.toGMTString() + ";path=/";
    }
    else{
        if (localStorage.getItem("din")) localStorage.removeItem("din");
        $("#accessCodeError").show();
    }
       
}

$(function(){
    if($(".form-bottom").length > 0) {
        $(".form-bottom").submit(function(e){
            e.preventDefault();
            enterSite();
            
        });    
        $("#accessCodeError").hide();
    }
    
});

$(function(){
    $(".form-top").submit(function(e){
        enterSiteTop();
        e.preventDefault();
        
        
    });    
    $("#accessCodeError").hide();
});

$(document).ready(function() {
    
    $("#submit-bottom").attr('disabled', 'disabled');
    $("#accessCode-bottom").keyup(function(e) {
        
        // To Disable Submit Button
        $("#submit-bottom").attr('disabled', 'disabled');
        
        // Validating Fields
        var chargeNumber = $(this).val().toUpperCase();
        var filter = new RegExp('^[0-9]{1}[A-Za-z]{1}[A-Za-z0-9]{3}[A-Za-z]{1}$');
        
        var chargeNumberList =  [   
            "9L455F", "9L003K", "9L816H", "0L127K", "0L127K", "0L342C", "0L741C", "1LU01F",
            "9L748K", "0L440C", "0L132W", "0L254C", "0L254M",
            "9L175C", "9L806F", "0L440J", "0L330A", "0L440J", "0L440Z",
            "9L426V", "9L352A", "9L352A", "9L003H", "0L002F", "0LU05H", "0LU16A", "0L342H", "0L419A", "0L419C", "0L555C", "0L496C", "0L218H", "1L137A",
            "9F004A", "0F010A", "0F010B", "0F030A", "0F030B", "1F068A", "1F068B",  
            "9F007A", "9F007A", "0F015A", "0F016A", "0F027A", "0F033A", "0F033B", "1F055A", "1F058A", "1F069A", "1F087A",
            "9F008A", "0F015A", "0F022A", "0F016A", "0F022B", "0F027A", "0F033A", "1F056A", "1F055A", "1F057A", "1F067A", "1F069A", "1F087A"                                   
        ];

        if (!(chargeNumber == "")) {
            if ( chargeNumberList.includes(chargeNumber) || chargeNumberList.indexOf(chargeNumber) !== -1) {                
                // To Enable Submit Button
                $("#submit-bottom").removeAttr('disabled');
            }
            else if (filter.test(chargeNumber) || filter2.test(chargeNumber)) {
                // To Enable Submit Button
                $("#submit-bottom").removeAttr('disabled');
            }
        } else {
            // To Disable Submit Button
            $("#submit-bottom").attr('disabled', 'disabled');
        }

    });
    
    $("#submit-top").attr('disabled', 'disabled');
    $("#accessCode-top").keyup(function(e) {
        
        // To Disable Submit Button
        $("#submit-top").attr('disabled', 'disabled');
        
        // Validating Fields
        var chargeNumber = $(this).val().toUpperCase();
        var filter = new RegExp('^[0-9]{1}[A-Za-z]{1}[A-Za-z0-9]{3}[A-Za-z]{1}$');
        
        var chargeNumberList =  [   
            "9L455F", "9L003K", "9L816H", "0L127K", "0L127K", "0L342C", "0L741C", "1LU01F",
            "9L748K", "0L440C", "0L132W", "0L254C", "0L254M",
            "9L175C", "9L806F", "0L440J", "0L330A", "0L440J", "0L440Z",
            "9L426V", "9L352A", "9L352A", "9L003H", "0L002F", "0LU05H", "0LU16A", "0L342H", "0L419A", "0L419C", "0L555C", "0L496C", "0L218H", "1L137A",
            "9F004A", "0F010A", "0F010B", "0F030A", "0F030B", "1F068A", "1F068B",  
            "9F007A", "9F007A", "0F015A", "0F016A", "0F027A", "0F033A", "0F033B", "1F055A", "1F058A", "1F069A", "1F087A",
            "9F008A", "0F015A", "0F022A", "0F016A", "0F022B", "0F027A", "0F033A", "1F056A", "1F055A", "1F057A", "1F067A", "1F069A", "1F087A"                                   
        ];

        if (!(chargeNumber == "")) {
            if ( chargeNumberList.includes(chargeNumber) || chargeNumberList.indexOf(chargeNumber) !== -1) {                
                // To Enable Submit Button
                $("#submit-top").removeAttr('disabled');
            }
            else if (filter.test(chargeNumber) || filter2.test(chargeNumber)) {
                // To Enable Submit Button
                $("#submit-top").removeAttr('disabled');
            }
        } else {
            // To Disable Submit Button
            $("#submit-top").attr('disabled', 'disabled');
        }

    });

    $('#btnLogoutbtn').click(function(e) {
	        e.preventDefault();
	        userLogout();
	        
	        console.log(document.referrer)
		})
		
		var delete_cookie = function(name) {
	    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;';
	};
});

function userLogout() {
    $.ajax({
        type: "POST",
        url: "/api/UserValidateApi/UserLogOut/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
			})
		.done(function (response) {
		    console.log(response);
		    eraseCookie('User');
		   eraseCookie('BatchLogin');
		   	delete_cookie('BatchLogin');
		   		delete_cookie('User');
		   	 //window.location ="/asthma-fachkreise";
		   	 
		   	/*if(getCookie('User') ==""){
		   	     window.location ="/asthma-fachkreise";
		   	}
		   	else if(getCookie('BatchLogin') == ""){
		   	    window.location ="/asthma-patienten";
		   	}*/
		   	
		 	if (document.referrer.includes('/patientenlogin')){
                 window.location ="/";
            }
		})
		.fail(function (e) {
			console.log(e);
			console.log("error");
		});
    
}

/* -------------------
BEGIN  
Sitecore Resource: 5cd2bd06-9576-4c40-8cfa-d8d9d59794e6
   -------------------*/
$(document).ready(function () {
    if(window.innerWidth <= 1024){
        $(".show_anna_info").click(function () { 
            $(".karin_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".anna_info").css('display', 'block');
        });
        $(".show_petra_info").click(function () { 
            $(".karin_info").css('display', 'none');
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'block');  
        });
        $(".show_karin_info").click(function () { 
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".karin_info").css('display', 'block');
        });
    }
    else {
         $(".show_anna_info").hover(function () { 
            $(".karin_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".anna_info").css('display', 'block');
        },
        function () {
            $(".anna_info").css('display', 'none');
        });
        $(".show_petra_info").hover(function () { 
            $(".karin_info").css('display', 'none');
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'block');  
        },
        function () {
            $(".petra_info").css('display', 'none');
        });
        $(".show_karin_info").hover(function () { 
            $(".anna_info").css('display', 'none');
            $(".petra_info").css('display', 'none');  
            $(".karin_info").css('display', 'block');
        },
        function () {
            $(".karin_info").css('display', 'none');
        });
    }
});

$(document).ready(function () {
    $('#patient-login').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.patient_login_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.patient_login_popup_section').css('top', scrollTopPosition + 'px');
    });
    
    $('#patient-login_m').click(function(e){
        e.preventDefault();
        $('.patient_login_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.patient_login_popup_section').css('top', scrollTopPosition + 'px');
    });

    $('#close_modal_pl').click(function(e){
        $('.patient_login_popup_section').hide(300);
    });


    $('.patient_login_popup').click(function(e) {
        event.stopPropagation();
    });
});
$(document).ready(function () {
    $('#service-kontakt').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.service_kontakt_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.service_kontakt_popup_section').css('top', scrollTopPosition + 'px');
    });
    
    $('#service-kontakt_m').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.service_kontakt_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.service_kontakt_popup_section').css('top', scrollTopPosition + 'px');
    });

    $('#close_modal_sk').click(function(e){
        $('.service_kontakt_popup_section').hide(300);
    });

    $("body").on('click', function(e) {
        var classList = e.target.classList;
        var externalLink = 0;

        for(var cn in classList) {
            if(classList[cn] == "external-link-new" || classList[cn] == "instagram-image" || classList[cn] == "thinner_p" || classList[cn] == "thinner_h") {
                externalLink = -1;
            }
        }

        if(e.target.id != 'patient-login' && e.target.id != 'patient-login_m' && e.target.id != "service-kontakt" && e.target.id != "service-kontakt_m" && e.target.id != "expertenfinder" && e.target.id != "expertenfinder_m" && externalLink == 0) {
            $('.section-modal').hide(300);
        }
    });

    $('.service_kontakt_popup').click(function(e) {
        event.stopPropagation();
    });
});
$(document).ready(function () {
    $('#expertenfinder').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.expert_find_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.expert_find_popup_section').css('top', scrollTopPosition + 'px');
    });
    
    $('#expertenfinder_m').click(function(e){
        e.preventDefault();
        $('.section-modal').hide(300);
        $('.expert_find_popup_section').show(300);

        var scrollTopPosition = $(window).scrollTop();
        $('.expert_find_popup_section').css('top', scrollTopPosition + 'px');
    });

    $('#close_modal_ef').click(function(e){
        $('.expert_find_popup_section').hide(300);
    });

    $('.expert_find_popup').click(function(e) {
        event.stopPropagation();
    });
});

$(document).ready(function () {
    $('#close_modal_im').click(function(e){
        $('.info_modal_section').hide(300);
    });

    $('.info_modal_section').find('.orange_link_btn').click(function(e) {
        e.preventDefault();
        $('.info_modal_section').hide(300);
    });
});

$(document).ready(function() {
    $('.external-link-new').click(function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('.info_modal_section').find('.orange_bg_btn_with_border').attr('href', link);
        
       
        $('.info_modal_section').find('.orange_bg_btn_with_border').attr('target', '_blank');
        

        //$('.section-modal').hide(300);
        $('.info_modal_section').show(300);
        
        var scrollTopPosition = $(window).scrollTop();
           
        $('.info_modal_section').css('top', scrollTopPosition + 'px');
    });
});

/* -------------------
BEGIN  
Sitecore Resource: c6244456-2950-402b-b4ee-12bf2da6ec7c
   -------------------*/
$(document).ready(function(){ 
    var gridItems = [];

    $('.hvs-offers.desktop-visible').find('.hvs-akt-grid__box').each(function(index, element) {
        gridItems.push($(element));
    });

    if(gridItems.length <= 6) {
        $(".hvs-navigation-akt").css("display", "none");
    }

    function navigationSteps() {
	
        $('.hvs-navigation-akt a').click(function() {

            var typeNav = $(this).data('type-nav');
            var navObj = ('.hvs-offers .hvs-akt-grid.akt-grid-desktop');
            var activeClass = 'hvs-akt-grid__active';

            if(typeNav == "next") {
                var step = $('.hvs-akt-grid__active').next(navObj);
            }
            else if(typeNav == "prev") {
                var step = $('.hvs-akt-grid__active').prev(navObj);
            }

            $('.hvs-akt-grid__active').removeClass(activeClass);

            step.addClass(activeClass);
            
            var stepIndex = $('.hvs-offers.desktop-visible .' + activeClass).index();
           
            $('.hvs-navigation__nav-disabled').removeClass('hvs-navigation__nav-disabled');

            if(typeNav == "next") {

                var maxSteps = $(navObj).length;

                if(stepIndex == maxSteps){
                    $(this).addClass('hvs-navigation__nav-disabled')
                }
            }

            else if(typeNav == "prev") {
                if(stepIndex == 1){
                    $(this).addClass('hvs-navigation__nav-disabled');
                }
            }

            $('.hvs-navigation__current-step').text(stepIndex);
            $('.hvs-navigation__all-steps').text(maxSteps);

            if($(".hvs-scroll-akt").length > 0) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(".hvs-scroll-akt").offset().top - 165
                }, 200);
            }
        });

        var navObj = ('.hvs-offers.desktop-visible .hvs-akt-grid');
        var maxSteps = $(navObj).length;

        $('.hvs-navigation__all-steps').text(maxSteps);
    }

    function compareDates(dateText) {
        $('.hvs-events').find('.event-date').each(function() {
            var today = new Date();
            today.setHours(0,0,0,0);
            var eventDate = $(this).text();
            var arrDate = eventDate.split("index.html");
            useDate = new Date(arrDate[2], arrDate[1] - 1, arrDate[0]);

            if (useDate.getTime() > today.getTime()) {
                // console.log('future event');
                $(this).parent().closest('.hvs-akt-grid__box').addClass('future-event');
                $(this).parent().closest('.hvs-akt-grid__box').attr('data-eid', '1');
            } else if (useDate.getTime() == today.getTime()) {
                // console.log('today event');
                $(this).parent().closest('.hvs-akt-grid__box').addClass('today-event');
                $(this).parent().closest('.hvs-akt-grid__box').attr('data-eid', '0');
            } else {
                // console.log('past event');
                $(this).parent().closest('.hvs-akt-grid__box').addClass('past-event');
                $(this).parent().closest('.hvs-akt-grid__box').attr('data-eid', '-1');
            }
        });
    }

    // function orderEventBoxes() {
    //     var events = $('.hvs-events .hvs-akt-grid');
    //     var eventBox = events.children('.hvs-akt-grid__box');

    //     eventBox.detach().sort(function(a, b) {
    //         var astts = $(a).data('data-eid');
    //         var bstts = $(b).data('data-eid');
    //         //return astts - bstts;
    //         return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
    //     });
        
    //     events.append(eventBox);
    // }

    function slickEventsCarousel() {

        $(window).on('load resize', function(){
            var slickElement = $('.slick-wrapper');
            // var status = $('.slick-slide-info');
            if( $(window).width() < 768 ) {
                $('.slick-wrapper.slick-initialized').slick('unslick');
                slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
                    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
                    var i = (currentSlide ? currentSlide : 0) + 1;
                    // status.html( '<span> ' + i + ' </span>' + '/ ' + slick.slideCount);
                });

                slickElement.slick({
                    autoplay: false,
                    draggable: false,
                    dots: false,
                    arrows: true,
                    slidesToShow: 3,
                    infinite: false,
                    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
                    nextArrow: '<button class="slide-arrow next-arrow"></button>',
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                draggable: true,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
        
                });
            } else {
                $('.slick-wrapper.slick-initialized').slick('unslick');
            }
        });

       
    }

    function slickOffersCarousel() {

        $(window).on('load resize', function(){
            setTimeout(function(){
            // var slickElement = $('.offers-slick-wrapper');
            var status = $('.slick-slide-info-offers');
            
            if( $(window).width() < 768 ) {
                $('.offers-slick-wrapper.slick-initialized').slick('unslick');
                $('.offers-slick-wrapper').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
                    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
                    var i = (currentSlide ? currentSlide : 0) + 1;
                    status.html( '<span> ' + i + ' </span>' + '/ ' + slick.slideCount);
                });

                $('.offers-slick-wrapper').slick({
                    autoplay: false,
                    draggable: true,
                    dots: false,
                    arrows: true,
                    slidesToShow: 1,
                    infinite: true,
                    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
                    nextArrow: '<button class="slide-arrow next-arrow"></button>',
                    slidesToScroll: 1,
                });
            } else {
                $('.offers-slick-wrapper.slick-initialized').slick('unslick');
            }
            // console.log('carousel init  ')
            }, 300);
        });
    } 

    //Swiper hero section

    // var swiper = new Swiper('.hvs-akt-swiper-home', {
    //     noSwiping: true,
    //     roundLengths: true,
    //     watchOverflow: true,
    //     preventClicks: false,
    //     preventClicksPropagation: false,
    //     autoplay: {
    //         delay: 8000,
    //     },
    //     speed: 600,
    //     pagination: {
    //         el: '.swiper-pagination-cs',
    //         clickable: true,
    //         type: 'fraction'
    //     },
    //     navigation: {
    //         nextEl: '.button-next',
    //         prevEl: '.button-prev',
    //     }
    // });

    function setSliderNavigationPosition() {

        if (window.innerWidth < 992 && window.innerWidth >= 768) {
    
            var headerHeight = $('.hvs-akt-swiper-home .header img.hidden-xs').height();
    
            $('.hvs-akt-swiper-home .swiper-navigation').css('top', headerHeight + 'px');
    
        } else if (window.innerWidth < 768) {
    
            var headerHeight = $('.hvs-akt-swiper-home .header img.visible-xs').height();
    
            $('.hvs-akt-swiper-home .swiper-navigation').css('top', headerHeight + 'px');
        } else {
    
            $('.hvs-akt-swiper-home .swiper-navigation').css('top', 'auto');
    
        }
    
    }

    setSliderNavigationPosition()

    $(window).resize(function () {
        setSliderNavigationPosition();
    });
    

    navigationSteps();
    compareDates();
    // orderEventBoxes();
    slickEventsCarousel();
    slickOffersCarousel();


});
$(document).ready(function() {
	if($('.hvs-filter--blog').length > 0) {

		//Fix
		$('.hvs-switch').find('input[type="checkbox"]').unbind('click');
		$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').unbind('click');

		var blogCategories = ['Schlaf', 'Triggerfaktoren', 'Behandlung', 'Ernährung', 'Begleiterkrankungen', 'Familie & Freunde', 'Neurodermitis im Alltag']
		//var blogFormats = [];
		var blogBloggers = [];

		var blogFormats = 
		[
			{
			  "id" : "Blog",
			  "name" : "Blogbeiträge"
			},
			{
			  "id" : "video",
			  "name" : "Video",
			},
			{
			  "id" : "podcast",
			  "name" : "Podcast"
			}
		];

		//Render all filters
		$('.hvs-lessons__hodler').find('.hvs-lesson__box').each(function(index, element) {
			var eCat = $(element).data('category');
			var eFormat = $(element).data('format');
			var eBlogger = $(element).data('blogger');

			if(blogCategories.indexOf(eCat) == -1) {
				blogCategories.push(eCat);
			}

			// if(blogFormats.indexOf(eFormat) == -1) {
			// 	blogFormats.push(eFormat);
			// }

			if(blogBloggers.indexOf(eBlogger) == -1) {
				blogBloggers.push(eBlogger);
			}
		});

		for(var k in blogCategories) {
			var filterWrapper = $('<div class="hvs-filter__custom-checkbox hvs-filter__custom-checkbox--button"></div>');
			var filterInput = $('<input type="checkbox" id="'+ blogCategories[k] +'" value="'+ blogCategories[k] +'" data-filter="category">');
			var filterLabel = $('<label for="'+ blogCategories[k] +'">'+ blogCategories[k] +'</label>');
			$('.hvs-filter__categories').append(filterWrapper);
			filterWrapper.append(filterInput);
			filterWrapper.append(filterLabel);
		}

		for(var k in blogFormats) {
			var filterWrapper = $('<div class="hvs-filter__custom-checkbox hvs-filter__custom-checkbox--box"></div>');
			var filterInput = $('<input type="checkbox" id="'+ blogFormats[k].id +'" value="'+ blogFormats[k].id +'" data-filter="format">');
			var filterLabel = $('<label for="'+ blogFormats[k].id +'">'+ blogFormats[k].name +'</label>');
			$('.hvs-filter__formats').append(filterWrapper);
			filterWrapper.append(filterInput);
			filterWrapper.append(filterLabel);
		}

		for(var k in blogBloggers) {
			var filterWrapper = $('<div class="hvs-filter__custom-checkbox hvs-filter__custom-checkbox--box"></div>');
			var filterInput = $('<input type="checkbox" id="'+ blogBloggers[k] +'" value="'+ blogBloggers[k] +'" data-filter="blogger">');
			var filterLabel = $('<label for="'+ blogBloggers[k] +'">'+ blogBloggers[k] +'</label>');
			$('.hvs-filter__bloggers').append(filterWrapper);
			filterWrapper.append(filterInput);
			filterWrapper.append(filterLabel);
		}

		filterBlogs(); 

		//Reset Filter
		$('.hvs-filter__reset').click(function() {
			$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').prop('checked', false);
			$('.hvs-switch').parents('.hvs-filter__accordeon').find('input[type="checkbox"]').prop('checked', false);

			filterBlogs(); 
		});

		
		//Select/unselect switch
		$('.hvs-switch').find('input[type="checkbox"]').change(function() {
			
			if($(this).is(':checked')) {
				$(this).parents('.hvs-filter__accordeon').find('input[type="checkbox"]').prop('checked', true);
				filterBlogs();
			}
			else {
				$(this).parents('.hvs-filter__accordeon').find('input[type="checkbox"]').prop('checked', false);
			}
			
			filterBlogs();
			
		});
	}

	$('.hvs-filter--blog').find('.hvs-filter__custom-checkbox input[type="checkbox"]').click(function() {
		var checked = $('input[data-filter="'+ $(this).data('filter') +'"]:checked').length;
		var unchecked = $('input[data-filter="'+ $(this).data('filter') +'"]:not(:checked)').length;
		var allgroupFilters = $('input[data-filter="'+ $(this).data('filter') +'"]').length;

		if(checked == allgroupFilters) {
			$(this).parents('.hvs-filter__col').find('.hvs-switch input[type="checkbox"]').prop('checked', true);
		}
		else {
			$(this).parents('.hvs-filter__col').find('.hvs-switch input[type="checkbox"]').prop('checked', false);
		}

		filterBlogs();
	});

	renderAuthorArticles();
	
});
$(document).ready(function(){ 

	if($.cookie('lessons-learned') != undefined){
		var learnedLessons = JSON.parse($.cookie('lessons-learned'));
	}
	else {
		var learnedLessons = [];
	}


	//Mark learned lessons
	if($('.hvs-cookie').length > 0) {
		$.each($('.hvs-cookie'), function(i, element){
			var learnedMark = $(element);
			
			if(learnedLessons.indexOf(learnedMark.data('cookie')) != -1){
				learnedMark.addClass('hvs-cookie-active');
			}
		});
	}
});
function refreshNavigation() {
	$('.hvs-navigation a').removeClass('hvs-navigation__nav-disabled');
	var navObject = $('.hvs-navigation a').data('nav-object');
	
	if($('.' + navObject).length == 1) {
		$('.hvs-navigation a').addClass('hvs-navigation__nav-disabled');
	}
	else {
		$('.hvs-navigation__next').removeClass('hvs-navigation__nav-disabled');
		$('.hvs-navigation__prev').addClass('hvs-navigation__nav-disabled');
	}
}

function navigationSteps() {
	
	$('.hvs-navigation a').click(function() {
		var typeNav = $(this).data('type-nav');
		var navObj = $(this).data('nav-object');
		var activeClass = navObj + '__active';
		if(typeNav == "next") {
			var step = $('.' + activeClass).next('.' + navObj);
		}
		else if(typeNav == "prev") {
			var step = $('.' + activeClass).prev('.' + navObj);
		}

		if(navObj == 'hvs-step') {
			$('.' + activeClass).removeClass(activeClass).fadeOut(500);

			step.addClass(activeClass).delay(500).fadeIn(500);
		}
		else {
			$('.' + activeClass).removeClass(activeClass);
			step.addClass(activeClass);
		}


		var stepIndex = $('.' + activeClass).index() + 1;
		var maxSteps = $('.' + navObj).length;

		$('.hvs-navigation__nav-disabled').removeClass('hvs-navigation__nav-disabled');

		if(typeNav == "next") {

			if(stepIndex == maxSteps){
				$(this).addClass('hvs-navigation__nav-disabled');

				//checkmark ie
				setTimeout(function(){ $('.checkmark-ie').addClass('checkmark-ie-checked'); }, 850);

				//Last step cookie
				if(navObj == 'hvs-step') {
					if($.cookie('lessons-learned') != undefined){
						var learnedLessons = JSON.parse($.cookie('lessons-learned'));
					}
					else {
						var learnedLessons = [];
					}

					var unit = $('.hvs-lesson').data('mark'); 
					
					if(learnedLessons.indexOf(unit) == -1){
						learnedLessons.push(unit);
						$.cookie('lessons-learned', JSON.stringify(learnedLessons) , { expires : 10,  path: '/' });
					}
				}
			}
		}
		else if(typeNav == "prev") {
			if(stepIndex == 1){
				$(this).addClass('hvs-navigation__nav-disabled');
			}
		}
	
		//ProgressBar
		if($('.hvs-navigation__progress-line').length > 0){
			$('.hvs-navigation__progress-line').css('width', progressLineStepWidth*stepIndex + '%');
		}
		
		$('.hvs-navigation__first').removeClass('hvs-navigation__current-step').text(stepIndex - 1);
		$('.hvs-navigation__second').removeClass('hvs-navigation__current-step').text(stepIndex);
		$('.hvs-navigation__third').removeClass('hvs-navigation__current-step').text(stepIndex + 1);

		if(stepIndex == 1) {
			$('.hvs-navigation__first').addClass('hvs-navigation__current-step');
			$('.hvs-navigation__first').text(stepIndex);
			$('.hvs-navigation__second').text(stepIndex + 1);
			$('.hvs-navigation__third').text(stepIndex + 2);
		}
		else if (stepIndex == maxSteps) {
			$('.hvs-navigation__third').addClass('hvs-navigation__current-step');
			$('.hvs-navigation__first').text(maxSteps - 2);
			$('.hvs-navigation__second').text(maxSteps - 1);
			$('.hvs-navigation__third').text(maxSteps);
		}
		else {
			$('.hvs-navigation__second').addClass('hvs-navigation__current-step');
			$('.hvs-navigation__first').text(stepIndex - 1);
			$('.hvs-navigation__second').text(stepIndex);
			$('.hvs-navigation__third').text(stepIndex + 1); 
		}
		

		// //Landing page scroll to the top
		// if($(".hvs-scroll-to").length > 0) {
		// 	$([document.documentElement, document.body]).animate({
		// 		scrollTop: $(".hvs-scroll-to").offset().top - 165
		// 	}, 200);
		// }
		
		//Lessons scroll to the top
		if ($(window).width() < 992) {
			if($(".hvs-steps").length > 0) {
				$([document.documentElement, document.body]).animate({
					scrollTop: $(".hvs-steps").offset().top - 50
				}, 200);
			}
		}
		else {
			if($(".hvs-steps").length > 0) {
				$([document.documentElement, document.body]).animate({
					scrollTop: $(".hvs-steps").offset().top - 90
				}, 200);
			}
		}
	});
}

function filterCategories(categories) {
	var lessons = [];
	var hvsLessonsHolder = $('.hvs-grid__filter-lessons');

	//Clear Results Container
	hvsLessonsHolder.empty();

	//Clear Cloned filters
	$('.hvs-filter__selected').empty();

	//Clone selected filters
	// if(categories.length < allCategories.length || categories.length == 0) {

	// 	$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').each(function(i, el) {
	// 		$(this).trigger('mouseleave');
	// 		var filterCategoryVal = $(el).val();
	// 		if(categories.indexOf(filterCategoryVal) > -1) {
	// 			$(el).parents('.hvs-filter__custom-checkbox').clone().appendTo('.hvs-filter__selected');
	// 		}
	// 	});
	// }
	

	if(categories == undefined) {
		lessons = $('.hvs-lessons__hodler').find('.hvs-lesson__box');
	}
	else {
		for(var category in categories) {
			var cat = categories[category];

			$('.hvs-lessons__hodler').find('.hvs-lesson__box[data-category="'+ cat +'"]').each(function(index, element) {
				lessons.push(element);
			});
		}
	}

	//Sort Lessons
	var lessonsWithId = [];

	$(lessons).each(function(index, lesson){
		var id = $(lesson).data('id');
		
		var current = [];
		current['id'] = id;
		current['lesson'] = lesson;

		lessonsWithId.push(current);
	});	

	 lessonsWithId.sort(compareLessons);
	
	//Show filtered lessons
	$(lessonsWithId).each(function(index, lesson){
		var lessonBox = lesson['lesson'];

		if(index == 0) {
			$('.hvs-grid__filter-lessons').append('<div class="hvs-units hvs-units__active"></div>');
			$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
		}
		else {
			if(index % 6 == 0) {				
				$('.hvs-grid__filter-lessons').append('<div class="hvs-units"></div>');
				$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
			}
			else{
				if(index % 3 == 0) {
					$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
				}
			}
		}

		var lastUnits = $('.hvs-units').last();
		var lastRow = lastUnits.find('.hvs-grid__row').last();

		$(lessonBox).clone().appendTo(lastRow);
	});


	//Change progressBar
	if($('.hvs-grid__filter-lessons').length > 0){

		var steps = $(".hvs-grid__filter-lessons").children('.hvs-units').length;
		if(steps == 0) {
			$('.hvs-navigation__current-step').text(0);
		}
		else {
			$('.hvs-navigation__current-step').text(1);
		}
		
		$('.hvs-navigation__all-steps').text(steps);
	}
	
	var navObject = $('.hvs-navigation a').data('nav-object');


	if($('.' + navObject).length == 0 || $('.' + navObject).length == 1) {
		$('.hvs-navigation__filter').hide();	
	} else {
		$('.hvs-navigation__filter').show();
	}

	refreshNavigation();
}

function filterBlogs() {

	var blogsFilteredCategories = [];
	var blogsFilteredFormats = [];
	var blogsFilteredBloggers = [];
	var blogs = [];
	var hvsLessonsHolder = $('.hvs-grid__filter-lessons');
	var selectedFilters = [];

	//Clear Results Container
	hvsLessonsHolder.empty();

	//Selected categories
	var uncheckedCategories = [];
	var checkedCategories = [];
	var selectedCategoriesFilters = [];
	var allCategoriesCount = $('input[data-filter="category"]').length;

	$('input[data-filter="category"]').each(function() {
		if($(this).prop('checked') == true) {
			checkedCategories.push($(this));
		}
		else {
			uncheckedCategories.push($(this));
		}
	});

	if(checkedCategories.length == 0) {
		selectedCategoriesFilters = uncheckedCategories;
	}
	else if(uncheckedCategories.length == 0) {
		selectedCategoriesFilters = checkedCategories;
	}
	else {
		selectedCategoriesFilters = checkedCategories
	}

	for(var key in selectedCategoriesFilters) {
		var filterType = $(selectedCategoriesFilters[key]).data('filter');
		selectedFilters.push({'filter': filterType, 'val': $(selectedCategoriesFilters[key]).val()});
	}

	//Selected formats
	var uncheckedFormats = [];
	var checkedFormats = [];
	var selectedFormatsFilters = [];

	$('input[data-filter="format"]').each(function() {
		if($(this).prop('checked') == true) {
			checkedFormats.push($(this));
		}
		else {
			uncheckedFormats.push($(this));
		}
	});

	if(checkedFormats.length == 0) {
		selectedFormatsFilters = uncheckedFormats;
	}
	else if(uncheckedFormats.length == 0) {
		selectedFormatsFilters = checkedFormats;
	}
	else {
		selectedFormatsFilters = checkedFormats;
	}

	for(var key in selectedFormatsFilters) {
		var filterType = $(selectedFormatsFilters[key]).data('filter');
		selectedFilters.push({'filter': filterType, 'val': $(selectedFormatsFilters[key]).val()});
	}


	//Selected bloggers
	var uncheckedBloggers = [];
	var checkedBloggers = [];
	var selectedBloggersFilters = [];

	$('input[data-filter="blogger"]').each(function() {
		if($(this).prop('checked') == true) {
			checkedBloggers.push($(this));
		}
		else {
			uncheckedBloggers.push($(this));
		}
	});

	if(checkedBloggers.length == 0) {
		selectedBloggersFilters = uncheckedBloggers;
	}
	else if(uncheckedBloggers.length == 0) {
		selectedBloggersFilters = checkedBloggers;
	}
	else {
		selectedBloggersFilters = checkedBloggers;
	}

	for(var key in selectedBloggersFilters) {
		var filterType = $(selectedBloggersFilters[key]).data('filter');
		selectedFilters.push({'filter': filterType, 'val': $(selectedBloggersFilters[key]).val()});
	}

	//Filter Blogs
	for(var key in selectedFilters) {
		var filterVal = selectedFilters[key].val;

		if(selectedFilters[key].filter == 'category') {
			$('.hvs-lessons__hodler').find('.hvs-lesson__box[data-category="'+ filterVal +'"]').each(function(index, element) {
				blogsFilteredCategories.push(element);
			});

			blogs = blogsFilteredCategories;
		}

		if(selectedFilters[key].filter == 'format') {
			$('.hvs-lessons__hodler').find('.hvs-lesson__box[data-format="'+ filterVal +'"]').each(function(index, element) {
				if(blogsFilteredCategories.indexOf(element) > -1) {
					blogsFilteredFormats.push(element);
				} 
			});

			blogs = blogsFilteredFormats;
		}

		if(selectedFilters[key].filter == 'blogger') {
			$('.hvs-lessons__hodler').find('.hvs-lesson__box[data-blogger="'+ filterVal +'"]').each(function(index, element) {
				if(blogsFilteredFormats.indexOf(element) > -1) {
					blogsFilteredBloggers.push(element);
				}
			});

			blogs = blogsFilteredBloggers;
		}

	}


	//Number of selected categories
	if(selectedCategoriesFilters.length == $('.hvs-filter__categories').find('input[type="checkbox"]').length){
		$('.hvs-filter__categories').parents('.hvs-filter__col').find('.hvs-filter__title span').empty();
	}
	else {
		$('.hvs-filter__categories').parents('.hvs-filter__col').find('.hvs-filter__title span').html('(' + selectedCategoriesFilters.length + ')');
	}

	//Number of selected formats
	if(selectedFormatsFilters.length == $('.hvs-filter__formats').find('input[type="checkbox"]').length){
		$('.hvs-filter__formats').parents('.hvs-filter__col').find('.hvs-filter__title span').empty();
	}
	else {
		$('.hvs-filter__formats').parents('.hvs-filter__col').find('.hvs-filter__title span').html('(' + selectedFormatsFilters.length + ')');
	}

	//Number of selected bloggers
	if(selectedBloggersFilters.length == $('.hvs-filter__bloggers').find('input[type="checkbox"]').length){
		$('.hvs-filter__bloggers').parents('.hvs-filter__col').find('.hvs-filter__title span').empty();
	}
	else {
		$('.hvs-filter__bloggers').parents('.hvs-filter__col').find('.hvs-filter__title span').html('(' + selectedBloggersFilters.length + ')');
	}

	//SortBlogs
	var blogsWithId = [];

	$(blogs).each(function(index, blog){
		var id = $(blog).data('article-id');
		id = id.replace('article', '');
		id = parseInt(id);
		var current = [];
		current['id'] = id;
		current['blog'] = blog;

		blogsWithId.push(current);
	});	

	blogsWithId.sort(compare);

	//Show blog Patrick beschreibt die Odyssee von Arzt zu Arzt für seine Neurodermitis-Behandlung first
	var firstBlogCondition = 0;
	$('.hvs-filter__categories').find('input[type="checkbox"]:checked').each(function(index, element){
		if($(element).val() == 'Behandlung') {
			firstBlogCondition = 1;
		}
	})

	if(firstBlogCondition == 1) {
		var firstSpecialBlog = 0;
	for(var blog in blogsWithId) {
	 	//console.log(blogsWithId[blog]);
	 	if(blogsWithId[blog].id == 16) {
	 		firstSpecialBlog = blogsWithId.indexOf(blogsWithId[blog]);
	 	}
	}

	if(firstSpecialBlog > 0) {
		arraymove(blogsWithId, firstSpecialBlog, 0);
	}
	}
	 
	//Add special box
	if(blogsWithId.length > 0) {
		var newBlogItem = [];
		var boxSpecialItem = $('<div class="hvs-grid__box--3col hvs-lesson__box hvs-lesson__box--special"></div>').html('<div class="hvs-grid__box-inner hvs-grid__box--orange"><a class="hvs-grid__link--overall scroll-to-bloggers" title="Zu den Steckbriefen" href="#bloogerInfo"></a><div class="hvs-grid__img custom-img-container"></div><div class="hvs-grid__info"><h3 class="hvs-grid__title">Lernen Sie unsere Blogger kennen</h3><p class="h-desktop-visible">Erfahren Sie in persönlichen Steckbriefen mehr über das Leben unserer Blogger.</p></div><div class="hvs-link--bottom"><span></span><span class="hvs-link hvs-link--white">Zu den Steckbriefen</span></div></div>');

		newBlogItem['blog'] = boxSpecialItem
		if(blogsWithId.length > 2) {
			blogsWithId.splice(2, 0, newBlogItem);
		}
		else {
			blogsWithId.push(newBlogItem);
		}
	}

	

	//Show filtered blogs

	$(blogsWithId).each(function(index, blogBox){

		if(index == 0) {
			$('.hvs-grid__filter-lessons').append('<div class="hvs-units hvs-units__active"></div>');
			$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
		}
		else {
			if(index % 6 == 0) {				
				$('.hvs-grid__filter-lessons').append('<div class="hvs-units"></div>');
				$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
			}
			else{
				if(index % 3 == 0) {
					$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
				}
			}
		}

		var lastUnits = $('.hvs-units').last();
		var lastRow = lastUnits.find('.hvs-grid__row').last();

		$(blogBox.blog).clone().appendTo(lastRow);

	});

	//Scroll to Bloggers
	$('.scroll-to-bloggers').on('click',function (e) {
		e.preventDefault();
		
		var headerHeight = $('#header').height();
		
		$("html, body").animate({ 
			scrollTop: $('#bloogerInfo').offset().top - headerHeight
		}, 500);
	});

	//Change progressBar
	if($('.hvs-grid__filter-lessons').length > 0){

		var steps = $(".hvs-grid__filter-lessons").children('.hvs-units').length;
		if(steps == 0) {
			$('.hvs-navigation__current-step').text(0);
		}
		else {
			$('.hvs-navigation__current-step').text(1);
		}
		
		$('.hvs-navigation__all-steps').text(steps);
	}
	
	var navObject = $('.hvs-navigation a').data('nav-object');


	if($('.' + navObject).length == 0 || $('.' + navObject).length == 1) {
		$('.hvs-navigation__filter').hide();	
	} else {
		$('.hvs-navigation__filter').show();
	}

	refreshNavigation();
}


function renderAuthorArticles() {
	if($('.hvs-gird__articles').length > 0) {
		var bloggerBlogs = [];
		$('.hvs-lessons__hodler').find('.hvs-lesson__box').each(function(index, element) {
			bloggerBlogs.push($(element));
		});
		
		if(bloggerBlogs.length <= 6) {
			$(".hvs-navigation").css("display", "none");
		}

		$(bloggerBlogs).each(function(index, blogBox){
			

			if(index == 0) {
				$('.hvs-grid__filter-lessons').append('<div class="hvs-units hvs-units__active"></div>');
				$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
			}
			else {
				if(index % 6 == 0) {				
					$('.hvs-grid__filter-lessons').append('<div class="hvs-units"></div>');
					$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
				}
				else{
					if(index % 3 == 0) {
						$('.hvs-units').last().append('<div class="hvs-grid__row"></div>');
					}
				}
			}

			var lastUnits = $('.hvs-units').last();
			var lastRow = lastUnits.find('.hvs-grid__row').last();

			$(blogBox).clone().appendTo(lastRow);

		});
	}
}

function compare(a,b) {
      var idA = a.id;
      var idB = b.id;
      if (idA > idB)
        return -1;
      if (idB < idB)
        return 1;
      return 0;
}

function compareLessons(a,b) {
      var idA = a.id;
      var idB = b.id;
      if (idB > idA)
        return -1;
      if (idB < idB)
        return 1;
      return 0;
}

function arraymove(arr, fromIndex, toIndex) {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
}
$(document).ready(function(){ 

    //  IE object-fit
    var userAgent, ieReg, ie;
    userAgent = window.navigator.userAgent;
    ieReg = /msie|Trident.*rv[ :]*11\./gi;
    ie = ieReg.test(userAgent);

    if (ie) {
        $('.hvs-hero__box-inner').addClass('hvs-is-ie');
        $('.hvs-blogger__img').addClass('ie-img-size');
        $(".custom-img-container").each(function() {
            var $container = $(this),
                imgUrl = $container.find("img").prop("src");
            if (imgUrl) {
                $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
            }
        });
        $('#ie-remove').remove();
        setTimeout(function(){
            $('head').append('<script type="text/javascript" src="../cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js"></script>');
        }, 100);   
    } else {
        setTimeout(function(){
            $('head').append('<script id="ie-remove" src="../unpkg.com/swiper%408.0.7/swiper-bundle.min.js"></script>');
        }, 100);
    }

    // slider 
    $slick_slider = $('.hvs-grid--carousel-small');
    settings_slider = {
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true
    }
    slick_on_mobile( $slick_slider, settings_slider);

    // slick on mobile
    function slick_on_mobile(slider, settings){
        $(window).on('load resize', function() {
        if ($(window).width() > 767) {
            if (slider.hasClass('slick-initialized')) {
            slider.slick('unslick');
            }
            return
        }
        if (!slider.hasClass('slick-initialized')) {
            return slider.slick(settings);
        }
        });
    };

    $('.hvs-blog-article__carousel').slick({
        slidesToShow: 3,
        infinite: true,
        arrows: true,
        prevArrow: '<button class="slide-arrow prev-arrow"></button>',
        nextArrow: '<button class="slide-arrow next-arrow"></button>',
        dots: true,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                }
            },
            {
                breakpoint: 560,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    // Blog sliding profile container
    $('.hvs-blogger--open').click(function () {
        $('.hvs-blogger--expanded').addClass('increased-margin').slideDown(500).css('display', 'flex');
        $('.hvs-blogger--collapsed').hide();
    });
    $('#hvs-blogger--close').click(function (e) {
        e.stopPropagation();
        $('.hvs-blogger--expanded').hide().removeClass('increased-margin');
        $('.hvs-blogger--collapsed').show();
    });        

    //  Swiper carousel - blog mosaic section
    if ($('.hvs-bloggers').length > 0) {
        setTimeout(function(){
            var blogSwiper = new Swiper('.swiper-blog', {
                slidesPerView: 1,
                slidesPerColumn: 1,
                spaceBetween: 15,
                pagination: {   
                    el: '.swiper-pagination',          
                    type: 'bullets',
                    clickable: true
                },
                navigation: {
                  nextEl: '.button-next',
                  prevEl: '.button-prev',
                },
                breakpoints: {
                    467: {
                        slidesPerView: 2,
                        slidesPerColumn: 1,
                        spaceBetween: 15,
                        pagination: {             
                            type: 'bullets'
                        },
                    },    
                    768: {
                        slidesPerView: 3,
                        slidesPerColumn: 1,
                        spaceBetween: 20,
                        pagination: {             
                            type: 'bullets'
                        },
                    },                   
                    991: {
                        slidesPerView: 2,
                        slidesPerColumn: 2,
                        spaceBetween: 0,
                        roundLengths: true,
                        watchOverflow: true,
                        slidesPerColumnFill: 'row', 
                        slidesPerGroup: 2,
                        speed: 500,
                        pagination: {
                            el: '.swiper-pagination',
                            type: 'fraction',
                            clickable: true
                        },               
                    },
                }        
            });
        }, 5000);
    }
    function cutHeadline () {
        var max = 40;
        var tot, str;
        $('.hvs-grid__title').each(function() {
            str = String($(this).html());
            tot = str.length;
            str = (tot <= max)
                ? str
            : str.substring(0,(max + 1))+"...";
            $(this).html(str);
        });
    }

    // cutHeadline();

    // Elements with same height
    $(window).on('resize', function() {
        var containerHeight = [];

        if ( $(window).width() < 992 ) {
            $('.swiper-blog .hvs-blogger').each(function () {
                containerHeight.push(($(this).outerHeight(true)));
                allHeight = Math.max.apply(Math, containerHeight);
                $(this).css({ 'height': allHeight + 'px' });
            });
        } 
        // else {
        //     $('.swiper-blog .hvs-blogger').each(function () {
        //         $(this).css({ 'height': 270 + 'px' });
        //     });
        // }
        
        $.fn.setMinHeight = function(setCount) {
        for(var i = 0; i < this.length; i+=setCount) {
            var curSet = this.slice(i, i+setCount), 
            height = 0;
            curSet.each(function() { height = Math.max(height, $(this).height()); })
              .css('height', height);
            }
            return this;
        };

        $('.swiper-blog .hvs-blogger').setMinHeight(100);

    });

    if ($('.hvs-bloggers').length > 0) {
        var initialWidth = window.innerWidth; // 1920
        var mobileBreakpoint = 991;
        var isDesktop = initialWidth > mobileBreakpoint;

        $(window).on("resize", function() {
            var tm;
            tm = setTimeout(function() {
                var currentWidth = window.innerWidth;

                if (isDesktop && currentWidth < mobileBreakpoint+1) {
                    location.reload();   
                }

                if (! isDesktop && currentWidth > mobileBreakpoint) {
                    location.reload();   
                }

            }, 100);
        });
    }

});
$(document).ready(function() {

	if($('.hvs-filter--lessons').length > 0) {
		var allCategories = [];

		$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').each(function() {
			allCategories.push($(this).val());
		});
		
		filterCategories(allCategories); 
		
		//All checkboxes :checked 
		//$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').prop('checked', 'checked');

		//Commented because of the above - ask Bogi
		// if($('.hvs-switch').find('input[type="checkbox"]').is(':checked')) {
		// 	$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').prop('checked', 'checked');
		// }

		var itemsChecked = $('.hvs-filter__wrapper').find('.hvs-filter__group').find('input[type="checkbox"]:checked').length;
		$('.hvs-filter__title span').html('(' + itemsChecked + ')');

		//Reset Filter
		$('.hvs-filter__reset').click(function() {
			$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').prop('checked', false);
			filterCategories(allCategories); 
			//$('.hvs-switch').find('input[type="checkbox"]').prop('checked', true);
		});

		$('.hvs-switch').find('input[type="checkbox"]').unbind('click');
		$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').unbind('click');
		
		// //Select/unselect all categories
		// $('.hvs-switch').find('input[type="checkbox"]').change(function() {
			
		// 	if($(this).is(':checked')) {
		// 		$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').prop('checked', true);
		// 		filterCategories(allCategories);
		// 	}
		// 	else {
		// 		$('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').prop('checked', false);
		// 		filterCategories([]);
		// 	}
			
		// 	var itemsChecked = $('.hvs-filter__wrapper').find('.hvs-filter__group').find('input[type="checkbox"]:checked').length
		// 	$('.hvs-filter__title span').html('(' + itemsChecked + ')');
			
		// });

		
		//Select/Unselect category
		$('.hvs-filter__wrapper').find('.hvs-filter__custom-checkbox').find('input[type="checkbox"]').change(function(){
			var categories = [];	
			
			$('.hvs-filter__wrapper').find('.hvs-filter__custom-checkbox').find('input[type="checkbox"]:checked').each(function() {
				categories.push($(this).val());
			});

			// if(categories.length == allCategories.length) {
			// 	$('.hvs-switch').find('input[type="checkbox"]').prop('checked', true);
			// }
			// else {
			// 	$('.hvs-switch').find('input[type="checkbox"]').prop('checked', false);
			// }
			
			if(categories.length == 0) {
				filterCategories(allCategories);
			}
			else {
				filterCategories(categories);
			}		
			
			var itemsChecked = $('.hvs-filter__wrapper').find('.hvs-filter__group').find('input[type="checkbox"]:checked').length
			$('.hvs-filter__title span').html('(' + itemsChecked + ')');
		});
	}
	
	
	
	//Slide toggle filter
	$('.data-click-to-expand').click(function(){
		$(this).toggleClass('hvs-filter__head--expanded');
        $('.data-expand').slideToggle();
    });

	
	//Mobile filter accordeons
	$(window).resize(function() {
		if($(window).width() >= 768) {
			$('[data-filter]').attr("style", '');
			$('[data-filter-expand]').removeClass('hvs-filter__title--expanded');
		}
	}).resize();
	
	$('[data-filter-expand]').click(function(){
		if($(window).width() < 768) {
			currentAccordeon = $(this).data('filter-expand');
			
			if (!$(this).hasClass('hvs-filter__title--expanded')) {
				$('[data-filter]').slideUp();
				$('[data-filter-expand]').removeClass('hvs-filter__title--expanded');
				
				$(this).addClass('hvs-filter__title--expanded');
				$('[data-filter="'+ currentAccordeon +'"]').slideDown();

			} else {
				$(this).removeClass('hvs-filter__title--expanded');
				$('[data-filter="'+ currentAccordeon +'"]').slideUp();
			}
		}
	});

	

});



$(document).ready(function(){ 
	if($(".hvs-steps").length > 0){
		var steps = $(".hvs-steps").find('.hvs-step').length;
		var progressLineStepWidthIndex = $('.hvs-navigation__progress').width()/steps;
		progressLineStepWidth = progressLineStepWidthIndex/$('.hvs-navigation__progress').width()*100;

		$('.hvs-navigation__progress-line').css('width', progressLineStepWidth + '%');	
	}
	
	refreshNavigation();
	
	//Navigation
	navigationSteps();

	
	if($.cookie('lessons-learned') != undefined){
		var learnedLessons = JSON.parse($.cookie('lessons-learned'));
	}
	else {
		var learnedLessons = [];
	}

});
$(document).ready(function(){ 
	$('.hvs-icon--share').on('click', function(e) {

	    // Satic content
	    var content = '';
	    // Call the function to open the popup with the content from var = content
	    openPopup(content);
	    copyURL();
	    closePopup();

	    // Function to open the popup
		function openPopup(content) {
			d = document.createElement('div');
			$(d).addClass("hvs-popup-container")
			    .html('<div class="hvs-popup-box"><div class="hvs-popupbox--row"><div class="hvs-step-checkmark-holder"><div class="hvs-step__checkmark-wrapper"><svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" /> <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" /> </svg> <div class="checkmark-ie"></div> </div></div><p class="hvs-share-text">Link kopiert</p><a href="javascript:;" class="close-popup" title="close"></a></div><div class="hvs-popup-input-container"><input type="text" class="hvs-popup-input" value=""></div><div class="hvs-link--bottom"><span class="copy-link hvs-link hvs-link--orange">Link kopieren</span></div></div>')
			    .appendTo($(".hvs-blogdetail-intro__person"))
		    $(this).fadeIn();
		    $('.hvs-icon--share a').addClass('active');
		} 

		function copyURL() {
			var	input = $('.hvs-popup-input'),
			    text = window.location.href;

			input.val(text);
			input.select();
			document.execCommand('copy');
			// input.attr('disabled','disabled');
			input.blur();

			$('.hvs-popup-container, .copy-link').click(function() {
				$('.hvs-popup-box p').removeClass('url-copied');
				$('.hvs-popup-box p').addClass('url-copied');
				$('.hvs-step__checkmark-wrapper').addClass('hidden');
				copyURL();
				setTimeout(function(){
					$('.hvs-step__checkmark-wrapper').removeClass('hidden');
				}, 300);
			})
		}

		function closePopup() {
			$('.close-popup').click(function(){
				$('.hvs-icon--share a').removeClass('active');
				$('.hvs-popup-container').fadeOut();
				$('.hvs-popup-container').remove();
			});
			$('.hvs-icon--share a.active').click(function(){
				$('.hvs-icon--share a').removeClass('active');
				$('.hvs-popup-container').fadeOut();
				$('.hvs-popup-container').remove();
			});
		}
	});

	function closePopupOutside(e) {
		var myPopup = $(".hvs-popup-container");
		if (!myPopup.is(e.target) && myPopup.has(e.target).length === 0) {
			myPopup.hide();
			$('.hvs-icon--share a').removeClass('active');
		}
	}

	$(document).mouseup(function(e) {
		closePopupOutside(e);
	});	

	$(document).on('touchend', function(e){
		closePopupOutside(e);
	});
});
/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD (Register as an anonymous module)
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {},
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling $.cookie().
			cookies = document.cookie ? document.cookie.split('; ') : [],
			i = 0,
			l = cookies.length;

		for (; i < l; i++) {
			var parts = cookies[i].split('='),
				name = decode(parts.shift()),
				cookie = parts.join('=');

			if (key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));

/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.6.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):"undefined"!=typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){"use strict";var b=window.Slick||{};b=function(){function c(c,d){var f,e=this;e.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:a(c),appendDots:a(c),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(b,c){return a('<button type="button" data-role="none" role="button" tabindex="0" />').text(c+1)},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnFocus:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},e.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},a.extend(e,e.initials),e.activeBreakpoint=null,e.animType=null,e.animProp=null,e.breakpoints=[],e.breakpointSettings=[],e.cssTransitions=!1,e.focussed=!1,e.interrupted=!1,e.hidden="hidden",e.paused=!0,e.positionProp=null,e.respondTo=null,e.rowCount=1,e.shouldClick=!0,e.$slider=a(c),e.$slidesCache=null,e.transformType=null,e.transitionType=null,e.visibilityChange="visibilitychange",e.windowWidth=0,e.windowTimer=null,f=a(c).data("slick")||{},e.options=a.extend({},e.defaults,d,f),e.currentSlide=e.options.initialSlide,e.originalSettings=e.options,"undefined"!=typeof document.mozHidden?(e.hidden="mozHidden",e.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(e.hidden="webkitHidden",e.visibilityChange="webkitvisibilitychange"),e.autoPlay=a.proxy(e.autoPlay,e),e.autoPlayClear=a.proxy(e.autoPlayClear,e),e.autoPlayIterator=a.proxy(e.autoPlayIterator,e),e.changeSlide=a.proxy(e.changeSlide,e),e.clickHandler=a.proxy(e.clickHandler,e),e.selectHandler=a.proxy(e.selectHandler,e),e.setPosition=a.proxy(e.setPosition,e),e.swipeHandler=a.proxy(e.swipeHandler,e),e.dragHandler=a.proxy(e.dragHandler,e),e.keyHandler=a.proxy(e.keyHandler,e),e.instanceUid=b++,e.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,e.registerBreakpoints(),e.init(!0)}var b=0;return c}(),b.prototype.activateADA=function(){var a=this;a.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})},b.prototype.addSlide=b.prototype.slickAdd=function(b,c,d){var e=this;if("boolean"==typeof c)d=c,c=null;else if(0>c||c>=e.slideCount)return!1;e.unload(),"number"==typeof c?0===c&&0===e.$slides.length?a(b).appendTo(e.$slideTrack):d?a(b).insertBefore(e.$slides.eq(c)):a(b).insertAfter(e.$slides.eq(c)):d===!0?a(b).prependTo(e.$slideTrack):a(b).appendTo(e.$slideTrack),e.$slides=e.$slideTrack.children(this.options.slide),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.append(e.$slides),e.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),e.$slidesCache=e.$slides,e.reinit()},b.prototype.animateHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.animate({height:b},a.options.speed)}},b.prototype.animateSlide=function(b,c){var d={},e=this;e.animateHeight(),e.options.rtl===!0&&e.options.vertical===!1&&(b=-b),e.transformsEnabled===!1?e.options.vertical===!1?e.$slideTrack.animate({left:b},e.options.speed,e.options.easing,c):e.$slideTrack.animate({top:b},e.options.speed,e.options.easing,c):e.cssTransitions===!1?(e.options.rtl===!0&&(e.currentLeft=-e.currentLeft),a({animStart:e.currentLeft}).animate({animStart:b},{duration:e.options.speed,easing:e.options.easing,step:function(a){a=Math.ceil(a),e.options.vertical===!1?(d[e.animType]="translate("+a+"px, 0px)",e.$slideTrack.css(d)):(d[e.animType]="translate(0px,"+a+"px)",e.$slideTrack.css(d))},complete:function(){c&&c.call()}})):(e.applyTransition(),b=Math.ceil(b),e.options.vertical===!1?d[e.animType]="translate3d("+b+"px, 0px, 0px)":d[e.animType]="translate3d(0px,"+b+"px, 0px)",e.$slideTrack.css(d),c&&setTimeout(function(){e.disableTransition(),c.call()},e.options.speed))},b.prototype.getNavTarget=function(){var b=this,c=b.options.asNavFor;return c&&null!==c&&(c=a(c).not(b.$slider)),c},b.prototype.asNavFor=function(b){var c=this,d=c.getNavTarget();null!==d&&"object"==typeof d&&d.each(function(){var c=a(this).slick("getSlick");c.unslicked||c.slideHandler(b,!0)})},b.prototype.applyTransition=function(a){var b=this,c={};b.options.fade===!1?c[b.transitionType]=b.transformType+" "+b.options.speed+"ms "+b.options.cssEase:c[b.transitionType]="opacity "+b.options.speed+"ms "+b.options.cssEase,b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.autoPlay=function(){var a=this;a.autoPlayClear(),a.slideCount>a.options.slidesToShow&&(a.autoPlayTimer=setInterval(a.autoPlayIterator,a.options.autoplaySpeed))},b.prototype.autoPlayClear=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer)},b.prototype.autoPlayIterator=function(){var a=this,b=a.currentSlide+a.options.slidesToScroll;a.paused||a.interrupted||a.focussed||(a.options.infinite===!1&&(1===a.direction&&a.currentSlide+1===a.slideCount-1?a.direction=0:0===a.direction&&(b=a.currentSlide-a.options.slidesToScroll,a.currentSlide-1===0&&(a.direction=1))),a.slideHandler(b))},b.prototype.buildArrows=function(){var b=this;b.options.arrows===!0&&(b.$prevArrow=a(b.options.prevArrow).addClass("slick-arrow"),b.$nextArrow=a(b.options.nextArrow).addClass("slick-arrow"),b.slideCount>b.options.slidesToShow?(b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.prependTo(b.options.appendArrows),b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.appendTo(b.options.appendArrows),b.options.infinite!==!0&&b.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},b.prototype.buildDots=function(){var c,d,b=this;if(b.options.dots===!0&&b.slideCount>b.options.slidesToShow){for(b.$slider.addClass("slick-dotted"),d=a("<ul />").addClass(b.options.dotsClass),c=0;c<=b.getDotCount();c+=1)d.append(a("<li />").append(b.options.customPaging.call(this,b,c)));b.$dots=d.appendTo(b.options.appendDots),b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")}},b.prototype.buildOut=function(){var b=this;b.$slides=b.$slider.children(b.options.slide+":not(.slick-cloned)").addClass("slick-slide"),b.slideCount=b.$slides.length,b.$slides.each(function(b,c){a(c).attr("data-slick-index",b).data("originalStyling",a(c).attr("style")||"")}),b.$slider.addClass("slick-slider"),b.$slideTrack=0===b.slideCount?a('<div class="slick-track"/>').appendTo(b.$slider):b.$slides.wrapAll('<div class="slick-track"/>').parent(),b.$list=b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),b.$slideTrack.css("opacity",0),(b.options.centerMode===!0||b.options.swipeToSlide===!0)&&(b.options.slidesToScroll=1),a("img[data-lazy]",b.$slider).not("[src]").addClass("slick-loading"),b.setupInfinite(),b.buildArrows(),b.buildDots(),b.updateDots(),b.setSlideClasses("number"==typeof b.currentSlide?b.currentSlide:0),b.options.draggable===!0&&b.$list.addClass("draggable")},b.prototype.buildRows=function(){var b,c,d,e,f,g,h,a=this;if(e=document.createDocumentFragment(),g=a.$slider.children(),a.options.rows>1){for(h=a.options.slidesPerRow*a.options.rows,f=Math.ceil(g.length/h),b=0;f>b;b++){var i=document.createElement("div");for(c=0;c<a.options.rows;c++){var j=document.createElement("div");for(d=0;d<a.options.slidesPerRow;d++){var k=b*h+(c*a.options.slidesPerRow+d);g.get(k)&&j.appendChild(g.get(k))}i.appendChild(j)}e.appendChild(i)}a.$slider.empty().append(e),a.$slider.children().children().children().css({width:100/a.options.slidesPerRow+"%",display:"inline-block"})}},b.prototype.checkResponsive=function(b,c){var e,f,g,d=this,h=!1,i=d.$slider.width(),j=window.innerWidth||a(window).width();if("window"===d.respondTo?g=j:"slider"===d.respondTo?g=i:"min"===d.respondTo&&(g=Math.min(j,i)),d.options.responsive&&d.options.responsive.length&&null!==d.options.responsive){f=null;for(e in d.breakpoints)d.breakpoints.hasOwnProperty(e)&&(d.originalSettings.mobileFirst===!1?g<d.breakpoints[e]&&(f=d.breakpoints[e]):g>d.breakpoints[e]&&(f=d.breakpoints[e]));null!==f?null!==d.activeBreakpoint?(f!==d.activeBreakpoint||c)&&(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):null!==d.activeBreakpoint&&(d.activeBreakpoint=null,d.options=d.originalSettings,b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b),h=f),b||h===!1||d.$slider.trigger("breakpoint",[d,h])}},b.prototype.changeSlide=function(b,c){var f,g,h,d=this,e=a(b.currentTarget);switch(e.is("a")&&b.preventDefault(),e.is("li")||(e=e.closest("li")),h=d.slideCount%d.options.slidesToScroll!==0,f=h?0:(d.slideCount-d.currentSlide)%d.options.slidesToScroll,b.data.message){case"previous":g=0===f?d.options.slidesToScroll:d.options.slidesToShow-f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide-g,!1,c);break;case"next":g=0===f?d.options.slidesToScroll:f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide+g,!1,c);break;case"index":var i=0===b.data.index?0:b.data.index||e.index()*d.options.slidesToScroll;d.slideHandler(d.checkNavigable(i),!1,c),e.children().trigger("focus");break;default:return}},b.prototype.checkNavigable=function(a){var c,d,b=this;if(c=b.getNavigableIndexes(),d=0,a>c[c.length-1])a=c[c.length-1];else for(var e in c){if(a<c[e]){a=d;break}d=c[e]}return a},b.prototype.cleanUpEvents=function(){var b=this;b.options.dots&&null!==b.$dots&&a("li",b.$dots).off("click.slick",b.changeSlide).off("mouseenter.slick",a.proxy(b.interrupt,b,!0)).off("mouseleave.slick",a.proxy(b.interrupt,b,!1)),b.$slider.off("focus.slick blur.slick"),b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow&&b.$prevArrow.off("click.slick",b.changeSlide),b.$nextArrow&&b.$nextArrow.off("click.slick",b.changeSlide)),b.$list.off("touchstart.slick mousedown.slick",b.swipeHandler),b.$list.off("touchmove.slick mousemove.slick",b.swipeHandler),b.$list.off("touchend.slick mouseup.slick",b.swipeHandler),b.$list.off("touchcancel.slick mouseleave.slick",b.swipeHandler),b.$list.off("click.slick",b.clickHandler),a(document).off(b.visibilityChange,b.visibility),b.cleanUpSlideEvents(),b.options.accessibility===!0&&b.$list.off("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().off("click.slick",b.selectHandler),a(window).off("orientationchange.slick.slick-"+b.instanceUid,b.orientationChange),a(window).off("resize.slick.slick-"+b.instanceUid,b.resize),a("[draggable!=true]",b.$slideTrack).off("dragstart",b.preventDefault),a(window).off("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).off("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.cleanUpSlideEvents=function(){var b=this;b.$list.off("mouseenter.slick",a.proxy(b.interrupt,b,!0)),b.$list.off("mouseleave.slick",a.proxy(b.interrupt,b,!1))},b.prototype.cleanUpRows=function(){var b,a=this;a.options.rows>1&&(b=a.$slides.children().children(),b.removeAttr("style"),a.$slider.empty().append(b))},b.prototype.clickHandler=function(a){var b=this;b.shouldClick===!1&&(a.stopImmediatePropagation(),a.stopPropagation(),a.preventDefault())},b.prototype.destroy=function(b){var c=this;c.autoPlayClear(),c.touchObject={},c.cleanUpEvents(),a(".slick-cloned",c.$slider).detach(),c.$dots&&c.$dots.remove(),c.$prevArrow&&c.$prevArrow.length&&(c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.prevArrow)&&c.$prevArrow.remove()),c.$nextArrow&&c.$nextArrow.length&&(c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.nextArrow)&&c.$nextArrow.remove()),c.$slides&&(c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){a(this).attr("style",a(this).data("originalStyling"))}),c.$slideTrack.children(this.options.slide).detach(),c.$slideTrack.detach(),c.$list.detach(),c.$slider.append(c.$slides)),c.cleanUpRows(),c.$slider.removeClass("slick-slider"),c.$slider.removeClass("slick-initialized"),c.$slider.removeClass("slick-dotted"),c.unslicked=!0,b||c.$slider.trigger("destroy",[c])},b.prototype.disableTransition=function(a){var b=this,c={};c[b.transitionType]="",b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.fadeSlide=function(a,b){var c=this;c.cssTransitions===!1?(c.$slides.eq(a).css({zIndex:c.options.zIndex}),c.$slides.eq(a).animate({opacity:1},c.options.speed,c.options.easing,b)):(c.applyTransition(a),c.$slides.eq(a).css({opacity:1,zIndex:c.options.zIndex}),b&&setTimeout(function(){c.disableTransition(a),b.call()},c.options.speed))},b.prototype.fadeSlideOut=function(a){var b=this;b.cssTransitions===!1?b.$slides.eq(a).animate({opacity:0,zIndex:b.options.zIndex-2},b.options.speed,b.options.easing):(b.applyTransition(a),b.$slides.eq(a).css({opacity:0,zIndex:b.options.zIndex-2}))},b.prototype.filterSlides=b.prototype.slickFilter=function(a){var b=this;null!==a&&(b.$slidesCache=b.$slides,b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.filter(a).appendTo(b.$slideTrack),b.reinit())},b.prototype.focusHandler=function(){var b=this;b.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick","*:not(.slick-arrow)",function(c){c.stopImmediatePropagation();var d=a(this);setTimeout(function(){b.options.pauseOnFocus&&(b.focussed=d.is(":focus"),b.autoPlay())},0)})},b.prototype.getCurrent=b.prototype.slickCurrentSlide=function(){var a=this;return a.currentSlide},b.prototype.getDotCount=function(){var a=this,b=0,c=0,d=0;if(a.options.infinite===!0)for(;b<a.slideCount;)++d,b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;else if(a.options.centerMode===!0)d=a.slideCount;else if(a.options.asNavFor)for(;b<a.slideCount;)++d,b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;else d=1+Math.ceil((a.slideCount-a.options.slidesToShow)/a.options.slidesToScroll);return d-1},b.prototype.getLeft=function(a){var c,d,f,b=this,e=0;return b.slideOffset=0,d=b.$slides.first().outerHeight(!0),b.options.infinite===!0?(b.slideCount>b.options.slidesToShow&&(b.slideOffset=b.slideWidth*b.options.slidesToShow*-1,e=d*b.options.slidesToShow*-1),b.slideCount%b.options.slidesToScroll!==0&&a+b.options.slidesToScroll>b.slideCount&&b.slideCount>b.options.slidesToShow&&(a>b.slideCount?(b.slideOffset=(b.options.slidesToShow-(a-b.slideCount))*b.slideWidth*-1,e=(b.options.slidesToShow-(a-b.slideCount))*d*-1):(b.slideOffset=b.slideCount%b.options.slidesToScroll*b.slideWidth*-1,e=b.slideCount%b.options.slidesToScroll*d*-1))):a+b.options.slidesToShow>b.slideCount&&(b.slideOffset=(a+b.options.slidesToShow-b.slideCount)*b.slideWidth,e=(a+b.options.slidesToShow-b.slideCount)*d),b.slideCount<=b.options.slidesToShow&&(b.slideOffset=0,e=0),b.options.centerMode===!0&&b.options.infinite===!0?b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)-b.slideWidth:b.options.centerMode===!0&&(b.slideOffset=0,b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)),c=b.options.vertical===!1?a*b.slideWidth*-1+b.slideOffset:a*d*-1+e,b.options.variableWidth===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow),c=b.options.rtl===!0?f[0]?-1*(b.$slideTrack.width()-f[0].offsetLeft-f.width()):0:f[0]?-1*f[0].offsetLeft:0,b.options.centerMode===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow+1),c=b.options.rtl===!0?f[0]?-1*(b.$slideTrack.width()-f[0].offsetLeft-f.width()):0:f[0]?-1*f[0].offsetLeft:0,c+=(b.$list.width()-f.outerWidth())/2)),c},b.prototype.getOption=b.prototype.slickGetOption=function(a){var b=this;return b.options[a]},b.prototype.getNavigableIndexes=function(){var e,a=this,b=0,c=0,d=[];for(a.options.infinite===!1?e=a.slideCount:(b=-1*a.options.slidesToScroll,c=-1*a.options.slidesToScroll,e=2*a.slideCount);e>b;)d.push(b),b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d},b.prototype.getSlick=function(){return this},b.prototype.getSlideCount=function(){var c,d,e,b=this;return e=b.options.centerMode===!0?b.slideWidth*Math.floor(b.options.slidesToShow/2):0,b.options.swipeToSlide===!0?(b.$slideTrack.find(".slick-slide").each(function(c,f){return f.offsetLeft-e+a(f).outerWidth()/2>-1*b.swipeLeft?(d=f,!1):void 0}),c=Math.abs(a(d).attr("data-slick-index")-b.currentSlide)||1):b.options.slidesToScroll},b.prototype.goTo=b.prototype.slickGoTo=function(a,b){var c=this;c.changeSlide({data:{message:"index",index:parseInt(a)}},b)},b.prototype.init=function(b){var c=this;a(c.$slider).hasClass("slick-initialized")||(a(c.$slider).addClass("slick-initialized"),c.buildRows(),c.buildOut(),c.setProps(),c.startLoad(),c.loadSlider(),c.initializeEvents(),c.updateArrows(),c.updateDots(),c.checkResponsive(!0),c.focusHandler()),b&&c.$slider.trigger("init",[c]),c.options.accessibility===!0&&c.initADA(),c.options.autoplay&&(c.paused=!1,c.autoPlay())},b.prototype.initADA=function(){var b=this;b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),b.$slideTrack.attr("role","listbox"),b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function(c){a(this).attr({role:"option","aria-describedby":"slick-slide"+b.instanceUid+c})}),null!==b.$dots&&b.$dots.attr("role","tablist").find("li").each(function(c){a(this).attr({role:"presentation","aria-selected":"false","aria-controls":"navigation"+b.instanceUid+c,id:"slick-slide"+b.instanceUid+c})}).first().attr("aria-selected","true").end().find("button").attr("role","button").end().closest("div").attr("role","toolbar"),b.activateADA()},b.prototype.initArrowEvents=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.off("click.slick").on("click.slick",{message:"previous"},a.changeSlide),a.$nextArrow.off("click.slick").on("click.slick",{message:"next"},a.changeSlide))},b.prototype.initDotEvents=function(){var b=this;b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&a("li",b.$dots).on("click.slick",{message:"index"},b.changeSlide),b.options.dots===!0&&b.options.pauseOnDotsHover===!0&&a("li",b.$dots).on("mouseenter.slick",a.proxy(b.interrupt,b,!0)).on("mouseleave.slick",a.proxy(b.interrupt,b,!1))},b.prototype.initSlideEvents=function(){var b=this;b.options.pauseOnHover&&(b.$list.on("mouseenter.slick",a.proxy(b.interrupt,b,!0)),b.$list.on("mouseleave.slick",a.proxy(b.interrupt,b,!1)))},b.prototype.initializeEvents=function(){var b=this;b.initArrowEvents(),b.initDotEvents(),b.initSlideEvents(),b.$list.on("touchstart.slick mousedown.slick",{action:"start"},b.swipeHandler),b.$list.on("touchmove.slick mousemove.slick",{action:"move"},b.swipeHandler),b.$list.on("touchend.slick mouseup.slick",{action:"end"},b.swipeHandler),b.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},b.swipeHandler),b.$list.on("click.slick",b.clickHandler),a(document).on(b.visibilityChange,a.proxy(b.visibility,b)),b.options.accessibility===!0&&b.$list.on("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),a(window).on("orientationchange.slick.slick-"+b.instanceUid,a.proxy(b.orientationChange,b)),a(window).on("resize.slick.slick-"+b.instanceUid,a.proxy(b.resize,b)),a("[draggable!=true]",b.$slideTrack).on("dragstart",b.preventDefault),a(window).on("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).on("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.initUI=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.show(),a.$nextArrow.show()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.show()},b.prototype.keyHandler=function(a){var b=this;a.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===a.keyCode&&b.options.accessibility===!0?b.changeSlide({data:{message:b.options.rtl===!0?"next":"previous"}}):39===a.keyCode&&b.options.accessibility===!0&&b.changeSlide({data:{message:b.options.rtl===!0?"previous":"next"}}))},b.prototype.lazyLoad=function(){function g(c){a("img[data-lazy]",c).each(function(){var c=a(this),d=a(this).attr("data-lazy"),e=document.createElement("img");e.onload=function(){c.animate({opacity:0},100,function(){c.attr("src",d).animate({opacity:1},200,function(){c.removeAttr("data-lazy").removeClass("slick-loading")}),b.$slider.trigger("lazyLoaded",[b,c,d])})},e.onerror=function(){c.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),b.$slider.trigger("lazyLoadError",[b,c,d])},e.src=d})}var c,d,e,f,b=this;b.options.centerMode===!0?b.options.infinite===!0?(e=b.currentSlide+(b.options.slidesToShow/2+1),f=e+b.options.slidesToShow+2):(e=Math.max(0,b.currentSlide-(b.options.slidesToShow/2+1)),f=2+(b.options.slidesToShow/2+1)+b.currentSlide):(e=b.options.infinite?b.options.slidesToShow+b.currentSlide:b.currentSlide,f=Math.ceil(e+b.options.slidesToShow),b.options.fade===!0&&(e>0&&e--,f<=b.slideCount&&f++)),c=b.$slider.find(".slick-slide").slice(e,f),g(c),b.slideCount<=b.options.slidesToShow?(d=b.$slider.find(".slick-slide"),g(d)):b.currentSlide>=b.slideCount-b.options.slidesToShow?(d=b.$slider.find(".slick-cloned").slice(0,b.options.slidesToShow),g(d)):0===b.currentSlide&&(d=b.$slider.find(".slick-cloned").slice(-1*b.options.slidesToShow),g(d))},b.prototype.loadSlider=function(){var a=this;a.setPosition(),a.$slideTrack.css({opacity:1}),a.$slider.removeClass("slick-loading"),a.initUI(),"progressive"===a.options.lazyLoad&&a.progressiveLazyLoad()},b.prototype.next=b.prototype.slickNext=function(){var a=this;a.changeSlide({data:{message:"next"}})},b.prototype.orientationChange=function(){var a=this;a.checkResponsive(),a.setPosition()},b.prototype.pause=b.prototype.slickPause=function(){var a=this;a.autoPlayClear(),a.paused=!0},b.prototype.play=b.prototype.slickPlay=function(){var a=this;a.autoPlay(),a.options.autoplay=!0,a.paused=!1,a.focussed=!1,a.interrupted=!1},b.prototype.postSlide=function(a){var b=this;b.unslicked||(b.$slider.trigger("afterChange",[b,a]),b.animating=!1,b.setPosition(),b.swipeLeft=null,b.options.autoplay&&b.autoPlay(),b.options.accessibility===!0&&b.initADA())},b.prototype.prev=b.prototype.slickPrev=function(){var a=this;a.changeSlide({data:{message:"previous"}})},b.prototype.preventDefault=function(a){a.preventDefault()},b.prototype.progressiveLazyLoad=function(b){b=b||1;var e,f,g,c=this,d=a("img[data-lazy]",c.$slider);d.length?(e=d.first(),f=e.attr("data-lazy"),g=document.createElement("img"),g.onload=function(){e.attr("src",f).removeAttr("data-lazy").removeClass("slick-loading"),c.options.adaptiveHeight===!0&&c.setPosition(),c.$slider.trigger("lazyLoaded",[c,e,f]),c.progressiveLazyLoad()},g.onerror=function(){3>b?setTimeout(function(){c.progressiveLazyLoad(b+1)},500):(e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),c.$slider.trigger("lazyLoadError",[c,e,f]),c.progressiveLazyLoad())},g.src=f):c.$slider.trigger("allImagesLoaded",[c])},b.prototype.refresh=function(b){var d,e,c=this;e=c.slideCount-c.options.slidesToShow,!c.options.infinite&&c.currentSlide>e&&(c.currentSlide=e),c.slideCount<=c.options.slidesToShow&&(c.currentSlide=0),d=c.currentSlide,c.destroy(!0),a.extend(c,c.initials,{currentSlide:d}),c.init(),b||c.changeSlide({data:{message:"index",index:d}},!1)},b.prototype.registerBreakpoints=function(){var c,d,e,b=this,f=b.options.responsive||null;if("array"===a.type(f)&&f.length){b.respondTo=b.options.respondTo||"window";for(c in f)if(e=b.breakpoints.length-1,d=f[c].breakpoint,f.hasOwnProperty(c)){for(;e>=0;)b.breakpoints[e]&&b.breakpoints[e]===d&&b.breakpoints.splice(e,1),e--;b.breakpoints.push(d),b.breakpointSettings[d]=f[c].settings}b.breakpoints.sort(function(a,c){return b.options.mobileFirst?a-c:c-a})}},b.prototype.reinit=function(){var b=this;b.$slides=b.$slideTrack.children(b.options.slide).addClass("slick-slide"),b.slideCount=b.$slides.length,b.currentSlide>=b.slideCount&&0!==b.currentSlide&&(b.currentSlide=b.currentSlide-b.options.slidesToScroll),b.slideCount<=b.options.slidesToShow&&(b.currentSlide=0),b.registerBreakpoints(),b.setProps(),b.setupInfinite(),b.buildArrows(),b.updateArrows(),b.initArrowEvents(),b.buildDots(),b.updateDots(),b.initDotEvents(),b.cleanUpSlideEvents(),b.initSlideEvents(),b.checkResponsive(!1,!0),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),b.setSlideClasses("number"==typeof b.currentSlide?b.currentSlide:0),b.setPosition(),b.focusHandler(),b.paused=!b.options.autoplay,b.autoPlay(),b.$slider.trigger("reInit",[b])},b.prototype.resize=function(){var b=this;a(window).width()!==b.windowWidth&&(clearTimeout(b.windowDelay),b.windowDelay=window.setTimeout(function(){b.windowWidth=a(window).width(),b.checkResponsive(),b.unslicked||b.setPosition()},50))},b.prototype.removeSlide=b.prototype.slickRemove=function(a,b,c){var d=this;return"boolean"==typeof a?(b=a,a=b===!0?0:d.slideCount-1):a=b===!0?--a:a,d.slideCount<1||0>a||a>d.slideCount-1?!1:(d.unload(),c===!0?d.$slideTrack.children().remove():d.$slideTrack.children(this.options.slide).eq(a).remove(),d.$slides=d.$slideTrack.children(this.options.slide),d.$slideTrack.children(this.options.slide).detach(),d.$slideTrack.append(d.$slides),d.$slidesCache=d.$slides,void d.reinit())},b.prototype.setCSS=function(a){var d,e,b=this,c={};b.options.rtl===!0&&(a=-a),d="left"==b.positionProp?Math.ceil(a)+"px":"0px",e="top"==b.positionProp?Math.ceil(a)+"px":"0px",c[b.positionProp]=a,b.transformsEnabled===!1?b.$slideTrack.css(c):(c={},b.cssTransitions===!1?(c[b.animType]="translate("+d+", "+e+")",b.$slideTrack.css(c)):(c[b.animType]="translate3d("+d+", "+e+", 0px)",b.$slideTrack.css(c)))},b.prototype.setDimensions=function(){var a=this;a.options.vertical===!1?a.options.centerMode===!0&&a.$list.css({padding:"0px "+a.options.centerPadding}):(a.$list.height(a.$slides.first().outerHeight(!0)*a.options.slidesToShow),a.options.centerMode===!0&&a.$list.css({padding:a.options.centerPadding+" 0px"})),a.listWidth=a.$list.width(),a.listHeight=a.$list.height(),a.options.vertical===!1&&a.options.variableWidth===!1?(a.slideWidth=Math.ceil(a.listWidth/a.options.slidesToShow),a.$slideTrack.width(Math.ceil(a.slideWidth*a.$slideTrack.children(".slick-slide").length))):a.options.variableWidth===!0?a.$slideTrack.width(5e3*a.slideCount):(a.slideWidth=Math.ceil(a.listWidth),a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0)*a.$slideTrack.children(".slick-slide").length)));var b=a.$slides.first().outerWidth(!0)-a.$slides.first().width();a.options.variableWidth===!1&&a.$slideTrack.children(".slick-slide").width(a.slideWidth-b)},b.prototype.setFade=function(){var c,b=this;b.$slides.each(function(d,e){c=b.slideWidth*d*-1,b.options.rtl===!0?a(e).css({position:"relative",right:c,top:0,zIndex:b.options.zIndex-2,opacity:0}):a(e).css({position:"relative",left:c,top:0,zIndex:b.options.zIndex-2,opacity:0})}),b.$slides.eq(b.currentSlide).css({zIndex:b.options.zIndex-1,opacity:1})},b.prototype.setHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.css("height",b)}},b.prototype.setOption=b.prototype.slickSetOption=function(){var c,d,e,f,h,b=this,g=!1;if("object"===a.type(arguments[0])?(e=arguments[0],g=arguments[1],h="multiple"):"string"===a.type(arguments[0])&&(e=arguments[0],f=arguments[1],g=arguments[2],"responsive"===arguments[0]&&"array"===a.type(arguments[1])?h="responsive":"undefined"!=typeof arguments[1]&&(h="single")),"single"===h)b.options[e]=f;else if("multiple"===h)a.each(e,function(a,c){b.options[a]=c});else if("responsive"===h)for(d in f)if("array"!==a.type(b.options.responsive))b.options.responsive=[f[d]];else{for(c=b.options.responsive.length-1;c>=0;)b.options.responsive[c].breakpoint===f[d].breakpoint&&b.options.responsive.splice(c,1),c--;b.options.responsive.push(f[d])}g&&(b.unload(),b.reinit())},b.prototype.setPosition=function(){var a=this;a.setDimensions(),a.setHeight(),a.options.fade===!1?a.setCSS(a.getLeft(a.currentSlide)):a.setFade(),a.$slider.trigger("setPosition",[a])},b.prototype.setProps=function(){var a=this,b=document.body.style;a.positionProp=a.options.vertical===!0?"top":"left","top"===a.positionProp?a.$slider.addClass("slick-vertical"):a.$slider.removeClass("slick-vertical"),(void 0!==b.WebkitTransition||void 0!==b.MozTransition||void 0!==b.msTransition)&&a.options.useCSS===!0&&(a.cssTransitions=!0),a.options.fade&&("number"==typeof a.options.zIndex?a.options.zIndex<3&&(a.options.zIndex=3):a.options.zIndex=a.defaults.zIndex),void 0!==b.OTransform&&(a.animType="OTransform",a.transformType="-o-transform",a.transitionType="OTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.MozTransform&&(a.animType="MozTransform",a.transformType="-moz-transform",a.transitionType="MozTransition",void 0===b.perspectiveProperty&&void 0===b.MozPerspective&&(a.animType=!1)),void 0!==b.webkitTransform&&(a.animType="webkitTransform",a.transformType="-webkit-transform",a.transitionType="webkitTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.msTransform&&(a.animType="msTransform",a.transformType="-ms-transform",a.transitionType="msTransition",void 0===b.msTransform&&(a.animType=!1)),void 0!==b.transform&&a.animType!==!1&&(a.animType="transform",a.transformType="transform",a.transitionType="transition"),a.transformsEnabled=a.options.useTransform&&null!==a.animType&&a.animType!==!1},b.prototype.setSlideClasses=function(a){var c,d,e,f,b=this;d=b.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),b.$slides.eq(a).addClass("slick-current"),b.options.centerMode===!0?(c=Math.floor(b.options.slidesToShow/2),b.options.infinite===!0&&(a>=c&&a<=b.slideCount-1-c?b.$slides.slice(a-c,a+c+1).addClass("slick-active").attr("aria-hidden","false"):(e=b.options.slidesToShow+a,
d.slice(e-c+1,e+c+2).addClass("slick-active").attr("aria-hidden","false")),0===a?d.eq(d.length-1-b.options.slidesToShow).addClass("slick-center"):a===b.slideCount-1&&d.eq(b.options.slidesToShow).addClass("slick-center")),b.$slides.eq(a).addClass("slick-center")):a>=0&&a<=b.slideCount-b.options.slidesToShow?b.$slides.slice(a,a+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):d.length<=b.options.slidesToShow?d.addClass("slick-active").attr("aria-hidden","false"):(f=b.slideCount%b.options.slidesToShow,e=b.options.infinite===!0?b.options.slidesToShow+a:a,b.options.slidesToShow==b.options.slidesToScroll&&b.slideCount-a<b.options.slidesToShow?d.slice(e-(b.options.slidesToShow-f),e+f).addClass("slick-active").attr("aria-hidden","false"):d.slice(e,e+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===b.options.lazyLoad&&b.lazyLoad()},b.prototype.setupInfinite=function(){var c,d,e,b=this;if(b.options.fade===!0&&(b.options.centerMode=!1),b.options.infinite===!0&&b.options.fade===!1&&(d=null,b.slideCount>b.options.slidesToShow)){for(e=b.options.centerMode===!0?b.options.slidesToShow+1:b.options.slidesToShow,c=b.slideCount;c>b.slideCount-e;c-=1)d=c-1,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d-b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");for(c=0;e>c;c+=1)d=c,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d+b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");b.$slideTrack.find(".slick-cloned").find("[id]").each(function(){a(this).attr("id","")})}},b.prototype.interrupt=function(a){var b=this;a||b.autoPlay(),b.interrupted=a},b.prototype.selectHandler=function(b){var c=this,d=a(b.target).is(".slick-slide")?a(b.target):a(b.target).parents(".slick-slide"),e=parseInt(d.attr("data-slick-index"));return e||(e=0),c.slideCount<=c.options.slidesToShow?(c.setSlideClasses(e),void c.asNavFor(e)):void c.slideHandler(e)},b.prototype.slideHandler=function(a,b,c){var d,e,f,g,j,h=null,i=this;return b=b||!1,i.animating===!0&&i.options.waitForAnimate===!0||i.options.fade===!0&&i.currentSlide===a||i.slideCount<=i.options.slidesToShow?void 0:(b===!1&&i.asNavFor(a),d=a,h=i.getLeft(d),g=i.getLeft(i.currentSlide),i.currentLeft=null===i.swipeLeft?g:i.swipeLeft,i.options.infinite===!1&&i.options.centerMode===!1&&(0>a||a>i.getDotCount()*i.options.slidesToScroll)?void(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d))):i.options.infinite===!1&&i.options.centerMode===!0&&(0>a||a>i.slideCount-i.options.slidesToScroll)?void(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d))):(i.options.autoplay&&clearInterval(i.autoPlayTimer),e=0>d?i.slideCount%i.options.slidesToScroll!==0?i.slideCount-i.slideCount%i.options.slidesToScroll:i.slideCount+d:d>=i.slideCount?i.slideCount%i.options.slidesToScroll!==0?0:d-i.slideCount:d,i.animating=!0,i.$slider.trigger("beforeChange",[i,i.currentSlide,e]),f=i.currentSlide,i.currentSlide=e,i.setSlideClasses(i.currentSlide),i.options.asNavFor&&(j=i.getNavTarget(),j=j.slick("getSlick"),j.slideCount<=j.options.slidesToShow&&j.setSlideClasses(i.currentSlide)),i.updateDots(),i.updateArrows(),i.options.fade===!0?(c!==!0?(i.fadeSlideOut(f),i.fadeSlide(e,function(){i.postSlide(e)})):i.postSlide(e),void i.animateHeight()):void(c!==!0?i.animateSlide(h,function(){i.postSlide(e)}):i.postSlide(e))))},b.prototype.startLoad=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.hide(),a.$nextArrow.hide()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.hide(),a.$slider.addClass("slick-loading")},b.prototype.swipeDirection=function(){var a,b,c,d,e=this;return a=e.touchObject.startX-e.touchObject.curX,b=e.touchObject.startY-e.touchObject.curY,c=Math.atan2(b,a),d=Math.round(180*c/Math.PI),0>d&&(d=360-Math.abs(d)),45>=d&&d>=0?e.options.rtl===!1?"left":"right":360>=d&&d>=315?e.options.rtl===!1?"left":"right":d>=135&&225>=d?e.options.rtl===!1?"right":"left":e.options.verticalSwiping===!0?d>=35&&135>=d?"down":"up":"vertical"},b.prototype.swipeEnd=function(a){var c,d,b=this;if(b.dragging=!1,b.interrupted=!1,b.shouldClick=b.touchObject.swipeLength>10?!1:!0,void 0===b.touchObject.curX)return!1;if(b.touchObject.edgeHit===!0&&b.$slider.trigger("edge",[b,b.swipeDirection()]),b.touchObject.swipeLength>=b.touchObject.minSwipe){switch(d=b.swipeDirection()){case"left":case"down":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide+b.getSlideCount()):b.currentSlide+b.getSlideCount(),b.currentDirection=0;break;case"right":case"up":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide-b.getSlideCount()):b.currentSlide-b.getSlideCount(),b.currentDirection=1}"vertical"!=d&&(b.slideHandler(c),b.touchObject={},b.$slider.trigger("swipe",[b,d]))}else b.touchObject.startX!==b.touchObject.curX&&(b.slideHandler(b.currentSlide),b.touchObject={})},b.prototype.swipeHandler=function(a){var b=this;if(!(b.options.swipe===!1||"ontouchend"in document&&b.options.swipe===!1||b.options.draggable===!1&&-1!==a.type.indexOf("mouse")))switch(b.touchObject.fingerCount=a.originalEvent&&void 0!==a.originalEvent.touches?a.originalEvent.touches.length:1,b.touchObject.minSwipe=b.listWidth/b.options.touchThreshold,b.options.verticalSwiping===!0&&(b.touchObject.minSwipe=b.listHeight/b.options.touchThreshold),a.data.action){case"start":b.swipeStart(a);break;case"move":b.swipeMove(a);break;case"end":b.swipeEnd(a)}},b.prototype.swipeMove=function(a){var d,e,f,g,h,b=this;return h=void 0!==a.originalEvent?a.originalEvent.touches:null,!b.dragging||h&&1!==h.length?!1:(d=b.getLeft(b.currentSlide),b.touchObject.curX=void 0!==h?h[0].pageX:a.clientX,b.touchObject.curY=void 0!==h?h[0].pageY:a.clientY,b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curX-b.touchObject.startX,2))),b.options.verticalSwiping===!0&&(b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curY-b.touchObject.startY,2)))),e=b.swipeDirection(),"vertical"!==e?(void 0!==a.originalEvent&&b.touchObject.swipeLength>4&&a.preventDefault(),g=(b.options.rtl===!1?1:-1)*(b.touchObject.curX>b.touchObject.startX?1:-1),b.options.verticalSwiping===!0&&(g=b.touchObject.curY>b.touchObject.startY?1:-1),f=b.touchObject.swipeLength,b.touchObject.edgeHit=!1,b.options.infinite===!1&&(0===b.currentSlide&&"right"===e||b.currentSlide>=b.getDotCount()&&"left"===e)&&(f=b.touchObject.swipeLength*b.options.edgeFriction,b.touchObject.edgeHit=!0),b.options.vertical===!1?b.swipeLeft=d+f*g:b.swipeLeft=d+f*(b.$list.height()/b.listWidth)*g,b.options.verticalSwiping===!0&&(b.swipeLeft=d+f*g),b.options.fade===!0||b.options.touchMove===!1?!1:b.animating===!0?(b.swipeLeft=null,!1):void b.setCSS(b.swipeLeft)):void 0)},b.prototype.swipeStart=function(a){var c,b=this;return b.interrupted=!0,1!==b.touchObject.fingerCount||b.slideCount<=b.options.slidesToShow?(b.touchObject={},!1):(void 0!==a.originalEvent&&void 0!==a.originalEvent.touches&&(c=a.originalEvent.touches[0]),b.touchObject.startX=b.touchObject.curX=void 0!==c?c.pageX:a.clientX,b.touchObject.startY=b.touchObject.curY=void 0!==c?c.pageY:a.clientY,void(b.dragging=!0))},b.prototype.unfilterSlides=b.prototype.slickUnfilter=function(){var a=this;null!==a.$slidesCache&&(a.unload(),a.$slideTrack.children(this.options.slide).detach(),a.$slidesCache.appendTo(a.$slideTrack),a.reinit())},b.prototype.unload=function(){var b=this;a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.remove(),b.$nextArrow&&b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.remove(),b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},b.prototype.unslick=function(a){var b=this;b.$slider.trigger("unslick",[b,a]),b.destroy()},b.prototype.updateArrows=function(){var b,a=this;b=Math.floor(a.options.slidesToShow/2),a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&!a.options.infinite&&(a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===a.currentSlide?(a.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-a.options.slidesToShow&&a.options.centerMode===!1?(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-1&&a.options.centerMode===!0&&(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},b.prototype.updateDots=function(){var a=this;null!==a.$dots&&(a.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),a.$dots.find("li").eq(Math.floor(a.currentSlide/a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))},b.prototype.visibility=function(){var a=this;a.options.autoplay&&(document[a.hidden]?a.interrupted=!0:a.interrupted=!1)},a.fn.slick=function(){var f,g,a=this,c=arguments[0],d=Array.prototype.slice.call(arguments,1),e=a.length;for(f=0;e>f;f++)if("object"==typeof c||"undefined"==typeof c?a[f].slick=new b(a[f],c):g=a[f].slick[c].apply(a[f].slick,d),"undefined"!=typeof g)return g;return a}});
//# sourceMappingURL=main.js.map

/* -------------------
BEGIN  
Sitecore Resource: 709b117c-3e11-49f7-b252-2488fb4e5fde
   -------------------*/
$(document).ready(function () {
    $('.hs__hidden').unbind('click');
    var filterObject = {
        1: false,
        2: false,
        3: false,
        4: false,
        5: false
    }
    var hiddenCards = Array.from(document.querySelectorAll('.hs__modules__col'));
    var cardWrapper = document.querySelector('.hs__modules__row');
    function filterCards(){
        cardWrapper.innerHTML = '';
        if(filterObject[1] === true && filterObject[2] === true && filterObject[3] === true  && filterObject[4] === true && filterObject[5] === true){
            hiddenCards.map(function(x){
                var item = x.cloneNode(true);
                cardWrapper.append(item);
            });
        }
        else if(filterObject[1] === false && filterObject[2] === false && filterObject[3] === false  && filterObject[4] === false && filterObject[5] === false){
            hiddenCards.map(function(x){
                var item = x.cloneNode(true);
                cardWrapper.append(item);
            });
        }
        else{
            var status = Object.entries(filterObject).filter(function(y){
                return y[1] === true;
            });
            hiddenCards.map(function(x){
                var category = x.getAttribute('data-category');
                status.map(function(z){
                    if(z[0] == category){
                        var item = x.cloneNode(true);
                        cardWrapper.append(item);
                    }
                });
            });
        }
    }

    $('#neurodermitis__allgemein').change(function(){
        if($('#neurodermitis__allgemein').is(':checked')){
            filterObject[1] = true;
        }
        else{
            filterObject[1] = false;
        }
        filterCards();
    });
    $('#schlafen').on('change',function(){
        if($('#schlafen').is(':checked')){
            filterObject[2] = true;
        }
        else{
            filterObject[2] = false;
        }
        filterCards();
    });
    $('#hautpflege').on('change', function(){
        if($('#hautpflege').is(':checked')){
            filterObject[3] = true;
        }
        else{
            filterObject[3] = false;
        }
        filterCards();
    });
    $('#umgang__mit__neurodermitis').on('change',function(){
        if($('#umgang__mit__neurodermitis').is(':checked')){
            filterObject[4] = true;
        }
        else{
            filterObject[4] = false;
        }
        filterCards();
    });
    $('#arztbesuch').on('change',function(){
        if($('#arztbesuch').is(':checked')){
            filterObject[5] = true;
        }
        else{
            filterObject[5] = false;
        }
        filterCards();
    });

    filterCards();
});
