
/* -------------------
BEGIN how-dupixent-is-taken 
Sitecore Resource: 3bb6e07e-e6d9-4ad3-b981-09af7d14af3f
   -------------------*/
$(function(){
    var next = $('.btn.next'),
        prev = $('.btn.prev'),
        showAll = $('.show-all'),
        showFewer = $('.show-fewer'),
        steps = $('.instructions-list'),
        length = steps.children('.diamond-ol').length;
        
    steps.children('.diamond-ol').first().addClass('active');
    prev.addClass('invisible');
    next.text('Die nächsten 2 Schritte');
    updateScrollPos($('.active', steps));
    
    next.on('click', function(e){
        e.preventDefault();
        $('.active', steps).removeClass('active').next('.diamond-ol').addClass('active');
        
        updateButtons();
        updateScrollPos();
    });
    
    prev.on('click', function(e){
        e.preventDefault();
        $('.active', steps).removeClass('active').prev('.diamond-ol').addClass('active');

        updateButtons();
        updateScrollPos();
    });
    
    showAll.on('click', function(e){
        e.preventDefault();
        
        steps.addClass('show-all-steps');

        //hide previous next
        next.addClass('invisible');
        prev.addClass('invisible');
        
        showAll.addClass('hidden');
        showFewer.removeClass('hidden');
        
        //console.log('html: ' + $('html').scrollTop(), 'body: ' + $('body').scrollTop());
        
        $('html, body').stop(true, false).animate({ 
            //scrollTop: window.scrollY + steps.scrollTop()
            scrollTop: $('#instructionsList').offset().top
        }, 1000); 
        
        steps.stop(true, false).animate({
            height: steps.get(0).scrollHeight,
            scrollTop: 0
        }, 1000, function(){
            $(this).css('height', 'auto');
        });
    });
    
    showFewer.on('click', function(e){
        e.preventDefault();
        steps.removeClass('show-all-steps');
        showFewer.addClass('hidden');
        showAll.removeClass('hidden');
        updateScrollPos($('.active', steps));
        updateButtons();
        
        $('html, body').stop(true, false).animate({ 
            //scrollTop: $('.instructions-intro').offset().top
            scrollTop: $('#instructionsList').offset().top
        }, 1000);
    });

    function updateButtons() {
        var count = $('.active', steps).next('.diamond-ol').children().length,
            stepText = (count > 1) ? count + ' Steps' : 'Step';

        //next.text('Next ' + stepText);
        
        ($('.active', steps).index() !== 0) ? prev.removeClass('invisible') : prev.addClass('invisible');
        ($('.active', steps).index() !== length - 1) ? next.removeClass('invisible') : next.addClass('invisible');
    }
    
    function updateScrollPos () {
        var active = $('.active', steps);
        
        if(!showAll.hasClass('hidden')){
            steps.stop(true, false).animate({
                scrollTop: active.position().top + steps.scrollTop(),
                height: active.outerHeight(true)
            }, 500);
        }
    }
    
    $(window).on('resize orientationchange', updateScrollPos);
})
