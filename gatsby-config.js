/**
 * @type {import('gatsby').GatsbyConfig}
 */
module.exports = {
  siteMetadata: {
    title: `LMND`,
    description: `Neurodermitis`,
    metaTitle: 'LMND',
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [
    'gatsby-plugin-sass',
    'gatsby-plugin-image',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-plugin-sitemap',
    'gatsby-plugin-fix-fouc',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/assets/images/`,
      },
      __key: 'images',
    },
    {
      resolve: `gatsby-transformer-csv`,
      options: {
        extensions: [`csv`],
        // delimiter: '*-*',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `cms`,
        path: `${__dirname}/src/cms/`,
      },
    },
    {
      resolve: 'gatsby-plugin-html-attributes',
      options: {
        lang: 'de',
      },
    },
    {
      resolve: 'gatsby-plugin-anchor-links',
      options: {
        duration: 0,
      },
    },
    {
      resolve: `gatsby-plugin-s3`,
      options: {
        bucketName: 'havaslmndbucket',
      },
    },
  ],
};
