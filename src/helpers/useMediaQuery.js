import { useState, useEffect } from 'react';
import { findClosest, sizes } from './helpers';

export const useMediaQuery = () => {
  const [matches, setMatches] = useState(window.innerWidth);
  const [device, setDevice] = useState('mobile');

  useEffect(() => {
    if (matches < 800) {
      setDevice('mobile');
    } else {
      setDevice('desktop');
    }
    const listener = () => setMatches(window.innerWidth);
    window.addEventListener('resize', listener);
    return () => window.removeEventListener('resize', listener);
  }, [matches]);

  return device;
};
