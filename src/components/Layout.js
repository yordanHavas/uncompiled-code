import React, { createContext, useEffect, useState } from 'react';
import BackToTopButton from './BackToTopButton';
import Footer from './Footer';
import Header from './Header';
import QuicklinkMenu from './QuicklinkMenu';

import '../styles/main.scss';

export const MobileContext = createContext(null);

export default function Layout({ children, className }) {
  const [width, setWidth] = useState(0);

  function handleWindowSizeChange() {
    setWidth(window.innerWidth);
  }

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener('resize', handleWindowSizeChange);
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange);
    };
  }, []);

  const isMobile = parseInt(width) <= 768;
  return (
    <MobileContext.Provider value={isMobile}>
      <Header />
      {/* {<DescktopThemenübersicht />} */}
      <main className={className}>{children} </main>
      <BackToTopButton />
      <Footer />
      <QuicklinkMenu />
    </MobileContext.Provider>
  );
}
