import React, { useContext } from 'react';
import Button from './Button';
import HeadingWithColored from './HeadingWithColored';
import { MobileContext } from './Layout';

import AF1 from '../assets/icons/teaser-arztefinder_img_1.svg';
import AF2 from '../assets/icons/teaser-arztefinder_img_2.svg';
import AF3 from '../assets/icons/teaser-arztefinder_img_3.svg';

import ST1 from '../assets/icons/teaser-selbsttest_img_1.svg';
import ST2 from '../assets/icons/teaser-selbsttest_img_2.svg';
import ST3 from '../assets/icons/teaser-selbsttest_img_3.svg';

const AF = [AF1, AF2, AF3];
const ST = [ST1, ST2, ST3];

let totalCounter = 0;
const Teaser = ({
  imgSrc,
  text,
  textList,
  buttonText,
  buttonLink,
  coloredWordsArr,
  style,
  bigSpanBg,
  linkType,
}) => {
  return (
    <div
      className={`teaser ${totalCounter++ % 2 === 0 ? 'reversed' : ''}`}
      style={style}>
      <div className="imageHolder">
        <img alt="" src={imgSrc} />
      </div>

      <div>
        <HeadingWithColored
          className={bigSpanBg ? 'bigSpanBg' : ''}
          textList={textList}
          coloredWordsArr={coloredWordsArr}
        />
        {text && <p>{text}</p>}
        <Button
          linkType={linkType === 'a' ? 'a' : ''}
          to={buttonLink}
          type="yellow"
          text={buttonText}
        />
      </div>
    </div>
  );
};

let selbsttestCounter = 0;
export function SelbsttestTeaser({ style, customeText, customImageIndex }) {
  const isMobile = useContext(MobileContext);

  return (
    <Teaser
      className="selbsttest-teaser"
      textList={
        customeText
          ? customeText
          : ['WIE SEHR HAST DU', 'DEINE NEURODERMITIS', 'UNTER KONTROLLE?']
      }
      coloredWordsArr={['UNTER KONTROLLE?']}
      buttonText={`${isMobile ? 'Zum Selbsttest' : 'Mach den Selbsttest'}`}
      buttonLink="/selbsttest"
      imgSrc={
        ST[
          customImageIndex === undefined
            ? selbsttestCounter++ % 3
            : customImageIndex
        ]
      }
      style={style}
      linkType="a"
    />
  );
}

let arztefinderCounter = 0;
export function ArztefinderTeaser({ style }) {
  return (
    <Teaser
      textList={['Finde Experten In ', 'deiner Nähe']}
      coloredWordsArr={['deiner Nähe']}
      text="Hier kannst Du ganz einfach einen Experten in Deiner Nähe finden."
      buttonText="Zum Ärztefinder"
      buttonLink="/aerztefinder"
      imgSrc={AF[arztefinderCounter++ % 3]}
      reversed={true}
      style={style}
      bigSpanBg={true}
    />
  );
}

export default Teaser;
