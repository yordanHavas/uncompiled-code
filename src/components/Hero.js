import React, { useContext } from 'react';
import Circle from './Circle';
import HeadingWithColored from './HeadingWithColored';
import { MobileContext } from './Layout';
import ResponsiveImage from './ResponsiveImage';

export default function Hero({
  headingTextList,
  color,
  imgName,
  altImg,
  text,
  coloredWordsArr,
  className,
  cicleClassName,
}) {
  const isMobile = useContext(MobileContext);
  if (isMobile) {
    return (
      <MobileHero
        className={className}
        headingTextList={headingTextList}
        coloredWordsArr={coloredWordsArr}
        color={color}
        imgName={imgName}
        altImg={altImg}
        text={text}
        cicleClassName={cicleClassName}
      />
    );
  } else {
    return (
      <DesktopHero
        className={className}
        headingTextList={headingTextList}
        color={color}
        imgName={imgName}
        coloredWordsArr={coloredWordsArr}
        altImg={altImg}
        text={text}
        cicleClassName={cicleClassName}
      />
    );
  }
}

function DesktopHero({
  headingTextList,
  color,
  imgName,
  coloredWordsArr,
  altImg,
  text,
  className,
  cicleClassName,
}) {
  return (
    <div className={`hero ${className ? className : ''}`}>
      <div>
        <HeadingWithColored
          textList={headingTextList}
          coloredWordsArr={coloredWordsArr}
          variant="h1"
          color={color}
        />
        <Circle className={cicleClassName}>
          <p>{text}</p>
        </Circle>
      </div>

      <ResponsiveImage
        imgName={imgName}
        altImg={altImg}
        className="image image-br image-tilt"
      />
    </div>
  );
}

function MobileHero({
  headingTextList,
  coloredWordsArr,
  color,
  imgName,
  altImg,
  text,
  className,
  cicleClassName,
}) {
  return (
    <div className={`hero ${className ? className : ''}`}>
      <HeadingWithColored
        textList={headingTextList}
        col
        coloredWordsArr={coloredWordsArr}
        variant="h1"
        color={color}
      />
      <ResponsiveImage
        className="image image-br image-tilt"
        imgName={imgName}
        altImg={altImg}
      />

      <Circle className={cicleClassName}>
        <p>{text}</p>
      </Circle>
    </div>
  );
}
