import React, { useContext, useEffect, useState } from 'react';
import { MobileContext } from '../components/Layout';
import CardBig from './CardBig';
import arrow_right from '../assets/icons/arrow_right.svg';
import arrow_left from '../assets/icons/arrow_left.svg';
import { Carousel, Stack } from 'react-bootstrap';
import ResponsiveImage from './ResponsiveImage';
import Button from './Button';

export default function FilteredSlider({ cardsL }) {
  const [checked, setChecked] = useState(false);
  const [Cards_List, setCards_List] = useState(cardsL);
  const [activeTab, setActiveTab] = useState('alle');
  const isMobile = useContext(MobileContext);

  const [index, setIndex] = useState(0);
  const onPrevClick = () => {
    if (index <= 0) {
      return;
    }

    setIndex(index - 3);
  };
  const onNextClick = () => {
    if (index >= Cards_List.length - 3) {
      return;
    }

    setIndex(index + 3);
  };

  useEffect(() => {
    setCards_List(
      cardsL.filter(
        (item) => item.category === activeTab || activeTab === 'alle'
      )
    );
  }, [activeTab]);

  return (
    <div className="filtered-slider">
      <div
        className="filter-content-container"
        style={{ marginBottom: '55px' }}>
        <div className="filter-toggle">
          <div
            className={`radio-button ${checked ? 'checked' : ''}`}
            onClick={() => {
              setChecked(!checked);
            }}>
            <div className="circle"></div>
          </div>
          <p className="filter-text">Veranstaltungen filtern</p>
        </div>

        <div
          style={{ marginTop: `${checked ? (isMobile ? '40px' : '0') : '0'}` }}
          className={`filter-categories ${checked ? 'open' : 'closed'}`}>
          <p style={{ fontWeight: '900', marginBottom: '15px' }}>Kanäle</p>
          <div className="filter-buttons-container">
            <div
              className={`filter-button ${
                activeTab === 'alle' ? 'active' : ''
              }`}
              onClick={() => {
                setActiveTab('alle');
              }}>
              Alle
            </div>
            <div
              className={`filter-button ${
                activeTab === 'webinar' ? 'active' : ''
              }`}
              onClick={() => {
                setActiveTab('webinar');
              }}>
              Webinar
            </div>
            <div
              className={`filter-button ${
                activeTab === 'instagram' ? 'active' : ''
              }`}
              onClick={() => {
                setActiveTab('instagram');
              }}>
              Instagram
            </div>
          </div>
        </div>
      </div>
      <div className="sliderContent">
        <div className="card-container var1">
          <div>
            <Carousel
              interval={null}
              sli
              activeIndex={index}
              indicators={false}
              controls={false}>
              {Cards_List.filter(
                (item) => item.category === activeTab || activeTab === 'alle'
              ).map((item, index) => {
                const nextItem = Cards_List[index + 1];
                const thirdItem = Cards_List[index + 2];

                return (
                  <Carousel.Item>
                    <div className="card-container var1">
                      <CardBig
                        image={item.image}
                        altImg={item.altImg}
                        subTitle={item.subTitle}
                        title={item.title}
                        text={item.text}
                        buttonText={item.buttonText}
                        buttonLink={item.buttonLink}
                        cardTitleMinHeight={item.cardTitleMinHeight}
                      />
                      {nextItem && (
                        <CardBig
                          image={nextItem.image}
                          altImg={nextItem.altImg}
                          subTitle={nextItem.subTitle}
                          title={nextItem.title}
                          text={nextItem.text}
                          buttonText={nextItem.buttonText}
                          buttonLink={nextItem.buttonLink}
                          cardTitleMinHeight={nextItem.cardTitleMinHeight}
                        />
                      )}
                      {thirdItem && (
                        <CardBig
                          image={thirdItem.image}
                          altImg={thirdItem.altImg}
                          subTitle={thirdItem.subTitle}
                          title={thirdItem.title}
                          text={thirdItem.text}
                          buttonText={thirdItem.buttonText}
                          buttonLink={thirdItem.buttonLink}
                          cardTitleMinHeight={thirdItem.cardTitleMinHeight}
                        />
                      )}
                    </div>
                  </Carousel.Item>
                );
              })}
            </Carousel>
          </div>
        </div>
        <div className="paginationContainer">
          <div onClick={onPrevClick} className="arrowConainer">
            <img src={arrow_left} />
          </div>

          <div>
            {Math.floor(index / 3) + 1}/
            {Math.floor((Cards_List.length - 1) / 3) + 1}
          </div>
          <div onClick={onNextClick} className="arrowConainer">
            <img src={arrow_right} />
          </div>
        </div>
      </div>
    </div>
  );
}
