import { MobileContext } from '../components/Layout';
import React, { useContext, useState } from 'react';
import CardBig from './CardBig';
import Accordion from 'react-bootstrap/Accordion';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import arrow from '../assets/icons/arrow_button.svg';
let openFirst = true;

function CustomToggle({ heading, eventKey, seminarLen }) {
  const [open, openAccordion] = useState(false);

  const decoratedOnClick = useAccordionButton(eventKey, () => {
    openAccordion(!open);
  });

  if (openFirst && seminarLen) {
    decoratedOnClick();
    openFirst = false;
  }

  return (
    <div className="akkordeon">
      <img
        onClick={decoratedOnClick}
        src={arrow}
        className={`${open ? 'arrow-up' : 'arrow-down'}`}
        alt="Arrow Down Icon"
      />
    </div>
  );
}

const VeranAkkordeon = ({ seminarArray }) => {
  const isMobile = useContext(MobileContext);
  return (
    <>
      <Accordion style={{ width: '100%' }}>
        <CustomToggle eventKey="1" seminarLen={seminarArray.length} />
        <Accordion.Collapse eventKey="1">
          <>
            {seminarArray.length ? (
              <DisplayAkkordeon seminarArray={seminarArray} />
            ) : (
              <h3 style={{ alignSelf: 'self-start', marginLeft: '0' }}>
                Aktuell stehen keine Veranstaltungen an.
              </h3>
            )}
          </>
        </Accordion.Collapse>
      </Accordion>
    </>
  );
};

const DisplayAkkordeon = ({ seminarArray }) => {
  const isMobile = useContext(MobileContext);
  return (
    <>
      <div className="seminarContainer">
        {!isMobile &&
          seminarArray.map((item) => {
            const {
              altImg,
              buttonLink,
              buttonText,
              className,
              displayDate: date,
              image,
              subTitle,
              text,
              title,
            } = item;
            const event = (
              <CardBig
                date={date}
                className={className}
                image={image}
                altImg={altImg}
                subTitle={subTitle}
                title={title}
                text={text}
                buttonText={buttonText}
                buttonLink={buttonLink}
              />
            );

            if (isMobile) {
              return event;
            } else {
              return <div>{event}</div>;
            }
          })}
      </div>
      {isMobile &&
        seminarArray.map((item) => {
          const {
            altImg,
            buttonLink,
            buttonText,
            className,
            displayDate: date,
            image,
            subTitle,
            text,
            title,
          } = item;
          const event = (
            <CardBig
              style={{ width: '100%' }}
              date={date}
              className={className}
              image={image}
              altImg={altImg}
              subTitle={subTitle}
              title={title}
              text={text}
              buttonText={buttonText}
              buttonLink={buttonLink}
            />
          );

          if (isMobile) {
            return event;
          } else {
            return <div>{event}</div>;
          }
        })}
    </>
  );
};

export default VeranAkkordeon;
