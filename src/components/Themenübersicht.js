import React, { useEffect, useState } from 'react';
import { Col, ListGroup } from 'react-bootstrap';
import Accordion from 'react-bootstrap/Accordion';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import arrow from '../assets/icons/arrow-downPNG.png';
import useScrollDirection from '../helpers/useScrollDirection';

function CustomToggle({ eventKey }) {
  const [open, openAccordion] = useState(true);

  const decoratedOnClick = useAccordionButton(eventKey, () => {
    openAccordion(!open);
  });

  return (
    <Card.Body
      className={`${open ? 'rounded-pill text-center' : 'roundBottom'}`}
      style={{
        height: '80px',
        color: 'white',
        backgroundColor: '#633bb0',
      }}>
      <Row className="g-0">
        <Col xs={10} className="">
          <Card.Title className="text-center cardTittle">
            Themenübersicht
          </Card.Title>
        </Col>
        <Col xs={2} className="buttonContainer">
          <Button
            className="rounded-circle button "
            onClick={decoratedOnClick}
            style={{
              backgroundColor: 'white',
              borderColor: '#633bb0',
            }}>
            <img
              src={arrow}
              className={`${
                open ? 'arrow-down-translate' : 'arrow-up-translate'
              }`}
              alt="Arrow Down Icon"
            />
          </Button>
        </Col>
      </Row>
    </Card.Body>
  );
}

// Тhe purpose of this component is to contain topic overview of the page
// and should containt links that scrolls to a specific topic of the page
// Example of list items [["WAS IST NEURODERMITIS","neurodermis_ID"]] OR see example on kopfhaut page
export function MobileThemenübersicht() {
  const data = GetData();

  return (
    <Accordion defaultActiveKey="1" className="mobileThemen">
      <Card className="border-0 themenubersicht">
        <Card.Header
          className="border-0"
          style={{ padding: '0', backgroundColor: '#eeeaf6' }}>
          <CustomToggle eventKey="0" />
        </Card.Header>
        <Accordion.Collapse
          eventKey="0"
          style={{
            backgroundColor: '#eeeaf6',
          }}>
          <Card
            style={{
              borderRadius: '0 0 40px 40px',
              overflow: 'hidden',
            }}>
            <ListGroup variant="flush">
              <ListGroup.Item className="listItem">
                {data.map((item, index) => {
                  const { text, click } = item;

                  return (
                    <a key={index} onClick={click}>
                      {text}
                    </a>
                  );
                })}
              </ListGroup.Item>
            </ListGroup>
          </Card>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
}

function GetData() {
  const [data, setData] = useState([]);

  const fn = () => {
    let first = true;

    const header = document.querySelector('[data-header]');
    const sections = [...document.querySelectorAll('[data-summary]')];

    let prevYPosition = 0;
    let direction = 'up';

    const options = {
      rootMargin: `${header.offsetHeight * -1}px`,
      threshold: 0,
    };

    const getTargetSection = (entry) => {
      const index = sections.findIndex((section) => section == entry.target);

      if (index >= sections.length - 1) {
        return entry.target;
      } else {
        return sections[index + 1];
      }
    };
    const shouldUpdate = (entry) => {
      if (direction === 'rami') {
        return false;
      }

      if (direction === 'down' && !entry.isIntersecting) {
        return true;
      }

      if (direction === 'up' && entry.isIntersecting) {
        return true;
      }

      return false;
    };

    const onIntersect = (entries) => {
      entries.forEach((entry) => {
        if (window.scrollY > prevYPosition) {
          direction = 'down';
        } else if (window.scrollY < prevYPosition) {
          direction = 'up';
        } else {
          direction = 'rami';
        }
        prevYPosition = window.scrollY;

        const target =
          direction === 'down' ? getTargetSection(entry) : entry.target;

        if (shouldUpdate(entry)) {
          const visibleElement = target.attributes['data-summary'].value;
          setData((items) => {
            return items.map((item) => {
              if (item.text === visibleElement) {
                return { ...item, isCurrent: true };
              }
              return { ...item, isCurrent: false };
            });
          });
        }
      });
    };

    const observer = new IntersectionObserver(onIntersect, options);
    sections.forEach((section, index) => {
      observer.observe(section);
      setData((prevArray) => [
        ...prevArray,
        {
          text: section.attributes['data-summary'].value,
          click: () => section.scrollIntoView(),
          isCurrent: index === 0 ? true : false,
        },
      ]);
    });
  };

  useEffect(() => {
    if (document.readyState === 'complete') {
      fn();
    } else {
      const li = document.addEventListener('readystatechange', fn);
    }

    return () => {
      document.removeEventListener('readystatechange', fn);
    };
  }, []);
  return data;
}

// Тhe purpose of this component is to contain topic overview of the page
// and should containt links that scrolls to a specific topic of the page
// Example of list items [["WAS IST NEURODERMITIS","neurodermis_ID"]] OR see example on kopfhaut page
export function DescktopThemenübersicht() {
  const data = GetData();
  const scrollInfo = useScrollDirection();
  const { onTop, direction } = scrollInfo || { onTop: true, direction: 'up' };

  if (!data.length) return <></>;
  let isCurrentPast = false;

  return (
    <>
      <ol
        style={{
          position: `${
            onTop ? 'relative' : direction === 'down' ? 'relative' : 'fixed'
          }`,
        }}
        className={`ProgressBar ${direction === 'down' ? 'hide' : 'show'}`}>
        {data.map((item, index) => {
          const { text, click, isCurrent } = item;
          if (isCurrent) {
            isCurrentPast = true;
          }
          return (
            <li
              key={index}
              className={`ProgressBar-step ${
                isCurrent ? 'is-current' : isCurrentPast ? '' : 'is-complete'
              }`}>
              <svg className="ProgressBar-icon"></svg>
              <a onClick={click} className="ProgressBar-stepLabel">
                {text}
              </a>
            </li>
          );
        })}
      </ol>
    </>
  );
}
