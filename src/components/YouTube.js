import React, { useContext, useEffect, useState } from 'react';
import youTubeIcon from '../assets/icons/you-tube-icon.svg';
import { MobileContext } from '../components/Layout';
import { timeFromDate } from '../helpers/helpers';

import Button from './Button';

const YOU_TUBE_LINK =
  'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.youtube.com%2Ffeeds%2Fvideos.xml%3Fchannel_id%3DUCT0SMSD60wN8HIKDVbOh9Zw';

const YouTube = () => {
  const [latestVideoId, setLatestVideoId] = useState('');
  const [timeBetween, setTimeBetween] = useState('');
  useEffect(() => {
    fetch(YOU_TUBE_LINK)
      .then((response) => response.json())
      .then((resultData) => {
        const firstVideo = resultData.items[0];
        setLatestVideoId(firstVideo.guid.split(':')[2]);
        setTimeBetween(timeFromDate(firstVideo.pubDate));
      });
  }, []);
  const isMobile = useContext(MobileContext);

  return (
    <div className="youTubeContainer">
      <h2>Schau auf unserem YouTube-Kanal vorbei</h2>
      <div className="youTubeLink">
        <img src={youTubeIcon} alt="You tube icon" />
        <span>Leben mit Neurodermitis</span>
      </div>
      {latestVideoId && (
        <iframe
          className={`frame ${isMobile ? 'iframeAbsolute' : ''}`}
          src={`https://www.youtube.com/embed/${latestVideoId}`}
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowfullscreen></iframe>
      )}
      <div className="timeContainer">
        <span className="text">Aktuelles Video</span>
        <span className="time">{timeBetween}</span>
      </div>
      <p>
        Auf unserem YouTube-Kanal gibt’s Wissenswertes zu Neurodermitis und Du
        erfährst mehr über die alltäglichen Herausforderungen von Betroffenen.
        Wir klären auf, geben Tipps und lassen Betroffene und Experten zu Wort
        kommen.
      </p>
      <Button
        text="Weiter zu YouTube"
        to={'https://www.youtube.com/@lebenmitneurodermitis'}
        type="primary"
      />
    </div>
  );
};

export default YouTube;
