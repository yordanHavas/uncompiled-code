import React from 'react';
import ResponsiveImage from './ResponsiveImage';

let tiltedImage = 0;
const ImageWithText = ({
  children,
  className,
  imgName,
  altImage,
  imageOnRight,
  style,
  noTilt,
  reverseOnDesktop,
  imagestyle,
}) => {
  return (
    <div
      style={style}
      className={`imageWithText ${className ? className : ''} ${
        imageOnRight ? 'reverse' : ''
      } ${reverseOnDesktop ? 'desktopReverse' : ''}`}>
      <div className="image-container">
        <ResponsiveImage
          className={`image image-br ${
            noTilt ? '' : tiltedImage++ % 2 ? 'image-tilt-r' : 'image-tilt'
          }`}
          imgName={`${imgName}`}
          altImg={altImage}
          style={imagestyle}
        />
      </div>
      <div className={`text`}>{children}</div>
    </div>
  );
};

export default ImageWithText;
