import React, { useEffect, useState } from 'react';
import { Col } from 'react-bootstrap';
import instagramIcon from '../assets/icons/instagram.svg';
import { timeFromDate } from '../helpers/helpers';
import Button from './Button';

const INSTAGRAM_LINK =
  'https://graph.instagram.com/me/media?fields=id,media_url,media_type,permalink,timestamp,caption,username,thumbnail_url,children{id,media_url,media_type,thumbnail_url,timestamp}&access_token=IGQVJYdHp0Sk1tSzhVMV9ZAdHRaM3pLRFlpNjBFbmpJOUF6S2g5UlI0TWRKMkp2RVpHSzNWUllXZADBiQzBjdFlKazVvQXNETXM3eGpYMjlQcWoyQzB2eWk1czh0cy1OQjY0dXYwWVN0dmZAQVHA5QW84ZAAZDZD';

const Instagram = () => {
  const [imageLink, setImageLink] = useState('');
  const [timeBetween, setTimeBetween] = useState('');
  useEffect(() => {
    fetch(INSTAGRAM_LINK)
      .then((response) => response.json())
      .then((resultData) => {
        setImageLink(resultData.data[0].media_url);
        setTimeBetween(timeFromDate(resultData.data[0].timestamp));
      });
  }, []);

  return (
    <Col style={{ marginTop: '75px' }} className="socialMediaContainer">
      <h2>Folge uns auf Instagram</h2>
      <div className="socialMediaLink">
        <img src={instagramIcon} alt="Instagram link" />
        <p>@leben_mit_neurodermitis.info</p>
      </div>
      <div className="latestInstagramImage">
        {imageLink && <img src={imageLink} alt="Latest Instagram post" />}
      </div>
      <div className="timeContainer">
        <span className="text">Aktueller Beitrag</span>
        <span className="time">{timeBetween}</span>
      </div>
      <p className="cardText">
        Du hast Neurodermitis oder interessierst Dich dafür, weil eine Dir
        nahestehende Person betroffen ist? Auf unserem Instagram-Kanal berichten
        u. a. unsere Blogger über ihr Leben mit Neurodermitis. Nimm teil,
        tausche Dich aus.
      </p>
      <p className="hashTag">#Leben_mit_Neurodermitis.info</p>
      <Button
        text="Weiter zu Instagram"
        type="primary"
        to={'https://www.instagram.com/leben_mit_neurodermitis.info/'}
      />
    </Col>
  );
};

export default Instagram;
