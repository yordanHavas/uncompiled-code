import React from 'react';
import caregiver from '../assets/icons/caregiver.svg';

export default function TippsBox({ heading, text, className }) {
  return (
    <div className={`tipps-round-box ${className ? className : ''}`}>
      <h3 className="headline">{heading}</h3>
      <p>{text}</p>
      <img src={caregiver} className="tippsImage" />
    </div>
  );
}
