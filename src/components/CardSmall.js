import { Link } from 'gatsby';
import React from 'react';
import ResponsiveImage from './ResponsiveImage';

export default function CardSmall({ elements, className, style }) {
  return (
    <div style={style} className={`card-small ${className ? className : ''}`}>
      {elements.map((element) => {
        const [imageName, altImage, text, link] = element;
        return (
          <Link to={link}>
            <div className="element-container">
              <div className="overlay"></div>
              <ResponsiveImage
                className="cardImage"
                altImg={altImage}
                imgName={imageName}
              />
              <h4>{text}</h4>
            </div>
          </Link>
        );
      })}
    </div>
  );
}
