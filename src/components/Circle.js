import React, { useContext, useEffect, useRef } from 'react';
import { MobileContext } from './Layout';

export default function Circle({ children, variant, className, style }) {
  const isMobile = useContext(MobileContext);
  const container = useRef(null);
  const circle = useRef(null);

  useEffect(() => {
    if (isMobile) {
      setTimeout(() => {
        if (!circle.current) return;
        circle.current.style.width = container.current.scrollHeight + 'px';
      }, 100);
    }
  });

  if (isMobile) {
    return (
      <MobileCircle variant={variant} className={className} style={style}>
        {children}
      </MobileCircle>
    );
  } else {
    return (
      <div className={`circle ${className ? className : ''}`}>{children}</div>
    );
  }

  function MobileCircle({ children, variant, className, style }) {
    return (
      <div
        style={style}
        ref={container}
        className={`circle ${className ? className : ''} ${
          variant ? variant : ''
        }`}>
        <div className="children">{children}</div>

        <div
          ref={circle}
          className={`circle-background ${variant ? variant : ''}`}
        />
      </div>
    );
  }
}
