import React, { useContext, useState } from 'react';
import { Button, Carousel, Stack } from 'react-bootstrap';
import arrowLeft from '../assets/icons/arrow_left.svg';
import arrowRight from '../assets/icons/arrow_right.svg';
import { MobileContext } from './Layout';

import ResponsiveImage from './ResponsiveImage';

const Slider_Desktop = ({
  items,
  special,
  minHeightDesktop,
  imageDesktopHeight,
  className,
}) => {
  const [index, setIndex] = useState(0);
  const generateTextitem = (item) => {
    if (item) {
      const { imgText, imgTextColor } = item;
      return (
        imgText &&
        imgTextColor && (
          <div
            className={`imageTextContainer ${
              imgTextColor === 'blue' && 'blue'
            }`}>
            {imgText}
          </div>
        )
      );
    }
  };

  const onPrevClick = () => {
    if (index <= 0) {
      return;
    }

    setIndex(index - 2);
  };
  const onNextClick = () => {
    if (index >= items.length - 2) {
      return;
    }

    setIndex(index + 2);
  };
  return (
    <div
      className={`slider ${special ? 'special' : ''} ${
        className ? className : ''
      }`}
      style={{ width: 921 }}>
      <Carousel
        interval={null}
        sli
        activeIndex={index}
        indicators={false}
        controls={false}>
        {items.map((item, index) => {
          const nextItem = items[index + 1];
          const imageStyle = { height: imageDesktopHeight };

          return (
            <Carousel.Item
              className="containerClass"
              style={{ textAlign: 'center' }}>
              <div className="desktop-content">
                <div className="desktop-item">
                  {generateTextitem(item)}
                  <ResponsiveImage
                    className="sami"
                    imgName={item.imgName}
                    altImg={item.altImage}
                    style={imageStyle}
                  />
                </div>
                {nextItem && (
                  <div className="desktop-item">
                    {generateTextitem(nextItem)}
                    <ResponsiveImage
                      className="sami"
                      imgName={items[index + 1].imgName}
                      altImg={nextItem.altImage}
                      style={imageStyle}
                    />
                  </div>
                )}
              </div>
            </Carousel.Item>
          );
        })}
      </Carousel>
      <div className="button-container">
        <Button
          variant="primary"
          onClick={onPrevClick}
          style={{
            backgroundColor: 'transparent',
            borderColor: 'transparent',
          }}>
          <img src={arrowLeft} alt="Arrow Left" />
        </Button>
        <Carousel
          style={{ flex: 1 }}
          interval={null}
          activeIndex={index}
          indicators={false}
          controls={false}>
          {items.map((item, index) => {
            const nextelement = items[index + 1];
            return (
              <Carousel.Item className="slider-text">
                <Stack
                  direction="horizontal"
                  className={`${
                    minHeightDesktop ? '' : 'h-100'
                  } justify-content-start align-items-start`}
                  gap={5}>
                  <div
                    style={{ height: minHeightDesktop }}
                    className="text-wrapper align-self-start">
                    {item.subHeading && (
                      <p className="subHeading">{item.subHeading}</p>
                    )}
                    {item.heading && (
                      <h3 className="heading">{item.heading}</h3>
                    )}
                    <p>{item.text}</p>
                  </div>
                  {nextelement && (
                    <div className="text-wrapper">
                      {nextelement.subHeading && (
                        <p className="subHeading">{nextelement.subHeading}</p>
                      )}
                      {nextelement.heading && (
                        <h3 className="heading">{nextelement.heading}</h3>
                      )}
                      <p>{nextelement.text}</p>
                    </div>
                  )}
                </Stack>
              </Carousel.Item>
            );
          })}
        </Carousel>

        <Button
          variant="primary"
          onClick={onNextClick}
          style={{
            backgroundColor: 'transparent',
            borderColor: 'transparent',
          }}>
          <img src={arrowRight} alt="Arrow Right" />
        </Button>
      </div>
      <p className="mt-3 text-center">
        {Math.floor(index / 2) + 1}/{Math.floor((items.length - 1) / 2) + 1}
      </p>
    </div>
  );
};

const Slider = ({
  items,
  special,
  minHeightDesktop,
  imageDesktopHeight,
  className,
}) => {
  const isMobile = useContext(MobileContext);

  if (isMobile) {
    return (
      <Slider_Mobile special={special} className={className} items={items} />
    );
  } else {
    return (
      <Slider_Desktop
        className={className}
        special={special}
        items={items}
        minHeightDesktop={minHeightDesktop}
        imageDesktopHeight={imageDesktopHeight}
      />
    );
  }
};

const Slider_Mobile = ({ items, special, className }) => {
  const [index, setIndex] = useState(0);

  const onPrevClick = () => {
    if (index <= 0) return;

    setIndex(index - 1);
  };
  const onNextClick = () => {
    if (index >= items.length - 1) return;
    setIndex(index + 1);
  };

  return (
    <div
      className={`slider ${special ? 'special' : ''} ${
        className ? className : ''
      }`}
      style={{ width: 335 }}>
      <Carousel
        className="imgContainerRev"
        interval={null}
        activeIndex={index}
        indicators={false}
        controls={false}>
        {items.map((item) => {
          return (
            <Carousel.Item style={{ textAlign: 'center' }}>
              <ResponsiveImage
                className="image"
                imgName={item.imgName}
                altImg={item.altImage}
              />
            </Carousel.Item>
          );
        })}
      </Carousel>
      <div className="button-container">
        <Button
          variant="primary"
          onClick={onPrevClick}
          style={{
            backgroundColor: 'transparent',
            borderColor: 'transparent',
          }}>
          <img src={arrowLeft} alt="Arrow Left" />
        </Button>
        <p>
          {index + 1}/{items.length}
        </p>
        <Button
          variant="primary"
          onClick={onNextClick}
          style={{
            backgroundColor: 'transparent',
            borderColor: 'transparent',
          }}>
          <img src={arrowRight} alt="Arrow Right" />
        </Button>
      </div>
      <Carousel
        interval={null}
        activeIndex={index}
        indicators={false}
        controls={false}
        style={{ width: 335 }}>
        {items.map((item) => {
          return (
            <Carousel.Item className="slider-text">
              {item.subHeading && (
                <p className="subHeading">{item.subHeading}</p>
              )}
              {item.heading && <h3 className="heading">{item.heading}</h3>}
              <p>{item.text}</p>
            </Carousel.Item>
          );
        })}
      </Carousel>
    </div>
  );
};

export default Slider;
