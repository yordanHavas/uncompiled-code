import React, { useContext } from 'react';
import Circle from './Circle';
import { MobileContext } from './Layout';

export default function Bulletlist({
  children,
  textList,
  heading,
  className,
  circleClassName,
  bigHeading,
  style,
  insideAkkordeon,
  firstColumnSize, // For situations when bulletlist doesn't seems equal erganzende-therapien example
}) {
  const isMobile = useContext(MobileContext);
  const textChunks = [];
  let chunkSize =
    textList.length > 5 ? Math.ceil(textList.length / 2) : textList.length;
  if (!firstColumnSize) {
    for (let i = 0; i < textList.length; i += chunkSize) {
      const chunk = textList.slice(i, i + chunkSize);
      textChunks.push(chunk);
    }
  } else {
    const chunk1 = textList.slice(0, firstColumnSize);
    const chunk2 = textList.slice(firstColumnSize, textList.length);
    textChunks.push(chunk1, chunk2);
  }

  const mobileElements = (
    <>
      {bigHeading && <h3 className="mb-3">{bigHeading}</h3>}
      {heading && <h4 className="bullet-list-headline-1">{heading}</h4>}
      <div className={`bullet-list ${className ? className : ''}`}>
        <ul>
          {textList.map((el) => (
            <p>{el}</p>
          ))}
        </ul>
      </div>
      {children}
    </>
  );

  if (isMobile) {
    return insideAkkordeon ? (
      <>{mobileElements}</>
    ) : (
      <Circle className={circleClassName} style={style}>
        {mobileElements}
      </Circle>
    );
  } else {
    return (
      <div
        className={`${
          insideAkkordeon ? 'bullet-list-akkordeon' : 'bullet-list'
        } ${className ? className : ''}`}>
        {bigHeading && <h3 className="mb-5">{bigHeading}</h3>}{' '}
        {/* By design heading must be 18px */}
        {heading && <h4 className="bullet-list-headline-1">{heading}</h4>}
        <div className="lists">
          {textChunks.map((chunk, idx) => {
            {
              if (chunk.length === 0) return;
              return (
                <ul
                  className={`${idx === 0 ? 'first' : ''} ${
                    textList.length <= 4 ? 'w-720' : ''
                  }`}>
                  {chunk.map((el) => {
                    return <p>{el}</p>;
                  })}
                </ul>
              );
            }
          })}
        </div>
        {children && <div className="children">{children}</div>}
      </div>
    );
  }
}
