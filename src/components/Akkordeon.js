import React, { useContext, useState } from 'react';
import Accordion from 'react-bootstrap/Accordion';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import arrow from '../assets/icons/arrow_button.svg';
import { MobileContext } from './Layout';

function CustomToggle({ heading, eventKey, defValue }) {
  const [open, openAccordion] = useState(defValue ? true : false);

  const decoratedOnClick = useAccordionButton(eventKey, () => {
    openAccordion(!open);
  });

  return (
    <div className="akkordeon">
      <h3 style={{ fontWeight: `${open ? '900' : 'bold'}` }}>{heading}</h3>
      <img
        onClick={decoratedOnClick}
        src={arrow}
        className={`${open ? 'arrow-up' : 'arrow-down'}`}
        alt="Arrow Down Icon"
      />
    </div>
  );
}

export default function Akkordeon({
  children,
  heading,
  open,
  className,
  style,
}) {
  const isMobile = useContext(MobileContext);
  return (
    <Accordion
      style={style}
      className={`${className ? className : ''} ${
        isMobile ? 'mobile-width' : 'desktop-width'
      } textAkkordeon`}
      defaultActiveKey={`${open ? '1' : '0'}`}>
      <CustomToggle eventKey="1" heading={heading} defValue={open} />
      <Accordion.Collapse eventKey="1">
        <div className="children-elements">{children}</div>
      </Accordion.Collapse>
    </Accordion>
  );
}
