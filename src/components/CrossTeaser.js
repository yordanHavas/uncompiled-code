import React from 'react';
import DownloadIcon from '../assets/icons/download_icon.svg';
import Button from './Button';
import ResponsiveImage from './ResponsiveImage';

const CrossTeaser = ({
  imgName,
  headText,
  description,
  btnText,
  altImage,
  link,
  customButton,
}) => {
  return (
    <div className="crossTeaser">
      <div className="imgContainer">
        <ResponsiveImage imgName={imgName} altImg={altImage} />
      </div>
      <h3 style={{ fontSize: '23px' }}>{headText}</h3>
      <p>{description}</p>
      {customButton ? (
        customButton
      ) : (
        <Button to={link} text={btnText} type="primary" img={DownloadIcon} />
      )}
    </div>
  );
};

export default CrossTeaser;
