import React, { useState } from 'react';
import activeTextIcon from '../assets/icons/activeTextIcon.svg';
import textIcon from '../assets/icons/textIcon.svg';
import annaImage from '../assets/images/anna.png';
import karinImage from '../assets/images/karin.png';
import claudiaImage from '../assets/images/petra.png';

const input = [
  {
    name: 'KARIN',
    img: karinImage,
    altImg:
      'Fotos der drei Mitarbeiterinnen Karin, Claudia und Anna beim Neurodermitis-Begleiter-Team',
    text: 'Hallo, ich bin Karin, von Beruf Krankenschwester. Ich liebe den Umgang mit Menschen und unternehme privat gerne entspannende Streifzüge durch die Natur. Stressreduktion ist ein wichtiges Thema und ich berate betroﬀene Neurodermitis-Patienten und deren Angehörige gerne auch zu diesem Schwerpunktthema.',
  },
  {
    name: 'Claudia',
    img: claudiaImage,
    altImg:
      'Fotos der drei Mitarbeiterinnen Karin, Claudia und Anna beim Neurodermitis-Begleiter-Team',
    text: 'Mein Name ist Claudia und ich bin seit fast 2 Jahren im Neurodermitis Team als Krankenschwester tätig. Ich berate Neurodermitis betroffene Erwachsene sowie auch Eltern von betroffenen Kindern und habe viel Freude daran, eine Unterstützung für verschiedenen Themen zu sein. In meiner Freizeit bewege ich mich sehr gern in der Natur und erfreue mich auch an den kleinen Dingen im Leben.',
  },
  {
    name: 'ANNA',
    img: annaImage,
    altImg:
      'Fotos der drei Mitarbeiterinnen Karin, Claudia und Anna beim Neurodermitis-Begleiter-Team',
    text: [
      'Hallo, ich bin Anna und seit',
      <br />,
      '4 ',
      'Jahren Ansprechpartnerin für Neurodermitis-Betroffene und auch für deren Angehörige. Zu meinen größten Leidenschaften zählt definitiv das Reisen. Es macht mir großen Spaß, neue Länder, Kulturen und Menschen kennenzulernen.',
    ],
  },
];

export default function ClickableImage({ style }) {
  const [textIndex, setTextIndex] = useState(2);
  const arrowPosition = [40, 196, 21];

  return (
    <div style={style} className="clickable-image">
      {input.map((person, index) => {
        const { name, img, altImg, text } = person;
        return (
          <div className="elementContainer">
            <div
              onClick={() => {
                setTextIndex(index);
              }}>
              <img alt={altImg} src={img} />
            </div>
            <div className="nameContainer">
              <img src={textIndex === index ? activeTextIcon : textIcon} />
              <span className={`${textIndex === index ? 'blue' : 'yellow'}`}>
                {name}
              </span>
            </div>
          </div>
        );
      })}

      <div className="textBox">
        <div
          style={{ left: `${arrowPosition[textIndex]}px` }}
          class="arrow-up"
        />
        <p>{input[textIndex].text}</p>
      </div>
    </div>
  );
}
