import React from 'react';
import calendar from '../assets/icons/InfoCalendar.svg';
import laptop from '../assets/icons/InfoLaptop.svg';
import phone from '../assets/icons/InfoPhone.svg';

const input = [
  { image: phone, header: 'Telefon', text: '0800 40 500 20' },
  {
    image: laptop,
    header: 'E-Mail',
    text: (
      <a
        className="contactText"
        href="mailto:service@meinneurodermitisbegleiter.de">
        service@meinneuro dermitisbegleiter.de
      </a>
    ),
  },
  {
    image: calendar,
    header: 'Wir sind für Dich da',
    text: ['Montag bis Freitag', <br />, 'von 8:00 bis 18:00 Uhr'],
  },
];

const InfoCircle = ({ style }) => {
  return (
    <div style={style} className="infoCircle">
      <div className="infoConainer">
        {input.map((item) => {
          const { header, image, text } = item;
          return (
            <div className="item">
              <div>
                <img src={image} />
              </div>
              <div className="textContainer">
                <p className="header">{header}</p>
                <p className="text">{text}</p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default InfoCircle;
