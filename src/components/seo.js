import React from 'react';
import { useSiteMetadata } from '../helpers/use-site-metadata';

export const SEO = ({
  title,
  description,
  trustKeyWords,
  children,
  metaTitle,
}) => {
  const {
    title: defaultTitle,
    description: defaultDescription,
    metaTitle: defaultMetaTitle,
  } = useSiteMetadata();

  const seo = {
    title: title || defaultTitle,
    description: description || defaultDescription,
    metaTitle: metaTitle || defaultMetaTitle,
    trustKeyWords,
  };

  return (
    <>
      <title>{seo.title}</title>
      <meta name="description" content={seo.description} />
      <meta name="title" content={seo.metaTitle} />
      {seo.trustKeyWords && <meta name="keywords" content={trustKeyWords} />}
      {children}
    </>
  );
};
