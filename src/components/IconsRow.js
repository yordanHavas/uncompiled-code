import { Link } from 'gatsby';
import React from 'react';
import instagramIcon from '../assets/icons/instaSocialIcon.svg';
import podcastIcon from '../assets/icons/podcastSocialIcon.svg';
import youTubeIcon from '../assets/icons/youTubeSocialIcon.svg';

const IconsRow = ({ style, className }) => {
  return (
    <div
      className={`iconsRow ${className ? className : ''}`}
      style={{
        display: 'flex',
        justifyContent: 'space-around',
        ...style,
      }}>
      <a href="https://www.instagram.com/leben_mit_neurodermitis.info/">
        <img style={{ cursor: 'pointer' }} src={instagramIcon} />
      </a>
      <a href="https://www.youtube.com/@lebenmitneurodermitis/featured">
        <img src={youTubeIcon} />
      </a>
      <a href="https://lebenmitneurodermitis.podigee.io/">
        <img src={podcastIcon} />
      </a>
    </div>
  );
};

export default IconsRow;
