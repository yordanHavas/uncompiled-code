import React from 'react';

import ResponsiveImage from './ResponsiveImage';

const TiltedImage = ({ imgName, altImg }) => {
  return (
    <ResponsiveImage
      imgName={imgName}
      altImg={altImg}
      className="tiltedImage"></ResponsiveImage>
  );
};

export default TiltedImage;
