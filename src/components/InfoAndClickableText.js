import React, { useContext } from 'react';
import ClickableImage from './ClickableImage';
import InfoCircle from './InfoCircle';
import { MobileContext } from './Layout';

const InfoAndClickableText = ({ style }) => {
  const isMobile = useContext(MobileContext);
  const allComponents = (
    <>
      <InfoCircle style={{ marginTop: `${isMobile ? '40px' : '131px'}` }} />
      <ClickableImage style={{ marginTop: `${isMobile ? '-80px' : '0px'}` }} />
    </>
  );

  return (
    <>
      {isMobile ? (
        allComponents
      ) : (
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-around',
            width: '100%',
            ...style,
          }}>
          {allComponents}
        </div>
      )}
    </>
  );
};

export default InfoAndClickableText;
