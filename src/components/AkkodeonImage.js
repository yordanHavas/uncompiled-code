import { Link } from 'gatsby';
import React, { useState } from 'react';
import Accordion from 'react-bootstrap/Accordion';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import arrow_down from '../assets/icons/arrow_down_small.png';
import arrow_up from '../assets/icons/arrow_down_small_rotated_down.png';
import caret from '../assets/icons/caret-right.svg';
import ResponsiveImage from './ResponsiveImage';

function CustomToggle({
  altTag,
  header,
  imgName,
  eventKey,
  violet,
  imgWidth,
  imgHeight,
}) {
  const [open, openAccordion] = useState(false);

  const decoratedOnClick = useAccordionButton(eventKey, () => {
    openAccordion(!open);
  });

  const imageCombinedStyle =
    imgHeight && imgWidth ? { width: imgWidth, height: imgHeight } : {};

  return (
    <div className="text-center akkordeon-image mb-3">
      <div
        className={`image-akkordeon-container ${violet ? 'violet' : 'white'}`}>
        <div className="col-offset-2 col-10 ">
          <ResponsiveImage
            altImg={altTag}
            imgName={imgName}
            style={imageCombinedStyle}
          />
        </div>
      </div>
      <div className="image-accordeon-button">
        {/* <Button
          className="rounded-circle button"
          onClick={decoratedOnClick}
          style={{
            backgroundColor: 'transparent',
            padding: '0px',
            width: '30px',
          }}> */}
        <img
          style={{ width: '16px' }}
          src={open ? arrow_down : arrow_up}
          alt="Arrow Down Icon"
          onClick={decoratedOnClick}
        />
        {/* </Button> */}
      </div>
      <div className="accordeon-header">
        <div className="col-12 text-start">
          <h4 style={{ fontSize: '24px' }}>{header}</h4>
        </div>
      </div>
    </div>
  );
}

export default function AkkordeonImage({
  altTag,
  children,
  header,
  imgName,
  link,
  text,
  violet,
  buttonText,
  imgWidth,
  imgHeight,
}) {
  return (
    <div className="accordion-wrapper">
      <Accordion style={{ marginBottom: '2em' }} defaultActiveKey="0">
        <CustomToggle
          altTag={altTag}
          header={header}
          imgName={imgName}
          violet={violet}
          eventKey="0"
          imgWidth={imgWidth}
          imgHeight={imgHeight}
        />
        <Accordion.Collapse
          className="akkordeon-image-hidden-text"
          eventKey="0">
          <div className="akkordeon-image-hidden-text">
            {text?.map((el, index) => {
              if (typeof el == 'string') {
                return (
                  <>
                    <p key={index}>{el}</p>
                    <br></br>
                  </>
                );
              } else {
                return (
                  <>
                    <p>
                      {el.previousText}
                      <a href={el.link}>{el.website}</a>
                      {el.followingText}
                    </p>
                    <br></br>
                  </>
                );
              }
            })}
          </div>
        </Accordion.Collapse>
      </Accordion>
      <div style={{ marginTop: 'auto' }}>
        <Link to={link} style={{ textDecoration: 'none' }}>
          <button
            style={{ height: '51px' }}
            className="tertiary-button rounded-pill text-center button-under-akkordeon">
            <p>{buttonText}</p>
            <img src={caret} />
          </button>
        </Link>
      </div>
    </div>
  );
}
