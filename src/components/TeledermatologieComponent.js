import React, { useContext } from 'react';
import caret from '../assets/icons/caret-right.svg';
import { MobileContext } from './Layout';
import ResponsiveImage from './ResponsiveImage';

export default function TeledermatologieComponent({ items }) {
  const isMobile = useContext(MobileContext);

  return isMobile ? (
    <TeledermatologieMobile items={items} />
  ) : (
    <TeledermatologieDesktop items={items} />
  );
}
const TeledermatologieDesktop = ({ items }) => {
  return (
    <>
      <div className="teledermatologieComponent">
        <div className="havas-container dark">
          <div className="grid-desktop">
            {items.map((item, index) => {
              const {
                img,
                altImage,
                width,
                height,
                header,
                headerText,
                text,
                buttonLink,
                buttonText,
              } = item;
              let isLargeHeader = false;
              if (header.length >= 25) {
                isLargeHeader = true;
              }
              return (
                <>
                  <div className="itemContainer">
                    <div className="imageContainer">
                      <ResponsiveImage
                        style={{ width, height }}
                        imgName={img}
                        altImg={altImage}
                      />
                    </div>
                    <h3
                      className={`${
                        isLargeHeader ? 'high-header' : 'low-header'
                      }`}
                      style={{
                        marginTop: '40px',
                        textAlign: 'left',
                      }}>
                      {header}
                    </h3>
                    <p
                      className="teledermatologie-first-paragraph"
                      style={{
                        marginTop: '20px',
                        marginBottom: `${text ? '0px' : '40px'}`,
                      }}>
                      {headerText}
                    </p>
                    {text && (
                      <>
                        <h3 style={{ marginTop: '40px' }} className="und">
                          Und so Geht’s
                        </h3>
                        <p
                          className="teledermatologie-second-paragraph"
                          style={{ marginTop: '12px', marginBottom: '40px' }}>
                          {text}
                        </p>
                      </>
                    )}
                    <div style={{ marginTop: 'auto' }} className="btnContainer">
                      <a href={buttonLink} className="button-anchor">
                        <button className={'primary-button'}>
                          {buttonText}
                          <img src={caret} alt="" />
                        </button>
                      </a>
                    </div>
                  </div>
                </>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

const TeledermatologieMobile = ({ items }) => {
  return (
    <>
      <div className="teledermatologieComponent">
        {items.map((item, index) => {
          const {
            img,
            altImage,
            width,
            height,
            header,
            headerText,
            text,
            buttonLink,
            buttonText,
          } = item;
          return (
            <>
              <div
                className={`havas-container ${index % 2 ? 'light' : 'dark'}`}>
                <div className="imageContainer">
                  <ResponsiveImage
                    style={{ width, height }}
                    imgName={img}
                    altImg={altImage}
                  />
                </div>
                <h3 style={{ marginTop: '40px' }}>{header}</h3>
                <p style={{ marginTop: '20px' }}>{headerText}</p>
                {text && (
                  <>
                    <h3 style={{ marginTop: '40px' }} className="und">
                      Und so Geht’s
                    </h3>
                    <p style={{ marginTop: '12px' }}>{text}</p>
                  </>
                )}
                <div style={{ marginTop: '40px' }} className="btnContainer">
                  <a href={buttonLink} className="button-anchor">
                    <button className={'primary-button'}>
                      {buttonText}
                      <img src={caret} alt="" />
                    </button>
                  </a>
                </div>
              </div>
            </>
          );
        })}
      </div>
    </>
  );
};
