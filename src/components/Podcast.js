import loadScripts from 'load-scripts';
import React, { useContext, useEffect } from 'react';
import config from '../assets/config/podcast.js';
import micIcon from '../assets/icons/microphone-svgrepo-com.svg';
import { MobileContext } from '../components/Layout';
const iframeLinks = [
  'https://lebenmitneurodermitis.podigee.io/7-episode-neurodermitis-auf-reha-eine-familie-ein-gemeinsames-ziel/embed?context=website',
  'https://lebenmitneurodermitis.podigee.io/9-new-episode/embed?context=website',
  'https://lebenmitneurodermitis.podigee.io/8-episode-neurodermitis-mit-der-richtigen-ernahrung-zur-besseren-haut/embed?context=website',
  'https://lebenmitneurodermitis.podigee.io/6-episode-typ-2-entzundung/embed?context=website',
  'https://lebenmitneurodermitis.podigee.io/5-episode-behandlungsmoeglichkeiten/embed?context=website',
  'https://lebenmitneurodermitis.podigee.io/3-mein-leben-mit-neurodermitis/embed?context=website',
  'https://lebenmitneurodermitis.podigee.io/t1-neue-episode/embed?context=website',
  'https://lebenmitneurodermitis.podigee.io/2-das-neurodermitis-abc/embed?context=website',
];

export default function Podcast() {
  // const script = document.createElement('script');
  // const script2 = document.createElement('script')

  // script.src = 'https://cdn.podlove.org/web-player/5.x/embed.js';
  // script2.innerText =
  //   "podlovePlayer('#example','https://backend.podlovers.org/wp-json/podlove-web-player/shortcode/publisher/60','https://backend.podlovers.org/wp-json/podlove-web-player/shortcode/config/podlovers/theme/default');";

  // document.body.appendChild(script);
  // document.body.appendChild(script2)

  const isMobile = useContext(MobileContext);

  const fetchPodcast = async () => {
    return await loadScripts(
      'https://cdn.podlove.org/web-player/5.x/embed.js'
    ).then(() => {
      if (typeof window.podlovePlayer !== 'undefined') {
        window.podlovePlayer(
          '#podcast-frame',
          'https://backend.podlovers.org/wp-json/podlove-web-player/shortcode/publisher/60',
          config
        );
      }
    });
  };

  useEffect(() => {
    fetchPodcast();
  });
  return (
    <div className="podcast">
      <h2>HÖR IN UNSEREN PODCAST REIN</h2>
      <div>
        <div className="sub-header">
          <img src={micIcon} alt="" />
          <p style={{ fontSize: '16px' }}>
            Leben mit Neurodermitis {isMobile && <br /> /*Feedback*/} - der
            Hautnah Podcast
          </p>
        </div>
        <p className="mb-4">
          In unserer Podcast-Reihe „Leben mit Neurodermitis – der
          Hautnah-Podcast“ beantworten Moderatorin Alissa Stein und Co-Moderator
          und Dermatologe Dr. Max Tischler zahlreiche Fragen zu Neurodermitis,
          räumen mit Mythen auf und geben viele hilfreiche Alltagstipps. Neben
          Betroffenen sind auch regelmäßig Experten zu Gast. Es lohnt sich also
          reinzuhören!
        </p>
        {
          <div className="iframeContent">
            {iframeLinks.map((item) => {
              return <iframe className="singlePod" src={item}></iframe>;
            })}
          </div>
        }
      </div>
    </div>
  );
}
