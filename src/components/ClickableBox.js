import React, { useContext, useState } from 'react';
import { Row } from 'react-bootstrap';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import AccordionContext from 'react-bootstrap/AccordionContext';
import Button from 'react-bootstrap/Button';
import clickableBoxIcon from '../assets/icons/clickable_box_icon.svg';
import minus from '../assets/icons/minus.svg';
import plus from '../assets/icons/plus.svg';
import { MobileContext } from './Layout';

export default function ClickableBox({
  children,
  title,
  header,
  headerOpen,
  className,
}) {
  const isMobile = useContext(MobileContext);

  if (isMobile) {
    return (
      <MobileBox
        className={className}
        title={title}
        header={header}
        headerOpen={headerOpen}>
        {children}
      </MobileBox>
    );
  } else {
    return (
      <DesktopBox
        className={className}
        title={title}
        header={header}
        headerOpen={headerOpen}>
        {children}
      </DesktopBox>
    );
  }
}

function MobileBox({ children, title, header, headerOpen, className }) {
  const isMobile = useContext(MobileContext);

  let eventKey = '0';
  const { activeEventKey } = useContext(AccordionContext);

  const [accordionOpen, setMenuOpen] = useState(!isMobile);

  function toggleMenu() {
    setMenuOpen(!accordionOpen);
  }

  const decoratedOnClick = useAccordionButton(eventKey, () => {
    toggleMenu();
  });
  return (
    <div
      className={`border-0 box-container ${className ? className : ''}`}
      style={
        accordionOpen
          ? { maxWidth: 320 }
          : { marginLeft: '4em', marginBottom: 40 }
      }>
      <div eventKey="0">
        {!accordionOpen && (
          <div style={{ height: '17.5em' }}>
            <div className="rounded-circle round-box text-center">
              <div
                className="headline m-2 mt-4"
                style={{
                  top: '25px',
                  color: 'white',
                  textTransform: 'uppercase',
                  fontSize: '24px',
                }}>
                {title}
              </div>
              <h4
                className="m-0 mt-2"
                style={{
                  color: 'white',
                  fontWeight: '900',
                }}>
                {header}
              </h4>
              <Button
                eventKey="0"
                className="rounded-circle button mt-2"
                onClick={decoratedOnClick}
                style={{
                  backgroundColor: 'white',
                  borderColor: 'white',
                  width: '48px',
                  height: '48px',
                  zIndex: '1',
                }}>
                <img src={plus} alt="Plus Icon" />
              </Button>
              <img alt="" src={clickableBoxIcon} className="m-0 image" />
            </div>
          </div>
        )}
        {accordionOpen && (
          <div className="top-accordion text-center" style={{ color: 'white' }}>
            <div className="m-0 mt-5">
              <h4
                style={{
                  textAlign: 'start',
                  color: 'white',
                  fontWeight: '900',
                }}>
                {headerOpen ? headerOpen : header}
              </h4>
            </div>
          </div>
        )}
      </div>
      {accordionOpen && (
        <div>
          <div className="text-holder">{children}</div>
          <div>
            <div className="bottom-accordion text-center">
              <Row className="justify-content-center">
                <Button
                  eventKey="0"
                  className="rounded-circle button"
                  onClick={decoratedOnClick}
                  style={{
                    backgroundColor: 'white',
                    borderColor: 'white',
                    width: '48px',
                    height: '48px',
                  }}>
                  <img src={minus} alt="Minus Icon" />
                </Button>
              </Row>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

function DesktopBox({ children, title, header, headerOpen, className }) {
  return (
    <>
      <div
        className={`border-0 desktop-box text-start ${
          className ? className : ''
        }`}>
        <p
          className="headline"
          style={{ textTransform: 'uppercase', fontSize: '25px' }}>
          {title}
        </p>
        {(header || headerOpen) && <h4>{headerOpen ? headerOpen : header}</h4>}
        <div className="text-holder">{children}</div>
        <img alt="" src={clickableBoxIcon} className="m-0 image" />
      </div>
    </>
  );
}
