import React from 'react';

import ResponsiveImage from './ResponsiveImage';

export default function CardMedium({
  image,
  title,
  text,
  style,
  altImg,
  className,
}) {
  return (
    <div className={`card medium ${className ? className : ''}`} style={style}>
      <div className="card-img-top-container">
        <ResponsiveImage
          className="card-img-top"
          imgName={image}
          altImg={altImg}
        />
      </div>
      <div className="card-body" style={{ justifyContent: 'flex-start' }}>
        <h3 className="card-title">{title}</h3>
        <p className="card-text">{text}</p>
      </div>
    </div>
  );
}
