import { graphql, useStaticQuery } from 'gatsby';
import GoogleMapReact from 'google-map-react-concurrent';
import React, { useContext, useEffect, useState } from 'react';
import active_maps_icon from '../assets/icons/active_maps.svg';
import button_list from '../assets/icons/button_list.svg';
import close_button_contact from '../assets/icons/close_button_contact.svg';
import doktor_kart from '../assets/icons/doktor_kart.svg';
import input_search from '../assets/icons/input_search.svg';
import input_target from '../assets/icons/input_target.svg';
import mapsPoin from '../assets/icons/maps_point.svg';
import minus_button from '../assets/icons/minus_button_icon.svg';
import plus_button from '../assets/icons/plus_button_icon.svg';
import onlineDoktorLogo from '../assets/images/OnlineDoctor-Logo-DE-RGB-Original.png';
import { MobileContext } from '../components/Layout';
import Button from './Button';
import { getGeocode, getLatLng } from 'use-places-autocomplete';
import { CalcDistanceBtwTwoCord, SortOriginalList } from './../helpers/helpers';
import Popup from './Popup';

export default function GoogleMap() {
  const isMobile = useContext(MobileContext);
  const [mapProps, setMap] = useState({
    center: {
      lat: 51.4556432,
      lng: 7.0115552,
    },
    zoom: 10,
  });

  const changeUrl = (slug) => {
    const nextURL = `/aerztefinder${slug ? '/#' + slug : slug}`;
    window.history.pushState({}, '', nextURL);
    window.history.replaceState({}, '', nextURL);
  };

  function onClick(obi) {
    const { lat, lng } = obi;
    setMap((previousState) => {
      return {
        zoom: previousState.zoom,
        center: {
          ...previousState.center,
          lat: lat,
          lng: lng,
        },
      };
    });
  }

  const [listOpen, setListOpen] = useState(false);

  const data = useStaticQuery(graphql`
    query {
      allDoctorlistCsv {
        nodes {
          id
          Column1
          Anrede
          Forename
          Surname
          Slug
          PLZ
          City
          Street
          Breitengrad
          Longengrad
          Profile_URL
          Image_Link
        }
      }
    }
  `);
  const originalList = data ? data.allDoctorlistCsv.nodes : [];
  const [searchValue, setSearchValue] = useState('');
  const [filteredUsers, setFilteredUsers] = useState(originalList);
  const [listUsers, setListUsers] = useState(originalList);
  const [contactOpen, setContactOpen] = useState(false);
  const [activeNode, setActiveNode] = useState({});

  function onTableClick(node, listOpen) {
    const lat = node.Breitengrad;
    const lng = node.Longengrad;
    const state = {
      center: {
        lat: parseFloat(lat - 0.007),
        lng: parseFloat(lng),
      },
      zoom: 15,
    };
    setMap((prev) => ({
      ...prev,
      center: {
        lat: parseFloat(lat - 0.007),
        lng: parseFloat(lng),
      },
      zoom: 15,
    }));
    setListOpen(listOpen);
    setActiveNode(node);
    const nextUsers = filteredUsers.map((item) => {
      if (item.id === node.id) {
        return { ...item, activeIcon: true };
      } else {
        return { ...item, activeIcon: false };
      }
    });
    setContactOpen(true);
    setFilteredUsers(nextUsers);
  }

  useEffect(() => {
    const lowerCaseSearchValue = searchValue.toLowerCase();
    // setFilteredUsers(
    //   originalList.filter((doctor) => {
    //     return (
    //       doctor.PLZ.toLowerCase().startsWith(lowerCaseSearchValue) ||
    //       doctor.City.toLowerCase().startsWith(lowerCaseSearchValue)
    //     );
    //   })
    // );
    setListUsers(
      originalList.filter((doctor) => {
        return (
          doctor.PLZ.toLowerCase().startsWith(lowerCaseSearchValue) ||
          doctor.City.toLowerCase().startsWith(lowerCaseSearchValue)
        );
      })
    );
  }, [searchValue]);

  if (!data) {
    return;
  } else {
    return (
      <div className="google-map-doctor-list-wrapper">
        <div
          style={{ marginTop: `${isMobile ? '30px' : '0'}` }}
          className="inputField">
          <img src={input_search} />
          <input
            type={'text'}
            placeholder="Stadt oder PLZ suchen"
            onKeyDown={async (event) => {
              if (event.key === 'Enter') {
                try {
                  const results = await getGeocode({
                    address: searchValue + ' germany',
                  });
                  const { lat, lng } = await getLatLng(results[0]);

                  if (listUsers.length === 0) {
                    const sortedUsers = SortOriginalList(
                      originalList,
                      lat,
                      lng
                    );
                    setListUsers(sortedUsers);
                  }

                  setMap((prev) => ({
                    ...prev,
                    center: {
                      lat,
                      lng,
                    },
                    zoom: 12,
                  }));
                } catch (error) {}
              }
            }}
            onChange={async (e) => {
              setSearchValue(e.target.value);
            }}
            value={searchValue}
          />
          <img src={input_target} />
        </div>
        <div style={{ marginBottom: '694px' }}>
          <div className="google-map centerAbsoluteDiv">
            <div className="zoomButtonsContainer">
              <div
                className="zoomButton plus"
                onClick={() => {
                  setMap((prev) => ({ ...prev, zoom: prev.zoom + 1 }));
                }}>
                <img src={plus_button} />
              </div>
              <div
                className="zoomButton minus"
                onClick={() => {
                  setMap((prev) => ({ ...prev, zoom: prev.zoom - 1 }));
                }}>
                <img src={minus_button} />
              </div>
            </div>
            <div
              className="listButtonContainer"
              onClick={() => setListOpen((prev) => !prev)}>
              <div className="listButton">
                <img src={button_list} />
                <span>Liste anzeigen</span>
              </div>
            </div>
            <div
              style={{ display: contactOpen ? 'flex' : 'none' }}
              className="contactInfoContainer">
              <div
                onClick={() => {
                  setContactOpen(false);
                  changeUrl('');
                  const nextUsers = filteredUsers.map((item) => {
                    if (item.id === activeNode.id) {
                      return { ...item, activeIcon: false };
                    } else {
                      return item;
                    }
                  });
                  setFilteredUsers(nextUsers);
                }}
                className="closeContactButton">
                <img src={close_button_contact} />
              </div>
              <img className="profileImage" src={activeNode.Image_Link}></img>
              <h4>
                Dr. {activeNode.Forename} {activeNode.Surname}
              </h4>
              <img className="doktorOnlineImage" src={onlineDoktorLogo}></img>
              <p style={{ marginBottom: '46px' }}>
                {activeNode.Street}
                <br />
                {activeNode.PLZ} {activeNode.City}
              </p>
              <p style={{ marginBottom: '32px' }}>
                Auf OnlineDoctor erhältst du schnelle fachärztliche Hilfe von
                Hautärztinnen und Hautärzten in deiner Nähe
              </p>
              <Button
                linkType={'a'}
                type="primary"
                text="Anfrage starten"
                to={activeNode.Profile_URL}
              />
            </div>
            <div
              style={{ display: listOpen ? 'flex' : 'none' }}
              className="doctor-list-container">
              <div className="doctor-list">
                {listUsers.map((node) => {
                  return (
                    <div className="doctor-container">
                      <h4>
                        Dr. {node.Forename} {node.Surname}
                      </h4>
                      <p style={{ marginBottom: '20px' }}>
                        {node.Street}
                        <br />
                        {node.PLZ} {node.City}
                      </p>
                      <Button
                        onClick={() => {
                          onTableClick(node, false);
                          changeUrl(node.Slug);
                        }}
                        text="Mehr Erfahren"
                        type="tertiary"
                      />
                      <div className="border_bottom"></div>
                    </div>
                  );
                })}
              </div>
              <div class="line-1"></div>
              <div
                className="listButtonContainer"
                style={{ position: 'initial' }}
                onClick={() => setListOpen((prev) => !prev)}>
                <div
                  style={{ width: '100%', padding: '30px' }}
                  className="listButton">
                  <img src={doktor_kart} />
                  <span>Karte anzeigen</span>
                </div>
              </div>
            </div>
            <GoogleMapReact
              options={() => ({
                zoomControl: false,
                fullscreenControl: false,
              })}
              yesIWantToUseGoogleMapApiInternals={true}
              onDragEnd={(e) => {
                const str = e.rmiUrl;
                if (e.rmiUrl) {
                  let completedString = '';
                  let comaCount = 0;
                  for (var i = str.indexOf('@') + 1; i < str.length; i++) {
                    if (str[i] === ',') {
                      comaCount++;
                      if (comaCount === 2) break;
                    }
                    completedString += str[i];
                  }

                  const [lat, lng] = completedString.split(',');
                  onClick({ lat, lng });
                }
              }}
              bootstrapURLKeys={{
                key: 'AIzaSyCkmLTWU-W_9fVMZSQSXaVz3LlpBG7U0cI',
              }}
              defaultCenter={mapProps.center}
              center={mapProps.center}
              zoom={mapProps.zoom}
              onZoomAnimationEnd={(e) => {
                setMap((prev) => ({ ...prev, zoom: e }));
              }}
              defaultZoom={mapProps.zoom}
              hoverDistance={20}>
              {filteredUsers.map((node, index) => {
                return (
                  <img
                    onClick={() => {
                      onTableClick(node, false);
                      changeUrl(node.Slug);
                    }}
                    lat={node.Breitengrad}
                    lng={node.Longengrad}
                    className={`google-map-icon ${
                      node.activeIcon ? 'active' : ''
                    }`}
                    src={node.activeIcon ? active_maps_icon : mapsPoin}
                    alt="Google Map Icon"
                  />
                );
              })}
            </GoogleMapReact>
          </div>
        </div>
        <Popup />
      </div>
    );
  }
}
