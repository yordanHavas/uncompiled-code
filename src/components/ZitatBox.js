import React, { useContext } from 'react';
import desktopIcon from '../assets/images/zitat_box_img_desktop.png';
import mobileIcon from '../assets/images/zitat_box_img_mobile.png';
import Circle from './Circle';
import { MobileContext } from './Layout';

//Microphone Image

export default function ZitatBox({ text, style, isLoudspeaker }) {
  const isMobile = useContext(MobileContext);
  return (
    <div className="zitat-box">
      <Circle className="zitat-box">
        <p>{text}</p>
      </Circle>
      <div className="image">
        <img src={isLoudspeaker ? desktopIcon : mobileIcon} alt="" />
      </div>
    </div>
  );
}
