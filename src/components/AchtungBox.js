import React, { useContext } from 'react';
import Circle from './Circle';
import { MobileContext } from './Layout';
import ResponsiveImage from './ResponsiveImage';

export default function AchtungBox({
  className,
  heading,
  text,
  iconName,
  altImg,
}) {
  const isMobile = useContext(MobileContext);
  if (isMobile) {
    return (
      <div className={`achtung-box`}>
        <h3 className="achtung">ACHTUNG</h3>
        <ResponsiveImage
          className="image"
          style={{ width: 79, height: 87 }}
          imgName={iconName}
          altImg={altImg}
        />
        <Circle
          className={`${heading ? '' : 'no-heading'}`}
          variant="purple"
          imgName={iconName}>
          <h3>{heading}</h3>
          <p>{text}</p>
        </Circle>
      </div>
    );
  } else {
    return (
      <div className={`achtung-box`}>
        <div className="achtung-headline">
          <h3 className="achtung">ACHTUNG</h3>
        </div>
        <ResponsiveImage className="image" imgName={iconName} altImg={altImg} />
        <div className="border-0 desktop-achtung text-start">
          <h4>{heading}</h4>
          <p>{text}</p>
        </div>
      </div>
    );
  }
}
