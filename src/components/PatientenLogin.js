import React, { useContext, useState } from 'react';
import loginIcon from '../assets/icons/Login-Icon.svg';
import { MobileContext } from './Layout';
import popUpIconClose from '../assets/icons/popUpIconClose.svg';
import loginBulb from '../assets/images/loginBulb.png';
import Button from './Button';

const PatientenLogin = () => {
  const isMobile = useContext(MobileContext);
  const [open, setOpen] = useState(false);
  const [buttonEnabled, setButtonEnabled] = useState(false);
  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  function validateNumberInput(numberInput) {
    var chargeNumber = numberInput.toUpperCase();
    var filter = new RegExp('^[0-9]{1}[A-Za-z]{1}[A-Za-z0-9]{3}[A-Za-z]{1}$');

    var chargeNumberList = [
      '9L455F',
      '9L003K',
      '9L816H',
      '0L127K',
      '0L127K',
      '0L342C',
      '0L741C',
      '1LU01F',
      '9L748K',
      '0L440C',
      '0L132W',
      '0L254C',
      '0L254M',
      '9L175C',
      '9L806F',
      '0L440J',
      '0L330A',
      '0L440J',
      '0L440Z',
      '9L426V',
      '9L352A',
      '9L352A',
      '9L003H',
      '0L002F',
      '0LU05H',
      '0LU16A',
      '0L342H',
      '0L419A',
      '0L419C',
      '0L555C',
      '0L496C',
      '0L218H',
      '1L137A',
      '9F004A',
      '0F010A',
      '0F010B',
      '0F030A',
      '0F030B',
      '1F068A',
      '1F068B',
      '9F007A',
      '9F007A',
      '0F015A',
      '0F016A',
      '0F027A',
      '0F033A',
      '0F033B',
      '1F055A',
      '1F058A',
      '1F069A',
      '1F087A',
      '9F008A',
      '0F015A',
      '0F022A',
      '0F016A',
      '0F022B',
      '0F027A',
      '0F033A',
      '1F056A',
      '1F055A',
      '1F057A',
      '1F067A',
      '1F069A',
      '1F087A',
    ];

    if (!(chargeNumber == '')) {
      if (
        chargeNumberList.includes(chargeNumber) ||
        chargeNumberList.indexOf(chargeNumber) !== -1 ||
        filter.test(chargeNumber)
      ) {
        // To Enable Submit Button
        setButtonEnabled(true);
        return;
      }
    }

    setButtonEnabled(false);
  }

  const mobileText = (
    <>
      <hr className="main-separator"></hr>
      <p
        onClick={handleClick}
        style={{ cursor: 'pointer' }}
        className="menu-link">
        Patienten-Login
      </p>
    </>
  );

  const desktopText = (
    <div onClick={handleClick} className="patient-login-button-container">
      <img src={loginIcon} alt="" />
      <p className="quote">Patienten-Login</p>
    </div>
  );

  return (
    <div className="patient-login">
      {isMobile ? mobileText : desktopText}

      <div className={`popUpContainer ${open ? '' : 'hidden'}`}>
        <h2 className="headerLogin">
          ZUGANG FÜR <br /> PATIENT/-INNEN
        </h2>
        <p className="textLogin">Chargennummer (Ch.-B.) auf der Packung:</p>
        <img
          onClick={handleClick}
          className="patient-icon-close"
          src={popUpIconClose}></img>

        <input
          placeholder="Nummer eintragen"
          onChange={(e) => {
            validateNumberInput(e.target.value);
          }}
        />
        <Button
          type="primary"
          text="Patienten Login"
          style={buttonEnabled ? {} : { opacity: 0.2 }}
          to={
            buttonEnabled
              ? 'https://www.leben-mit-neurodermitis.info/patientenlogin/homepage/'
              : null
          }
        />
        <div className="infoContainer">
          <div>
            <img src={loginBulb}></img>
          </div>
          <div>
            <h4>Jetzt anmelden</h4>
            <p>
              Dir wurde zur Behandlung Deiner Neurodermitis eine Therapie von
              Sanofi verordnet. Im Login-Bereich findest Du weitere
              Informationen zum Produkt und dessen Anwendung. Du kannst Dich mit
              der Chargennummer (Ch.-B.) auf der Packung des Medikaments hier
              anmelden.
            </p>
          </div>
        </div>
      </div>
      <div className={`overlay ${open ? '' : 'hidden'}`}></div>
    </div>
  );
};

export default PatientenLogin;
