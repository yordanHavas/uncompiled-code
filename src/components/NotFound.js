import React, { useContext } from 'react';
import questionIcon from '../assets/icons/Group4982.svg';
import blueWrench from '../assets/images/blue_wrench.svg';
import Button from '../components/Button';
import HeadingWithColored from '../components/HeadingWithColored';
import { MobileContext } from '../components/Layout';

const NotFound = () => {
  const isMobile = useContext(MobileContext);

  return (
    <div
      className="wrenchContainer"
      style={{ marginTop: `${isMobile ? '0' : ''}` }}>
      <div className="imageContainer">
        <img src={blueWrench} alt="Blue wrench" />
      </div>
      <div className="textConainer">
        <HeadingWithColored
          textList={['FEHLER 404']}
          coloredWordsArr={['404']}
        />
        <h2 className="headlineWrench">
          ENTSCHULDIGUNG. WIR KONNTEN DIE SEITE NICHT FINDEN.
        </h2>
        <p style={{ fontSize: `16px` }}>
          Wir sind nicht sicher, warum diese Seite nicht auffindbar ist.
        </p>
        <Button
          style={{ marginTop: '25px' }}
          text="ZURÜCK ZUR STARTSEITE"
          type="tertiary"
          to={'/'}
        />

        <div className="reasons">
          <div className="reasons_icon">
            <img src={questionIcon} alt="Question icon" />
          </div>
          <div className="reasons_list">
            <h4 style={{ fontSize: '13px' }}>
              ES KÖNNTE ABER AN FOLGENDEN MÖGLICHKEITEN LIEGEN:
            </h4>
            <ul>
              <li>Fehler in der URL</li>
              <li>Die Seite existiert nicht mehr</li>
              <li>Die Seite wurde verschoben</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
