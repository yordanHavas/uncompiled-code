import React from 'react';
import calendarIcon from '../assets/icons/calendar_icon.png';
import Button from './Button';
import ResponsiveImage from './ResponsiveImage';

export default function CardBig({
  image,
  subTitle,
  title,
  text,
  buttonText,
  buttonLink,
  altImg,
  className,
  noMarginBottom,
  linkType,
  date,
  style,
  cardTitleMinHeight,
}) {
  return (
    <div
      style={{ ...style }}
      className={`card big ${className ? className : ''}`}>
      <ResponsiveImage
        className="card-img-top"
        imgName={image}
        altImg={altImg}
      />
      <div className="card-body">
        {subTitle && (
          <h4 className={`card-subtitle ${noMarginBottom ? 'mb-0' : ''}`}>
            {subTitle}
          </h4>
        )}
        <h3
          style={{
            minHeight: `${cardTitleMinHeight ? cardTitleMinHeight : ''}`,
          }}
          className="card-title">
          {title}
        </h3>
        <p style={{ fontSize: '16px' }} className="card-text">
          {text}
        </p>
        <div className="calendar">
          <img width={100} height={100} src={calendarIcon} alt="" />
          <div className="text">
            <p>{date}</p>
          </div>
        </div>
        <div className="button-container d-flex justify-content-center">
          <Button
            linkType={linkType === 'a' ? 'a' : ''}
            to={buttonLink}
            text={buttonText}
            type="primary"
          />
        </div>
      </div>
    </div>
  );
}
