import React from "react";

const TestComponent = ({ cardHeadingInfo, cardTitle }) => {
  return (
    <div>
      Info came from props {cardHeadingInfo} and {cardTitle}
    </div>
  );
};

export default TestComponent;
