import { AnchorLink } from 'gatsby-plugin-anchor-links';
import React, { useContext } from 'react';
import Bulletlist from '../components/Bulletlist';
import Button from '../components/Button';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import ZitatBox from '../components/ZitatBox';

export default function TippsFürDenAlltag() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={[
            'Alltag  mit',
            'Neurodermitis',
            'bei Kindern und',
            'Jugendlichen',
          ]}
          coloredWordsArr={['Alltag']}
          color="yellow"
          imgName="tippsfurdenalltag_img_1"
          altImg="Ein Vater kuschelt mit seinem Sohn unter einer bunten Decke und sie schauen zusammen auf ein Handy."
          text="Der Alltag mit Neurodermitis kann eine Herausforderung sein. Für betroffene Kinder und Jugendliche, aber auch für die ganze Familie. Vielleicht fühlt Ihr Euch manchmal überfordert oder seid frustriert. Hier findet Ihr einige hilfreiche Tipps, die Euch durch Euren Familienalltag begleiten können."
        />

        <MobileThemenübersicht></MobileThemenübersicht>

        <p>
          Eine chronische Erkrankung wie Neurodermitis kann Veränderungen mit
          sich bringen, die Euer gewohntes Familienleben auf den Kopf stellen.
          Aufgaben- und Rollenverteilungen können sich im Verlauf der Erkrankung
          ändern. Je älter betroffene Kinder werden, umso eigenständiger werden
          sie. Wenn Ihr Euch bei allen Veränderungen gegenseitig mit Verständnis
          und Geduld begegnet, ist es meist leichter, den Alltag mit
          Neurodermitis zu meistern und ein weitgehend normales Leben zu führen.
        </p>
      </div>
      <div
        className="havas-container dark"
        data-summary="Juckreiz – ein ständiger Begleiter">
        <h2 className="w-100">Juckreiz – ein ständiger Begleiter</h2>

        <p>
          Das Thema Jucken und Kratzen wird Euch einige Zeit begleiten und auch
          für schlaflose Nächte sorgen – das ist ein Fakt, aber kein Grund zu
          verzweifeln. Die Forschung entwickelt sich stetig weiter und es gibt
          inzwischen auch für Kinder viele Behandlungsmöglichkeiten, die helfen
          können, die Neurodermitis langfristig in den Griff zu bekommen.
          Sprecht mit Eurem Dermatologen, damit Ihr gemeinsam eine geeignete
          Behandlung finden könnt.
        </p>

        <ImageWithText
          imgName="newPicture1"
          altImage="Ein Vater, trägt Creme auf die Wange seiner Tochter auf">
          <h3>Behandlung von Neurodermitis bei Kindern und Jugendlichen</h3>
          <p style={{ textAlign: `${isMobile ? 'left' : 'center'}` }}>
            Ausführliche Informationen zur medikamentösen Behandlung bei Kindern
            und Jugendlichen könnt ihr hier nachlesen.
          </p>
          <Button text="Mehr erfahren" type="primary" to="/kinder-behandlung" />
        </ImageWithText>

        <h3>Der Juck-Kratz-Kreislauf – Erklärung und Bedeutung</h3>

        <ImageWithText
          imgName="tippsfurdenalltag_img_2"
          altImage="Nahaufnahme einer Hand, die gekratzt wird">
          <p>
            Der oft unerträgliche und anhaltende Juckreiz löst häufig einen kaum
            zu beherrschenden Kratzdrang aus. Auch wenn Ihr das Kratzen nicht
            ganz unterbinden könnt, ist es wichtig, dass Euer Kind möglichst
            wenig kratzt. Es verschafft nur für einen kurzen Moment Linderung;
            langfristig führt es zu Entzündungen, offenen Stellen und stärkerem
            Juckreiz.
          </p>
          <AnchorLink to="/verstehen#juck-kratz-kreislauf" stripHash={true}>
            Mehr zum Thema Juck-Kratz-Kreislauf könnt Ihr hier nachlesen.
          </AnchorLink>

          <p>
            Nicht kratzen – das ist leichter gesagt als getan. Kratzen ist
            jedoch nicht gleich Kratzen. Sobald Euer Kind alt genug ist, könnt
            Ihr mit ihm alternative Kratzmethoden üben, zum Beispiel mit der
            flachen Hand oder mit den Knöcheln. Es kann auch helfen, die
            juckende Stelle zu kneten, sie zu massieren, abzuklopfen, zu drücken
            oder zu streicheln.
          </p>
        </ImageWithText>

        <Bulletlist
          firstColumnSize={3}
          heading="Tipps gegen Juckreiz:"
          textList={[
            'Kühlende Umschläge oder eine Kühlkompresse aus dem Kühlschrank können den Juckreiz lindern.',
            'Das Tragen von leichten Baumwollhandschuhen in der Nacht verhindert das unbewusste Kratzen im Schlaf.',
            'Es gibt auch spezielle Anti-Juckreiz-Sprays, die den Juck-Kratz-Kreislauf unterbrechen.',
            'Kurz geschnittene und glatt gefeilte Fingernägel verhindern starke Hautirritationen.',
            'Vor allem Ablenkung kann helfen, den Juckreiz zu vergessen. Findet gemeinsam etwas Schönes, Spannendes oder Spaßiges, das vom Juckreiz ablenkt.',
          ]}></Bulletlist>
        <SelbsttestTeaser
          customImageIndex={0}
          style={
            isMobile
              ? {
                  position: 'relative',
                  bottom: '90px',
                  zIndex: '1',
                  marginBottom: '-90px',
                }
              : {}
          }
        />
      </div>
      <div
        className="havas-container light  "
        data-summary="Körperpflege – die Basis für eine entspannte Haut">
        <h2 className="w-100">
          Körperpflege – die Basis für eine entspannte Haut
        </h2>
        <ImageWithText
          imgName="tippsfurdenalltag_img_3"
          altImage="Ein Baby wird am Bein eingecremt.">
          <p>
            Konsequente Hautpflege ist für Menschen mit Neurodermitis eine
            wichtige Aufgabe – egal in welchem Alter. Das Eincremen der
            trockenen Hautstellen gehört zur täglichen Routine. Eine liebevolle
            und sanfte Massage, verbunden mit beruhigender Musik, einer schönen
            Geschichte oder einem ruhigen Spiel, können kleine Patienten dazu
            bringen, das Eincremen schon früh mit positiven Erfahrungen zu
            verbinden. So wird daraus ein schönes Ritual, auf das sich Euer Kind
            freuen kann.
          </p>
        </ImageWithText>

        <div className="picture-text" style={{ marginTop: '0' }}>
          <div className="item" style={{ margin: 'auto' }}>
            <p>
              Je älter und eigenständiger Euer Kind wird, umso wahrscheinlicher
              ist es dann, dass es selbst ein schönes Ritual aus der
              Körperpflege macht, anstatt sie genervt abzuarbeiten. Es wird auch
              Phasen geben, in denen Euer Kind keine Lust auf das tägliche
              Eincremen hat. Dann heißt es: Geduldig bleiben. Atmet tief durch
              und haltet einen Moment inne, denn Stress und Hektik können sich
              übertragen.
            </p>
            <br></br>
            <p>
              Übrigens solltet Ihr Geduld nicht nur Eurem Kind entgegenbringen,
              sondern auch Euch selbst.
            </p>
          </div>

          <div className="item">
            <ZitatBox
              isLoudspeaker={true}
              text="„Geduld kann man üben und der erste Schritt zu mehr Gelassenheit ist Akzeptanz.“"
            />
          </div>
        </div>

        <h3>Tipps zur Körperpflege</h3>

        {isMobile && (
          <ImageWithText
            imgName="tippsfurdenalltag_img_4"
            altImage="Ein Kleinkind sitzt in der Badewanne und hat Schaum auf dem Kopf"
            imagestyle={
              isMobile
                ? {
                    position: 'relative',
                    top: '90px',
                    zIndex: '1',
                    marginTop: '-90px',
                  }
                : {}
            }>
            <Bulletlist
              className="violet"
              textList={[
                'Nutzt am besten seifenfreie und pH-neutrale Pflegeprodukte ohne Parfüm.',
                'Langes Baden trocknet die Haut aus. Kinder mit Neurodermitis sollten möglichst auf Duschen umsteigen. Auch dabei gilt: nicht zu lange und nicht zu heiß.',
                'Baden ist aber generell kein Tabu. Allerdings sollte Euer Kind nicht zu häufig baden. Nutzt rückfettende Badezusätze. Das Badewasser sollte eine lauwarme Temperatur von höchstens 35 Grad haben.',
                'Nach dem Bad oder der Dusche ist es besser, die Haut nur sanft trocken zu tupfen, anstatt sie abzureiben. Zudem solltet Ihr die Haut Eures Kindes mit rückfettenden Pflegeprodukten eincremen.',
                'Das tägliche Eincremen ist generell sehr wichtig.',
              ]}></Bulletlist>
          </ImageWithText>
        )}

        {!isMobile && (
          <>
            <ImageWithText
              imgName="tippsfurdenalltag_img_4"
              altImage="Ein Kleinkind sitzt in der Badewanne und hat Schaum auf dem Kopf"
            />
            <Bulletlist
              firstColumnSize={3}
              className="violet"
              textList={[
                'Nutzt am besten seifenfreie und pH-neutrale Pflegeprodukte ohne Parfüm.',
                'Langes Baden trocknet die Haut aus. Kinder mit Neurodermitis sollten möglichst auf Duschen umsteigen. Auch dabei gilt: nicht zu lange und nicht zu heiß.',
                'Baden ist aber generell kein Tabu. Allerdings sollte Euer Kind nicht zu häufig baden. Nutzt rückfettende Badezusätze. Das Badewasser sollte eine lauwarme Temperatur von höchstens 35 Grad haben.',
                'Nach dem Bad oder der Dusche ist es besser, die Haut nur sanft trocken zu tupfen, anstatt sie abzureiben. Zudem solltet Ihr die Haut Eures Kindes mit rückfettenden Pflegeprodukten eincremen.',
                'Das tägliche Eincremen ist generell sehr wichtig.',
              ]}></Bulletlist>
          </>
        )}
      </div>
      <div
        className="havas-container dark"
        data-summary="Schlaf – Erholung für Körper und Geist">
        <h2 className="w-100">Schlaf – Erholung für Körper und Geist</h2>

        <p>
          Kinder mit Neurodermitis haben oft Schlafprobleme. Schätzungsweise 7
          von 10 der betroffenen Kinder leiden unter Schlafmangel. Um die
          Herausforderungen des Alltags gut bewältigen zu können, sollte Euer
          Kind lernen, dem Tag-Nacht-Rythmus zu folgen. Das heißt: Macht die
          Nacht nicht zum Tag. In der Regel ist es besser, in der Nacht auf
          Bäder, Umschläge oder spannende Geschichten zu verzichten. Versorgt
          Euer Kind liebevoll, wenn es nachts wach wird und unterstützt es
          dabei, wieder einzuschlafen. Meist ist es auch besser, kein Licht
          anzuknipsen - ein kleines Nachtlicht reicht meist, denn helles Licht
          ist ein starker Reiz, der leicht wach machen kann. Vermeidet am Abend
          Aufregung, zu spannende Bücher oder digitale Medien.
        </p>

        <Bulletlist
          heading="Tipps bei Schlafstörungen"
          textList={[
            'In einem kühlen, gut gelüfteten Schlafzimmer schläft es sich besser – Schweiß kann ein möglicher Auslöser für Juckreiz sein.',
            'Schlafkleidung sollte leicht und atmungsaktiv sein, z. B. aus dünner Baumwolle.',
            'Liebevolle Einschlafrituale helfen, trotz Juckreiz gut ein- und möglichst durchzuschlafen.',
          ]}></Bulletlist>

        <h3>Milben – kleine Quälgeister</h3>

        <p>
          Milben bzw. deren Kot können zu allergischen Reaktionen führen. Sie
          lieben ein feuchtwarmes Umfeld und leben von abgestorbenen
          Hautschuppen. Deshalb ist das Bett eine Umgebung, in der sie sich sehr
          wohlfühlen. Was sie nicht mögen, sind frische Luft und Bewegung. Daher
          ist es wichtig, jeden Morgen Kopfkissen, Bettdecke und Kuscheltiere
          auszuschütteln und das Zimmer gut zu lüften.
        </p>

        <p>
          Um allergischen Reaktionen vorzubeugen, verwendet am besten
          Bettwäsche, die bei 60 Grad waschbar ist. Kuscheltiere, die nicht in
          der Waschmaschine gewaschen werden dürfen, könnt Ihr ab und zu für
          einen Tag in die Tiefkühltruhe legen. Das macht den Milben in der
          Regel den Garaus.
        </p>
      </div>
      <div
        className="havas-container light"
        data-summary="Gesunde Ernährung – was gibt es zu beachten?">
        <h2 className="w-100">Gesunde Ernährung – was gibt es zu beachten?</h2>

        <ImageWithText
          imgName="tippsfurdenalltag_img_5"
          altImage="Ein Mädchen sitzt am Tisch und beißt in eine Karotte">
          <p>
            Eine spezielle Neurodermitis-Diät gibt es nicht. Gesundes und
            abwechslungsreiches Essen mit möglichst wenigen Einschränkungen ist
            in der Regel das Beste für ein Kind – für die körperliche
            Entwicklung, aber auch für die psychische. Denn in einem Leben, in
            dem die Neurodermitis so manches bestimmt, ist es wunderbar, essen
            zu können, was einem schmeckt.
          </p>
        </ImageWithText>

        <p>
          Etwa jedes zweite Kind mit Neurodermitis reagiert allergisch gegen
          bestimmte Nahrungsmittel. Doch nur bei etwa jedem dritten moderat bis
          schwer betroffenen Kind führt dies tatsächlich zu einer
          Verschlechterung des Hautbildes. Also sind übermäßige Einschränkungen
          bei der Nahrungsmittelwahl selten notwendig.
        </p>

        <p>
          Habt Ihr den Verdacht, dass Euer Kind auf ein bestimmtes
          Nahrungsmittel allergisch reagiert, könnt Ihr dieses, nach ärztlicher
          Rücksprache, versuchsweise vom Speiseplan streichen. Verschwindet die
          Allergie, habt Ihr den Auslöser sehr wahrscheinlich gefunden.
        </p>

        {/* <ClickableBox title="Gut zu wissen" header="Nahrungsmittelallergien">
        </ClickableBox> */}
        <p>
          Nahrungsmittelallergien verschwinden manchmal wieder, sodass nach 1
          oder 2 Jahren das Nahrungsmittel durchaus wieder vertragen werden
          kann. Wenn Ihr sichergehen wollt, solltet Ihr Euren Dermatologen
          einbeziehen. Spezielle Allergietests können Vermutungen medizinisch
          absichern.
        </p>

        <ClickableBox
          title="Gut zu wissen"
          header="Hilfe durch Ernährungsberater">
          <p>
            Ernährungsberater haben manchmal tolle Ideen, wie Nahrungsmittel
            kreativ ersetzt werden können. Meist reichen hier schon 1–2 Stunden,
            um eine kindgerechte und abwechslungsreiche Kost zusammenzustellen.
          </p>
          <p>
            Erkundigt Euch in Eurer Kinderarztpraxis oder auch bei Eurer
            Krankenkasse, wenn ihr mehr Infos und Kontakte zu
            Ernährungsberatungsstellen benötigt.
          </p>
        </ClickableBox>
      </div>
      <div
        className="havas-container dark"
        data-summary="Hautnah dran – Kleidung">
        <h2 className="w-100">Hautnah dran – Kleidung</h2>

        <ImageWithText
          imgName="tippsfurdenalltag_img_6"
          altImage="Ein kleines Kind mit einer rosa Strickjacke"></ImageWithText>
        <p>
          Kleidung hat meist direkten Kontakt zur Haut und kann diese reizen.
          Deshalb gibt es einige Tipps, die Ihr bei der Wahl der Kleidung
          beachten könnt:
        </p>

        <Bulletlist
          textList={[
            'Die Kleidung sollte glatt, saugfähig und luftdurchlässig sein. Am wohlsten fühlt sich empfindliche Haut in lockerer Baumwoll- oder Leinenkleidung.',
            'Enge, raue und synthetische Kleidung solltest Du vermeiden. Auch Kleidung aus Wolle kann die Haut reizen.',
            'Wärme oder gar ein Hitzestau lösen sehr schnell einen starken Juckreiz aus. Daher empfiehlt sich ein Lagenlook, sodass je nach Außentemperatur die Schichten angepasst werden können.',
            'Neue Kleidungsstücke solltet Ihr vor dem Tragen waschen, um Schadstoffe auszuspülen. Entfernt auch Etiketten in der Kleidung, denn sie könnten auf der Haut reiben.',
          ]}></Bulletlist>
      </div>
      <div className="havas-container light" data-summary="Fazit">
        <h2 className="w-100">Fazit</h2>

        <p>
          Der Alltag mit einer chronischen Erkrankung wie Neurodermitis kann
          ganz schön anstrengend sein. Betroffene Kinder und Jugendliche können
          nicht aus ihrer Haut und müssen tagein, tagaus die Anforderungen ihrer
          Erkrankung meistern. Eltern wollen ihre Kinder dabei natürlich
          unterstützen. Helfen bedeutet aber manchmal, die Eigenständigkeit
          betroffener Kinder zu fördern und loszulassen. Bei allen
          Herausforderungen hilft es meist, offen miteinander zu reden und sich
          gegenseitig zuzuhören. Gemeinsam ist es leichter, einen Weg zu finden,
          sich aufeinander einzustellen und füreinander da zu sein. Mit
          Neurodermitis zu leben, erfordert Kraft. Holt Euch Hilfe, damit Ihr
          Euren Alltag gemeinsam aktiv gestalten könnt.
        </p>

        {/* <ArztefinderTeaser /> */}
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Tipps für den Alltag"
    metaTitle="Alltag mit Neurodermitis bei Kindern und Jugendlichen"
    description="Leben mit Neurodermitis kann eine Herausforderung sein. Für betroffene Kinder und für die ganze Familie. Hier findet Ihr Tipps für den gemeinsamen Alltag."
  />
);
