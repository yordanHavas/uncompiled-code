import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Akkordeon from '../components/Akkordeon';
import Bulletlist from '../components/Bulletlist';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import TippsBox from '../components/TippsBox';

export default function Hand() {
  const isMobile = useContext(MobileContext);

  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light" style={{ paddingBottom: 20 }}>
        <Hero
          headingTextList={['Neurodermitis', 'an der Hand']}
          coloredWordsArr={['Hand']}
          imgName="hand_img_1"
          altImg="Zwei junge Frauen bilden mit ihren Händen gemeinsam ein Herz"
          color="yellow"
          text="Neurodermitis ist mehr als ein körperliches Leiden. Die chronisch-entzündliche Erkrankung kann auch Dein soziales Leben beeinträchtigen. Wir erläutern in diesem Artikel, was Neurodermitis an den Händen ist, wie ein Handekzem aussieht, was Neurodermitis an den Händen auslöst, was Du dagegen tun kannst und vieles mehr."
        />
        <p>
          Ob Anziehen, Schreiben, Kochen oder Klavierspielen, unsere Hände sind
          wahre Alleskönner und kaum eine Tätigkeit kommt ohne ihren Einsatz
          aus. Deshalb ist Neurodermitis an den Händen meist sehr belastend.
          Jedes Zugreifen, Anfassen oder Berühren kann zur Qual werden, denn
          viele selbstverständliche Dinge sind dann schmerzhaft.
        </p>
        <p>
          Außerdem sind die Hände die Körperteile, die mit unserem Umfeld in
          Kontakt treten – egal ob wir unserem Gegenüber zuwinken oder ihm die
          Hand schütteln. Ein sichtbares Handekzem ist oft mehr als eine
          körperliche Einschränkung, Neurodermitis an den Händen kann auch das
          soziale Leben beeinträchtigen.{' '}
        </p>
        <MobileThemenübersicht className="dropdown-1" />
      </div>

      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Was ist Neurodermitis an den Händen?">
        <h2>Was ist Neurodermitis an den Händen?</h2>
        <p>
          Neurodermitis wird auch atopische Dermatitis oder atopisches Ekzem
          genannt und ist eine chronisch-entzündliche Erkrankung, die sich vor
          allem auf der Haut zeigt. Tritt sie an den Händen auf, spricht man
          auch von einem Handekzem.
        </p>
        <p>
          Die typischen Symptome der Neurodermitis treten in der Regel je nach
          Alter an unterschiedlichen Körperregionen auf. An den Händen kann sich
          Neurodermitis in jedem Alter zeigen. Nur bei Säuglingen macht sich die
          Erkrankung meist an anderen Stellen bemerkbar.
        </p>
        <ClickableBox
          title="Gut zu wissen"
          header="Neurodermitis kontrollieren">
          <p>
            Neurodermitis ist zwar nicht heilbar, aber heutzutage gibt es viele
            Pflegeprodukte und <Link to="/ueberblick/">Medikamente</Link>, die
            Dir helfen können, Deine Neurodermitis zu kontrollieren.
          </p>
        </ClickableBox>

        <p>
          Die Symptome der Neurodermitis sind nicht immer gleich stark
          ausgeprägt. Es kann Phasen geben, in denen Du keinerlei Beschwerden
          hast, und dann gibt es Schübe mit starken Beeinträchtigungen.
          Neurodermitis ist von Mensch zu Mensch verschieden und die Symptome
          können sich in ihrer Dauer und Schwere stark unterscheiden.
        </p>
      </div>

      <div
        className={`havas-container ${isMobile ? 'dark' : 'light'}`}
        data-summary="Wie sieht Neurodermitis an den Händen aus?">
        <h2>Wie sieht Neurodermitis an den Händen aus?</h2>

        <ImageWithText
          imgName="hand_img_2"
          altImage="Hände mit sichtbaren Ekzemen, die ein Seil halten">
          <p>
            Charakteristisch für Neurodermitis an den Händen sind auffällige
            Rötungen. Die Haut ist sehr trocken und rau sowie rissig und
            schuppig. Auch Bläschen oder Pusteln, Einschnitte, Hautverdickungen
            oder nässende Ekzeme sind mögliche Symptome. Zudem quält Betroffene
            ein starker Juckreiz. Kratzen hilft allerdings nicht, dadurch wird
            die Haut nur weiter geschädigt und es können Schadstoffe in die Haut
            eindringen.
          </p>
        </ImageWithText>

        <TippsBox
          heading="unser tipp"
          text={[
            'Eine ',
            <Link to="/aeusserliche-therapien">konsequente Pflege</Link>,
            ' Deiner Haut mit rückfettenden Cremes ist in jeder Phase der Erkrankung wichtig, auch wenn Du gerade keine Beschwerden hast.',
          ]}
        />

        <p>
          Neurodermitis an den Händen kann sich bis zu den Handgelenken
          ausbreiten. Am Handrücken sind die Ekzeme besonders auffällig, aber
          auch auf den Handinnenflächen kann die Haut trocken, gerötet, schuppig
          und rissig sein. Sollte die Neurodermitis die Finger betreffen, kann
          sie sich bis zu den Fingerkuppen ausbreiten. Meist ist die Haut
          zwischen den Fingern und direkt an den Fingergelenken betroffen.
        </p>

        <Bulletlist
          heading="Typische Anzeichen für Neurodermitis an den Händen"
          textList={[
            'Gerötete Hautstellen',
            'Trockene und raue Haut',
            'Risse und/oder Einschnitte',
            'Bläschen und/oder Pusteln',
            'Nässende Hautstellen',
            'Schmerzhafte Entzündungen',
            'Hautverdickungen',
            'Starker Juckreiz',
          ]}
        />

        <p>
          Falls typische Symptome einer Neurodermitis an Deinen Händen
          auftreten, solltest Du einen Dermatologen aufsuchen, um eine passende
          Behandlung zu bekommen.
        </p>

        <ArztefinderTeaser />
      </div>
      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Woher kommt Neurodermitis an den Händen?">
        <h2>Woher kommt Neurodermitis an den Händen?</h2>
        <ImageWithText
          imgName="hand_img_3"
          altImage="Person, die sich an der Hand kratzt">
          <p>
            Die genauen Ursachen für Neurodermitis sind noch nicht vollständig
            geklärt, aber Wissenschaftler auf der ganzen Welt versuchen diese zu
            ergründen. Klar ist, Neurodermitis zeigt sich zwar an der Haut, sie
            ist aber keine reine Hautkrankheit. Es sind verschiedene Faktoren,
            die dazu beitragen können, dass die Krankheit ausbricht oder Schübe
            ausgelöst werden.
          </p>
        </ImageWithText>
        <Link to="/verstehen" style={{ textDecoration: 'none' }}>
          <Bulletlist
            style={isMobile ? { height: 540 } : {}}
            heading="Bekannte Faktoren, die das Auftreten einer Neurodermitis begünstigen können, sind:"
            textList={[
              'Erbliche Vorbelastungen',
              'Eine gestörte Barrierefunktion der Haut',
              'Eine Überempfindlichkeit des Immunsystems',
              'Verschiedene Umweltfaktoren',
            ]}>
            <Link to="/verstehen">
              Detaillierte Informationen dazu kannst Du hier nachlesen.
            </Link>
          </Bulletlist>
        </Link>
      </div>

      <div
        className={`havas-container ${isMobile ? 'dark' : 'light'}`}
        data-summary="Was löst Neurodermitis an den Händen aus?">
        <h2>Was löst Neurodermitis an den Händen aus?</h2>
        <p>
          Unsere Hände kommen jeden Tag mit unzähligen Stoffen in Kontakt und
          sind vielen verschiedenen Umwelteinflüssen ausgesetzt. Einige können
          das Krankheitsgeschehen negativ beeinflussen. Im Fachjargon spricht
          man von Provokationsfaktoren oder Triggern. Welche Trigger einen
          Neurodermitis-Schub auslösen, ist von Mensch zu Mensch verschieden und
          kann sich im Laufe der Zeit auch verändern.
        </p>

        <Bulletlist
          heading="Umweltfaktoren oder äußere Einflüsse, die sich negativ auf Neurodermitis an den Händen auswirken können, sind zum Beispiel:"
          textList={[
            'Falsche oder übermäßige Hautreinigung, z. B. häufiges Händewaschen',
            'Seifen oder Pflegeprodukte mit Duft- und Konservierungsstoffen',
            'Kratzende Kleidung, z. B. Handschuhe aus Wollfasern oder Synthetikstoffen',
            'Mechanische Beanspruchung, z. B. das Arbeiten mit Papier oder anderen Stoffen, die Deine Hände austrocknen können',
            'Stehende Feuchtigkeit, z. B. Dein eigener Schweiß unter luftundurchlässigen Handschuhen',
            'Klima, z. B. geringe Luftfeuchtigkeit in beheizten Räumen',
            'Stress, z. B. Lärm, Leistungsdruck',
            'Allergene, z. B. Pflanzenpollen, Tierhaare oder auch einige Nahrungsmittel',
          ]}
        />

        <SelbsttestTeaser />
      </div>

      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Was tun bei Neurodermitis an der Hand?">
        <h2>Was tun bei Neurodermitis an der Hand?</h2>
        <ImageWithText
          imgName="hand_img_4"
          altImage="Frau, die sich die Hände eincremt">
          <p>
            Neurodermitis ist eine chronische Erkrankung, die nicht geheilt
            werden kann. Sie wird Dich Dein ganzes Leben begleiten. Dabei wird
            es voraussichtlich immer wieder Phasen geben, in denen die
            Erkrankung ruhig ist und Du kaum Beschwerden hast. Auch dann ist es
            wichtig, bei der Pflege Deiner Haut konsequent zu sein und an
            langfristigen Therapiemaßnahmen dranzubleiben.
          </p>
        </ImageWithText>

        <h3>Trigger – die gilt es zu vermeiden</h3>
        <p>
          Um Neurodermitis-Schübe zu vermeiden, ist es zunächst wichtig
          herauszufinden, was sie auslöst, denn was bei dem einen zu Beschwerden
          führt, bleibt bei dem anderen folgenlos. Nur wenn Du Deine
          individuellen Trigger kennst, kannst Du sie auch gezielt vermeiden.
          Ein Tagebuch kann dabei helfen herauszufinden, was einen Schub bei Dir
          triggert. Sprich auch mit Deinem Arzt, denn es gibt verschiedene
          Tests, die zeigen können, ob Du auf bestimmte Stoffe allergisch
          reagierst.
        </p>
        <p>
          Denk daran: Leben heißt Veränderung und auch Deine Erkrankung kann
          sich wandeln. Deshalb solltest Du Deine Neurodermitis langfristig im
          Auge behalten, ohne sie dabei zu Deinem Lebensmittelpunkt zu machen.
          Ganz nach dem Motto „so viel wie nötig, so wenig wie möglich“.
        </p>

        <h3>Basistherapie – pflege Deine Hände regelmäßig</h3>

        <p>
          Wie der Name schon sagt, ist die Basistherapie die Grundlage einer{' '}
          <Link to="/ueberblick">Behandlung der Neurodermitis.</Link> Sie sollte
          ein fester Bestandteil in Deinem Alltag sein. Da die Haut an Deinen
          Händen besonders beansprucht wird, solltest Du ihnen dabei auch
          besondere Aufmerksamkeit schenken.
        </p>

        <TippsBox
          heading="unser tipp"
          text="Mach aus der täglichen Hautpflege ein Wellnessritual, statt sie als lästige Pflicht zu sehen."
        />

        <p>
          Es gibt Präparate, die speziell für die Pflege Deiner Hände entwickelt
          wurden. Eine geeignete Salbe oder Creme kann über Nacht gut einziehen,
          wenn Du sie abends etwas dicker aufträgst und dünne Baumwollhandschuhe
          überstreifst. Mit einer kleinen Cremetube für unterwegs kannst Du
          Deine Hände auch zwischendurch immer wieder pflegen. Sprich mit Deinem
          Dermatologen über geeignete Pflegeprodukte oder frage einen Apotheker.
        </p>

        <Link to="/aeusserliche-therapien">
          Weitere Informationen zur Basistherapie findest Du hier.
        </Link>
        <h3 className="w-100">Medikamentöse Therapien</h3>
        <p>
          Bei einem akuten Schub mit Entzündungen wird Dir Dein Dermatologe
          wirkstoffhaltige Präparate verschreiben, die äußerlich direkt auf die
          betroffenen Stellen aufgetragen werden. Neben diesen sogenannten{' '}
          <Link to="/aeusserliche-therapien">topischen Präparaten</Link> gibt es
          auch{' '}
          <Link to="/innerliche-therapien">
            systemische (von innen wirkende)
          </Link>{' '}
          Medikamente zur Behandlung von Neurodermitis. Diese werden in der
          Regel langfristig angewendet und kommen nicht nur bei einem akuten
          Schub zum Einsatz.
        </p>

        <ClickableBox
          title="Gut zu wissen"
          header="Topisch vs. systemisch, was ist der Unterschied?">
          <p>
            <strong>Topische Medikamente</strong> sind Präparate, die genau an
            der Stelle angewendet werden, an der sie wirken sollen. Hierzu
            gehören zum Beispiel Salben oder Cremes.
          </p>
          <p>
            <strong>Systemische Medikamente</strong> werden meist gespritzt oder
            oral verabreicht. Diese Arzneien verteilen sich dann über den
            Blutkreislauf im ganzen Körper und gelangen so an den Ort, an dem
            sie wirken sollen.
          </p>
        </ClickableBox>

        <ImageWithText
          imgName="hand_img_5"
          imageOnRight={true}
          altImage="Eine Ärztin im Gespräch mit einer Patientin">
          <p>
            In einigen Fällen kann zusätzlich eine Behandlung mit UV-Licht
            sinnvoll sein. Diese Behandlung nennt man Phototherapie und sie
            sollte immer von medizinischem Fachpersonal durchgeführt werden.
          </p>

          <Link to="/ueberblick">
            Mehr zur Behandlung der Neurodermitis findest Du hier.
          </Link>
        </ImageWithText>
        <p style={isMobile ? {} : { marginTop: 60 }}>
          Denke daran: Jeder Mensch ist anders und auch die Neurodermitis ist
          nicht bei allen gleich. Deshalb ist es wichtig den Therapieplan auf
          Deinen Krankheitsverlauf und Deine Bedürfnisse abzustimmen. Sprich mit
          Deinem Dermatologen und lass Dir alles erklären. Frag auch nach, wenn
          Du etwas nicht verstanden hast oder wenn Dir etwas zu schnell ging.
          Wenn Du Deine Erkrankung besser verstehst, kann Dir das beim Umgang
          mit ihr helfen.
        </p>

        <ArztefinderTeaser />
      </div>
      <div className={`havas-container light`}>
        <h3>Handhygiene bei Neurodermitis</h3>
        <ImageWithText
          imgName="hand_img_6"
          altImage="Person wäscht sich die Hände am Waschbecken">
          <p>
            Bei der Pflege spielt auch das Händewaschen eine wichtige Rolle.
            Verwende dabei eine rückfettende oder milde Seife mit neutralem
            pH-Wert. Am besten wäschst Du Deine Hände nicht zu häufig und nutzt
            lauwarmes Wasser. Trockne sie behutsam tupfend ab, statt sie fest zu
            reiben. Achte dabei auch auf die Fingerzwischenräume. Bleiben sie
            feucht, kann das die Haut reizen. Nach jedem Händewaschen solltest
            Du eine rückfettende Handcreme zur Pflege verwenden.
          </p>
        </ImageWithText>

        <ClickableBox
          className={''}
          title="Gut zu wissen"
          header="Weniger ist mehr – vermeide den Kontakt mit Wasser und Schadstoffen">
          <p>
            Versuche den Wasserkontakt zu minimieren und schütze Deine Hände vor
            Schadstoffen oder mechanischen Reizen. Beim Putzen oder auch beim
            Kochen, bei Gartenarbeiten oder beim Basteln solltest Du
            Schutzhandschuhe tragen.
          </p>
        </ClickableBox>

        <p>
          Seit Corona wissen wir, dass es erforderlich sein kann, die Hände
          häufiger zu waschen. Das greift allerdings Deine ohnehin schon
          gestresste Haut weiter an. Eine Barriere-Creme, die selbst bei
          mehrfachem Waschen auf den Händen bleibt, kann helfen Deine Haut zu
          schützen. Auch Desinfektionsmittel auf Alkoholbasis bieten eine
          Alternative zum Händewaschen. In einer Studie konnte gezeigt werden,
          dass sie die Haut an den Händen weniger belasten als das Waschen mit
          Wasser und Seife. Allerdings können alkoholbasierte
          Desinfektionsmittel auf Deiner Haut brennen. Das ist unangenehm, aber
          nicht unbedingt ein Zeichen, dass Deine Haut geschädigt wird.
        </p>
      </div>

      <div
        className={`havas-container dark`}
        data-summary="Was Du sonst noch tun kannst bei Neurodermitis an den Händen">
        <h2>Was Du sonst noch tun kannst bei Neurodermitis an den Händen</h2>

        <ImageWithText
          imgName="hand_img_7"
          altImage="Eine junge Frau, die auf dem Rücken liegt und die Arme über den Kopf streckt">
          <h3 className="w-100">Der Umgang mit Stress</h3>
          <p>
            Wie Dein Körper braucht auch Deine Psyche hin und wieder etwas
            Zuwendung. Vor allem Stress solltest Du vermeiden, denn der zählt zu
            den Auslösern für Neurodermitis. Leichter gesagt als getan. Stress
            lässt sich nicht immer umgehen. Deshalb ist es wichtig, Dir in
            Deinem Alltag Ruhephasen zu gönnen, denn Stress ist vor allem dann
            problematisch, wenn Du keinen Ausgleich schaffst.
          </p>
          <p>
            Wie Du mit Stress umgehen kannst, kannst Du im Lernmodul{' '}
            <a style={{ display: 'inline' }} href="/lernplattform/elearning-6">
              „Tipps für eine bessere Stressbewältigung“
            </a>{' '}
            nachlesen.
          </p>
        </ImageWithText>

        <h3>Der Umgang mit Dir selbst</h3>

        <p>
          Neurodermitis an den Händen ist meist mehr als ein körperliches
          Leiden. Die psychische Belastung kann enorm sein, denn ähnlich wie bei
          der <Link to="/gesicht">Neurodermitis im Gesicht</Link> sind die
          Handekzeme für Dein Umfeld sichtbar. Wer Handekzeme hat, fühlt sich
          oft nicht attraktiv. Betroffene fühlen sich ausgegrenzt und haben mit
          negativen Emotionen zu kämpfen. Dabei ist eine positive Einstellung zu
          Dir und Deinem Leben hilfreich für den Umgang mit Deiner Erkrankung.{' '}
        </p>

        <TippsBox
          heading="unser tipp"
          text="Für Menschen mit Neurodermitis gibt es spezielle Schulungen, deren Kosten meist von den Krankenkassen übernommen werden."
        />

        <p>
          Auch Sport und Bewegung sowie eine ausgewogene Ernährung können zu
          Deinem Wohlbefinden beitragen und Dir so beim Umgang mit Deiner
          Erkrankung helfen. Hilfreiche Infos zum Thema Ernährung findest Du im
          Lernmodul{' '}
          <a href="/lernplattform/elearning-12">
            „Ernährung bei Neurodermitis“
          </a>
          .
        </p>
      </div>

      <div
        className={`havas-container light`}
        data-summary="Fluch oder Segen – Handschuhe bei Neurodermitis">
        <h2>Fluch oder Segen – Handschuhe bei Neurodermitis</h2>
        <p>
          Ob beim Hausputz, bei der Gartenarbeit oder im Beruf: Wenn die Hände
          über längere Zeit Wasser oder Reizstoffen ausgesetzt sind, können
          Handschuhe sie schützen. Im Winter bieten sie Schutz vor klirrender
          Kälte. Handschuhe können Deine Hände aber auch reizen.
        </p>

        <Akkordeon heading="Gummihandschuhe" open={true}>
          <p>
            Gummihandschuhe sollen die Haut eigentlich schützen, sie können für
            Neurodermitis-Hände aber zum Problem werden. Das Material lässt zwar
            keine Flüssigkeit rein, aber auch nicht raus. Der Schweiß sammelt
            sich unter den Handschuhen und bleibt auf der Haut. Das kann Deine
            Haut reizen. Nutze deshalb Handschuhe mit einem Baumwollfutter oder
            mit Baumwollinnenhandschuhen. Es gibt auch spezielle
            Schutzhandschuhe für Menschen mit empfindlicher Haut.
          </p>
        </Akkordeon>

        <Akkordeon heading="Einmalhandschuhe">
          <p>
            Einmalhandschuhe können Deine Hände ebenfalls vor Wasser oder
            Schadstoffen schützen. Aber Achtung: Einige Menschen haben eine
            Allergie gegen Latex! Dann kannst Du Handschuhe aus Nitril oder
            Vinyl nutzen.
          </p>
          <p>
            <strong>
              Feuchtigkeits- oder wasserabweisende Handschuhe solltest Du nicht
              länger als 2 Stunden am Stück tragen.
            </strong>
          </p>
          <p>
            Falls Du länger als 2 Stunden mit Handschuhen arbeiten möchtest oder
            musst, solltest Du Deiner Haut zwischendurch Zeit geben, sich zu
            regenerieren. Ziehe die Handschuhe dann aus und pflege Deine Hände
            mit einer Creme oder Lotion.
          </p>
        </Akkordeon>

        <Akkordeon heading="Baumwollhandschuhe">
          <p>
            Baumwollhandschuhe sind zwar nicht geeignet, um Deine Hände gegen
            Wasser zu schützen, aber sie können bei der Pflege oder Behandlung
            zum Einsatz kommen. Dabei trägst Du eine Salbe bzw. Creme auf und
            streifst die Handschuhe darüber. Die Handschuhe schützen Deine
            empfindliche Haut und sorgen dafür, dass die Salbe oder Creme
            richtig aufgenommen wird. Baumwollhandschuhe können im Winter auch
            eine Alternative zu Wollhandschuhen sein, um Deine Hände vor Kälte
            zu schützen.
          </p>
        </Akkordeon>
        <Akkordeon heading="Wollhandschuhe">
          <p>
            Wollhandschuhe können die Haut der Hände irritieren und den Juckreiz
            fördern. Das liegt am Material selbst, aber auch an seiner
            Verarbeitung. Deshalb solltest Du auf Handschuhe aus Wolle
            verzichten. Auch raue Innenfutter können die Haut reizen. Gut
            geeignet für den direkten Kontakt mit der Haut sind weiche
            Baumwolle, Viskose und Fleecestoffe.
          </p>
        </Akkordeon>

        <ClickableBox
          title="Gut zu wissen"
          header="Wie erkenne ich verträgliche Putz- und Reinigungsmittel?">
          <p>
            Um Menschen mit Allergien, Asthma und Neurodermitis im Alltag zu
            unterstützen, hat der Deutsche Allergie- und Asthmabund (DAAB) ein
            Label entwickelt, mit dem besonders verträgliche Produkte
            gekennzeichnet sind. Darunter gibt es auch Putz- und
            Reinigungsmittel. Eine vollständige Produktliste kannst Du auf der
            Webseite des DAAB per E-Mail anfordern. Schau mal hier vorbei:
            www.daab.de.
          </p>
        </ClickableBox>
      </div>

      <div className={`havas-container dark`} data-summary="Fazit">
        <h2 className="w-100">Fazit</h2>
        <p>
          Wir begreifen unser Leben im wahrsten Sinne des Wortes mit den Händen.
          Sie helfen uns Dinge zu erfassen und sind unsere Botschafter im
          Kontakt mit unseren Mitmenschen. Du kannst die Neurodermitis an den
          Händen nicht einfach wegpflegen, aber konsequente Pflege und eine für
          Dich passende Behandlung können Dir helfen, einen positiven Umgang mit
          Deiner Neurodermitis zu finden. Du kannst auch Deine Einstellung zu
          Deiner Erkrankung beeinflussen und möglichst viele Chancen ergreifen.
          Werde aktiv und lass Dich beraten.
        </p>

        <ArztefinderTeaser />
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Hand"
    metaTitle="Neurodermitis an der Hand"
    description="Neurodermitis ist mehr als ein körperliches Leiden, ein Handekzem kann auch Dein soziales Leben beeinträchtigen. Erfahre mehr über Neurodermitis an den Händen."
    trustKeyWords="Neurodermitis, Händen, Juckreiz, Pflege, Symptome, Ekzeme, Atopische, Hautpflege, Trockene, Creme, Behandlung, Handekzem"
  />
);
