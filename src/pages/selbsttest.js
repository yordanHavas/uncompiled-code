import { MobileContext } from '../components/Layout';
import React, { useContext } from 'react';
import Hero from '../components/Hero';
import HeadingWithColored from '../components/HeadingWithColored';
import ResponsiveImage from '../components/ResponsiveImage';
import ImageWithText from '../components/ImageWithText';
import Button from '../components/Button';
import InfoAndClickableText from '../components/InfoAndClickableText';

const Selbsttest = () => {
  const isMobile = useContext(MobileContext);
  return (
    <>
      <div className="havas-container light">
        <HeadingWithColored
          textList={['Selbsttest']}
          coloredWordsArr={['test']}
          variant="h1"
        />
        <ImageWithText
          imgName={'Selbsttest_img_1'}
          altImage="Junge Frau sitzt am Laptop"
        />

        <section class="self-test">
          <iframe
            loading="lazy"
            id="self-test"
            width="100%"
            src="https://adct.peercode.nl/leben-mit-neurodermitis/de-de"></iframe>
        </section>
      </div>
      <div className="havas-container dark">
        <h2>Ist deine Neurodermitis Unkontrolliert ?</h2>
        <p>
          Hier findest Du hilfreiche Services und Informationen, die Dich dabei
          unterstützen können, Deine Neurodermitis in den Griff zu bekommen.
        </p>
        <ImageWithText
          imgName={'GettyImages-539669603_gk'}
          altImage="Ein junges Mädchen lächelt in die Kamera, im Hintergrund ist
          eine Ärztin zu sehen">
          <h3>Informiere Dich über Behandlungsmöglichkeiten</h3>
          <p>
            Wenn Deine Neurodermitis unkontrolliert ist, solltest Du Deinen
            Therapiestatus mit einem Dermatologen besprechen. Damit Du gut
            gerüstet in das Gespräch gehst, ist es sinnvoll Dir vorab einen
            Überblick über die aktuelle Therapiemöglichkeiten zu verschaffen.
            Die medizinischer Forschung entwickelt sich stetig weiter und
            inzwischen stehen auch langfristige Behandlungsmöglichkeiten zur
            Verfügung. Informiere Dich hier.
          </p>
          <Button text={'Mehr erfahren'} type="primary" to={'/ueberblick'} />
        </ImageWithText>
        <ImageWithText
          reverseOnDesktop={true}
          imgName={'hand_img_5'}
          altImage="Eine Ärztin im Gespräch mit einer Patientin">
          <h3>Finde einen Experten</h3>
          <p>
            Um die richtige Therapie für Dich zu finden, ist der Austausch mit
            einem Dermatologen wichtig. Du hast noch keinen passenden Experten
            gefunden? Dann informier Dich hier über Möglichkeiten in einer Nähe.
          </p>
          <Button text={'Mehr erfahren'} type="primary" to={'/aerztefinder'} />
        </ImageWithText>
        <ImageWithText
          imgName={'teledermatologie_img_1'}
          altImage="Eine junge Frau mit Kaffeetasse und Handy">
          <h3>Teledermatologie</h3>
          <p>
            Du hast Probleme einen Dermatologen in Deiner Nähe zu finden oder
            einen Termin zu bekommen. Informiere Dich hier über schnelle
            digitale Kontaktmöglichkeiten zu Fachärzten.
          </p>
          <Button
            text={'Mehr erfahren'}
            type="primary"
            to={'/teledermatologie'}
          />
        </ImageWithText>
      </div>
      <div className="havas-container light">
        <h2>Unser Neurodermitis Begleiter Team ist für Dich da</h2>
        <p>
          Neurodermitis kann für Betroffene und Angehörige eine große
          Herausforderung sein. Zögere daher nicht, Deinen Dermatologen oder das
          Behandlungsteam anzusprechen.
        </p>

        <p>
          Hast Du akut Fragen zu Therapieoptionen oder benötigst Tipps für den
          Alltag mit Neurodermitis, stehen Dir unsere medizinisch ausgebildeten
          Ansprechpartnerinnen Karin, Anna und Claudia beratend zur Verfügung.
          Diese Service ist für Dich kostenfrei.
        </p>
        <InfoAndClickableText />
      </div>
    </>
  );
};

export default Selbsttest;
