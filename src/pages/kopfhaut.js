import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Akkordeon from '../components/Akkordeon';
import Bulletlist from '../components/Bulletlist';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import TippsBox from '../components/TippsBox';

export default function Kopfhaut() {
  const isMobile = useContext(MobileContext);

  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={['Neurodermitis', 'auf der', 'Kopfhaut']}
          coloredWordsArr={['Kopfhaut']}
          color="yellow"
          imgName="kopfhaut_img_1"
          altImg="Frau untersucht im Spiegel ihre Kopfhaut"
          text="Wenn die Kopfhaut schuppt und juckt, kann das ganz schön nerven. Eine Ursache dafür kann Neurodermitis sein. Wir erläutern im folgenden Artikel, wie sich Neurodermitis auf der Kopfhaut äußert, wie die chronisch-entzündliche Erkrankung entsteht, wie sich die Kopfhaut von der Haut am Körper unterscheidet, was Du bei Neurodermitis auf der Kopfhaut tun kannst und vieles mehr. "
        />
        <p>
          Unser Kopf ist ein ziemlich wichtiger Teil unseres Körpers – sozusagen
          die Hauptsache. Hier laufen alle Fäden zusammen und alle
          Körperfunktionen werden koordiniert und organisiert. Weil der Kopf so
          wichtig ist, ist er auch besonders geschützt und unser Schädel kann so
          einiges vertragen. Wenn es der Kopfhaut allerdings nicht gut geht und
          sie wegen Deiner Neurodermitis schuppt, spannt und juckt, kann das
          ganz schön auf die Nerven gehen.
        </p>
        <MobileThemenübersicht className="dropdown-1" />
      </div>
      <div
        className="havas-container light"
        data-summary="Kann man Neurodermitis auf der Kopfhaut haben?">
        <h2 className="w-100">
          Kann man Neurodermitis auf der Kopfhaut haben?
        </h2>
        <p>
          Neurodermitis ist eine chronisch-entzündliche Erkrankung, die an
          vielen Hautstellen auftreten kann – auch auf der Kopfhaut. Sie kommt
          meist in Schüben und wird auch atopische Dermatitis und atopisches
          Ekzem genannt.
        </p>
        <p>
          Schuppen, trockene Stellen und/oder Juckreiz am Kopf müssen allerdings
          nicht von einer Neurodermitis kommen. Es gibt auch Schuppenprobleme,
          die nicht von einer Krankheit verursacht werden. Welche Hinweise für
          eine atopische Dermatitis sprechen, kannst Du im folgenden Kapitel
          nachlesen.
        </p>
      </div>
      <div
        data-summary="Wie äußert sich Neurodermitis auf der Kopfhaut?"
        className="havas-container dark">
        <h2 className="w-100">
          Wie äußert sich Neurodermitis auf der Kopfhaut?
        </h2>
        <p>
          Zu den typischen Symptomen der Neurodermitis gehören extrem trockene
          Hautstellen, die unerträglich jucken. Auf der Kopfhaut können ein
          starkes Spannungsgefühl und weißliche Schuppen hinzukommen. Anzeichen,
          die für eine Neurodermitis als Ursache der Schuppen sprechen, sind:
        </p>
        <Bulletlist
          textList={[
            'Die Schuppen bei Neurodermitis sind meist recht dick und groß und es entstehen schnell neue, wenn Du sie weggekratzt hast.',
            'Wenn juckende Stellen nach dem Kratzen zu nässen beginnen und sich Wundschorf bildet, weist das auch auf Neurodermitis hin.',
            'Auch wenn andere Körperstellen bei Dir von Neurodermitis betroffen sind, liegt es nahe, dass sich die chronisch-entzündliche Erkrankung ebenfalls auf der Kopfhaut zeigt.',
          ]}
        />
        <div style={{ marginTop: `${isMobile ? '-60px' : ''}` }}>
          <ImageWithText
            imgName="kophaut_44"
            altImage="Ärztin mit Patient im Gespräch">
            <p>
              Sprich mit einem Dermatologen, falls Du unsicher bist. Eine klare
              Diagnose ist wichtig, denn wenn Du Anti-Schuppen-Produkte oder
              Produkte gegen fettige Kopfhaut bei einer Neurodermitis nutzt,
              kann das Deinen Hautzustand und den Juckreiz verschlimmern.
            </p>
          </ImageWithText>
        </div>

        <h3>Haarausfall bei Neurodermitis?</h3>
        <p>
          Ein mögliches Symptom der Neurodermitis ist kreisrunder Haarausfall.
          Dazu muss es aber nicht kommen. Es ist auch bisher nicht sicher, ob
          Alopecia areata, so der medizinische Fachbegriff, wirklich im
          Zusammenhang mit Neurodermitis steht oder ob der Haarverlust doch
          andere Ursachen hat. Das atopische Ekzem und andere entzündliche
          Erkrankungen der Kopfhaut werden unter Experten jedoch als Auslöser
          für den Haarausfall diskutiert.
        </p>
      </div>
      <div
        data-summary={'Wie entsteht Neurodermitis auf der Kopfhaut?'}
        className="havas-container light">
        <h2 className="w-100">Wie entsteht Neurodermitis auf der Kopfhaut?</h2>
        <p>
          Die genauen Ursachen für Neurodermitis sind noch nicht vollständig
          erforscht. Wissenschaftler auf der ganzen Welt sind ihnen jedoch auf
          der Spur. Man weiß heute schon: Neurodermitis zeigt sich zwar auf der
          Haut, es ist aber keine reine Hautkrankheit. Sie geht mit einer
          Überempfindlichkeit des Immunsystems gegen eigentlich harmlose Stoffe
          einher. Meist spielt auch eine erbliche Vorbelastung eine Rolle. Zudem
          können Umwelteinflüsse und eine Störung der Hautbarriere dazu
          beitragen, dass die Krankheit ausbricht oder Schübe ausgelöst werden.
        </p>
        <Link to="/ursachen">
          Detaillierte Informationen zu den Ursachen der Neurodermitis kannst Du
          hier nachlesen.
        </Link>
        <p>
          Umweltfaktoren oder äußere Einflüsse, die Neurodermitis-Schübe
          auslösen können, werden auch Trigger oder Provokationsfaktoren
          genannt.
        </p>
        <Bulletlist
          className="violet"
          heading="Das sind zum Beispiel:"
          textList={[
            'Falsche oder übermäßige Haut- und Haarreinigung',
            'Shampoos oder Pflegeprodukte mit Duft- und Konservierungsstoffen',
            'Mechanische Beanspruchungen',
            'Stehende Feuchtigkeit, z. B. unter einem Hut oder einer Mütze',
            'Klima, z. B. geringe Luftfeuchtigkeit in beheizten Räumen',
            'Stress, z. B. Lärm, Leistungsdruck',
            'Allergene, z. B. Pflanzenpollen, Tierhaare oder auch einige Nahrungsmittel',
          ]}
        />
        <TippsBox
          text={
            'Für Menschen mit Neurodermitis gibt es spezielle Schulungen. Sicher findest Du auch entsprechende Programme in Deiner Nähe. Die Kosten werden meist übernommen.'
          }
          heading={'Unser tipp'}
        />

        <SelbsttestTeaser />
      </div>
      <div
        data-summary={
          'Wie unterscheidet sich die Kopfhaut von der Haut am Körper?'
        }
        className="havas-container dark">
        <h2>Wie unterscheidet sich die Kopfhaut von der Haut am Körper?</h2>
        <p>
          Unter Deinen Kopfhaaren herrschen andere Bedingungen als am restlichen
          Körper. An keiner Stelle ist Deine Haut so dick wie hier. Sie ist Teil
          der sogenannten Kopfschwarte. Das ist ein Gewebeverbund, der das
          Schädeldach überzieht und in erster Linie zum Schutz dient. Die
          Kopfhaut ist aber auch an der Regulierung unseres Wasser-haushalts und
          unserer Körpertemperatur beteiligt. Dafür besitzt sie besonders viele
          Talg- und Schweißdrüsen.
        </p>
        <p>
          Dann gibt es da noch die vielen Haarfollikel. Das sind die Strukturen,
          die Deine Haarwurzeln umgeben und die Haare in der Haut verankern.
          Auch die Haare schützen Deinen Kopf, vor allem vor Kälte und
          Sonneneinstrahlung. Für Deine Schaltzentrale ist diese Schutzfunktion
          auch gut, für Deine Neurodermitis-Haut jedoch nicht unbedingt.
        </p>

        <Bulletlist
          textList={[
            'Gerade im Sommer, wenn es warm ist, bildet sich vermehrt Schweiß. So entsteht ein feuchtwarmes Mikroklima und das ist ein perfekter Nährboden für Bakterien und Pilze.',
            'Außerdem sind die zahlreichen Haarfollikel eine Eintrittspforte, durch die Reizstoffe in den Körper eindringen können.',
            'Durch die Haare lassen sich Pflegeprodukte auch schlecht auf der Kopfhaut verteilen und Rückstände können an den Haaren haften bleiben.',
          ]}
        />
      </div>
      <div
        data-summary={'Was tun bei Neurodermitis auf der Kopfhaut?'}
        className="havas-container light">
        <h2>Was tun bei Neurodermitis auf der Kopfhaut?</h2>
        <ImageWithText
          altImage="Frau steht unter der Dusche und massiert Shampoo in die Kopfhaut ein"
          imgName="kopfhaur_img_3">
          <p>
            Die Pflege der Kopfhaut ist eine besondere Herausforderung. Was
            Deiner Haut guttut, kann Deine Frisur ruinieren, und was Deinem Haar
            Halt und Fülle gibt, kann schlecht für Deine Kopfhaut sein. Statt
            Cremes können geeignete Shampoos zur Pflege der Kopfhaut eingesetzt
            werden.
          </p>
        </ImageWithText>

        <h3>
          Welches Shampoo ist bei Neurodermitis auf der Kopfhaut geeignet?
        </h3>
        <p>
          Ein Shampoo sollte gut Fett lösen, darf aber gleichzeitig die
          empfindliche Haut nicht austrocknen. Beruhigend und
          feuchtigkeitsbindend wirken zum Beispiel milde Urea- oder
          Glycerin-haltige Shampoos. Am besten nutzt Du Pflegeshampoos ohne
          Silikon, Konservierungsstoffe oder Duftstoffe. Es gibt auch Shampoos
          mit Wirkstoffen zur Zuckreizlinderung. Sogenannte Leave-in-Produkte
          mit Zusatzstoffen, Spülungen, Haarspray oder Schaumfestiger trocknen
          die Kopfhaut meist aus. Die solltest Du besser vermeiden.
        </p>
        <ClickableBox
          title="Gut zu wissen"
          header="No-Poo – kann ich auf Shampoos verzichten?">
          <p>
            No-Poo ist die Abkürzung von „No Shampoo“. Anhänger dieser Methode
            verzichten komplett auf Shampoo und reinigen ihre Haare mit
            hautfreundlichen Alternativen. Manche „No-Pooler“ verzichten sogar
            ganz aufs Haarewaschen. Ob dieser neue Trend der gestressten
            Neurodermitis-Kopfhaut hilft, wurde bisher nicht wissenschaftlich
            untersucht. Falls Du es mal ausprobieren möchtest, dann suche im
            Internet einfach nach „No-Poo“. Dort findest Du zahlreiche
            Informationen und Tipps.
          </p>
          <p>
            Meist musst Du verschiedene Shampoo-Alternativen ausprobieren, bis
            Du ein für Dich passendes Mittel gefunden hast, denn jedes Haar und
            jede Kopfhaut ist anders. Aber vielleicht lohnt sich der Aufwand.
            Für die Umwelt ist es sicher gut, denn Du sparst jede Menge Plastik.
          </p>
        </ClickableBox>
        <h3>Das solltest Du beim Haarewaschen beachten</h3>
        <p>
          Haarewaschen steht am besten unter dem Motto: wenig und lauwarm.
          Übermäßiger Kontakt mit Wasser verringert die natürliche
          Schutzbarriere der Haut. Dennoch solltest Du Shampoos immer gut
          ausspülen, sonst können Shampoo-Reste auf der Kopfhaut verbleiben und
          das kann brennen oder jucken. Du solltest Deine Haare deshalb nicht zu
          häufig waschen.
        </p>
        <p>
          Verwende am besten lauwarmes Wasser. Zu heißes oder zu kaltes Wasser
          kann die Kopfhaut unnötig reizen. Falls Du den Wasserstrahl einstellen
          kannst, mit dem Du Deine Haare wäschst, kannst Du die mechanische
          Belastung verringern, indem Du einen weichen Wasserstrahl wählst.
          Haarewaschen unter der Dusche ist in der Regel auch besser als im
          Vollbad, da Deine Kopfhaut dabei mit weniger Wasser in Kontakt kommt.
        </p>
        <p>
          Wer nicht auf Pflegeprodukte wie Conditioner und Kuren verzichten
          möchte, sollte diese nur in den Haarspitzen verteilen. Auch diese
          Produkte sollten frei von Duft- und Konservierungsstoffen sein.
        </p>

        <TippsBox
          text={
            'Generell gilt: Je weniger Inhaltsstoffe Pflegeprodukte haben, umso weniger Gefahr besteht, dass sie die Kopfhaut reizen.'
          }
          heading={'Unser tipp'}
        />
        <h3>Und nach dem Haarewaschen?</h3>
        <ImageWithText
          altImage="Eine Junge Frau im Bad, die sich am Unterarm kratzt"
          imgName="kopfhaur_img_4">
          <p>
            Vermeide es, nach dem Haarewaschen die Kopfhaut trocken zu rubbeln.
            Zum Vortrocknen kannst Du Deine Haare mit einem Handtuch stramm um
            den Kopf wickeln. Lass sie danach am besten an der Luft trocknen.
            Wenn Du Deine Haare föhnst, solltest Du das auf niedrigster
            Wärmestufe tun. Lockenstab oder Glätteisen solltest Du vermeiden.
          </p>
        </ImageWithText>

        <h3>Wie kann ich meine Kopfhaut noch pflegen?</h3>
        <p>
          Zwar sind Leave-in-Produkte mit Duft- oder Konservierungsstoffen keine
          gute Idee, es gibt jedoch auch Produkte für die Kopfhaut, die diese
          pflegen und ihr Feuchtigkeit spenden. Sie werden meist nach dem
          Waschen auf die trockene oder feuchte Kopfhaut aufgetragen und nicht
          wieder ausgespült. Lass Dich von Deinem Dermatologen beraten oder
          frage einen Apotheker.
        </p>
        <h3>Kann ich meine Haare bei Neurodermitis auf der Kopfhaut stylen?</h3>
        <p>
          Generell solltest Du verhindern, dass Deine Kopfhaut weiter
          austrocknet und mit reizenden Stoffen in Kontakt kommt. Deshalb
          empfiehlt es sich bei Neurodermitis, die auch atopische Dermatitis
          genannt wird, das Haar nicht zu färben oder zu tönen. Auch Dauerwellen
          sind in der Regel keine gute Idee.
        </p>
        <p>
          Beim Kämmen und Bürsten der Haare solltest Du Vorsicht walten lassen.
          Die mechanische Reizung der Kopfhaut kann diese sonst weiter belasten.
        </p>
        <p>
          Auch auf Stylingprodukte solltest Du nach Möglichkeit verzichten.
          Falls Du sie anwenden möchtest, dann gib sie nur in die Haarspitzen
          und verwende Produkte ohne Duft- und Konservierungsstoffe.
        </p>
        <ClickableBox
          title="Gut zu wissen"
          header="Hut ab bei Neurodermitis auf der Kopfhaut">
          <p>
            Für Menschen mit Neurodermitis können Kopfbedeckungen wie Mützen
            oder Kappen problematisch sein. Bakterien und Pilze schätzen das
            feuchtwarme Klima darunter und können Infektionen verursachen.
          </p>
        </ClickableBox>
        <ArztefinderTeaser />
      </div>
      <div
        data-summary={
          'Naturkosmetik und Hausmittel bei Neurodermitis auf der Kopfhaut'
        }
        className="havas-container dark">
        <h2>Naturkosmetik und Hausmittel bei Neurodermitis auf der Kopfhaut</h2>
        <p>
          Bei Naturkosmetika solltest Du eher vorsichtig sein. Laut Deutschem
          Allergie- und Asthmabund (DAAB) bergen sie manchmal Inhaltsstoffe mit
          hohem Allergiepotenzial. Auch manche selbstgemachte Tinkturen aus
          Kräutern können Allergien auslösen. Es gibt jedoch einige Hausmittel,
          die für Deine neurodermitisgeplagte Kopfhaut gut geeignet sind:
        </p>

        <Akkordeon
          heading={'Sauer macht nicht nur lustig'}
          open={true}
          className="akkordeonComponent">
          <p>
            Wenn die Kopfhaut nach der Haarwäsche spannt, kannst Du Haut und
            Haare mit Apfelessig spülen.{' '}
            <b style={{ fontWeight: '900' }}>Aber Achtung</b>: Die Säure kann
            die Kopfhaut auch reizen und die Haare austrocknen. Das passiert,
            wenn Du große Mengen unverdünnten Apfelessig verwendest und die
            empfohlene Einwirkzeit überschreitest. Sprich am besten mit einem
            Arzt, ob und wie Du Apfelessig anwenden solltest.
          </p>
        </Akkordeon>
        <Akkordeon
          heading={'Was soll der Quark?'}
          className="akkordeonComponent">
          <p>
            Mit einer Quarkpackung kannst Du Deine trockene Kopfhaut besonders
            pflegen. Der Quark beruhigt gespannte Haut und versorgt die Haut mit
            Feuchtigkeit. In den Quark kannst Du auch ein Eigelb oder
            Aloe-vera-Gel unterrühren. Damit der Quark streichfähig wird,
            solltest Du ihn mit einem pflegenden Öl oder etwas Wasser verdünnen.
            Verteile die Quarkpackung großzügig auf der Kopfhaut und massiere
            sie leicht ein. Nach 20 bis 30 Minuten kannst Du alles wieder
            gründlich mit lauwarmem Wasser auswaschen.
          </p>
        </Akkordeon>
        <Akkordeon heading={'Gieß Öl in Feuer'} className="akkordeonComponent">
          <p>
            Auch eine Öl-Kur mit ungesättigten Fettsäuren gibt der Haut Fett und
            Feuchtigkeit zurück. Dafür eignen sich ein spezielles Olivenöl für
            die Haare, Sonnenblumenöl, Sesamöl, Jojobaöl oder Mandelöl. Verteile
            das Öl am Abend auf Deiner Kopfhaut und lass es über Nacht
            einwirken. Am nächsten Morgen kannst Du es gründlich ausspülen. Du
            kannst auch etwas Öl in Dein Shampoo geben.
          </p>
        </Akkordeon>
      </div>
      <div
        data-summary={
          'Wie wird Neurodermitis auf der Kopfhaut medikamentös behandelt?'
        }
        className="havas-container light">
        <h2>Wie wird NEURODERMITIS auf der Kopfhaut medikamentös behandelt?</h2>
        <p>
          Bei einem akuten Schub mit stark entzündeter Kopfhaut können
          wirkstoffhaltige Shampoos oder Haartinkturen mit entzündungshemmenden
          Eigenschaften eingesetzt werden. Diese solltest Du aber nur bei Bedarf
          und nur kurzzeitig nutzen, da die empfindliche Kopfhaut sonst
          möglicherweise mit Irritationen reagiert.
        </p>
        <TippsBox
          text={
            'Halte Dich bei der Anwendung von wirkstoffhaltigen Shampoos immer an die Anweisungen Deines Dermatologen oder frage einen Apotheker.'
          }
          heading={'Unser tipp'}
        />
        <p>
          Neben diesen sogenannten{' '}
          <Link to="/aeusserliche-therapien">topischen Präparaten</Link> die
          äußerlich und direkt an der Stelle der Beschwerden angewendet werden,
          können auch systemische Medikamente zum Einsatz kommen. Diese werden
          über den Blutkreislauf im Körper verteilt und erreichen so ihren
          Wirkort. Wenn das Medikament direkt in den Körper gelangen soll,
          werden systemische Medikamente als Tabletten eingenommen oder
          gespritzt. Systemische Mittel werden in der Regel langfristig
          angewendet und kommen nicht nur bei einem akuten Schub zum Einsatz.
        </p>
        <Link to="/innerliche-therapien">
          Weitere hilfreiche Infos findest Du auf der Seite ,,Innerliche
          Therapien"
        </Link>

        <ImageWithText
          imgName="kopfhaur_img_5"
          altImage="Eine Ärztin im Gespräch mit einer Patientin">
          <p>
            Die Neurodermitis ist nicht bei allen Betroffenen gleich. Deshalb
            ist es wichtig, Deine Therapie an Deinen Krankheitsverlauf und Deine
            Bedürfnisse anzupassen. Sprich mit Deinem Dermatologen und schildere
            Deine Beschwerden möglichst genau. Es ist hilfreich, Deine Symptome
            und Hautzustände regelmäßig in Form eines Tagebuchs zu
            dokumentieren. Nur wenn Dein Arzt Deine Situation kennt, könnt Ihr
            gemeinsam ein passendes Therapiekonzept für Dich finden. Trau Dich
            auch ruhig nachzufragen, wenn Du etwas nicht verstanden hast. Denke
            daran: Wenn Du Deine Neurodermitis gut verstehst, kann Dir das beim
            Umgang mit ihr helfen.
          </p>
        </ImageWithText>

        <SelbsttestTeaser />
      </div>
      <div
        data-summary={
          'Was kannst Du sonst noch gegen Neurodermitis auf der Kopfhaut tun?'
        }
        className="havas-container dark">
        <h2>
          Was kannst du sonst noch gegen neurodermitis auf der kopfhaut tun?
        </h2>

        <ImageWithText
          imgName="greenPulover"
          altImage="Mann kratzt sich mit verzerrtem Gesicht den Kopf">
          <h3>Trigger – die gilt es zu vermeiden</h3>
          <p>
            Um Neurodermitis-Schübe zu vermeiden, ist es zunächst wichtig
            herauszufinden, was sie auslöst. Wer seine individuellen Trigger
            kennt, kann sie auch aus seinem Alltag verbannen. Ein Tagebuch kann
            Dir helfen, herauszufinden, was einen Schub bei Dir triggert. Sprich
            auch mit Deinem Arzt, denn es gibt verschiedene Tests, die zeigen
            können, ob Du auf bestimmte Stoffe allergisch reagierst.
          </p>
        </ImageWithText>
        <h3>Stress – nicht immer zu vermeiden</h3>
        <p>
          Stress solltest Du vermeiden, soweit das eben geht. Der zählt zu den
          Neurodermitis-Triggern und ist auf Dauer generell nicht gut für Deine
          Gesundheit. Aber wie vermeidet man Stress? Das klappt leider nicht
          immer, denn Stress lässt sich manchmal nicht umgehen. Stress ist aber
          vor allem dann problematisch, wenn Du keinen Ausgleich schaffst.
          Deshalb ist es wichtig, in Deinem Alltag auch mal runterzukommen. Gönn
          Dir etwas Zeit für Dich selbst und sei gut zu Dir. Übrigens: Eine
          positive Einstellung zu Dir und Deinem Leben ist meist auch hilfreich
          für den Umgang mit Deiner Erkrankung.
        </p>
        <a href="/lernplattform">
          Wie Du mit Stress umgehen kannst, kannst Du im Lernmodul „Tipps für
          eine bessere Stressbewältigung“ nachlesen.
        </a>
        <h3>Iss das gesund? Neurodermitis auf der Kopfhaut und Ernährung</h3>

        <ImageWithText imgName="kopfhaur_img_7" altImage="Ein Kürbisgericht">
          <p>
            Eine ausgewogene Ernährung kann zu Deinem Wohlbefinden beitragen und
            Dir so beim Umgang mit Deiner Erkrankung helfen. Zudem zählen einige
            Lebensmittel zu den möglichen Triggern. Weil die Auslöser der
            Neurodermitis aber sehr individuell sind, gibt es keine generelle
            „Neurodermitis-Diät". Es gibt jedoch einige allgemeine Regeln einer
            ausgewogenen und abwechslungsreichen Ernährung.
          </p>
          <p>
            Welche das sind und weitere hilfreiche Infos zum Thema Ernährung
            findest Du im Lernmodul{' '}
            <a href="/lernplattform">„Ernährung bei Neurodermitis"</a>.
          </p>
        </ImageWithText>

        <h3>Was tun gegen den Juckreiz?</h3>
        <p>
          Bei Neurodermitis auf der Kopfhaut kann der Juckreiz Betroffene in den
          Wahnsinn treiben. Kratzen hilft allerdings nicht, dadurch wird die
          empfindliche Haut nur geschädigt, es können Schadstoffe in die Haut
          eindringen und das führt letztlich zu mehr Juckreiz. Versuche es
          stattdessen mit Kneten, Klopfen oder einer Kühlkompresse – so bleibt
          Deine Haut intakt. Auch Ablenkung kann bei Juckreiz helfen.
        </p>
        <p>
          Wie Du Dich vom Juckreiz ablenken kannst, kannst Du im Lernmodul
          <a href="/lernplattform/elearning-9">
            {' '}
            „Ablenkungstechniken bei Juckreiz“{' '}
          </a>
          nachlesen.
        </p>
      </div>
      <div data-summary={'Fazit'} className="havas-container light">
        <h2 className="w-100" style={isMobile ? { width: '100%' } : {}}>
          Fazit
        </h2>
        <p>
          Wenn die Neurodermitis Dir den Kopf verdreht und Du an nichts anderes
          mehr denken kannst als an Deine juckende Kopfhaut, solltest Du aktiv
          werden. Die Neurodermitis ist zwar eine chronische Erkrankung, aber
          heutzutage gibt es viele Pflegeprodukte und moderne Therapieoptionen,
          die Dir helfen können, Deine Erkrankung zu kontrollieren. Informiere
          Dich und lass Dich von Deinem Dermatologen beraten.{' '}
          <Link to="/ueberblick">
            Hier erhältst Du einen Überblick über aktuelle Therapieoptionen.
          </Link>
        </p>
        <ArztefinderTeaser />
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Kopfhaut"
    metaTitle="Neurodermitis auf der Kopfhaut"
    description="Wenn die Kopfhaut, schuppt und juckt, kann das ganz schön nerven. Ist das vielleicht Neurodermitis? Hier findest Du Antworten und kannst Dich informieren."
    trustKeyWords="Neurodermitis, Kopfhaut, Juckreiz, Dermatitis, Shampoo, Schuppen, Haare, Symptome, Trockene, Haarausfall, Pflege, Empfindliche Haut"
  />
);
