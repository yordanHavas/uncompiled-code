import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Bulletlist from '../components/Bulletlist';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import Slider from '../components/Slider';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';

export default function Ursachen() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={['Neurodermitis-', 'Ursachen']}
          coloredWordsArr={['Ursachen']}
          color="#60e2e3"
          imgName="ursachen_img_1"
          altImg="Eine Junge Fau, die etwas auf einem iPad liest"
          text="Neurodermitis ist eine chronisch-entzündliche Erkrankung. Sie zeigt sich überwiegend auf der Haut, ist aber nicht darauf beschränkt. Die atopische Dermatitis, wie Neurodermitis auch genannt wird, kann sich auf viele Lebensbereiche auswirken und sowohl den Körper als auch die Psyche beeinträchtigen. Die genauen Ursachen sind noch nicht vollständig geklärt. Klar ist, dass mehrere Faktoren dazu beitragen, dass die Krankheit ausbricht bzw. Schübe ausgelöst werden, und dass eine Überreaktion des Immunsystems immer wieder Entzündungen entfacht."
        />

        <MobileThemenübersicht></MobileThemenübersicht>

        <p>
          Forscher weltweit arbeiten daran, die genauen Ursachen der
          Neurodermitis zu entschlüsseln, um betroffenen Menschen besser helfen
          zu können. Denn je besser die Zusammenhänge erforscht sind, die eine
          Neurodermitis auslösen, umso besser können maßgeschneiderte
          Therapieansätze entwickelt werden. Schon heute gibt es{' '}
          <Link to="/innerliche-therapien">moderne Medikamente</Link>, die nicht
          nur die Symptome lindern, sondern an den bekannten Ursachen der
          Erkrankung ansetzen.
        </p>
      </div>
      <div
        className="havas-container dark"
        data-summary="Welche Ursachen hat Neurodermitis?">
        <h2 className="w-100">Welche Ursachen hat Neurodermitis?</h2>

        <p>
          Auch wenn noch nicht genau geklärt ist, was Neurodermitis verursacht,
          haben Wissenschaftler 4 Komponenten identifiziert, die eine
          Neurodermitis begünstigen können:
        </p>

        <Bulletlist
          textList={[
            'Genetische Veranlagung',
            'Überempfindlichkeit des Immunsystems',
            'Gestörte Barrierefunktion der Haut',
            'Verschiedene Umweltfaktoren',
          ]}
        />

        <h3 style={isMobile ? { minWidth: 287 } : {}}>
          Genetische Veranlagung
        </h3>

        <p>
          Neurodermitis gehört zum sogenannten atopischen Formenkreis. Dazu
          gehören unter anderem allergisches Asthma, allergische
          Bindehautentzündung und allergischer Schnupfen (allergische Rhinitis).
        </p>

        <ImageWithText
          className={`${!isMobile ? 'mt-3' : ''}`}
          imgName="verstehen_img_3"
          altImage="Grafische Darstellung des atopischen Formenkreises">
          <p>
            All diese Erkrankungen haben eine genetische Komponente, sie selbst
            werden jedoch nicht vererbt, sondern eine Veranlagung, dass das
            Immunsystem überempfindlich reagiert — eine sogenannte Atopie. Die
            wohl bekannteste Erscheinungsform hiervon sind Allergien, zum
            Beispiel Nahrungsmittelallergien oder Heuschnupfen. Bei einer Atopie
            verursachen eigentlich harmlose Stoffe, mit denen der Körper in
            Kontakt kommt, eine allergische Reaktion, die verschiedene
            Beschwerden hervorrufen kann. Bei der Neurodermitis zeigen sich die
            Symptome in der Regel auf der Haut.
          </p>
        </ImageWithText>

        <ClickableBox
          title="Gut zu wissen"
          header="Weitere Bezeichnungen für Neurodermitis">
          <p>
            Neurodermitis wird auch atopische Dermatitis oder atopisches Ekzem
            genannt.
          </p>
        </ClickableBox>

        <p>
          Das Risiko, an einer solchen Überempfindlichkeit des Immunsystems zu
          erkranken, erhöht sich deutlich, wenn ein oder sogar beide Elternteile
          eine solche Veranlagung haben.
        </p>

        <Slider
          imageDesktopHeight={'241px'}
          items={[
            {
              imgName: 'ursachen_slider_1',
              altImage:
                'Grafische Darstellung des erblichen Erkrankungsrisikos bei Neurodermitis',
              text: [
                <strong>
                  Beide Eltern leiden an der gleichen atopischen Erkrankung
                  <br />
                  <br />
                  Erkrankungsrisiko: 60-80%
                </strong>,
              ],
            },
            {
              imgName: 'ursachen_slider_1',
              altImage:
                'Grafische Darstellung des erblichen Erkrankungsrisikos bei Neurodermitis',
              text: [
                <strong>
                  Beide Eltern leiden an einer atopischen Erkrankung
                  <br />
                  <br />
                  Erkrankungsrisiko: 40-60%
                </strong>,
              ],
            },
            {
              imgName: 'ursachen_slider_2',
              altImage:
                'Grafische Darstellung des erblichen Erkrankungsrisikos bei Neurodermitis',
              text: [
                <strong>
                  Ein Elternteil leidet an einer atopischen Erkrankung
                  <br />
                  <br />
                  Erkrankungsrisiko: 20-40%
                </strong>,
              ],
            },
            {
              imgName: 'ursachen_slider_3',
              altImage:
                'Grafische Darstellung des erblichen Erkrankungsrisikos bei Neurodermitis',
              text: [
                <strong>
                  Kein Elternteil leidet an einer atopischen Erkrankung
                  <br />
                  <br />
                  Erkrankungsrisiko: 5-15%
                </strong>,
              ],
            },
          ]}
        />

        <p>
          Zudem sind aufgrund von genetischen Veränderungen bei manchen
          Betroffenen bestimmte Bausteine der Haut beschädigt. Das kann eine
          gestörte Barrierefunktion zur Folge haben.
        </p>

        <SelbsttestTeaser />

        <h3>Überempfindlichkeit des Immunsystems</h3>

        <p>
          Das Immunsystem ist das körpereigene Abwehrsystem. Seine Hauptaufgabe
          ist, unseren Körper vor Fremdstoffen und Krankheitserregern zu
          schützen. Dafür ist es bestens ausgestattet und hat ein breites
          Arsenal an Immunzellen, die unterschiedliche Funktionen erfüllen.
          Damit jede Reaktion des Immunsystems koordiniert und gezielt abläuft,
          müssen die Immunzellen miteinander kommunizieren. Dies geschieht
          mithilfe von verschiedenen Botenstoffen. Das sind spezielle Eiweiße,
          die von den Immunzellen produziert, freigesetzt und aufgenommen
          werden. Ihre Wirkung entfalten sie über spezifische Andockstellen auf
          Zellen, sogenannte Rezeptoren.
        </p>

        <h3>Neurodermitis ist mehr als eine Hauterkrankung</h3>

        <p>
          Bei Menschen mit Neurodermitis ist das Immunsystem des Körpers aus den
          Fugen geraten. Es kommt zu einer Fehlregulation und damit zu einer
          Immunantwort gegen eigentlich harmlose Substanzen. Bestimmte Zellen
          des Immunsystems, sogenannte Typ-2-Immunzellen, sind dann in einem
          andauernden Aktivierungszustand und produzieren vermehrt
          entzündungsfördernde Typ-2-Botenstoffe – vor allem Interleukin-4
          (IL-4) und Interleukin-13 (IL-13). Aufgrund des Überschusses dieser
          Typ-2-Botenstoffe ist das Immunsystem überaktiv und verursacht die
          andauernden und wiederkehrenden – also chronischen – Entzündungen der
          Haut. Das äußert sich bei Betroffenen durch die typischen
          Hautveränderungen und Juckreiz. Die Art von Entzündung, bei denen die
          Botenstoffe IL-4 und IL-13 beteiligt sind, bezeichnet man im
          Fachjargon als Typ-2-Entzündung.
        </p>

        <ClickableBox
          title="Gut zu wissen"
          header={
            isMobile ? (
              <span style={{ textAlign: 'center' }}>
                Was sind <br /> Interleukine?
              </span>
            ) : (
              'Was sind Interleukine?'
            )
          }>
          <p>
            Als Interleukine bezeichnet man verschiedene Botenstoffe des
            Immunsystems. Sie werden von bestimmten Immunzellen produziert und
            haben bei körperlichen Abwehrreaktionen ganz unterschiedliche
            Aufgaben. Interleukine regulieren z. B. die Abwehr gegen
            Krankheitserreger und fördern Entzündungsprozesse. Nach der
            Reihenfolge ihrer Entdeckung sind Interleukine in mehrere
            Untergruppen unterteilt, die durch Zahlen gekennzeichnet sind.
          </p>
        </ClickableBox>

        <h3>Störungen der Hautbarriere</h3>

        <p>
          Die menschliche Haut ist von einer Vielzahl an Mikroorganismen
          besiedelt, wie Bakterien und Pilzen. Bei gesunden Menschen leben diese
          dort in einem Gleichgewicht. Dieses sogenannte Hautmikrobiom ist
          wichtig für die Gesundheit der Haut. Es steht in ständigem Austausch
          mit der Hautbarriere und dem Immunsystem. Bei Menschen mit
          Neurodermitis ist dieses Gleichgewicht durch eine veränderte
          Zusammensetzung der Mikroorganismen gestört.
        </p>

        <p style={{ marginRight: `${isMobile ? '0px' : '89px'}` }}>
          Während die gesunde Haut eine natürliche Barriere zur Umwelt darstellt
          und Fremdstoffe wie Bakterien und Schadstoffe am Eindringen in den
          Körper hindert, ist diese Schutzfunktion bei Menschen mit
          Neurodermitis beeinträchtigt: Der Haut fehlen wichtige Bestandteile
          wie Feuchtigkeit, bestimmte Fette und Eiweiße. Diese veränderte
          Zusammensetzung macht die Haut durchlässiger, wodurch Fremdstoffe
          leichter eindringen und Immunreaktionen hervorrufen können.
        </p>

        <Slider
          className={'customImage'}
          minHeightDesktop="212px"
          items={[
            {
              imgName: 'ursachen_second_slider_1',
              heading: 'Intakte Hautbarriere',
              altImage:
                'Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis',
              text: [
                'Die ',
                <strong>intakte Haut</strong>,
                ' stellt eine natürliche und geschlossene Barriere zwischen der Innen- und Außenwelt dar. Für Fremdstoffe ist es fast unmöglich, sie im gesunden Zustand zu überwinden.',
              ],
            },
            {
              imgName: 'ursachen_second_slider_2',
              heading: 'Beeinträchtigte Hautbarriere',
              altImage:
                'Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis',
              text: [
                'Die ',
                <strong>Hautbarriere bei Neurodermitis</strong>,
                ' ist beeinträchtigt. Ihr fehlen wichtige Bestandteile, die zur Aufrechterhaltung der Schutzfunktion nötig sind (z.B. Feuchtigkeit). Fremdstoffe können eindringen und weitere Entzündungen hervorrufen.',
              ],
            },
            {
              imgName: 'ursachen_second_slider_3',
              heading: 'Überaktives Immunsystem',
              altImage:
                'Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis',
              text: [
                'Zudem ist das Immunsystem überaktiv und bestimme Immunzellen produzieren vermehrt Botenstoffe, die ',
                <strong>dauerhaft Entzündungs-reaktionen</strong>,
                ' auslösen.',
              ],
            },
          ]}
        />

        <ArztefinderTeaser />

        <h3>Einfluss von Umweltfaktoren</h3>

        <p>
          Umweltfaktoren sind äußere Faktoren, die das Krankheitsgeschehen
          beeinflussen können. Im Fachjargon spricht man von
          Provokationsfaktoren, weil sie das Auftreten der Symptome
          „provozieren“ können. Man nennt diese Faktoren auch Trigger oder
          Auslöser. Diese sind bei jedem Patienten individuell und können sich
          im Laufe der Zeit auch verändern.{' '}
        </p>

        <p>
          Im Gegensatz zu den anderen Ursachen sind die äußeren Auslöser
          teilweise beeinflussbar. So kann neben der Vermeidung von Stress und
          Allergenen die Wahl von Pflegeprodukten oder der Stoffe von
          Kleidungsstücken bereits zu einer Entlastung der Haut führen.{' '}
        </p>

        <ClickableBox
          title="Gut zu wissen"
          header="Welche Rolle spielen Allergien?">
          <p>
            Viele Neurodermitis-Patienten haben auch eine Allergie – z. B. gegen
            Pflanzenpollen, Tierhaare oder Hausstaubmilben. Substanzen, die eine
            Allergie auslösen, zählen auch zu Provokationsfaktoren. Sie sind
            eigentlich harmlos, werden aber vom Körper als fremd erkannt und vom
            Immunsystem „bekämpft“. Bei Patienten mit Neurodermitis kann der
            Kontakt mit Allergenen einen Neurodermitis-Schub provozieren.
          </p>
        </ClickableBox>
      </div>

      <div
        className="havas-container light"
        data-summary="Wann bricht eine Neurodermitis aus?">
        <h2>Wann bricht eine Neurodermitis aus?</h2>

        <ImageWithText
          className={`${!isMobile ? 'mt-2' : ''}`}
          imgName="ursachen_img_3"
          altImage="Nahaufnahme von geröteter Haut am Arm, die jemand kratzt">
          <p>
            Die atopische Dermatitis tritt meist in Schüben auf und äußert sich
            vor allem durch sichtbare Hautveränderungen und starken Juckreiz. Es
            gibt also Phasen, in denen juckende, entzündete Stellen Dein
            Hautbild bestimmen, und dann gibt es Zeiten, in denen Du kaum
            Beschwerden hast. Allerdings ist die Erkrankung selbst in weniger
            aktiven Phasen nicht weg. Wenn die Neurodermitis für das Auge nicht
            sichtbar ist, ist Dein Immunsystem weiterhin überaktiv und
            verursacht immer wieder Entzündungen. Welche Auslöser dann einen
            Neurodermitis-Schub provozieren, ist sehr individuell. Auch wie
            stark die einzelnen Schübe sind, wie lange sie dauern und wie häufig
            sie auftreten, ist von Mensch zu Mensch verschieden. Die Frage, wann
            eine Neurodermitis ausbricht, lässt sich also nicht eindeutig
            beantworten. Es gibt jedoch einige Faktoren, die bei vielen Menschen
            mit Neurodermitis Schübe auslösen können.{' '}
          </p>
        </ImageWithText>

        <p>
          Umweltfaktoren oder äußere Einflüsse, die sich häufig negativ auf den
          Verlauf der Neurodermitis auswirken oder einen Schub auslösen können,
          sind:
        </p>

        <Bulletlist
          firstColumnSize={3}
          textList={[
            'Falsche oder übermäßige Hautreinigung (z. B. häufiges Duschen bzw. Gebrauch von Seifen, Pflegeprodukten mit Duft- und Konservierungsstoffen)',
            'Zigarettenrauch und Umweltschadstoffe (z. B. Abgase)',
            'Kratzende Kleidung (z. B. aus Wollfasern oder Synthetikstoffen)',
            'Klima (z. B. geringe Luftfeuchtigkeit in Räumen durch Heizungsluft)',
            'Stress (z. B. Lärm, Leistungsdruck)',
            'Allergene (z. B. Pflanzenpollen, Tierhaare, bestimmte Nahrungsmittel)',
            'Infektionen (z. B. durch Viren, Bakterien oder Pilze)',
          ]}></Bulletlist>

        {/* <ResponsiveImage
          imgName="ursachen_img_4"
          className="image-br image-tilt"
          altImg="Nahaufnahme des Arms einer Frau unter der Dusche, die sich einseift"
          style={
            isMobile
              ? { marginTop: '-3em', marginBottom: 15, width: 270, height: 270 }
              : {
                  position: 'relative',
                  bottom: '6em',
                  width: '380px',
                  height: '380px',
                }
          }
        /> */}

        <p>
          Wenn Du Schüben vorbeugen möchtest, ist es wichtig, Deine Trigger zu
          kennen. Nur dann kannst Du sie gezielt vermeiden. Ein Tagebuch kann
          dabei helfen, Einflüsse zu erkennen, die sich negativ auf Deine
          Neurodermitis auswirken. Du kannst zum Beispiel Lebensmittel erkennen,
          die Du besser vermeiden solltest, oder Pflegeprodukte, die nicht gut
          für dich sind.
        </p>

        <Link
          to="/broschueren-und-downloads"
          style={{ textDecoration: 'underline', color: '#302156' }}>
          Eine Vorlage für ein Tagebuch kannst Du hier herunterladen.
        </Link>

        <SelbsttestTeaser />
      </div>
      <div
        className="havas-container dark"
        data-summary="Wie erkenne ich eine Neurodermitis?">
        <h2>Wie erkenne ich eine Neurodermitis?</h2>

        <p>
          Wie bereits erwähnt, äußert sich Neurodermitis vor allem durch
          Entzündungen der Haut.
        </p>

        <Bulletlist
          heading="Häufige Symptome sind unter anderem:"
          textList={[
            'Starker Juckreiz',
            'Entzündete, teilweise blutende Hautstellen (Ekzeme)',
            'Hautrötungen (Erytheme)',
            'Hautverdickungen und Vergröberungen der Haut (Lichenifikationen)',
            'Dicke, tiefsitzende Knoten (Prurigo-Knoten)',
            'Hauteinrissez. B. an Mundwinkeln, Ohrläppchen oder Fingern (Rhagaden)',
            'Trockene Haut (Xerose)',
            'Schuppung',
            'Schwellung',
            'Nässende Bläschen',
          ]}></Bulletlist>

        <p>
          Da einige Hauterkrankungen ähnliche Symptome hervorrufen wie
          Neurodermitis, kann in der Regel nur ein Dermatologe eine Diagnose
          stellen. Solltest Du immer wieder Hautausschlag haben und juckende,
          entzündete Stellen, kann das ein Zeichen für Neurodermitis sein. Dann
          solltest Du einen Hautarzt aufsuchen und die Ursachen für Deine
          Beschwerden abklären lassen.
        </p>

        <ImageWithText
          imgName="ursachen_img_5"
          altImage="Nahaufnahme, kleines Mädchen mit Neurodermitis am Hals">
          <p>
            Die Bereiche, an denen Neurodermitis-Symptome auftreten, kann sich
            im Krankheitsverlauf verändern und sind häufig vom Alter der
            Betroffenen abhängig. Im frühen Kindesalter sind Ekzeme im Gesicht,
            auf der Kopfhaut sowie an den Streckseiten von Armen und Beinen
            vorherrschend. Bei Jugendlichen und Erwachsenen treten in der Regel
            Ekzeme am Hals auf, an den Augenlidern, Ellenbogen, in den
            Kniekehlen sowie an den Füßen und Händen.
          </p>
        </ImageWithText>

        <Link to="/symptome" style={{ textDecoration: 'underline' }}>
          Detaillierte Informationen zu betroffenen Hautstellen kannst Du hier
          nachlesen.
        </Link>
      </div>

      <div className="havas-container light" data-summary="Fazit">
        <h2>Fazit</h2>

        <p>
          Die genauen Ursachen der Neurodermitis sind zwar noch nicht ganz
          geklärt, aber es gibt schon heute verschiedene langfristige
          Therapieoptionen, mit denen Du Deine Neurodermitis unter Kontrolle
          halten kannst. Auch wenn die Neurodermitis Dich vielleicht Dein ganzes
          Leben begleitet, kannst Du ein gutes Leben haben. Informiere Dich und
          lass Dich von Experten beraten. Auch der Austausch mit anderen
          Betroffenen kann eine Hilfe sein.{' '}
        </p>

        <ArztefinderTeaser />
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Ursachen"
    metaTitle="Neurodermitis Ursachen"
    description="Neurodermitis ist eine chronisch-entzündliche Hauterkrankung. Die Ursachen sind noch nicht vollständig erforscht, man weiss aber, dass mehrere Faktoren eine Rolle spielen."
    trustKeyWords="Ursachen Neurodermitis, Neurodermitis Auslöser, Neurodermitis Trigger, Neurodermitis Anzeichen, Neurodermitis Symptome erwachsene, Neurodermitis Schub Dauer, Hand Ekzem psychische Ursachen, Ekzem Ursachen psychisch, Neurodermitis Ursachen bei erwachsenen, Hautausschlag Neurodermitis, Neurodermitis Schub Auslöser, Neurodermitis am Auge Ursache, Atopie Symptome, allergische Reaktion Haut, Neurodermitis Lebensmittel vermeiden"
  />
);
