import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Akkordeon from '../components/Akkordeon';
import Bulletlist from '../components/Bulletlist';
import Button from '../components/Button';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import Slider from '../components/Slider';
import { ArztefinderTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import TippsBox from '../components/TippsBox';
import ZitatBox from '../components/ZitatBox';

export default function Uberblick() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={['Wie', 'Neurodermitis', 'behandelt ', 'wird']}
          coloredWordsArr={['behandelt']}
          imgName="uberblick_img_1"
          altImg="Ein junges Mädchen lächelt in die Kamera, im Hintergrund ist eine Ärztin zu sehen"
          text="Neurodermitis ist eine chronisch-
          entzündliche Erkrankung, die sich 
          überwiegend auf der Haut zeigt 
          und individuell sehr unterschiedlich ausgeprägt ist.
          Die Behandlung richtet sich deshalb nach dem Schweregrad Deiner Symptome. Um für Dich das optimale Behandlungsergebnis zu erreichen, sollte Deine 
          Neurodermitis-Therapie immer
          in enger Zusammenarbeit mit einem Dermatologen
          abgestimmt werden."
        />

        <MobileThemenübersicht className="dropdown-1" />

        <ImageWithText
          imgName="uberblick_img_2"
          altImage="Eine Junge Frau, die sich am Ellenbogen kratzt">
          <p>
            Neurodermitis tritt meist in Schüben auf. Das heißt, es gibt Zeiten,
            in denen Du fast keine Symptome hast, und dann gibt es akute Phasen,
            in denen juckende, entzündete Stellen Dein Hautbild bestimmen. Eine
            maßgeschneiderte Neurodermitis-Behandlung kann Dir helfen, akute
            Symptome zu lindern, und die Zeiten verlängern, in denen Du
            beschwerdefrei bist. Heilbar ist die atopische Dermatitis, wie
            Neurodermitis auch genannt wird, allerdings nicht. Die Erkrankung
            ist nie wirklich verschwunden und selbst in weniger aktiven Phasen,
            wenn die Neurodermitis für das Auge nicht sichtbar ist, „brodelt“
            die Entzündung unter der Haut weiter. Heutzutage gibt es jedoch
            moderne Medikamente, mit denen sich die Neurodermitis besser und
            langfristiger kontrollieren lässt.
          </p>
        </ImageWithText>
      </div>
      <div
        data-summary="Was hilft am besten gegen Neurodermitis?"
        className="havas-container dark">
        <h2>Was hilft am besten gegen Neurodermitis?</h2>
        <p>
          Neurodermitis hat viele Facetten und die chronisch-entzündliche
          Erkrankung ist nicht bei jedem Betroffenen gleich. Wie stark die
          einzelnen Neurodermitis-Schübe sind und wie häufig sie auftreten, ist
          individuell verschieden. Deshalb gibt es auf diese Frage nicht die
          eine Antwort. So unterschiedlich die Beschwerden sind, so verschieden
          ist auch die Therapie und sie sollte auf Dich und Deine Bedürfnisse
          zugeschnitten sein.
        </p>
        <ImageWithText
          imgName="uberblick_img_3"
          altImage="Frau, die sich die Hände eincremt">
          <h3>Äußerliche Therapien</h3>
          <p>
            Äußerliche Therapien werden genau an der Stelle angewendet, an der
            sie wirken sollen. Hierzu gehören zum Beispiel Salben oder Cremes.
            Äußerliche Anwendungen auf der Hautoberfläche werden auch als
            topische Therapien bezeichnet.
          </p>
          <Button
            type="primary"
            text="Mehr erfahren"
            to="/aeusserliche-therapien"
          />
        </ImageWithText>
        <ImageWithText
          reverseOnDesktop={true}
          imgName="ausserliche-therapien_img_6"
          altImage="Apotheker öffnet Schublade, in der sich Medikamente befinden.">
          <h3>Innerliche Therapien</h3>
          <p>
            Innerliche Therapien können zum Einsatz kommen, wenn äußerliche
            Behandlungsmethoden nicht ausreichen. Einige Medikamente werden als
            Tablette eingenommen, andere über eine Spritze injiziert. So werden
            beispielsweise Biologika unter die Haut gespritzt und gelangen dann
            über das Blutsystem in den Körper. Diese Therapieformen bekämpfen
            gezielt von innen die Ursache der Neurodermitis und werden in der
            Regel langfristig angewendet.
          </p>
          <Button
            text="Mehr erfahren"
            type="primary"
            to="/innerliche-therapien"
          />
        </ImageWithText>

        <ImageWithText
          imgName="ergänzende_img_1"
          altImage="Eine junge Frau, die auf dem Rücken liegt und die Arme über den Kopf streckt">
          <h3>Ergänzende Maßnahmen</h3>
          <p>
            Ergänzende Maßnahmen können zusätzlich zur innerlichen und
            äußerlichen Therapie zum Einsatz kommen. Dazu zählt zum Beispiel die
            Phototherapie. Aber auch psychotherapeutische Maßnahmen oder
            Entspannungstechniken zum Stressabbau können die Behandlung der
            atopischen Dermatitis sinnvoll ergänzen.
          </p>
          <Button
            type="primary"
            text="Mehr erfahren"
            to="/ergaenzende-therapien"
          />
        </ImageWithText>
        {isMobile && (
          <ZitatBox
            isLoudspeaker={false}
            text="„Neurodermitis wird häufig auch atopische Dermatitis oder atopisches Ekzem genannt.“"
          />
        )}
        <div className="picture-text">
          <div className="item" style={{ margin: 'auto' }}>
            <h3>Das A und O bei Neurodermitis – die Basistherapie</h3>
            <p className="mt-4">
              Die atopische Haut ist sehr trocken und empfindlich. Es mangelt
              ihr an Feuchthaltefaktoren und sie kann schlecht Wasser binden.
              Die Hautbarriere wird dadurch durchlässig und Fremdstoffe können
              eindringen und Entzündungen hervorrufen. Deshalb ist eine
              konsequente Pflege der Haut mit rückfettenden Cremes wichtig,
              selbst wenn Du gerade keine Beschwerden hast. Bei der
              Basistherapie kommen Produkte zum Einsatz, die keine medizinischen
              Wirkstoffe, wie beispielsweise Kortison, enthalten.
            </p>
            <Link
              to="/aeusserliche-therapien"
              style={{ display: 'inline-block' }}>
              Weitere hilfreiche Infos zur Basistherapie findest Du auf der
              Seite „äußerlichen Therapien“.
            </Link>
          </div>
          {!isMobile && (
            <div className="item" style={{ marginLeft: '15px' }}>
              <ZitatBox
                isLoudspeaker={false}
                text="„Neurodermitis wird häufig auch atopische Dermatitis oder atopisches Ekzem genannt.“"></ZitatBox>
            </div>
          )}
        </div>
        <TippsBox
          className="large-tips-box"
          text="Für Neurodermitis-Patienten 
gibt es spezielle Schulungsprogramme, die Dir beim Umgang mit Deiner Erkrankung im Alltag helfen können. Die Kosten hierfür werden meist von den Krankenkassen übernommen. Frage dazu Deinen Dermatologen oder Deine Krankenkasse."
          heading="Unser tipp"
        />
      </div>
      <div
        data-summary="Welcher Arzt behandelt Neurodermitis?"
        className="havas-container light">
        <h2>Welcher Arzt behandelt Neurodermitis?</h2>
        <p>
          Bei Neurodermitis sind Fachärzte für Hautkrankheiten die besten
          Ansprechpartner. Sie können eine Diagnose stellen und Dir die
          verschiedenen Behandlungsmöglichkeiten erläutern. Fachärzte für
          Hautkrankheiten werden auch Hautärzte oder Dermatologen genannt.
        </p>
        <ArztefinderTeaser />
        <h3>Wichtige Begleiter bei Deiner Neurodermitis-Behandlung</h3>
        <p>
          Ein gutes Verhältnis zu Deinem Hautarzt ist die Grundlage für eine
          gute Gesundheitsversorgung. Aber was heißt das, ein gutes Verhältnis?
          Um ehrlich und offen mitzuteilen, wie Du Dich fühlst, und die Fragen
          zu stellen, die Du wirklich klären möchtest, musst Du Deinem
          Dermatologen vertrauen. Sicher, viele Mediziner stehen heutzutage
          unter Zeitdruck und manche Betroffenen fühlen sich deshalb nicht gut
          beraten. Vergiss aber nicht: Dein Hautarzt steht auf Deiner Seite und
          möchte das Beste für Dich und Deine Gesundheit. Außerdem hast Du es
          teilweise selbst in der Hand, das Beste aus Deinem Arztbesuch
          rauszuholen.
        </p>
        <h3>Hilfreiche Tipps zur Vorbereitung auf Dein Arztgespräch</h3>
        <Akkordeon heading="Frage nach und informiere Dich" open={true}>
          <p>
            Damit Deine Therapie erfolgreich verlaufen kann, ist es für Dich
            wichtig, dass Du gut informiert in das Gespräch gehst und den
            aktuellen Stand Deiner Neurodermitis und die
            Behandlungsmöglichkeiten kennst. Sei dir dessen bewusst, dass sich
            die Forschung stetig weiterentwickelt und sprich Deinen Dermatologen
            darauf an, wenn Du für Dich Chancen und Wege siehst, Deine Therapie
            anzupassen. Denk daran: Du entscheidest mit!{' '}
          </p>
          <a
            target="_blank"
            href="https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Behandlungsmoeglichkeiten.pdf?la=de-DE">
            Hier findest Du eine Übersicht über Behandlungsmöglichkeiten bei
            Neurodermitis als Broschüre.
          </a>
        </Akkordeon>
        <Akkordeon heading="Bereite Dich auf Deine Arzttermine vor">
          <p>
            Am besten notierst Du Dir im Alltag auftauchende Fragen direkt und
            bringst Deine Liste mit zum Arzttermin. Deine Fragen kannst Du zum
            Beispiel in einem{' '}
            <a
              href="https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Neurodermitis_Patiententagebuch_interaktiv.pdf?la=de-DE"
              target="_blank">
              Neurodermitis-Tagebuch
            </a>{' '}
            sammeln. So gehen sie nicht verloren und auch Dein Arzt kann sich
            ein besseres Bild vom Krankheitsverlauf machen. Je klarer Du
            berichten kannst, desto klarer kann Dir Dein Dermatologe mit einer
            Therapie helfen.
          </p>
          <a
            href="https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Ratgeber_Arztgespraech.pdf?la=de-DE"
            target="_blank">
            Unser Ratgeber für das Arztgespräch kann Dir helfen, Dich optimal
            auf Deinen nächsten Termin vorzubereiten.
          </a>
        </Akkordeon>
        <Akkordeon heading="Sei offen und ehrlich">
          <p>
            Auch wenn Du Dich anfangs etwas unwohl dabei fühlst, sage Deinem
            Dermatologen, was für Dich persönlich besonders wichtig ist. Es gibt
            hier keine Tabus. Nur so kann er die aus medizinischer Sicht beste
            Behandlung für Dich finden. Übrigens: Wenn Du etwas nicht verstehst
            oder wenn Dir eine Erklärung zu schnell ging, scheue Dich nicht,
            nochmal nachzufragen. Denk daran: Es ist Dein Arzttermin und Du
            solltest das Beste für Dich rausholen.
          </p>
        </Akkordeon>
        <Akkordeon heading="Dokumentiere Deine Schübe">
          <p>
            Schreibe auf, wie viele Schübe Du zwischen den Arztterminen hattest,
            wie stark sie waren und wodurch sie möglicherweise ausgelöst wurden.
            Dafür kannst Du ein Tagebuch anlegen, egal ob auf Papier oder
            digital. So behältst Du den Überblick über Deine Neurodermitis und
            kannst Deinen Krankheitsverlauf auch Deinem Hautarzt detailliert
            mitteilen. Vielleicht machst Du sogar ein Foto, wenn etwas anders
            war als bisher. Damit fühlst Du Dich sicher und Dein Hautarzt kann
            gezielt darauf eingehen.
          </p>
        </Akkordeon>
        <p>
          Im{' '}
          <a
            href="https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Ratgeber_Arztgespraech.pdf?la=de-DE"
            target="_blank">
            Ratgeber für das Arztgespräch bei Neurodermitis
          </a>{' '}
          findest Du viele Tipps, wie du das Beste aus einem Termin bei Deinem
          Dermatologen herausholst.
        </p>
      </div>
      <div
        data-summary="Welche Hausmittel helfen bei Neurodermitis?"
        className="havas-container dark">
        <h2>Welche Hausmittel helfen bei Neurodermitis?</h2>
        <p>
          Viele Menschen mit Neurodermitis fragen sich, ob es neben der
          Basistherapie und verschiedenen Medikamenten auch Hausmittel gibt, mit
          denen sie die Behandlung sinnvoll ergänzen können. Am besten Du
          besprichst mit Deinem Arzt, welche Hausmittel für Dich persönlich in
          Frage kommen.
        </p>
        <Slider
          minHeightDesktop={'445px'}
          items={[
            {
              heading: 'Feuchtigkeit von innen',
              imgName: 'uberblick_slider_1',
              altImage: 'Illustration einer Wasserflasche',
              text: [
                `Ein wichtiges „Hausmittel“ ist Feuchtigkeit von innen. Das heißt, Du solltest ausreichend trinken. Die Deutsche Gesellschaft für Ernährung empfiehlt als Richtwert 1,5 Liter pro Tag. Wenns' richtig heiß ist und Du viel schwitzt, kann allerdings deutlich mehr notwendig sein. Am besten sind Wasser sowie Früchte- und Kräutertees. Im Alltagsstress vergisst man allerdings manchmal zu trinken.`,
                <br />,
                <br />,
                <strong style={{ fontSize: '18px' }}>Unser Tipp: </strong>,
                'Wenn es Dir schwerfällt, regelmäßig und vor allem ausreichend zu trinken, kannst Du eine App nutzen, die Dich daran erinnert.',
              ],
            },
            {
              heading: 'Karotten',
              imgName: 'uberblick_slider_2',
              altImage: 'Illustration einer Karotte',
              text: [
                'Karotten enthalten jede Menge Betacarotin. Das ist eine Vorstufe von Vitamin A und ein wichtiger Zellschutzstoff unseres Körpers. Als Mittel gegen trockene Haut kannst Du 1–2 Karotten entsaften. Nutze dafür Karotten, die frei von Schadstoffen sind. Gib den Saft auf ein Watte-Pad und tupfe ihn sanft auf die Haut oder massiere die betroffenenen Stellen damit ein. Lasse den Saft ca. 20 Minuten einwirken und wasche ihn danach mit lauwarmem Wasser ab. Aber Achtung, Karottensaft kann zu Verfärbungen führen!',
              ],
            },
            {
              heading: 'Milchprodukte',
              imgName: 'uberblick_slider_3',
              altImage: 'llustration von Milchprodukten',
              text: [
                'Milchprodukte aus dem Kühlschrank, äußerlich angewendet, können ebenfalls für schnelle Hilfe sorgen. Joghurt oder Quark tun der von Neurodermitis geplagten Haut meist richtig gut. Die darin enthaltenen Milchsäuren spenden Feuchtigkeit und fetten die Haut nachhaltig. Eine kalte Maske aus Joghurt oder Quark kühlt, kann so den Juckreiz lindern und wirkt darüber hinaus auch noch entzündungshemmend. Der Konsum von Michprodukten - also innerlich angewendet - könnte die Neurodermitis bei Patienten allerdings verschlimmern.',
              ],
            },
            {
              heading: 'Honig',
              imgName: 'uberblick_slider_4',
              altImage: 'llustration einer Biene mit Bienenwaben',
              text: [
                'Honig ist ebenfalls ein Hausmittel bei trockener Haut und kann auch bei spröden Lippen gut eingesetzt werden. Honig ist ein natürlicher Feuchtigkeitsspender und äußerlich angewendet wirkt er antibakteriell, beruhigend und entzündungshemmend. Sein saurer pH-Wert stabilisiert zudem den Schutzmantel der Haut.',
              ],
            },
            {
              heading: 'Natürliche Öle',
              imgName: 'uberblick_slider_5',
              altImage: 'Illustration von Ölen',
              text: [
                'Natürliche Öle können ebenfalls schnell gegen trockene Haut helfen. Besonders effektiv sind etwa Olivenöl, Weizenkeimöl und Sesamöl. Das Öl einfach dünn auf die Haut auftragen und einmassieren. Anschließend noch einige Minuten einwirken lassen und mit warmem Wasser gut wieder entfernen.',
              ],
            },
            {
              heading: 'Aloe vera',
              imgName: 'uberblick_slider_6',
              altImage: 'llustration einer Aloe Vera-Pflanze',
              text: [
                'Aloe vera enthält entzündungshemmende, antibakterielle sowie reizlindernde Wirkstoffe und spendet viel Feuchtigkeit. Das Gel der Wüstenpflanze soll auch die Durchblutung fördern und den Juckreiz hemmen. Trage das Hausmittel einfach auf die trockene Haut auf und lasse es einziehen. Für eine wirksame Anwendung solltest Du Aloe Vera über einen längeren Zeitraum anwenden.',
              ],
            },
          ]}
        />
      </div>
      <div
        data-summary="Was sollte man bei Neurodermitis nicht tun?"
        className="havas-container light">
        <h2>Was sollte man bei Neurodermitis nicht tun?</h2>
        <p>
          Im Alltag ist Deine Haut vielen Umwelteinflüssen ausgesetzt: Kälte,
          Hitze, Wind, Regen, Pollen, kratzende Kleidung, hautreizende
          Inhaltsstoffe etc. Einige dieser Einflüsse können Deine Neurodermitis
          negativ beeinflussen. Im Fachjargon spricht man dann von
          Provokationsfaktoren oder Triggern. Auf welche Trigger jemand
          reagiert, ist von Mensch zu Mensch verschieden. Typische Trigger für
          eine Neurodermitis sind zum Beispiel:
        </p>
        <Bulletlist
          textList={[
            'Falsche oder übermäßige Reinigung Deiner Haut',
            'Reinigungs- und Pflegeprodukte mit reizenden Duft- und Konservierungsstoffen',
            'Zigarettenrauch und Umwelt-schadstoffe, z. B. Abgase',
            'Klima, z. B. geringe Luftfeuchtigkeit in beheizten Räumen',
            'Stress, z. B. Lärm, Leistungsdruck',
            'Allergene, z. B. Pflanzenpollen, Tierhaare oder einige Nahrungsmittel',
          ]}
        />
        <ImageWithText
          imgName="kopfhaur_img_6"
          altImage="Eine junge Frau schreibt lächelnd in ein Tagebuch"
          style={{ marginTop: `${isMobile ? '-40px' : '80px'}` }}>
          <p>
            Um Schüben vorzubeugen, ist es wichtig Deine Trigger zu kennen, denn
            dann kannst Du sie gezielt vermeiden. Ein Tagebuch kann dabei
            helfen, Einflüsse zu erkennen, die sich negativ auf Deine
            Neurodermitis auswirken.
          </p>
          <a
            href="https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Neurodermitis_Patiententagebuch_interaktiv.pdf?la=de-DE"
            target="_blank">
            Eine Vorlage für ein Tagebuch kannst Du übrigens hier herunterladen.
          </a>
        </ImageWithText>
        <h3>Was löst Neurodermitis aus?</h3>
        <p>
          Forscher weltweit versuchen die genauen Ursachen für Neurodermitis zu
          entschlüsseln, um die Erkrankung besser zu verstehen und moderne
          Medikamente zu entwickeln, mit denen sich die Auswirkungen der
          Neurodermitis gut kontrollieren lassen. Gemäß aktuellem Stand, sind
          die Ursachen allerdings noch nicht vollständig geklärt. Man weiß
          jedoch, dass die folgenden Faktoren den Ausbruch der Erkrankung
          begünstigen können:
        </p>
        <Bulletlist
          style={isMobile ? { height: 500 } : {}}
          textList={[
            'Erbliche Vorbelastungen',
            'Eine gestörte Barrierefunktion der Haut',
            'Eine Überempfindlichkeit des Immunsystems',
            'Verschiedene Umweltfaktoren',
          ]}>
          <Link to="/ursachen">
            Detaillierte Informationen zu den Ursachen einer Neurodermitis
            kannst Du hier nachlesen.
          </Link>
        </Bulletlist>
      </div>
      <div
        data-summary="Was tun, wenn auch die Psyche leidet?"
        className="havas-container dark">
        <h2>Was tun, wenn auch die Psyche leidet?</h2>
        <p>
          Viele Menschen mit Neurodermitis leiden nicht nur körperlich, die oft
          sichtbaren Ekzeme sind auch eine seelische Belastung und können
          psychische Folgen haben. Für den Erfolg Deiner
          Neurodermitis-Behandlung ist es aber wichtig, dass Du Dich gut fühlst.
          Denn wie bereits erwähnt, ist Stress ein häufiger Trigger. Wenn Du
          Dich ausgeliefert oder ausgegrenzt fühlst, Du über einen längeren
          Zeitraum niedergeschlagen oder antriebslos bist oder wenn Du oft Angst
          hast und Deine Gedanken fast nie zur Ruhe kommen, kann es eventuell
          sinnvoll sein, therapeutische Hilfe in Anspruch zu nehmen.
        </p>
      </div>
      <div data-summary="Fazit" className="havas-container light">
        <h2>Fazit</h2>
        <p>
          Neurodermitis ist zwar nicht heilbar, aber eine konsequente Pflege und
          eine für Dich passende Behandlung können Dir helfen, Deine Erkrankung
          zu kontrollieren. Es ist wichtig, einen positiven Umgang mit Deiner
          Neurodermitis zu finden. Das ist sicher nicht immer leicht, aber es
          lohnt sich, den Blick auf das Positive zu lenken. Es gibt heutzutage
          viele moderne Behandlungsmöglichkeiten, die Dich im Umgang mit Deiner
          Erkrankung unterstützen und Dir ein glückliches und erfülltes Leben
          ermöglichen können. Werde aktiv und lass Dich beraten.
        </p>
        <ArztefinderTeaser />
      </div>
    </>
  );
}

export const Head = () => <SEO />;
