import React from 'react';
import NotFound from '../components/NotFound';
import { SEO } from '../components/seo';

export default function Symptome() {
  return (
    <>
      <div className="havas-container light">
        <NotFound />
      </div>
    </>
  );
}

export const Head = () => <SEO />;
