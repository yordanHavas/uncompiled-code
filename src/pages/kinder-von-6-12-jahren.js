import { Link } from 'gatsby';
import React from 'react';
import Akkordeon from '../components/Akkordeon';
import Bulletlist from '../components/Bulletlist';
import Button from '../components/Button';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { SEO } from '../components/seo';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';

export default function Schulkindern() {
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          className="image-position-left"
          headingTextList={['Neurodermitis bei', 'Schulkindern']}
          coloredWordsArr={['Schulkindern']}
          imgName="schulkinder_img_1"
          color="yellow"
          altImg="Ein Mädchen trägt einen Schulranzen und lächelt zufrieden."
          text="Wenn Euer Kind in die Schule geht, beginnt eine wichtige Lebensphase. Jetzt werden körperliche und geistige Grundlagen für das Erwachsenwerden gelegt. Auch Kinder mit Neurodermitis werden nun immer eigenständiger und verstehen mehr und mehr Zusammenhänge. Sie können Schritt für Schritt in eine aktive Rolle im Umgang mit ihrer Krankheit hineinwachsen."
        />

        <MobileThemenübersicht className="dropdown-1" />

        <p>
          Neurodermitis betrifft in westlichen Industrieländern etwa jedes 10.
          Kind. Die Auswirkungen der chronisch-entzündlichen Erkrankung sind oft
          nicht nur auf die Haut begrenzt. Sie kann sich auf viele
          Lebensbereiche auswirken und sowohl den Körper als auch die Psyche
          Eures Kindes beeinträchtigen. Als Eltern könnt Ihr Eurem Kind die Last
          der Erkrankung nicht abnehmen, aber Ihr könnt ihm den Rücken stärken,
          damit es sein Leben mit Neurodermitis selbstbestimmt und zufrieden
          gestalten kann.
        </p>

        <h3>
          In welchem Alter beginnt Neurodermitis?<sup>1</sup>
        </h3>

        <p>
          Neurodermitis beginnt meist schon im Säuglings- und Kleinkindalter.
          Bei 60 Prozent der betroffenen Kinder tritt die Erkrankung bereits im
          1. Lebensjahr auf, 90 Prozent erkranken vor dem 5. Lebensjahr.
          Allerdings kann die atopische Dermatitis, wie Neurodermitis auch
          genannt wird, in jedem Alter auftreten und sich bei Schulkindern
          erstmals zeigen.
        </p>
      </div>

      <div
        className="havas-container dark"
        data-summary="Woran erkennt man Neurodermitis bei Kindern?">
        <h2>Woran erkennt man Neurodermitis bei Kindern?</h2>
        <p>
          Zu den sichtbaren Hautveränderungen bei Kindern, sowie auch bei
          Erwachsenen gehören Ekzeme. Das sind entzündete, teilweise blutende
          Hautstellen. Auch Hautrötungen (Erytheme), Hautverdickungen
          (Lichenifika-tionen) und feine Hauteinrisse sind für Neurodermitis
          typisch, genauso wie trockene Haut. Die Hautveränderungen werden meist
          von starkem Juckreiz begleitet.
        </p>

        <ImageWithText
          imgName="schulkindern_2"
          altImage="Von Neurodermitis betroffene Stellen bei Kindern"
          noTilt={true}
          className="plNone">
          <p>
            Bei der atopischen Dermatitis ist die Lage der betroffenen Bereiche
            häufig vom Alter abhängig und kann sich im Verlauf der Erkrankung
            verändern. Kommen Kinder ins Kindergarten- und Grundschulalter,
            „wandern” die Ekzeme häufig zu den Ellenbeugen und Kniekehlen sowie
            zu den Fuß- und Handgelenken.
          </p>
          <p>
            Die Erkrankung verläuft schubförmig und ruhige Phasen wechseln sich
            mit solchen ab, in denen stark juckende, entzündete Stellen das
            Hautbild bestimmen. Kinder befinden sich in akuten Schüben häufig in
            einem belastenden Teufelskreis aus Jucken und Kratzen. Es gibt aber
            auch immer wieder Phasen, in denen die Haut Eures Kindes nahezu
            gesund erscheint. Doch auch wenn es für das Auge nicht sichtbar ist:
            Neurodermitis ist eine chronische Erkrankung und die Entzündung
            unter der Haut ist immer da.
          </p>
        </ImageWithText>
      </div>

      <div
        className="havas-container light"
        data-summary="Was tun bei Neurodermitis bei Kindern?">
        <h2>Was tun bei Neurodermitis bei Kindern?</h2>
        <ImageWithText
          imgName="schulkindern_3"
          altImage="Ein kleiner Junge, der bei einem Arzttermin bei seiner Mutter auf dem Schoß sitzt">
          <p>
            Durch eine konsequente Behandlung von Anfang an lässt sich oft
            verhindern, dass sich die Neurodermitis stärker ausbreitet. Ein
            wesentlicher Bestandteil der Behandlung ist dabei die Basispflege.
          </p>
        </ImageWithText>
        <h3>Die Basispflege – ein fester Bestandteil im Alltag</h3>
        <p>
          Eine konsequente und liebevolle Hautpflege ist bei Kindern mit
          Neurodermitis unerlässlich. Das gilt auch in Phasen, in denen die Haut
          gesund wirkt. Die Basistherapie hilft dabei, dass Symptome sich nicht
          verschlimmern, und sie kann neuen Krankheitsschüben entgegenwirken.
          Sie sollte sich immer am aktuellen Hautzustand orientieren, d. h.
          fette Salbengrundlagen auf trockener Haut, Feuchtigkeitscremes bei
          leicht trockener Haut bzw. bei entzündeter oder nässender Haut.
        </p>
        <Link to="/kinder-behandlung#basistherapieAnchor">
          Weitere Informationen zur Basispflege kannst du hier nachlesen.
        </Link>
        <h3>Wie wird Neurodermitis bei Kindern medikamentös behandelt?</h3>
        <p>
          Gegen die Entzündungen und starken Juckreiz helfen oft nur
          Medikamente. Heute stehen für Kinder anerkannte Arzneimittel zur
          Verfügung, die entweder äußerlich (topisch) oder innerlich
          (systemisch) angewandt werden und gute Erfolge erzielen können.
        </p>
        {/* <Link to="/babys-und-kindern">
          Weitere Informationen zur medikamentösen Behandlung bei Kindern kannst
          Du hier nachlesen
        </Link> */}
        <ImageWithText
          imgName="jugendlichen_22"
          altImage="Ein Vater, trägt Creme auf die Wange seiner Tochter auf">
          <h3>Behandlung von Neurodermitis bei Babys und Kindern</h3>
          <p>
            Ausführliche Informationen zur medikamentösen Behandlung bei Kindern
            könnt Ihr hier nachlesen.
          </p>
          <Button text="Mehr erfahren" type="primary" to="/kinder-behandlung" />
        </ImageWithText>
        <p>
          Neurodermitis ist nicht gleich Neurodermitis und das Krankheitsbild
          kann sich im Verlauf der Erkrankung verändern. Deshalb ist es wichtig,
          dass die Schwere der Neurodermitis regelmäßig kontrolliert wird und
          Ihr gemeinsam mit dem Dermatologen Eures Kindes überprüft, ob die
          aktuellen Therapiemaßnahmen die Erkrankung ausreichend kontrollieren.
          So kann die Behandlung bei Bedarf jederzeit an die Bedürfnisse Eures
          Kindes angepasst werden.
        </p>
        <SelbsttestTeaser
          customeText={[
            'WIE SEHR HABT IHR',
            'DIE NEURODERMITIS',
            'UNTER KONTROLLE?',
          ]}
        />
        <h3>Den Juckreiz bei Kindern mit Neurodermitis lindern</h3>
        <p>
          Nicht kratzen – das ist leichter gesagt als getan. Kratzen ist jedoch
          nicht gleich Kratzen. Sobald Euer Kind alt genug ist, könnt Ihr mit
          ihm alternative Kratzmethoden üben, zum Beispiel mit der flachen Hand
          oder mit den Knöcheln. Es kann auch helfen, die juckende Stelle zu
          kneten, sie zu massieren, abzuklopfen, zu drücken oder zu streicheln.
        </p>
        <p>
          Es gibt auch spezielle Anti-Juckreiz-Sprays oder -Cremes, mit denen
          Ihr den Juckreiz behandeln könnt. Auch gekühlte,
          feuchtigkeitsspendende Pflegeprodukte oder kühlende Umschläge bzw.
          eine Kühlkompresse aus dem Kühlschrank können gegen den Juckreiz
          helfen.
        </p>
        <Link to="/tipps-fuer-den-alltag">
          Weitere Tipps gegen den Juckreiz findet Ihr auf der Seite „Tipps für
          den Alltag“.{' '}
        </Link>
      </div>

      <div
        className="havas-container dark"
        data-summary="Was löst Neurodermitis bei Kindern aus?">
        <h2>Was löst Neurodermitis bei Kindern aus?</h2>
        <p>
          Die genauen Ursachen für Neurodermitis bei Kindern sind bis heute
          nicht vollständig geklärt. Sie unterscheiden sich bei Kindern oder
          Erwachsenen allerdings nicht. Man weiß heute, dass die folgenden
          Faktoren den Ausbruch der Erkrankung begünstigen können:
        </p>

        <Bulletlist
          textList={[
            'Erbliche Vorbelastungen',
            'Eine gestörte Barrierefunktion der Haut',
            'Eine Überempfindlichkeit des Immunsystems',
            'Verschiedene Umweltfaktoren',
          ]}>
          <a
            style={{ display: 'block', paddingTop: 20, paddingBottom: 20 }}
            href="/ursachen">
            Detaillierte Informationen zu den Ursachen einer Neurodermitis
            kannst Du hier nachlesen.
          </a>
        </Bulletlist>
      </div>

      <div
        className="havas-container light"
        data-summary="Wie können Eltern helfen?">
        <h2>Wie können Eltern helfen?</h2>
        <ImageWithText
          imgName="schulkindern_4"
          altImage="Ein kleines Kind dem eine Person Creme ins Gesicht streicht.">
          <p>
            Ein strukturierter Tagesablauf, in dem das Pflegen und Eincremen
            fest verankert sind, kann Eurem Kind beim Umgang mit der
            Neurodermitis helfen. Besprecht gemeinsam, wie Euer Kind diese
            Zeiten gestalten möchte, und macht das Pflegeritual zu einem schönen
            Teil des Tages. Mochte Euer Kind früher besonders vorgelesene
            Geschichten, machen ihm nun vielleicht Hörspiele mehr Spaß. Es ist
            wichtig, dem eigenen Kind einen liebevollen Umgang mit seiner Haut
            vorzuleben – behutsame Streicheleinheiten und Zuwendung können ihm
            im Alltag Halt geben und es lehren, seine Haut zu akzeptieren.
          </p>
        </ImageWithText>

        <h3>So viel wie nötig, so wenig wie möglich</h3>
        <p>
          Besonders wichtig sind jetzt kindgerechte Erklärungen. Damit stellt
          Ihr Eurem Kind das „Handwerkszeug“ für den Umgang mit seiner
          Erkrankung bereit. Die Neurodermitis sollte jedoch nicht als
          Dauerthema alles andere überschatten. Ganz nach der Devise „so wenig
          wie möglich und so viel wie nötig”.
        </p>

        <ClickableBox
          title="Gut zu wissen"
          header="Eigene Grenzen erkennen und wahren">
          <p>
            Als Vater oder Mutter wollt Ihr Eure Belastung vielleicht nicht
            zeigen und häufig stellt Ihr Eure Bedürfnisse zurück. Es ist jedoch
            wichtig, dass Ihr Euch nicht überlastest und Ihr eigene Freiräume
            bewahrt. Das hat nichts mit Egoismus zu tun. Wird die Situation für
            Euch mal zu viel, fragt eventuell die Großeltern, andere
            Familienmitglieder oder Freunde, ob sie mal einspringen und Euch
            unterstützen können.
          </p>
        </ClickableBox>
        <h3>Wissen mit Wirkung–Neurodermitis-Schulungen</h3>
        <p>
          Kinder sind auf ihre Eltern angewiesen und sie brauchen viel Zuwendung
          und Unterstützung. Kommen die Herausforderungen der chronischen
          Erkrankung hinzu, kann das für die ganze Familie zu einer enormen
          Belastung werden. Schulungen können betroffenen Kindern und ihren
          Eltern im Umgang mit der chronischen Erkrankung und den damit
          verbundenen Herausforderungen im Alltag helfen. Sie können das
          Krankheitsverständnis und den selbstbestimmten Umgang mit der
          Neurodermitis fördern.
        </p>
        <Link to="/anlaufstellen">
          Hier findet Ihr hilfreiche Adressen und Anlaufstellen.
        </Link>
        <h3>Weitere begleitende Maßnahmen</h3>
        <ImageWithText
          imgName="schulkindern_5"
          altImage="Ein Mädchen blickt traurig aus dem Fenster.">
          <p>
            Neurodermitis kann euer Kind stark belasten. Neben den körperlichen
            Beschwerden kommt oft seelisches Leid dazu. Euer Kind fühlt sich
            vielleicht anders als die anderen, es hat Angst vor Ausgrenzung und
            Ablehnung. Wenn die psychologische Belastung zu groß ist, kann
            professionelle Unterstützung helfen, z.B. eine Familien- oder
            Psychotherapie. Es gibt auch spezielle Akzeptanztherapien oder
            Therapien zum Selbstmanagement. Sogar Entspannungstechniken wie
            Yoga, Meditation, autogenes Training oder Atemtherapie können für
            die ganze Familie sinnvoll sein.
          </p>
        </ImageWithText>

        <ClickableBox
          title="Gut zu wissen"
          header="Das Neurodermitis-Begleiter-Team">
          <p>
            Im Neurodermitis-Begleiter-Team unterstützen Euch geschulte
            Fachkräfte mit medizinisch gesicherten Informationen zur Erkrankung
            und Therapie. Sie helfen auch mit gesundheitspsychologischen Tipps
            oder praktischen Ratschlägen weiter.{' '}
            <Link to="/neurodermitis-berater-kontaktieren">
              Hier findet Ihr die Kontaktdaten.
            </Link>
          </p>
        </ClickableBox>
      </div>

      <div
        className="havas-container dark"
        data-summary="Was sollten Kinder mit Neurodermitis nicht essen?">
        <h2>Was sollten Kinder mit Neurodermitis nicht essen?</h2>
        <ImageWithText
          imgName="schulkindern_6"
          altImage="Ein Mädchen isst Himbeeren, die auf ihren Fingern stecken.">
          <p>
            Etwa jedes 2. Kind mit Neurodermitis reagiert allergisch auf
            bestimmte Nahrungsmittel. Doch nur bei etwa jedem 3. moderat bis
            schwer betroffenen Kind führt dies tatsächlich zu einer
            Verschlechterung des Hautbildes. Also sind übermäßige
            Einschränkungen bei der Nahrungsmittelwahl selten notwendig.
          </p>
          <p>
            Habt Ihr einen Verdacht, dass Euer Kind auf ein bestimmtes
            Nahrungsmittel allergisch reagiert, könnt Ihr dieses, nach
            ärztlicher Rücksprache, versuchsweise vom Speiseplan streichen.
            Verschwindet die Allergie, habt Ihr sehr wahrscheinlich einen
            Auslöser gefunden.
          </p>
        </ImageWithText>
        <p>
          Nahrungsmittelallergien verschwinden manchmal wieder, sodass nach 1
          oder 2 Jahren das Nahrungsmittel durchaus wieder vertragen wird. Wenn
          Ihr sichergehen wollt, solltet Ihr Euren Dermatologen einbeziehen.
          Spezielle Allergietests können Vermutungen medizinisch absichern.
        </p>
        <ClickableBox
          title="Gut zu wissen"
          header="Hilfe durch Ernährungsberater">
          <p>
            Ernährungsberater können bei einer Umstellung der Ernährung oft
            helfen. Sie haben tolle Ideen, wie Nahrungsmittel kreativ ersetzt
            werden können. Meist reichen hier schon 1–2 Stunden, um eine
            kindgerechte und abwechslungsreiche Kost zusammenzustellen.
          </p>
        </ClickableBox>
        <ArztefinderTeaser />
      </div>

      <div
        className="havas-container light"
        data-summary="Das könnte Euch auch interessieren">
        <h2>Das könnte Euch auch interessieren</h2>
        <Akkordeon heading="Neurodermitis in der Schule" open={true}>
          <p>
            Ein gestörter Schlaf in der Nacht führt nicht selten zu
            Konzentrations- und Leistungseinbußen am Tag. Kinder mit
            Neurodermitis können deshalb im Unterricht oft nicht ihr volles
            Potenzial ausschöpfen. Es kann auch passieren, dass Euer Kind wegen
            seiner Erkrankung vermehrt Fehltage hat. Die folgenden Tipps können
            helfen, verpassten Lernstoff nachzuarbeiten und den Anschluss nicht
            zu verlieren.
          </p>
          <Bulletlist
            insideAkkordeon={true}
            textList={[
              'Lasst Euch von Lehrern Übungen und Aufgaben geben, die Ihr zu Hause bearbeiten könnt.',
              'Bittet Mitschüler, Euch Hausaufgaben und Mitschriften vorbeizubringen und diese mit Eurem Kind durchzugehen.',
              'Nach einer langen Fehlzeit kann der zuständige Vertrauenslehrer Deinem Kind helfen, wieder in den Schulalltag zurückzufinden.',
              'Manchmal kann Nachhilfeunterricht hilfreich sein.',
            ]}></Bulletlist>
          <p>
            Bezieht die Lehrer in der Schule mit ein und informiert sie über die
            Herausforderung der Neurodermitis. Erklärt ihnen, wie die Krankheit
            verläuft und welche Auslöser es gibt. So können sie verständnisvoll
            damit umgehen und auch Mitschüler darauf einstellen.
          </p>
        </Akkordeon>
        <Akkordeon heading="Sport und Bewegung bei Kindern mit Neurodermitis">
          <p>
            Je nach Krankheitsschwere oder aufgrund von akuten Schüben kann eine
            Befreiung vom Sportunterricht empfehlenswert sein. Gleiches gilt,
            wenn Euer Kind übermüdet und dadurch eventuell unfallgefährdet ist.
            Verträgt es Chlorwasser schlecht, könnt Ihr es auch vom
            Schwimmunterricht befreien lassen.
          </p>
          <p>
            Euer Kind sollte sich aber nicht dauernd schonen. Kinder wollen sich
            bewegen und sie brauchen das Herumtoben mit anderen für ihre
            psychische und soziale Gesundheit. Der eigene Schweiß kann die Haut
            jedoch reizen und Juckreiz hervorrufen. Nach dem Toben wäre es gut,
            wenn Euer Kind sich kurz abduscht, um das Nachschwitzen zu
            verhindern. Wenn das nicht möglich ist, könnt Ihr Eurem Kind
            Kleidung zum Wechseln mitgeben. Vor allem bei Sport und Bewegung
            bietet sich atmungsaktive Kleidung an, die Schweiß vom Körper
            wegtransportiert. Diese sollte jedoch nicht zu eng anliegen – das
            könnte die Haut sonst reizen.
          </p>
        </Akkordeon>
        <Akkordeon heading="Kinder mit Neurodermitis und Mobbing">
          <p>
            Euer Kind wird eventuell auf die sichtbaren Symptome der
            Neurodermitis angesprochen und deswegen gemobbt. So etwas sollte nie
            ignoriert werden. Je besser es über seine Erkrankung Bescheid weiß,
            desto erfolgreicher kann es sich wehren. Sprecht darüber, dass die
            Krankheit nicht ansteckend ist, dass viele Kinder Neurodermitis
            haben und warum das so ist.
          </p>
          <p>
            Erklärt Eurem Kind, dass es nicht seine Schuld ist, wenn andere es
            mobben oder hänseln, und dass es Euch oder einem anderen Erwachsenen
            – zum Beispiel einer Lehrkraft – Bescheid geben soll. Gebt Eurem
            Kind Zeit und Gelegenheit, von sich aus über Probleme und Ängste zu
            sprechen. Fällt es ihm schwer, seine Gefühle mitzuteilen, und
            reagiert es nicht gut auf direkte Fragen? Dann ist es vielleicht
            besser, ihm ganz allgemeine Fragen über seinen Tag zu stellen und zu
            beobachten, ob sich sein Verhalten ändert.
          </p>
        </Akkordeon>
      </div>

      <div className="havas-container dark" data-summary="Fazit">
        <h2>Fazit</h2>
        <p>
          Vom Tag der Geburt Eures Kindes an seid Ihr Zeugen eines stetigen
          Wandels. Dabei hat jede Lebensphase ihre Besonderheiten. Im Schulalter
          wird Euer Kind immer selbstständiger und es kann seine Erkrankung
          immer besser verstehen. Das heißt nicht, dass es sie auch immer
          akzeptieren kann. Sicher wird es Phasen geben, in denen Euer Kind
          traurig ist und gegen die Neurodermitis „rebelliert“. Lasst Euch
          helfen und werdet aktiv. Sprecht mit den Ärzten Eures Kindes über eine
          passende Behandlung. Mit konsequenter Hautpflege und etwas
          Unterstützung kann Euer Kind auch in der Schulzeit einen positiven
          Umgang mit seiner Erkrankung finden.
        </p>

        <ArztefinderTeaser />

        <div className="bottom-text">
          <p>
            <sup>1</sup>{' '}
            https://www.deutsche-apotheker-zeitung.de/daz-az/2010/daz-38-2010/bei-neurodermitis-gut-ernaehrt
          </p>
        </div>
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Schulkinder"
    metaTitle="Neurodermitis bei Kindern"
    description="Neurodermitis ist eine der häufigsten Hauterkrankungen bei Kindern. Bei Schulkindern sind meist die Ellenbeugen, Kniekehlen, Fuß- und Handgelenke betroffen."
    trustKeyWords="Neurodermitis Schulkind, Neurodermitis bei Kindern Ursachen, Neurodermitis bei Kindern Verlauf, Neurodermitis bei Kindern Juckreiz behandeln, Neurodermitis bei Kindern Juckreiz lindern, Neurodermitis bei Kindern psychologische Belastung "
  />
);
