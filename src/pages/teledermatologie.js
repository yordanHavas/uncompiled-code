import React, { useContext } from 'react';
import Hero from '../components/Hero';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import TeledermatologieComponent from '../components/TeledermatologieComponent';

const items = [
  {
    img: 'Derma2go',
    altImage: 'derma2go Logo',
    width: '224px',
    height: '60px',
    header: 'derma2go – Dein Hautarzt online',
    headerText:
      'derma2go ermöglicht Dir einen schnellen Zugang zu Fachärzten der Dermatologie.',
    text: 'Bei derma2go musst Du zunächst einen medizinischen Fragebogen ausfüllen und Du kannst Bilder Deiner Hautveränderung hochladen. Nachdem Du Deine Daten abgeschickt hast, erhältst Du innerhalb weniger Stunden eine Diagnose mit Arztbericht und Therapieplan. Sollten Deine Beschwerden digital nicht gelöst werden können, vermittelt derma2go Dich an einen Hautfacharzt oder eine Klinik in Deiner Umgebung.',
    buttonText: 'Zu derma2go',
    buttonLink: 'https://www.derma2go.com/',
  },
  {
    img: 'dermanostics',
    altImage: 'dermanostic Logo',
    width: '232px',
    height: '57px',
    header: 'dermanostic – Dein Hautarzt per App',
    headerText:
      'Die innovative Hautarztpraxis dermanostic ermöglicht Menschen weltweit einen schnellen und einfachen Zugang zu Hautärzten per App.',
    text: 'Du kannst Dir die App auf Dein Handy über den Apple oder Google Play Store runterladen. Danach kannst Du Bilder hochladen und füllst einen Fragebogen aus. Nach spätestens 24 Stunden erhältst Du einen ausführlichen Arztbrief inkl. Privatrezept zu Deiner Hautveränderung von dem hautärztlichen Praxisteam. Darin wird die Erkrankung präzise und verständlich beschrieben und eine Therapieempfehlung abgegeben.',
    buttonText: 'Zu dermanostic',
    buttonLink: 'https://dermanostic.com/',
  },
  {
    img: 'online-doctor',
    altImage: 'Online Doctor Logo',
    width: '181px',
    height: '63px',
    header: 'OnlineDoctor – schnelle Hilfe bei Hauterkrankungen',
    headerText:
      'Bei OnlineDoctor bekommst Du innerhalb weniger Stunden eine fachärztliche Diagnose und eine konkrete Empfehlung, was Du in Bezug auf dein Hautproblem tun kannst.',
    text: 'Bei OnlineDoctor kannst Du bestimmen, wer sich Deine Beschwerden online anschauen soll. Dazu kannst Du zunächst den gewünschten Hautarzt auswählen. Danach beschreibst Du Dein Hautleiden mithilfe eines intelligenten Chat-Assistenten und fotografierst die betroffene Hautstelle. Innerhalb kurzer Zeit erhältst Du dann eine schriftliche,  fachärztliche Einschätzung.',
    buttonText: 'Zu OnlineDoctor',
    buttonLink: 'https://www.onlinedoctor.de/',
  },
  {
    img: 'Nia_app',
    altImage: 'Nia Logo',
    width: '125px',
    height: '66px',
    header: 'Nia – die Neurodermitis-App für mehr Wohlbefinden',
    headerText:
      'Nia ist eine Begleitung für Neurodermitis-Betroffene per App. Patienten oder Eltern von betroffenen Kindern können Hautzustände und Schubauslöser täglich dokumentieren und erhalten auf Basis der Dokumentation ein Reporting vom Krankheitsverlauf, um für das nächste Arztgespräch gut vorbereitet zu sein.  Des Weiteren bietet Nia Tipps und Tricks für den Alltag, die zu mehr Wohlbefinden verhelfen können. Mit Nia lernst Du Deine Neurodermitis besser zu verstehen.',
    buttonText: 'Zu Nia',
    buttonLink: 'https://www.nia-health.de/',
  },
];
export default function ErganzendeTherapien() {
  const isMobile = useContext(MobileContext);

  return (
    <>
      <div className="havas-container light">
        {isMobile ? heroMobile : heroDesktop}
      </div>
      <TeledermatologieComponent items={items} />
      <div className="havas-container dark">
        <p>
          Alle vorgestellten Services sind kostenpflichtig. Teilweise werden die
          Gebühren von gesetzlichen Krankenkassen übernommen. Genauere Infos
          dazu findest Du auf der Website der jeweiligen Anbieter.
        </p>
      </div>
    </>
  );
}

const heroDesktop = (
  <>
    <Hero
      headingTextList={['Teledermatologie', 'bei Neurodermitis']}
      className="l-height-desktop"
      coloredWordsArr={['Teledermatologie']}
      imgName="teledermatologie_img_1"
      altImg="Eine junge Frau mit Kaffeetasse und Handy"
      text={[
        'Wer einen Termin bei einem Dermatologen will, muss oft mit langen Wartezeiten rechnen, und zwar nicht nur vor Ort in der Praxis, sondern auch was die Terminvergabe angeht. Viele Fachärzte vergeben Termine nur noch langfristig und es kann Wochen oder sogar Monate dauern, bis Du einen Termin bekommst.',
      ]}
    />
    <p>
      Gerade wenn Du eine chronische Erkrankung wie Neurodermitis hast, tauchen
      im Alltag immer wieder Fragen auf. Und genau dann ist es von Vorteil,
      akute Hilfestellung zu bekommen, falls der nächste Termin bei Deinem
      Dermatologen noch auf sich warten lässt und du vorab einen Rat brauchst.
      Außerdem kann es nie schaden, sich eine Zweitmeinung einzuholen, falls Du
      mit Deiner aktuellen Therapie unsicher oder gar unzufrieden bist.
    </p>

    <p>
      Teledermatologie ist ein digitaler Dienst, der Betroffenen einen schnellen
      Kontakt zu einem Facharzt bietet. Die teledermatologische Versorgung
      erfolgt auf räumliche Distanz mittels digitaler Kommunikation (wie z.B.
      Apps). Insbesondere die Dermatologie bietet sehr gute Voraussetzungen, die
      Telemedizin zu nutzen, da Bilder des Hautzustandes übermittelt werden
      können. Die Teledermatologie eignet sich zur Diagnose von Hautkrankheiten,
      für eine schnelle Versorgung von akuten Beschwerden, die Behandlung von
      chronischen Erkrankungen und zur Prävention.
    </p>
  </>
);

const heroMobile = (
  <Hero
    headingTextList={['Teledermatologie', 'bei Neurodermitis']}
    className="l-height-desktop centerAbsoluteHeading"
    coloredWordsArr={['Teledermatologie']}
    imgName="teledermatologie_img_1"
    altImg="Eine junge Frau mit Kaffeetasse und Handy"
    text={[
      <p style={{ marginTop: '20px' }}>
        Wer einen Termin bei einem Dermatologen will, muss oft mit langen
        Wartezeiten rechnen, und zwar nicht nur vor Ort in der Praxis sondern
        auch was die Terminvergabe angeht. Viele Fachärzte vergeben Termine nur
        noch langfristig und es kann Wochen oder sogar Monate dauern, bis Du
        einen Termin bekommst. Gerade wenn Du eine chronische Erkrankung wie
        Neurodermitis hast, tauchen im Alltag immer wieder Fragen auf. Und genau
        dann ist es von Vorteil, akute Hilfestellung zu bekommen, falls der
        nächste Termin bei Deinem Dermatologen noch auf sich warten lässt und du
        vorab einen Rat brauchst. Außerdem kann es nie schaden, sich eine
        Zweitmeinung einzuholen, falls Du mit Deiner aktuellen Therapie unsicher
        oder gar unzufrieden bist.
      </p>,

      <p style={{ marginTop: '30px' }}>
        Teledermatologie ist ein digitaler Dienst, der Betroffenen einen
        schnellen Kontakt zu einem Facharzt bietet. Die teledermatologische
        Versorgung erfolgt auf räumliche Distanz mittels digitaler Kommunikation
        (wie z.B. Apps). Insbesondere die Dermatologie bietet sehr gute
        Voraussetzungen, die Telemedizin zu nutzen, da Bilder des Hautzustandes
        übermittelt werden können. Die Teledermatologie eignet sich zur Diagnose
        von Hautkrankheiten, für eine schnelle Versorgung von akuten
        Beschwerden, die Behandlung von chronischen Erkrankungen und zur
        Prävention.
      </p>,
    ]}
  />
);

export const Head = () => (
  <SEO
    title="Teledermatologie"
    metaTitle="Teledermatologie bei Neurodermitis"
    description="Die Teledermatologie bietet Menschen mit Neurodermitis oder anderen Hautbeschwerden einen schnellen digitalen Zugang zu Hautärzten und Hautärztinnen."
    trustKeyWords="Teledermatologie"
  />
);
