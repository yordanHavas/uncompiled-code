import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Akkordeon from '../components/Akkordeon';
import Bulletlist from '../components/Bulletlist';
import Button from '../components/Button';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { ArztefinderTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import ZitatBox from '../components/ZitatBox';

export default function Jugendlichen() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}
      <div className="havas-container light">
        <Hero
          className="widerHero"
          color="yellow"
          headingTextList={['Neurodermitis', 'bei Babys und', 'Kleinkindern']}
          coloredWordsArr={['Babys', 'Kleinkindern']}
          imgName="babys_img_1"
          altImg="Bild eines lachenden Babys"
          text="Etwa jedes 4. Baby und Kleinkind ist von Neurodermitis betroffen. Die Hautentzündung ist die häufigste chronische Erkrankung im Kindesalter. Durch moderne Medikamente und eine fachkundige Behandlung kann man Neurodermitis schon bei den Kleinsten in den Griff bekommen. Mit einer maßgeschneiderten Therapie und etwas Unterstützung können die Kleinsten in der Regel ein weitgehend normales Leben führen."
        />

        <MobileThemenübersicht className="dropdown-1" />
        <p>
          Für Babys und Kleinkinder ist die Haut ein besonders wichtiges
          Sinnesorgan. Sanfte Berührungen und liebevolles Streicheln vermitteln
          ihnen nicht nur Sicherheit und Geborgenheit, sondern auch ein Gefühl
          für den eigenen Körper, seine Ausmaße und Grenzen. Verlasst Euch beim
          Hautkontakt auf Euer Gespür – Ihr wisst meist recht gut, was Eurem
          Kind guttut und was es nicht mag.
        </p>
      </div>
      <div
        data-summary="Neurodermitis bei Babys – Was sind erste Symptome?"
        className="havas-container dark">
        <h2>Neurodermitis bei Babys – Was sind erste Symptome?</h2>
        <p>
          Säuglinge zeigen als erste Symptome meist Milchschorf, gerötete,
          schuppende Haut und Juckreiz. Allerdings hat nicht jedes Baby mit
          Milchschorf auch tatsächlich Neurodermitis. Oft klingt der Milchschorf
          bis zum 2. Lebensjahr einfach ab, und es zeigen sich keine weiteren
          Merkmale einer Neurodermitis. Milchschorf hat übrigens nichts mit
          einer Milchunverträglichkeit zu tun. Der Name kommt daher, dass die
          Hautveränderungen ähnlich aussehen wie verbrannte Milch.
        </p>
        <ClickableBox title="Gut zu wissen" header="Kopfgneis bei Neugeborenen">
          <p>
            Viele Neugeborene haben in den ersten Lebensmonaten talgige,
            gelblich braune Schuppen auf der Kopfhaut. Das ist meist kein
            Milchschorf, sondern Kopfgneis. Er ist harmlos und verschwindet in
            der Regel im 1 Lebensjahr von allein.
          </p>
        </ClickableBox>

        <ImageWithText
          className="combinedImageWithText"
          noTilt={true}
          imgName="Gruppe 8237"
          altImage="Von Neurodermitis betroffene Stellen bei Babys und Kleinkindern">
          <p>
            Bei Kindern mit Neurodermitis zeigen sich neben dem Milchschorf bald
            weitere Symptome und aus den geröteten Hautstellen entwickeln sich
            stark juckende, schuppige Ekzeme. Gleichzeitig ist die Haut von
            betroffenen Säuglingen und Kleinkindern sehr trocken.{' '}
          </p>
          <p>
            Die Ekzeme treten bei Babys und Kleinkindern vor allem im Gesicht,
            an den Ohren und an anderen Stellen am Kopf auf. Der Windelbereich
            ist in den meisten Fällen nicht betroffen.{' '}
          </p>
        </ImageWithText>
        <h3>Geht Neurodermitis bei Kindern wieder weg?</h3>
        <p>
          Bei vielen Kindern mit Neurodermitis bessert sich die Haut vor der
          Pubertät und es treten keine Symptome mehr auf. Das heißt aber nicht,
          dass die Erkrankung verschwunden ist, denn die Atopie, die einer
          Neurodermitis meist zugrunde liegt, bleibt bestehen. Das heißt, das
          Immunsystem neigt dauerhaft zu einer Überreaktion gegen harmlose
          Stoffe oder Reize aus der Umwelt. Unter Umständen kann die
          Neurodermitis im späteren Leben wieder zum Vorschein kommen und die
          Symptome flammen erneut auf.
        </p>
      </div>
      <div
        data-summary="Was löst Neurodermitis bei Babys und Kleinkindern aus?"
        className="havas-container light">
        <h2>Was löst Neurodermitis bei Babys und Kleinkindern aus?</h2>
        <p>
          Die genauen Ursachen für atopische Dermatitis, wie Neurodermitis auch
          genannt wird, sind noch nicht vollständig geklärt. Die Faktoren, die
          zu ihrer Entstehung beitragen, unterscheiden sich bei Erwachsenen und
          Kindern nicht. Man weiß heute, dass eine erbliche Vorbelastung, eine
          gestörte Barrierefunktion der Haut, eine Überempfindlichkeit des
          Immunsystems und verschiedene Umweltfaktoren den Ausbruch der
          Erkrankung begünstigen können.
        </p>
        <Link to="/ursachen">
          Detaillierte Informationen zu den Ursachen einer Neurodermitis kannst
          Du hier nachlesen.
        </Link>
        <h3>Was tun bei erblicher Vorbelastung?</h3>
        <p>
          Wenn in Eurer Familie bereits atopische Erkrankungen bekannt sind, ist
          das Risiko für Euer Kind erhöht, dass es eine Neurodermitis
          entwickelt. Atopische Erkrankungen sind neben Neurodermitis zum
          Beispiel allergisches Asthma, Heuschnupfen oder andere Allergien; zum
          Beispiel gegen bestimmte Nahrungsmittel. In diesem Fall kann es
          sinnvoll sein, einige Maßnahmen zu berücksichtigen:
        </p>
        <Bulletlist
          textList={[
            [
              'Kinder, die ein erhöhtes Risiko haben, an einer Neurodermitis zu erkranken, und die nicht voll gestillt werden, sollten, nach Absprache mit dem Kinderarzt, hypoallergene Säuglingsnahrung erhalten.',
              <sup>1</sup>,
            ],
            'Für stillende Mütter kann eine ausgewogene und abwechslungsreiche Ernährung mit vielen Omega-3-Fettsäuren sinnvoll sein. Diese können Entzündungsprozesse reduzieren und das Immunsystem stärken. Sie sind z. B. in fettem Fisch wie Lachs und Makrele, aber auch pflanzlich in Leinöl oder Avocados enthalten.',
            'Für Risikokinder kann es besser sein, wenn im Haushalt keine Haustiere leben.',
          ]}
        />
        <p>
          Sprecht im Falle einer erblichen Vorbelastung mit einem Dermatologen
          und lasst Euch beraten, ob es weitere hilfreiche Maßnahmen gibt.
        </p>
        <ArztefinderTeaser />
        <ClickableBox
          title="Gut zu wissen"
          header="Ist Neurodermitis bei Babys heilbar?">
          <p>
            Neurodermitis ist nicht heilbar, aber schon bei den Kleinsten können
            die Symptome durch verschiedene Therapiemaßnahmen meist unter
            Kontrolle gebracht werden.
          </p>
        </ClickableBox>
      </div>
      <div
        data-summary="Was kann man gegen Neurodermitis bei Babys und Kleinkindern tun?"
        className="havas-container dark">
        <h2>
          Was kann man gegen Neurodermitis bei Babys und Kleinkindern tun?
        </h2>
        <p>
          Auch bei Säuglingen kann die atopische Dermatitis frühzeitig behandelt
          werden. So lässt sich oft verhindern, dass sich die entzündliche
          Hauterkrankung stärker ausbreitet. Ein wesentlicher Bestandteil der
          Behandlung ist die Basistherapie.
        </p>
        <h3>Die Basispflege – Hautpflege von Anfang an</h3>
        <p>
          Die Hautbarriere ist selbst bei gesunden Babys noch nicht vollständig
          ausgebildet. Auch der Säureschutzmantel entwickelt sich erst langsam.
          Deshalb ist Babyhaut generell viel empfindlicher und es kommt leichter
          zu Hautreizungen und Infektionen. Auch die Schweißdrüsen nehmen ihre
          Funktion erst nach und nach auf, weshalb Neugeborene die Temperatur
          noch nicht so gut regulieren können.
        </p>
        <ImageWithText
          imgName="GettyImages-1072352062"
          altImage="Nahaufnahme einer Hand, die das Bei eines Babys eincremt">
          <p>
            Eine konsequente und liebevolle Hautpflege von Anfang an ist deshalb
            bei Neugeborenen und Kleinkindern mit Neurodermitis unerlässlich.
            Das gilt auch in Phasen, in denen die Haut gesund wirkt. Das
            tägliche Eincremen ist ein wichtiger Teil der Behandlung von
            Neurodermitis und kann sogar Schüben vorbeugen.
          </p>
        </ImageWithText>
        <Link to="/kinder-behandlung">
          Welche Creme für Babys mit Neurodermitis gut geeignet ist und viele
          weitere Informationen zur Basispflege kannst Du hier nachlesen.
        </Link>
        <h3>Wie wird Neurodermitis bei Kindern medikamentös behandelt?</h3>
        <p>
          Gegen starken Juckreiz und Entzündungen helfen oft nur Medikamente.
          Babys und Kleinkinder haben jedoch eine wesentlich empfindlichere Haut
          als Erwachsene und reagieren anders auf Wirkstoffe. Deshalb sind
          manche Medikamente, die bei Erwachsenen eingesetzt werden, für Kinder
          nicht zugelassen. Heute stehen jedoch einige anerkannte Arzneimittel
          schon für kleine Kinder zur Verfügung, die entweder äußerlich
          (topisch) oder innerlich (systemisch) angewandt werden und gute
          Erfolge erzielen können. Informiert Euch und lasst Euch vom Kinderarzt
          oder Dermatologen beraten.
        </p>
        <ImageWithText
          imgName="jugendlichen_22"
          altImage="Ein Vater, trägt Creme auf die Wange seiner Tochter auf">
          <h3>Behandlung von Neurodermitis bei Babys und Kindern</h3>
          <p>
            Ausführliche Informationen zur medikamentösen Behandlung bei Kindern
            könnt Ihr hier nachlesen.
          </p>
          <Button text="Mehr erfahren" type="primary" to="/kinder-behandlung" />
        </ImageWithText>
        <ClickableBox
          title="Gut zu wissen"
          header="Neurodermitis ist nicht gleich Neurodermitis">
          <p>
            Das Krankheitsbild der Neurodermitis unterscheidet sich von Kind zu
            Kind. Damit die Therapie an die Bedürfnisse Eures Kindes angepasst
            werden kann, ist es wichtig, dass die Erkrankung zunächst richtig
            diagnostiziert und die Schwere korrekt festgestellt wird.
          </p>
        </ClickableBox>
      </div>
      <div
        data-summary="Kann ich Babys mit Neurodermitis baden?"
        className="havas-container light">
        <h2>Kann ich Babys mit Neurodermitis baden? </h2>

        <ImageWithText
          imgName="jugendlichen_23"
          imageOnRight={true}
          altImage="Ein Baby im Bad mit einer gelben Quietscheente">
          <p>
            Das Säuglingsbad hat viele positive Effekte und dass es die
            Eltern-Kind-Bindung fördert, ist sicher einer der bedeutsamsten.
            Deshalb solltet Ihr nicht darauf verzichten. Um seine Haut jedoch
            nicht zusätzlich auszutrocknen, solltet Ihr Euer Kind nicht zu
            häufig baden. Nutzt rückfettende Badezusätze. Das Badewasser sollte
            eine lauwarme Temperatur von höchstens 35 Grad haben. Tupft die Haut
            nach dem Baden ab, anstatt sie trockenzureiben. Nach dem Bad solltet
            Ihr die Haut Eures Kindes mit rückfettenden Pflegeprodukten
            eincremen.
          </p>
          <p>
            Bei schweren Ekzemen kann nach ärztlicher Absprache zwei- bis
            dreimal pro Woche ein Ölbad empfehlenswert sein.
          </p>
        </ImageWithText>
        <ClickableBox
          title="Gut zu wissen"
          header="Welche Pflegeprodukte eignen sich für Babys?">
          <p>
            Babys brauchen noch keine speziellen Pflegeprodukte wie Shampoos
            oder Seifen. Es reicht völlig aus, Haare und Körper der Kleinen mit
            klarem Wasser zu waschen.
          </p>
        </ClickableBox>
      </div>
      <div
        data-summary="Wie können Eltern ihren Kindern mit Neurodermitis helfen?"
        className="havas-container dark">
        <h2>Wie können Eltern ihren Kindern mit Neurodermitis helfen?</h2>
        <ImageWithText
          imgName="jugendlichen_24"
          altImage="Ein junger Mann, der sanft ein fröhliches Baby im Arm hält.">
          <p>
            Neurodermitis macht Babys und Kleinkindern schwer zu schaffen. Die
            Kleinen verstehen nicht, was mit ihnen los ist – sie wissen ihr
            Unwohlsein und ihre Beschwerden nicht anders auszudrücken als durch
            Schreien und Unruhe. Ablenkung, Geduld und liebevolle Zuwendung
            können ihnen helfen, sich zu entspannen.
          </p>
        </ImageWithText>
        <p>
          Denkt daran: Jede innige und achtsame Berührung setzt bei Eurem Kind
          positive Gefühle frei, die Stress und Ängste reduzieren und damit auch
          der Haut spürbar guttun.
        </p>
        <Akkordeon heading="Im Alltag wichtig: Trigger vermeiden" open={true}>
          <p>
            Die Haut Eures Kindes ist vielen Umwelteinflüssen ausgesetzt: Kälte,
            Wärme, trockene Luft, Pollen, Kleidung usw. Einige dieser Faktoren
            können die Neurodermitis negativ beeinflussen. Auf welche
            sogenannten Trigger ein Kind reagiert, ist nicht bei allen gleich,
            sondern individuell verschieden. Wenn Ihr Schüben vorbeugen möchtet,
            ist es wichtig, diese Trigger zu kennen, denn nur dann könnt Ihr sie
            gezielt vermeiden.
          </p>
          <Link to="/ursachen">
            Hier könnt Ihr mehr zum Thema Trigger lesen.
          </Link>
        </Akkordeon>
        <Akkordeon heading="Tipps gegen den Juckreiz">
          <p>
            Juckreiz begleitet Kinder mit Neurodermitis ständig, vor allem bei
            einem akuten Schub. Aber auch ohne die quälenden Entzündungen kann
            die trockene Haut Juckreiz verursachen. Für Babys und Kleinkinder
            ist es allerdings schwer zu verstehen, dass sie nicht kratzen
            sollen. Es gibt einige Tipps, die Euch im Alltag helfen können:
          </p>
          <Bulletlist
            insideAkkordeon={true}
            textList={[
              'Spezielle Anti-Juckreiz-Sprays oder -Cremes können das Jucken bei Babys mit Neurodermitis lindern.',
              'Auch gekühlte, feuchtigkeitsspendende Pflegeprodukte oder kühlende Umschläge bzw. eine Kühlkompresse aus dem Kühlschrank können gegen den Juckreiz helfen.',
              'Kurz geschnittene und glatt gefeilte Fingernägel verhindern starke Hautirritationen, wenn sich die Kleinen kratzen.',
              'Schweiß kann die Haut zusätzlich irritieren. Deshalb solltet Ihr darauf achten, dass bei Eurem Kind kein Wärmestau entsteht. Das ist gerade bei Babys wichtig, da sie ihre Körpertemperatur noch nicht so gut selbst regulieren können.',
              'Auch Ablenkung kann helfen, den Juckreiz zu vergessen. Findet etwas Schönes, Spannendes oder Spaßiges, das Euer Baby vom Juckreiz ablenkt.',
            ]}
          />
        </Akkordeon>
        <Akkordeon heading="Was tun gegen das Kratzen in der Nacht?">
          <p>
            Gerade in der Nacht können sich die Kleinen beim unkontrollierten
            Kratzen leicht selbst verletzten. Dafür gibt es spezielle
            Baumwollhandschuhe. Die können diese Gefahr verringern. Auch die
            Schlafkleidung kann Schutz bieten. Sie sollte am besten bis zum Hals
            geschlossen sein, denn je weniger Hautstellen freiliegen, desto
            geringer ist die Gefahr, dass Euer Kind sich im Schlaf aufkratzt. Es
            kann auch helfen, Euer Kind vor dem Schlafengehen gut einzucremen.
          </p>
        </Akkordeon>
      </div>
      <div
        data-summary="Was dürfen Babys und Kleinkinder mit Neurodermitis nicht essen?"
        className="havas-container light">
        <h2>Was dürfen Babys und Kleinkinder MIT Neurodermitis nicht essen?</h2>
        <ImageWithText
          imgName="jugendlichen_25"
          altImage="Eine lächelnde Mutter, die ihr kleines Kind mit Brei füttert.">
          <p>
            Diese Frage kann man leider nicht pauschal beantworten. Es gibt
            allerdings einige Lebensmittel, die allergische Reaktionen bei
            Kindern auslösen können, wie beispielsweise Milch, Eier, Nüsse oder
            Weizenprodukte, weshalb sich in machen Fällen die Intensität der
            Ekzeme verringern kann, wenn ihr diese Nahrungsmittel aus dem
            Speiseplan Eures Kindes streicht. Allerdings reagiert nur etwa jedes
            2. Kind mit Neurodermitis allergisch auf bestimmte Nahrungsmittel
            und nur bei etwa jedem 3. moderat bis schwer betroffenen Kind führt
            dies tatsächlich zu einer Verschlechterung des Hautbildes. Also sind
            übermäßige Einschränkungen selten notwendig und eine sogenannte
            Eliminationsdiät ist nur im Fall einer tatsächlichen Allergie
            sinnvoll.
          </p>
        </ImageWithText>
        <p>
          Ihr solltet Euch darüber im Klaren sein, dass Diäten zu einer Mangel-
          bzw. Fehlernährung führen können und sich dadurch der
          Gesundheitszustand Eures Kindes verschlechtern kann. Daher sollten
          spezielle Ernährungsformen immer unter ärztlicher Aufsicht erfolgen.{' '}
        </p>
        <p>
          Sprecht am besten mit dem Arzt Eures Kindes, denn spezielle Tests
          können Nahrungsmittelallergien in der Regel abklären. Liegen diese
          tatsächlich vor, könnt Ihr in Absprache mit einem Spezialisten den
          Ernährungsplan für Euer Kind anpassen.
        </p>
        <ArztefinderTeaser />
      </div>
      <div
        data-summary="Neurodermitis bei Kleinkindern – Der richtige Umgang mit der Erkrankung"
        className="havas-container dark">
        <h2>
          Neurodermitis bei Kleinkindern – Der richtige Umgang mit der
          Erkrankung
        </h2>
        <p>
          Je älter Kinder werden, umso mehr Worte nutzen sie. Allerdings ist die
          Sprache eines Kindes mit 3 Jahren noch einfach und der Wortschatz
          recht klein. Das hält die Kleinen aber nicht davon ab, zu reden. Sind
          die Möglichkeiten, sich sprachlich mitzuteilen, noch begrenzt, ist es
          wichtig, dass Euer Kind Zeit hat, Gedanken und Gefühle auszu-drücken.
          Dabei können Spiele oder Hilfsmittel wie Buntstifte helfen. Es gibt
          viele Formen, wie sich Kinder äußern können, und jedes Kind macht das
          anders.
        </p>
        <ZitatBox
          isLoudspeaker={false}
          text="„Mit Geduld und Aufmerksamkeit wird es leichter, die Sprache Eures Kindes in jedem Alter zu verstehen.“"
          applySecondImage={true}
        />
        <p>
          Mit der Zeit wird es auch für Euch einfacher Dinge zu erklären, denn
          Euer Kind wird immer verständiger und kann Zusammenhänge besser
          begreifen. Sprecht mit Eurem Kind altersgerecht. Das heißt, nutzt
          möglichst einfache Worte und Beschreibungen und „verabreicht
          Informationen nur in kleinen Portionen“.
        </p>
        <Akkordeon heading="Mit gutem Beispiel voran" open={true}>
          <p>
            Eine wichtige Ebene der Kommunikation kommt ganz ohne Worte aus.
            Kinder schauen sich viele Dinge von ihren Eltern ab. Deshalb ist es
            wichtig, dass Ihr Eurem Kind einen liebevollen Umgang mit seiner
            Haut vorlebt und es so annehmt, wie es ist. Je selbstverständlicher
            und geduldiger Ihr die notwendigen Pflegeroutinen angeht, umso
            leichter wird es Eurem Kind später fallen, diese in sein Leben zu
            integrieren.
          </p>
        </Akkordeon>
        <Akkordeon heading="Neurodermitis in der Kita oder dem Kindergarten">
          <p>
            Möglicherweise wird Euer Kind in der Kita oder dem Kindergarten auf
            seine Neurodermitis angesprochen. Sprecht mit den Erziehern und
            erklärt ihnen, welche Belastungen mit einer Neurodermitis
            einhergehen. Je mehr das Umfeld über medizinische Hintergründe,
            Symptome und Auswirkungen der Neurodermitis weiß, desto kompetenter
            können die Menschen damit umgehen und gegen Vorurteile und
            Missverständnisse angehen.
          </p>
        </Akkordeon>
        <Akkordeon heading="Weitere Unterstützung und Austausch in einer Community ">
          <p>
            Babys und Kleinkinder sind auf ihre Eltern angewiesen und sie
            brauchen viel Zuwendung und Unterstützung. Kommen die
            Herausforderungen der chronischen Erkrankung hinzu, kann das für die
            ganze Familie zu einer enormen Belastung werden. Nutzt das
            medizinische Netzwerk aus Dermatologen, Kinderärzten,
            Psychotherapeuten und Krankenkassen für Euch und lasst Euch beraten.
            Auch Selbsthilfegruppen können eine große Unterstützung sein, denn
            Ihr seid nicht allein. Der Austausch mit anderen Eltern kann eine
            wertvolle Unterstützung sein. Auch das Internet und die sozialen
            Medien können Euch Zugang zu seriösen und informativen
            Betroffenen-Communitys bieten.
          </p>
          <Link to="/anlaufstellen">
            Hier findet Ihr hilfreiche Adressen und Anlaufstellen.
          </Link>
        </Akkordeon>
        <ClickableBox
          title="Gut zu wissen"
          header="Das Neurodermitis-Begleiter-Team">
          <p>
            Im Neurodermitis-Begleiter-Team unterstützen Euch geschulte
            Fachkräfte mit medizinisch gesicherten Informationen zur Erkrankung
            und Therapie. Sie helfen auch mit gesundheitspsychologischen Tipps
            oder praktischen Ratschlägen weiter.
          </p>
          <Link
            style={{
              color: 'white',
              marginTop: '20px',
              display: 'inline-block',
            }}
            to="/neurodermitis-berater-kontaktieren">
            Hier findet Ihr die Kontaktdaten.
          </Link>
        </ClickableBox>
      </div>
      <div data-summary="Fazit" className="havas-container light">
        <h2>Fazit</h2>
        <p>
          Eltern können ihrem Kind die Last Ihrer Erkrankung nicht abnehmen,
          aber sie können ihm den Rücken stärken, damit es sein Leben mit
          Neurodermitis selbstbestimmt gestalten kann. Dabei seid Ihr nicht
          allein. Viele Entscheidungen werden Euch wahrscheinlich
          leichterfallen, wenn Ihr den Rat von Experten einholt, denen Ihr
          vertraut. Sprecht mit den Ärzten Eures Kindes und lasst Euch von
          Spezialisten und Eurem Umfeld helfen. Mit einer angepassten Therapie
          und etwas Unterstützung steht einem normalen Familienleben in der
          Regel nichts im Weg.
        </p>
        <ArztefinderTeaser />
      </div>
      <div style={{ backgroundColor: `${isMobile && '#eeeaf6'}` }}>
        <div className={`havas-container`}>
          <div
            className="bottom-text"
            style={{
              marginTop: `${isMobile ? '0' : '120px'}`,
            }}>
            <p style={{ marginTop: `${isMobile && '0'}` }}>
              <sup>1 </sup>
              https://www.kindergesundheit-info.de/themen/ernaehrung/0-12-monate/allergierisiko
              (abgerufen am 25.01.2023).
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Babies"
    metaTitle="Neurodermitis bei Babys und Kleinkindern"
    description="Etwa jedes 4. Kind ist von Neurodermitis betroffen. Die Ekzeme treten bei Babys und Kleinkindern vor allem am Kopf, im Gesicht und an den Ohren auf."
    trustKeyWords="Neurodermitis Säugling, Neurodermitis bei Säuglingen, Neugeborenen Neurodermitis, Neugeborenen Ekzem, Ekzem bei Babys, Anfang Neurodermitis bei Babys, Neurodermitis bei Babys behandeln, Juckreiz bei Babys, Juckreiz bei Babys lindern, Atopische Dermatitis Säugling, Creme für Babys bei Neurodermitis "
  />
);
