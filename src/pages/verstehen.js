import { Link } from 'gatsby';
import React, { useContext } from 'react';
import AchtungBox from '../components/AchtungBox';
import Bulletlist from '../components/Bulletlist';
import CardMedium from '../components/CardMedium';
import ClickableBox from '../components/ClickableBox';
import Button from '../components/Button';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import Slider from '../components/Slider';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';

export default function Verstehen() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className={`havas-container light`}>
        <Hero
          headingTextList={['NEURODERMITIS', 'VERSTEHEN']}
          coloredWordsArr={['VERSTEHEN']}
          imgName="verstehen_img_1"
          altImg="Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt."
          text="Neurodermitis ist eine komplexe chronisch-entzündliche
                Hauterkrankung mit unterschiedlichen Facetten, die viele
                Herausforderungen mit sich bringt. Je besser Du die Erkrankung
                kennst, umso besser kannst Du lernen, damit umzugehen. Wir erklären
                Dir in diesem Artikel, was Neurodermitis ist, wie sich die
                Erkrankung zeigt und vieles mehr."
        />
        <MobileThemenübersicht className="dropdown-1" />
        <p>
          Unsere Haut hat viele Aufgaben: Sie regelt die Körpertemperatur,
          schützt das Körperinnere vor Verletzungen und Austrocknung, sie hilft
          dem Körper, Vitamin D zu produzieren, und ist ein wichtiges
          Sinnesorgan. Einige Menschen bezeichnen die Haut als „Spiegel der
          Seele“, weil sie auch Emotionen offenbaren kann. Ist die Haut krank,
          leiden Betroffene oft nicht nur körperlich – auch die psychische
          Belastung kann sehr hoch sein.
        </p>
      </div>
      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Was ist Neurodermitis?">
        <h2>WAS IST NEURODERMITIS?</h2>
        <p>
          Neurodermitis ist eine chronische Erkrankung, die nicht ansteckend
          ist. Sie äußert sich vor allem durch Entzündungen der Haut. Diese
          entstehen dadurch, dass das Immunsystem auf eigentlich harmlose Stoffe
          reagiert, als wären es gefährliche Angreifer, und immer wieder
          Entzündungen entfacht, um sie zu bekämpfen.
        </p>

        <Bulletlist
          heading="Häufige Symptome sind unter anderem:"
          textList={[
            'Starker Juckreiz',
            'Entzündete, teilweise blutende Hautstellen (Ekzeme)',
            'Hautrötungen (Erytheme)',
            'Hautverdickungen und Vergröberungen der Haut (Lichenifikationen)',
            'Dicke, tiefsitzende Knoten (Prurigo-Knoten)',
            'Hauteinrisse, z. B. an Mundwinkeln, Ohrläppchen oder Fingern (Rhagaden)',
            'Trockene Haut (Xerose)',
            'Schuppung',
            'Schwellung',
            'Nässende Bläschen',
          ]}
        />

        <ClickableBox
          title="Gut zu wissen"
          header="Was ist ein atopisches Ekzem?">
          <p>
            Neurodermitis wird in Fachkreisen auch als atopisches Ekzem oder
            atopische Dermatitis bezeichnet. Manchmal wird auch der Begriff
            allergische Dermatitis genutzt.
          </p>
          <p>
            <strong>Atopisch</strong> heißt, der Körper reagiert allergisch auf
            den Kontakt mit ansonsten harmlosen Substanzen aus der Umwelt. Diese
            Überempfindlichkeit ist häufig genetisch bedingt.
          </p>
          <p>
            <strong>Ekzem und Dermatitis</strong> sind Überbegriffe für eine
            Entzündungsreaktion der Haut, die mit Juckreiz, Rötung, Schuppung,
            Schwellung, nässenden Bläschen und Verdickung (bei chronischen
            Verläufen) einhergehen kann.
          </p>
        </ClickableBox>

        <h3 className="text-left">
          Neurodermitis ist mehr als eine Hautkrankheit
        </h3>

        <p>
          Bei der Neurodermitis handelt es sich um eine sogenannte
          Systemerkrankung. Neurodermitis ist also nicht einfach ein Ausschlag;
          die Krankheit wird durch eine Überreaktion des Immunsystems
          verursacht, die immer wieder Entzündungen entfacht. Im Falle der
          atopischen Dermatitis zeigen sich diese Entzündungen an der Haut.
          Neurodermitis kann sich aber auf viele Lebensbereiche auswirken und
          sowohl den Körper als auch die Psyche beeinträchtigen.
        </p>

        <p className="mt-4">
          Das Spektrum der Erkrankung reicht von milden, symptomarmen Formen bis
          hin zu schweren Verlaufsformen, bei denen eine ununterbrochene,
          intensive Therapie notwendig ist. Für Patienten mit mittelschwerer bis
          schwerer Ausprägung kann Neurodermitis zu einer dauerhaften Belastung
          werden. Schlafprobleme, Konzentrationsschwierigkeiten, psychische
          Belastungen, Probleme in der Familie oder in der Partnerschaft sowie
          berufliche Beeinträchtigungen können durch Neurodermitis verursacht
          werden.
        </p>

        <AchtungBox
          heading="Auch Depressionen sind bei Neurodermitis keine Seltenheit:"
          text="In Zeiten von makellosen Schönheitsidealen werden Hautveränderungen häufig mit „krank“ und „ungepflegt“ assoziiert. Das äußere Erscheinungsbild spielt also eine bedeutsame Rolle für das Wohlbefinden. Menschen mit Neurodermitis schämen sich häufig für ihre Haut und isolieren sich. Daraus können sich Angstzustände, Schlafstörungen oder sogar Depressionen entwickeln. Wenn der Leidensdruck zu hoch ist, kann eine seelische Betreuung oder Psychotherapie Dir eventuell Unterstützung geben."
          iconName="exclamation_icon"
        />

        <h3>
          Etwa jeder zweite Patient ist von einer mittelschwer bis schwer
          ausgeprägten Neurodermitis betroffen.
        </h3>

        <p>
          Die atopische Dermatitis – wie Mediziner die Erkrankung nennen – tritt
          meist in Schüben auf und äußert sich vor allem durch sichtbare
          Hautveränderungen und starken Juckreiz. Das heißt, es gibt Phasen, in
          denen Du kaum Beschwerden hast. Dann gibt es wiederum Zeiten, in denen
          juckende, entzündete Stellen das Hautbild bestimmen. Die Erkrankung
          ist jedoch nie wirklich verschwunden. Selbst in weniger aktiven
          Phasen, wenn die Neurodermitis für das Auge nicht sichtbar ist,
          „brodelt“ die Entzündung unter der Haut weiter.
        </p>

        <SelbsttestTeaser />
      </div>

      <div
        className={`havas-container ${isMobile ? 'dark' : 'light'}`}
        data-summary="Was fehlt dem Körper bei Neurodermitis?"
        style={{ backgroundColor: `${isMobile ? 'white' : ''}` }}>
        <h2>WAS FEHLT DEM KÖRPER BEI NEURODERMITIS?</h2>

        <p>
          Die Hautbarriere besteht aus Hornzellen, die übereinandergeschichtet
          sind. Diese Zellen werden normalerweise durch Hornfette
          zusammengehalten. Bei Menschen mit Neurodermitis werden nicht
          ausreichend Fettstoffe produziert. Der atopischen Haut mangelt es an
          Feuchthaltefaktoren und sie kann schlecht Wasser binden. Der Verbund
          der Hautzellen wird durchlässig und die Haut ist sehr trocken und
          empfindlich.
        </p>

        <div className="card-container">
          <CardMedium
            image="MOD1"
            altImg="Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis"
            title="Intakte Hautbarriere"
            text="Die intakte Haut stellt eine natürliche und geschlossene Barriere zwischen der Innen- und Außenwelt dar. Für Fremdstoffe ist es fast unmöglich, sie im gesunden Zustand zu überwinden."
          />

          <CardMedium
            image="MOD2"
            altImg="Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis"
            title="Beeinträchtigte Hautbarriere"
            text="Die Hautbarriere bei Neurodermitis ist beeinträchtigt. Ihr fehlen wichtige Bestandteile, die zur Aufrechterhaltung der Schutzfunktion nötig sind (z.B. Feuchtigkeit). Fremdstoffe können eindringen und weitere Entzündungen hervorrufen."
          />

          <CardMedium
            className="biggerTextImage"
            image="MOD3"
            altImg="Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis"
            title="Überaktives Immunsystem"
            text="Zudem ist das Immunsystem überaktiv und bestimmte Immunzellen produzieren 
            vermehrt Botenstoffe, die dauerhaft Entzündungsreaktionen auslösen."
          />
        </div>

        <ClickableBox
          title="Gut zu wissen"
          header="Trockene Haut oder trockenes Ekzem?"
          headerOpen="Was ist der Unterschied zwischen „trockener Haut“ und einem „trockenen Ekzem“?">
          <p>
            Trockene Haut hat noch eine normale Hautfarbe, sie schuppt aber
            verstärkt und kann rissig sein; oft auch begleitet von einem
            Spannungsgefühl oder Juckreiz. Ein trockenes Ekzem dagegen geht mit
            einer Entzündungsreaktion einher und deshalb mit einer sichtbaren
            Rötung der Haut. Bei einem Hautekzem kann die Haut zudem schuppen
            und es können nässende Bläschen auftreten.
          </p>
        </ClickableBox>
      </div>

      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Neurodermitis – ein Kreislauf von Jucken und Kratzen"
        id="juck-kratz-kreislauf">
        <h2>Neurodermitis – ein Kreislauf von Jucken und Kratzen</h2>
        {isMobile ? (
          <p>
            Ein besonders belastendes Symptom der Neurodermitis ist der starke
            Juckreiz. Gerade bei einem akuten Schub sind Menschen mit
            Neurodermitis häufig in einem Teufelskreis aus Jucken und Kratzen
            gefangen. Das Kratzen der Haut lindert den Neurodermitis-Juckreiz
            aber nur für einen kurzen Moment. Langfristig fügst Du Deiner Haut
            weitere Verletzungen zu, die in der Folge zu weiteren
            Juckreizattacken führen.
          </p>
        ) : (
          <>
            <p>
              Ein besonders belastendes Symptom der Neurodermitis ist der starke
              Juckreiz. Gerade bei einem akuten Schub sind Menschen mit
              Neurodermitis häufig in einem Teufelskreis aus Jucken und Kratzen
              gefangen.
            </p>{' '}
            <p>
              Das Kratzen der Haut lindert den Neurodermitis-Juckreiz aber nur
              für einen kurzen Moment. Langfristig fügst Du Deiner Haut weitere
              Verletzungen zu, die in der Folge zu weiteren Juckreizattacken
              führen.
            </p>
          </>
        )}
        <Slider
          minHeightDesktop="238px"
          imageDesktopHeight="260.55px"
          items={[
            {
              altImage:
                'Darstellung des Juck-Kratz-Kreislauf bei Neurodermitis',
              imgName: 'LMN_reizstoffe',
              heading: '1. Geschädigte Hautbarriere',
              text: [
                'Die ',
                <strong>Barrierefunktion</strong>,
                ' der Haut ist geschädigt. Reizstoffe und Allergene können leichter eindringen.',
              ],
            },
            {
              altImage:
                'Darstellung des Juck-Kratz-Kreislauf bei Neurodermitis',
              imgName: 'LMN_entzuendung2xfb88',
              heading: '2. Rötung',
              text: [
                'Das Immunsystem reagiert mit Entzündung. Diese ist an der ',
                <strong>Rötung</strong>,
                ' der Haut erkennbar.',
              ],
            },
            {
              altImage:
                'Darstellung des Juck-Kratz-Kreislauf bei Neurodermitis',
              imgName: 'LMN_juckreiz2xc741',
              heading: '3. Juckreiz',
              text: [
                'An der entzündeten, geröteten Stelle beginnt die Haut zu ',
                <strong>jucken</strong>,
                '.',
              ],
            },
            {
              altImage:
                'Darstellung des Juck-Kratz-Kreislauf bei Neurodermitis',
              imgName: 'LMN_kratzen2xd4d8',
              heading: '4. Kratzen',
              text: [
                'Als Reaktion auf den Juckreiz wird gekratzt. Dadurch wird die oberste Hautschicht zusätzlich geschädigt und weitere Allergene, aber auch Bakterien und Viren können eindringen. Das Immunsystem reagiert weiter und verstärkt mit Entzündungen. Der ',
                <strong>Juck-Kratz-Kreislauf</strong>,
                ' beginnt.',
              ],
            },
          ]}
        />
        <p>
          Mit welchen Strategien Du den Neurodermitis-Juckreiz lindern kannst,
          erfährst Du im Ratgeber Juckreiz.
        </p>
        <ArztefinderTeaser />
      </div>

      <div
        className={`havas-container ${isMobile ? 'dark' : 'light'}`}
        data-summary="Welche Hautstellen können von Symptomen betroffen sein?">
        <h2>Welche Hautstellen können von Symptomen betroffen sein?</h2>

        {isMobile && (
          <>
            <p>
              Die Bereiche, an denen Neurodermitis-Symptome auftreten, können
              sich im Krankheitsverlauf verändern. Die Neurodermitis äußert sich
              in jedem Alter häufig unterschiedlich. Im frühen Kindesalter sind
              Ekzeme im Gesicht, auf der Kopfhaut sowie an den Streckseiten von
              Armen und Beinen vorherrschend. Bei Jugendlichen und Erwachsenen
              treten in der Regel Ekzeme am Hals auf, an den Augenlidern,
              Ellenbogen, in den Kniekehlen, sowie an den Füßen und Händen.
            </p>
            <ImageWithText
              noTilt={true}
              imgName="verstehen_img_7"
              altImage="Darstellung verschiedener Hautstellen, an denen Symptome auftreten können und wie diese sich mit zunehmendem Alter verändern.">
              <Link to="/symptome">
                Detaillierte Informationen zu betroffenen Hautstellen kannst Du
                hier nachlesen.
              </Link>
            </ImageWithText>
          </>
        )}

        {!isMobile && (
          <ImageWithText
            noTilt={true}
            imgName="verstehen_img_7"
            altImage="Darstellung verschiedener Hautstellen, an denen Symptome auftreten können und wie diese sich mit zunehmendem Alter verändern.">
            <p>
              Die Bereiche, an denen Neurodermitis-Symptome auftreten, können
              sich im Krankheitsverlauf verändern. Die Neurodermitis äußert sich
              in jedem Alter häufig unterschiedlich. Im frühen Kindesalter sind
              Ekzeme im Gesicht, auf der Kopfhaut, sowie an den Streckseiten von
              Armen und Beinen vorherrschend.
            </p>
            <p>
              Bei Jugendlichen und Erwachsenen treten in der Regel Ekzeme am
              Hals auf, an den Augenlidern, Ellenbogen, in den Kniekehlen, sowie
              an den Füßen und Händen.
            </p>
            <Link to="/symptome">
              Detaillierte Informationen zu betroffenen Hautstellen kannst Du
              hier nachlesen.
            </Link>
          </ImageWithText>
        )}
      </div>

      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Zahlen und Fakten zu Neurodermitis">
        <h2>Zahlen und Fakten zu Neurodermitis</h2>

        <p style={{ alignSelf: 'center' }}>
          Weltweit leiden Millionen Menschen an Neurodermitis – mit steigender
          Tendenz.
        </p>

        <Slider
          items={[
            {
              altImage: 'Grafische Darstellung Neurodermitis in Zahlen',
              imgName: 'MicrosoftTeams-image21',
              text: [
                'Neurodermitis kommt etwa gleich häufig bei Männern und Frauen vor.',
                <sup>1</sup>,
              ],
            },
            {
              altImage: 'Grafische Darstellung Neurodermitis in Zahlen',
              imgName: 'MicrosoftTeams-image22',
              text: [
                'Bei mehr als 3,6 Millionen Menschen in Deutschland haben Ärzte atopische Dermatitis diagnostiziert.',
                <sup>2</sup>,
              ],
            },
            {
              altImage: 'Grafische Darstellung Neurodermitis in Zahlen',
              imgName: 'MicrosoftTeams-image24',
              text: [
                'Die Neurodermitis kann bei Menschen aller Hautfarben auftreten.',
                <sup>3</sup>,
              ],
            },
            {
              altImage: 'Grafische Darstellung Neurodermitis in Zahlen',
              imgName: 'MicrosoftTeams-image26',
              text: [
                'Etwa jedes 4. Baby und Kleinkind ist betroffen.',
                <sup>4</sup>,
              ],
            },
            {
              altImage: 'Grafische Darstellung Neurodermitis in Zahlen',
              imgName: 'MicrosoftTeams-image27',
              text: ['Etwa jeder 12. Jugendliche ist betroffen.', <sup>4</sup>],
            },
            {
              altImage: 'Grafische Darstellung Neurodermitis in Zahlen',
              imgName: 'MicrosoftTeams-image23',
              text: ['Etwa jeder 25. Erwachsene ist betroffen.', <sup>4</sup>],
            },
          ]}
        />

        <p>
          Eine Neurodermitis tritt in der Regel bereits in den ersten beiden
          Lebensjahren auf, typischerweise zwischen dem 3. und 6. Lebensmonat.
          Einige Kinder erkranken auch später. Dass sich eine Neurodermitis erst
          nach dem 5. Lebensjahr entwickelt, ist jedoch eher selten.
        </p>

        <Link to="/kinder-behandlung">
          Viele hilfreiche Informationen zu Neurodermitis bei Babys und Kindern
          findest Du hier.
        </Link>

        <p>
          Schätzungen zufolge ist die Erkrankung bei über 80% aller Kinder etwa
          10 Jahre nach dem ersten Auftreten überstanden oder deutlich
          abgeklungen. Die Neurodermitis wächst sich aber nicht immer aus, sie
          kann auch eine Zeit lang verschwinden und im Alter wiederkehren. Dass
          eine Neurodermitis im Erwachsenenalter zum ersten Mal auftritt, ist
          allerdings selten.
        </p>
        <p>
          Die Häufigkeit der Neurodermitis ist unter anderem auch vom Klima
          abhängig. Im eher sonnenarmen Nordeuropa sind bis zu 25% betroffen,
          wohingegen an den Küsten Südeuropas nur etwa 1% der Menschen
          erkranken.
        </p>
        <p>
          Bei der Hautfarbe macht die Neurodermitis keine Unterschiede, sie kann
          bei Menschen aller Hautfarben auftreten. Interessanterweise sind
          Menschen in Städten häufiger betroffen als die Landbevölkerung. Warum
          das so ist, ist bisher allerdings unklar.
        </p>

        <SelbsttestTeaser />
      </div>

      <div
        className={`havas-container ${isMobile ? 'dark' : 'light'}`}
        data-summary="Was löst Neurodermitis aus?">
        <h2>Was löst Neurodermitis aus?</h2>
        <p>
          Was die genauen Ursachen für Neurodermitis sind, ist bis heute noch
          nicht vollständig geklärt. Man weiß jedoch, dass die folgenden
          Faktoren den Ausbruch der Erkrankung begünstigen können:
        </p>
        <Bulletlist
          className="violet"
          textList={[
            'Erbliche Vorbelastungen',
            'Eine gestörte Barrierefunktion der Haut',
            'Eine Überempfindlichkeit des Immunsystems',
            'Verschiedene Umweltfaktoren',
          ]}>
          <Link style={{ display: 'block' }} href="/ursachen">
            Detaillierte Informationen zu den Ursachen einer Neurodermitis
            kannst Du hier nachlesen.
          </Link>
        </Bulletlist>
      </div>

      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Neurodermitis kommt selten allein – Der atopische Formenkreis">
        <h2>
          Neurodermitis kommt selten allein –{isMobile ? <></> : <br />} Der
          atopische Formenkreis
        </h2>

        <ImageWithText
          className="reverse"
          imgName="verstehen_img_3"
          altImage="Grafische Darstellung des atopischen Formenkreises">
          <p>
            Neurodermitis zählt zu den Erkrankungen des sogenannten atopischen
            Formenkreises. Eine Atopie beschreibt die genetische Veranlagung,
            empfindlich auf verschiedene, oft harmlose Stoffe aus der Umwelt zu
            reagieren. Weitere Erkrankungen des atopischen Formenkreises sind
            unter anderem allergisches Asthma, allergische Bindehautentzündung,
            allergischer Schnupfen (allergische Rhinitis) und
            Nahrungsmittelallergien.
          </p>
          <p>
            Menschen mit einer atopischen Erkrankung haben oft ein erhöhtes
            Risiko, weitere Erkrankungen aus dem atopischen Formenkreis zu
            entwickeln.
          </p>
        </ImageWithText>
      </div>

      <div
        className={`havas-container ${isMobile ? 'dark' : 'light'}`}
        data-summary="Wie wird Neurodermitis diagnostiziert?">
        <h2>Wie wird Neurodermitis diagnostiziert?</h2>
        <p>
          Bei der Untersuchung wird Deine Haut gründlich unter die Lupe genommen
          und ein besonderes Augenmerk auf die Ekzeme gelegt. Zudem werden
          oftmals Fragen zur familiären Vorgeschichte gestellt, denn die
          Neurodermitis hat meist auch eine genetische Komponente.
        </p>

        <ClickableBox title="Gut zu wissen" header="Stellung der Diagnose">
          <p>
            Da verschiedene Hauterkrankungen ähnliche Symptome wie eine
            Neurodermitis hervorrufen, kann in der Regel nur ein Dermatologe
            eine Diagnose stellen.
          </p>
        </ClickableBox>

        <ArztefinderTeaser />

        <Bulletlist
          className="violet"
          heading="Anzeichen, die für eine Neurodermitis sprechen, sind unter anderem:"
          textList={[
            'Dem Alter entsprechende Ausprägungen und Verteilungen der Beschwerden',
            'Ein Beginn der Beschwerden im frühen Lebensalter',
            'Andere atopische Erkrankungen bei Dir selbst oder bei nahen Verwandten',
            'Ein schubweiser Verlauf',
          ]}
        />

        <p style={{ marginTop: isMobile ? 40 : 80 }}>
          Zudem gibt es sogenannte Atopie-Zeichen, die bei Menschen mit
          atopischen Erkrankungen häufig auftreten und bei Hautekzemen meist
          ebenfalls für eine Neurodermitis sprechen.
        </p>

        <Bulletlist
          className="violet"
          heading="Dazu zählen:"
          textList={[
            'Trockene Haut',
            'Verstärkte Linienzeichnung an den Innenflächen der Hände',
            'Eine sogenannte Dennie-Morgan-Falte – eine doppelte Lidfalte unterhalb der Augen',
            'Das feste Streichen über die Haut mit einem Holzspatel ruft bei atopischen Patienten eine weiße Linie hervor (weißer Dermographismus), während die Linie bei Nicht-Atopikern gerötet ist.',
            'Dunkle Haut im Bereich der Augen',
            'Ausdünnung der seitlichen Augenbrauen (Hertoghe-Zeichen)',
            'Neigung zu Ohr- und Mundwinkeleinrissen',
          ]}
        />

        <h3>Allergietests</h3>
        <p>
          Häufig wird bei der Neurodermitis-Diagnose auch getestet, ob Du
          Allergien hast. Neben einer Blutuntersuchung, bei der spezifische
          Antikörper nachgewiesen werden, kommt auch der sogenannte Prick-Test
          zum Einsatz. Dabei ritzt ein Arzt Dir kleine Mengen eines Allergens in
          die Haut und bewertet die Reaktion nach 15 Minuten. Bei beiden
          Untersuchungen zeigt ein positives Ergebnis allerdings nur an, dass Du
          auf bestimmte Allergene empfindlich reagierst. Es beweist jedoch
          nicht, dass dieses Allergen auch Deine Neurodermitis beeinflusst.
        </p>

        <AchtungBox
          text="Sogenannte Provokationstests, bei denen Du bewusst in Kontakt mit einem mutmaßlichen Allergen gebracht wirst, darf nur ein Arzt machen. Mache solche Tests auf keinen Fall in Eigenregie, da es dabei zu lebensgefährlichen Zwischenfällen kommen kann!"
          iconName="exclamation_icon"
        />

        <SelbsttestTeaser />
      </div>

      <div
        className={`havas-container ${isMobile ? 'light' : 'dark'}`}
        data-summary="Wie wird Neurodermitis behandelt?">
        {isMobile ? (
          <h2>Wie wird Neuroder - mitis behandelt?</h2>
        ) : (
          <h2>Wie wird Neurodermitis behandelt?</h2>
        )}

        <ImageWithText
          imgName="verstehen_img_2"
          altImage="Eine Dose Creme von oben">
          <p>
            Neurodermitis ist nicht heilbar, aber Du kannst einiges tun, um
            Deine Erkrankung im Zaum zu halten. Das A und O bei Neurodermitis
            ist die konsequente Pflege Deiner Haut mit rückfettenden Cremes,
            selbst wenn Du gerade keine Beschwerden hast. Diese Basistherapie
            sollte ein fester Bestandteil in Deinem Alltag sein.
          </p>
          <Link to="/ueberblick">
            Weitere Informationen zur Basistherapie findest Du hier.
          </Link>
        </ImageWithText>

        <h3>Medikamentöse Therapien</h3>

        <p>
          Bei einem akuten Neurodermitis-Schub mit Entzündungen wird Dir Dein
          Dermatologe wirkstoffhaltige Präparate verschreiben, die äußerlich
          angewendet werden. Sie werden direkt auf die betroffenen Stellen
          aufgetragen. Neben diesen sogenannten{' '}
          <Link to="/aeusserliche-therapien">topischen Präparaten</Link> gibt es
          auch systemische Medikamente zur Behandlung von Neurodermitis. Diese
          wirken von innen und werden in der Regel langfristig angewendet, um
          die Neurodermitis an der Wurzel zu behandeln. Das heißt, sie kommen
          nicht nur bei einem akuten Schub zum Einsatz.
        </p>

        <p>
          Im Bereich „Behandlung“ findest Du detaillierte Informationen zu den{' '}
          <Link to="/aeusserliche-therapien">äußerlichen</Link> und{' '}
          <Link to="/innerliche-therapien">innerlichen Therapieformen</Link>{' '}
          sowie zu{' '}
          <Link to="/ergaenzende-therapien">ergänzenden Therapien.</Link>
        </p>
        <ImageWithText
          imgName="uberblick_img_1"
          altImage="Eine Dose Creme von oben">
          <h3>Behandlung von Neurodermitis</h3>
          <p style={{ textAlign: `${isMobile ? 'left' : 'center'}` }}>
            Informiere Dich hier über die verschiedenen Behandlungsmöglichkeiten
            bei Neurodermitis.
          </p>
          <Button text="Mehr erfahren" type="primary" to="/ueberblick" />
        </ImageWithText>
      </div>
      <div
        className={`havas-container ${isMobile ? '`dark`' : 'light'}`}
        data-summary="Fazit">
        <h2>Fazit</h2>
        <p>
          Die Neurodermitis ist eine chronische Erkrankung, die bisher nicht
          heilbar ist. Auch wenn bei vielen Kindern die Beschwerden im Laufe der
          Zeit nachlassen, kann Dich die Erkrankung Dein ganzes Leben begleiten.
          Eine konsequente Pflege und eine für Dich passende Behandlung können
          Dir helfen, einen positiven Umgang mit der Erkrankung zu finden – denn
          auch mit Neurodermitis kannst Du ein glückliches und erfülltes Leben
          führen. Werde aktiv und lass Dich beraten.
        </p>

        <ArztefinderTeaser />
      </div>
      <div className={`havas-container ${isMobile ? 'light' : 'dark'}`}>
        <div className="bottom-text">
          <p style={{ marginTop: '7px' }}>
            <sup>1</sup> Langen U et al. Bundesgesundheitsbl 2013;56:698–706.
          </p>
          <p
            style={{ wordBreak: 'break-all', width: '101%', marginTop: '7px' }}>
            <sup>2</sup>{' '}
            https://www.barmer.de/presse/presseinformationen/pressemitteilungen/barmer-analyse-vor-allem-kinder-und-frauen-
            leiden-unter-neurodermitis-257016 (zuletzt geöffnet: 03/2021).
          </p>
          <p style={{ marginTop: '7px' }}>
            <sup>3</sup> Kaufmann BP et al. Exp Dermatol 2018;27(4):340–357.
          </p>
          <p style={{ marginTop: '7px' }}>
            <sup>4</sup> ecarf.org/info-portal/erkrankungen/neurodermitis
            (zuletzt geöffnet: 03/2021).
          </p>
        </div>
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Verstehen"
    metaTitle="Neurodermitis verstehen"
    description="Neurodermitis ist eine chronisch-entzündliche Erkrankung mit unterschiedlichen Facetten, die nicht ansteckend ist und sich überwiegend an der Haut zeigt."
    trustKeyWords="Atopische Dermatitis, Atopisches Ekzem, Atopische Haut, Neurodermitis Schub, Neurodermitis Ausschlag, Neurodermitis Allergie, Neurodermitis heilbar, Neurodermitis im Alter, Neurodermitis Bläschen, Neurodermitis Juckreiz lindern, Trockenes Ekzem, Allergische Dermatitis"
  />
);
