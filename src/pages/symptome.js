import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Bulletlist from '../components/Bulletlist';
import CardSmall from '../components/CardSmall';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import TippsBox from '../components/TippsBox';

export default function Symptome() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={['Neurodermitis', 'Symptome']}
          coloredWordsArr={['Symptome']}
          imgName="symptome_1"
          altImg="Eine Junge Frau, die sich am Ellenbogen kratzt"
          text="Neurodermitis ist eine sogenannte Systemerkrankung. Das bedeutet, sie ist nicht nur auf die Haut begrenzt, sondern kann sich auf den kompletten Organismus auswirken. Das Spektrum der Erkrankung reicht dabei von milden, symptomarmen bis hin zu schweren Verlaufsformen."
        />
        <MobileThemenübersicht className="dropdown-1" />
      </div>
      <div
        className="havas-container light"
        data-summary="Wo fängt Neurodermitis an?">
        <h2 style={{ marginTop: `${isMobile ? '0' : '10px'}` }}>
          Wo fängt Neurodermitis an?
        </h2>
        <p>
          Darauf gibt es keine eindeutige Antwort, denn Neurodermitis zeigt sich
          bei jedem Menschen anders. Typische Anzeichen sind unter anderem
          entzündete, teilweise blutende Hautstellen, sogenannte Ekzeme,
          Hautrötungen und Hautverdickungen, trockene Haut, Schuppen,
          Schwellungen und nässende Bläschen. Ein besonders belastendes Symptom
          der Neurodermitis ist der starke Juckreiz.
        </p>

        <ClickableBox
          title="Gut zu wissen"
          header="Weitere Bezeichnungen für Neurodermitis">
          <p>
            Neurodermitis wird auch atopische Dermatitis oder atopisches Ekzem
            genannt. Seltener wird auch der Begriff allergische Dermatitis
            genutzt.
          </p>
        </ClickableBox>

        <p>
          An welchen Körperstellen Symptome vorkommen, kann sich im Verlauf der
          Krankheit verändern und ist häufig vom Alter abhängig.
        </p>
        <Bulletlist
          // className="small"
          textList={[
            'Typische Neurodermitis-Symptome bei Babys und Kindern sind Ekzeme im Bereich des Gesichts, der Kopfhaut sowie an den Streckseiten von Armen und Beinen.',
            'Bei Jugendlichen und Erwachsenen treten in der Regel Ekzeme eher am Hals, an den Augenlidern, Ellenbogen, in den Kniekehlen sowie an den Händen und Füßen auf.',
          ]}
        />

        <p>
          Das heißt aber nicht, dass die Ekzeme je nach Alter nur an diesen
          Stellen auftreten. Im Grunde kann es bei Neurodermitis jederzeit in
          allen Hautbereichen zu den typischen Hautentzündungen kommen.
        </p>
        <p>
          Detaillierte Informationen dazu, wie sich die Neurodermitis an
          verschiedenen Körperstellen zeigt, findest Du auf folgenden Seiten:
        </p>
        <CardSmall
          style={isMobile ? {} : { marginTop: 40 }}
          className="symptomeImages"
          elements={[
            [
              'symptome-3',
              'Frau untersucht im Spiegel ihre Kopfhaut',
              'Neurodermitis auf der Kopfhaut',
              '/kopfhaut',
            ],
            [
              'symptome-2',
              'Zwei junge Frauen bilden mit ihren Händen gemeinsam ein Herz',
              ['Neurodermitis an', <br />, 'der Hand'],
              '/hand',
            ],
            [
              'symptome-4',
              'Frau kratzt sich im Gesicht',
              'Neurodermitis im Gesicht',
              '/gesicht',
            ],
          ]}
        />

        <ImageWithText
          imageOnRight={true}
          imgName="symptome-5"
          altImage="Nahaufnahme einer Hand, die das Bein eines Babys eincremt">
          <h3>Wie sieht Neurodermitis am Anfang aus?</h3>
          <p>
            Bei Babys und Kleinkindern kann Milchschorf ein erstes Anzeichen für
            eine Neurodermitis sein. Die Betonung liegt auf „kann“, denn nicht
            jedes Kind mit Milchschorf entwickelt auch eine atopische
            Dermatitis. Anzeichen für eine Neurodermitis im Kindesalter können
            Bläschen sein, die zu Knötchen werden und sich in Hautfalten des
            Körpers ausbreiten.
          </p>
        </ImageWithText>
        <h3>
          Wie sieht Neurodermitis aus, wenn sie erst im Erwachsenenalter
          auftritt?
        </h3>
        <p>
          Bei Erwachsenen im Alter zwischen dem 20. und dem 40. Lebensjahr die
          keine Neurodermitis-Symptome in der Kindheit angeben, treten oft nur
          milde Symptome auf mit Ekzemen besonders im Kopf- und Halsbereich.
        </p>
        <ImageWithText
          imgName="symptome-6"
          altImage="Eine Frau liegt wach im Bett">
          <p>
            Bei Patienten über 65 Jahren, die keine Neurodermitis-Symptome in
            der Kindheit angeben, treten eher schwerwiegende Symptome auf.
            Häufig ist die gesamte Haut stark gerötet, schuppend und juckend.
            Eine schwere Neurodermitis im Alter schränkt die Betroffenen in
            ihrer Lebensqualität stark ein, da auch Schlafstörungen und/oder
            Depressionen auftreten können.
          </p>
          <Link to="/verstehen">
            Weitere Fakten zu Neurodermitis kannst Du hier nachlesen.
          </Link>
        </ImageWithText>
        <SelbsttestTeaser />
      </div>
      <div className="havas-container dark">
        <h2 data-summary="Kann man plötzlich Neurodermitis bekommen?">
          Kann man plötzlich Neurodermitis bekommen?
        </h2>
        <p>
          Die atopische Dermatitis ist eine der häufigsten chronischen
          Hauterkrankungen im Kindesalter. Dass sich der typische Hautausschlag
          auch plötzlich bei Erwachsenen zeigen kann, wissen viele nicht.
          Manchmal erinnern sich ältere Menschen auch nicht mehr, dass sie als
          Kind Neurodermitis hatten. Da viele Erwachsene nicht damit rechnen,
          dass Neurodermitis sie im späteren Leben treffen kann, bleibt die
          Erkrankung oft unerkannt und damit auch unbehandelt.
        </p>
        <ClickableBox title="Gut zu wissen" header="Neurodermitis im Alter">
          <p>
            Neurodermitis kann auch zwischen 20 und 40 Jahren plötzlich
            auftreten und sogar noch dach dem 65. Lebensjahr.
          </p>
        </ClickableBox>
        <h3>Wie merke ich, dass ich Neurodermitis habe?</h3>

        <ImageWithText
          imgName="symptome-7"
          altImage="Eine junge Frau mit roten Hautstellen im Gesicht- und Halsbereich">
          <p>
            Die chronisch-entzündliche Erkrankung äußert sich vor allem durch
            sichtbare Hautveränderungen. Neurodermitis tritt meist in Schüben
            auf, die sich in Dauer und Intensität von Patient zu Patient stark
            unterscheiden können. Das heißt, es gibt Phasen, in denen Du kaum
            Beschwerden hast. Bei einem akuten Schub jedoch ist die Haut
            entzündet, sie schwillt an, ist gerötet, nässt und bildet Bläschen.
            Ein ganz typisches Anzeichen für Neurodermitis ist der starke
            Juckreiz.
          </p>
          <p>
            Neben der trockenen Haut gibt es weitere sogenannte
            <b style={{ fontWeight: '900' }}> Atopie-Zeichen</b>, die bei
            Hautekzemen für eine Neurodermitis sprechen können.
          </p>
        </ImageWithText>
        <Bulletlist
          heading="Dazu zählen:"
          textList={[
            'Verstärkte Linienzeichnung an den Innenflächen der Hände',
            'Eine sogenannte Dennie-Morgan-Falte – eine doppelte Lidfalte unterhalb der Augen',
            'Das feste Streichen über die Haut mit einem Holzspatel ruft bei atopischen Patienten eine weiße Linie hervor (sog. weißer Dermographismus).',
            'Dunkle Haut im Bereich der Augen',
            'Ausdünnung der seitlichen Augenbrauen (Hertoghe-Zeichen)',
            'Neigung zu Ohr- und Mundwinkeleinrissen',
          ]}
        />
        <p>
          Da verschiedene Hauterkrankungen ähnliche Symptome wie Neurodermitis
          hervorrufen, kann in der Regel nur ein Dermatologe eine gesicherte
          Diagnose stellen und mit Dir über Therapieoptionen sprechen, die für
          Dich geeignet sind.
        </p>
        <TippsBox
          heading={'Unser tipp'}
          text={
            'Wenn Du an sehr trockener Haut leidest, die schuppt und juckt, solltest Du einen Dermatologen aufsuchen.'
          }
        />

        <h3>Warum tritt Neurodermitis manchmal plötzlich auf?</h3>
        <p>
          Wie bereits beschrieben müssen meist mehrere Faktoren zusammenkommen,
          bevor eine Neurodermitis ausbricht. Ein Faktor, die genetische
          Komponente, verändert sich im Laufe des Lebens nicht. Es kann also
          theoretisch jederzeit passieren, dass zu dieser Veranlagung weitere
          Faktoren hinzukommen, die dazu führen, dass die Neurodermitis erstmals
          auftritt.
        </p>

        <p>
          Bei betroffenen Kindern verschwinden die Symptome oft vor der
          Pubertät. Das heißt aber nicht, dass die Erkrankung tatsächlich
          verschwunden ist. Sie „schlummert“ sozusagen. Unter bestimmten
          Umständen kann die Neurodermitis dann reaktiviert werden.
        </p>
        <p>
          Die genauen Mechanismen, die Neurodermitis in höherem Alter aktivieren
          oder reaktivieren, sind bisher nicht vollständig geklärt,
          Wissenschaftler weltweit arbeiten jedoch daran, diese Fragen zu
          beantworten. Einige Experten haben verschiedene Theorien aufgestellt:
        </p>

        <Bulletlist
          textList={[
            'Eine Hypothese ist, dass hormonelle Einflüsse eine Rolle spielen (z.B. die Wechseljahre).',
            'Eine weitere Hypothese ist, dass ungeeignete Hautpflege zu einer Schwächung der bereits angegriffenen Hautbarriere führt, die im Alter sowieso schon abnimmt.',
          ]}
        />
        <Link to="/ursachen">
          Detaillierte Informationen zu den Ursachen einer Neurodermitis kannst
          Du hier nachlesen.
        </Link>
        <ArztefinderTeaser />
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Symptome"
    metaTitle="Neurodermitis Symptome"
    description="Die Symptome der Neurodermitis zeigen sich überwiegend auf der Haut. Welche Bereiche betroffen sind, kann sich dabei mit zunehmendem Alter verändern."
    trustKeyWords="Anzeichen (für) Neurodermitis, Neurodermitis Symptome Erwachsene, Neurodermitis Schub Dauer, Hautausschlag Neurodermitis, Atopische Dermatitis Symptome, Neurodermitis erste Anzeichen, Neurodermitis bei Kleinkindern erkennen, Neurodermitis Symptome Kinder"
  />
);
