import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Bulletlist from '../components/Bulletlist';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import ZitatBox from '../components/ZitatBox';

export default function InnerlicheTherapien() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={[
            'NEURODERMITIS-',
            'BEHANDLUNG– ',
            'INNERLICHE THERAPIEN',
          ]}
          className={'heroComponent'}
          coloredWordsArr={['INNERLICHE']}
          color="yellow"
          imgName="innerliche_therapien_img_1"
          altImg="Apotheker öffnet Schublade, in der sich Medikamente befinden."
          text={[
            'Neurodermitis ist mehr als „nur eine Hauterkrankung“',
            <sup>1</sup>,
            '.',
            ' Sie wird teils durch genetische, teils durch Umweltfaktoren hervorgerufen, und auch ein Ungleichgewicht im Immunsystem sowie Störungen der Hautbarriere tragen zu den andauernden Entzündungsprozessen bei. Selbst wenn Betroffene tage-, wochen- oder sogar monatelang keinen Schub haben, brodelt die Entzündung unter der Hautoberfläche oft weiter. Moderne systemische Therapien setzen hier an und regulieren die Reaktionen des fehlgeleiteten Immunsystems.',
          ]}
        />

        <MobileThemenübersicht></MobileThemenübersicht>

        <p>
          Heutzutage gibt es eine Fülle an Therapiemöglichkeiten. Vielleicht
          hast Du auch schon einige ausprobiert, um Deine
          Neurodermitis-Beschwerden in Schach zu halten. Aktuelle
          Forschungsergebnisse deuten allerdings darauf hin, dass etwa 3 von 5
          Personen mit mittelschwerer bis schwerer Neurodermitis unzureichend
          versorgt sind.
          {!isMobile && (
            <>
              <br />
              <br />
            </>
          )}{' '}
          Das heißt: Auch Du erhältst möglicherweise nicht die Behandlung, die
          für Dich am besten geeignet ist.<sup>2</sup> In diesem Artikel möchten
          wir Dir die innerlichen Therapien vorstellen, die nicht nur bei einem
          Schub zum Einsatz kommen, sondern langfristig angewendet werden.
        </p>
      </div>
      <div
        className="havas-container dark"
        data-summary="Was ist eine systemische / innere Therapie?">
        <h2 className="w-100">Was ist eine systemische / innere Therapie?</h2>

        <ImageWithText
          className="image-position-right"
          imageOnRight={`${isMobile ? true : false}`}
          imgName="innerliche_therapien_img_2"
          altImage="Ärztin mit Patient im Gespräch">
          <p>
            Systemische Therapien werden innerlich angewendet. Das heißt, einige
            erfolgen durch Einnahme einer Tablette, andere durch subkutane
            Spritzen unter die Haut. Die Wirkstoffe in den Medikamenten
            verteilen sich dann über den Blutkreislauf im ganzen Körper – also
            im ganzen System. So gelangen sie an den Ort, an dem sie wirken
            sollen. Diese modernen Therapieformen bekämpfen gezielt von innen
            die Ursache der Neurodermitis. Man unterscheidet dabei klassische
            Behandlungsformen von modernen Therapieansätzen.
          </p>
        </ImageWithText>

        <h3>Klassische systemische Therapien</h3>

        <p>
          Bis vor einigen Jahren wurden bei einer systemischen Therapie vor
          allem Immunsuppressiva wie Kortison und Ciclosporin eingesetzt. Diese
          Therapiemöglichkeiten eignen sich jedoch nur für eine kurzfristige
          Therapie, da sonst schwere Nebenwirkungen drohen.
        </p>

        <h3>Moderne systemische Therapien</h3>

        <p>
          Zu den moderneren Therapiemöglichkeiten, die langfristig eingesetzt
          werden können, zählen sogenannte Biologika und JAK-Hemmer. Diese
          Therapiemöglichkeiten eignen sich besonders für eine langfristige
          Therapie, aus diesem Grund werden immer mehr Betroffene damit
          behandelt.
        </p>
        <ClickableBox
          title="Gut zu wissen"
          header="Wann werden systemische Therapeutika eingesetzt?">
          <p>
            Systemische Therapien kommen meist dann zum Einsatz, wenn äußerliche
            Behandlungsmethoden nicht mehr ausreichen und Du Deine Erkrankung
            damit nicht ausreichend kontrollieren kannst.
          </p>
        </ClickableBox>
      </div>

      <div className="havas-container light" data-summary="Was sind Biologika?">
        <h2 style={{ marginBottom: isMobile ? '-70px' : '0px' }}>
          Was sind Biologika?
        </h2>

        <ImageWithText
          style={
            isMobile
              ? {
                  position: 'relative',
                  top: '70px',
                  zIndex: '1',
                }
              : {}
          }
          imageOnRight={true}
          imgName="innerliche_therapien_img_3"
          altImage="Hautveränderungen der Neurodermitis im Nacken und Schulterbreich">
          <p>
            Biologika sind eine Gruppe von Medikamenten, die biotechnologisch
            hergestellt werden. Die Therapie mit Biologika ist ein
            verhältnismäßig neuer Ansatz, der die Behandlung vieler Erkrankungen
            revolutioniert hat. 2017 wurde das erste Biologikum zur Therapie der
            Neurodermitis zugelassen.<sup>1</sup> Die modernen Medikamente
            können hartnäckigen Juckreiz und Entzündungen langfristig lindern.
          </p>
        </ImageWithText>

        <Bulletlist
          className="violet"
          bigHeading="Wichtige Fakten im Überblick:"
          heading="Biologika"
          textList={[
            [
              'können eine Option sein, wenn topische, also äußerliche Therapien, zum Beispiel Kortison, allein nicht die gewünschte Krankheitskontrolle bewirken.',
              <sup>2</sup>,
            ],
            [
              'wirken systemisch, das heißt im Inneren des Körpers, und werden als subkutane Injektion (unter die Haut gespritzt) verabreicht.',
              <sup>3</sup>,
              ' Diese Darreichungsform sorgt dafür, dass die empfindlichen Wirkstoffe nicht im Magen abgebaut werden, sondern direkt das Immunsystem erreichen können.',
            ],
            [
              'wirken gezielt gegen die Entzündungsprozesse, die Deinen Neurodermitis-Symptomen zugrunde liegen, und helfen auf diesem Weg, den Juckreiz zu lindern und das Hautbild zu verbessern.',
              <sup>2</sup>,
            ],
            [
              'können entweder allein oder in Kombination mit topischen Therapien (Cremes oder Salben) eingesetzt werden, um zur Kontrolle Deiner Neurodermitis beizutragen.',
              <sup>2</sup>,
            ],
          ]}></Bulletlist>

        <p>
          Wenn Du ein Biologikum bekommst, erhältst Du von Deinem Dermatologen
          Unterstützung und ein Training für die Injektion des Medikaments, um
          diese auch zu Hause durchführen zu können.
        </p>
        <h3>Welche Nebenwirkungen können Biologika haben?</h3>

        <p>
          Aufgrund der spezifischen Wirkweise der Biologika kann das Risiko von
          Nebenwirkungen gesenkt werden. Dennoch können verschiedene Biologika
          unterschiedliche Nebenwirkungen haben, wie zum Beispiel Reaktionen an
          der Einstichstelle, erkältungsähnliche Beschwerden, Kopfschmerzen oder
          Bindehautentzündungen.<sup>3</sup> Besprich mit Deinem Dermatologen,
          welche Nebenwirkungen bei welchem Medikament auftreten können.
        </p>

        <h3>Eine besondere Art der Biologika – monoklonale Antikörper</h3>

        <ImageWithText
          imgName="innerliche_therapien_img_4"
          altImage="Vergrößerte Darstellung eines Antikörpers">
          <p>
            Bei den Biologika, die derzeit (Stand Oktober 2022) bei atopischer
            Dermatitis eingesetzt werden, handelt es sich um sogenannte
            monoklonale Antikörper. Das sind künstlich hergestellte Eiweiße, die
            gezielt in körpereigene Mechanismen eingreifen können und so das
            weitere Fortschreiten krankhafter Vorgänge im Körper verhindern.
            Obwohl es sich dabei um neuartige Wirkstoffe handelt, kommen sie
            schon seit Jahren erfolgreich bei verschiedenen Erkrankungen zum
            Einsatz, wie zum Beispiel bei Asthma, Rheuma oder Multipler
            Sklerose.
          </p>
        </ImageWithText>

        <div className="picture-text row-reverse">
          <div className="item">
            <ZitatBox
              isLoudspeaker={false}
              text="„Die richtige Behandlung kann mehr verändern als nur Deine Neurodermitis.“"></ZitatBox>
          </div>
          <div className="item" style={{ margin: 'auto' }}>
            <p>
              Die effektive Wirkung von monoklonalen Antikörpern beruht auf
              ihrer Genauigkeit: Sobald sie sich im Körper befinden, begeben sie
              sich auf die Suche nach ihrem Ziel. Das Ziel kann zum Beispiel die
              Andockstelle von Botenstoffen auf einer Zelle sein – ein
              sogenannter Rezeptor –, aber auch der Botenstoff selbst kann
              blockiert werden. Findet der Antikörper sein Ziel, hält er sich
              daran fest und blockiert dessen Funktion. Das bedeutet konkret:
              Wenn der Antikörper einen wichtigen Bestandteil einer
              Entzündungsreaktion zum Ziel hat und daran bindet, wird die
              schädigende, entzündungsfördernde Wirkung eingedämmt. Monoklonale
              Antikörper sind also in der Lage, sehr gezielt in die
              Entzündungsprozesse der Neurodermitis einzugreifen und direkt eine
              der Ursachen einzudämmen. Auch das Risiko von Nebenwirkungen kann
              aufgrund der spezifischen Wirkweise gesenkt werden.
            </p>
          </div>
        </div>

        <p>
          Da Antikörper Eiweißstoffe sind und Eiweiße im Magen verdaut werden,
          können sie nicht in Tablettenform eingenommen werden, sondern werden
          unter die Haut gespritzt. Das nennt man eine subkutane Injektion.
          Sofern der behandelnde Dermatologe es als angemessen erachtet, können
          nach einer fachgerechten Einweisung und je nach Medikament, Betroffene
          selbst, die Eltern eines betroffenen Kindes oder eine Pflegeperson die
          Injektion zu Hause durchführen.
        </p>

        <SelbsttestTeaser />
      </div>

      <div className="havas-container dark" data-summary="Was sind JAK-Hemmer?">
        <h2>Was sind JAK-Hemmer?</h2>

        <p>
          Januskinase (JAK)-Inhibitoren gehören zu den sogenannten kleinen
          Molekülen. Sie greifen in das Immunsystem ein, indem sie bestimmte
          Enzyme hemmen, die Januskinasen. Diese spielen unter anderem beim
          Entzündungsgeschehen eine Rolle. JAK-Hemmer greifen etwas später in
          das Krankheitsgeschehen ein als Biologika. Im Gegensatz zu diesen
          verhindern die JAK-Hemmer, dass das Entzündungssignal der Botenstoffe
          in die Zelle weitergeleitet wird, wogegen Biologika verhindern, dass
          das Signal überhaupt entsteht. Durch ihre Wirkweise können JAK-Hemmer
          helfen, Neurodermitis-Symptome wie Juckreiz und Ekzeme zu lindern. Sie
          werden in der Regel als Tabletten eingenommen.
        </p>

        <Bulletlist
          bigHeading="Wichtige Fakten im Überblick:"
          heading="JAK-Hemmer"
          textList={[
            [
              'bremsen die Aktivität sogenannter Januskinasen. Dabei handelt es sich um Eiweißstoffe (Enzyme) im Immunsystem, die unter anderem an Entzündungsprozessen beteiligt sind.',
              <sup>4-6</sup>,
            ],
            [
              'können verschrieben werden, wenn sich mit topischen Therapien allein Deine Neurodermitis nicht gut genug unter Kontrolle bringen lässt.',
              <sup>5,6</sup>,
            ],
            [
              'können entweder allein oder in Kombination mit topischen Therapien eingesetzt werden, um zur Kontrolle Deiner Neurodermitis beizutragen.',
              <sup>5,6</sup>,
            ],
            [
              'benötigen während der Anwendung Voruntersuchungen und Laborkontrollen, damit überwacht werden kann, wie die Behandlung wirkt, und um sicherzustellen, dass sie keine Probleme verursacht.',
              <sup>7</sup>,
            ],
          ]}></Bulletlist>

        <h3>Welche Nebenwirkungen können bei JAK-Hemmern auftreten?</h3>

        <p>
          Es gibt verschiedene JAK-Hemmer, die auch unterschiedliche
          Nebenwirkungen haben können. Besprich mit Deinem Hautarzt, welche
          Nebenwirkungen bei welchem Medikament möglicherweise vorkommen.
        </p>

        <p>
          Zu den häufigsten Nebenwirkungen von JAK-Hemmern bei Neurodermitis
          zählen ein erhöhter „schlechter“ Cholesterinspiegel, Infektionen,
          Infekte der oberen Atemwege und Kopfschmerzen.<sup>5,6</sup>
        </p>

        <ArztefinderTeaser />
      </div>
      <div
        className="havas-container light"
        data-summary="Was sind Immunsuppressiva?">
        <h2>Was sind Immunsuppressiva?</h2>
        <p>
          Immunsuppressiva gehören zu den klassischen Systemtherapien, die nur
          kurzfristig eingesetzt werden sollten, da sonst schwere Nebenwirkungen
          drohen. Dazu gehören beispielsweise orale Glukokortikoide, die besser
          als Kortison-Tabletten bekannt sind, und Ciclosporin. Ciclosporin ist
          ein Immunsuppressivum, das für die Behandlung von Kindern und
          Jugendlichen jedoch nicht zugelassen ist und nur in sehr schweren
          Fällen von Ärzten verordnet wird.
        </p>
        <p>
          Immunsuppressiva sind Medikamente, die die Aktivität des Immunsystems
          umfassend hemmen. Damit können sie zwar die mit der Neurodermitis
          verbundenen Entzündungsprozesse reduzieren, aber auch andere
          eigentlich sinnvolle Funktionen im Körper beeinflussen.<sup>8</sup>
        </p>
        <p>
          Wenn topische Therapien allein Deine Neurodermitis-Symptome nicht
          unter Kontrolle bringen und die Beschwerden Deine Lebensqualität stark
          beeinträchtigen, wird Dein Dermatologe möglicherweise mit Dir über den
          Einsatz von Immunsuppressiva sprechen.<sup>8</sup> Idealerweise bist
          du bereits gut informiert und kennst Deine Behandlungsmöglichkeiten.{' '}
        </p>
        <p>
          In unserem Behandlungsratgeber findest Du eine kompakte Übersicht über
          die Behandlungsmöglichkeiten.
        </p>
        <Bulletlist
          className="violet"
          bigHeading="Wichtige Fakten im Überblick:"
          heading={['Immunsuppressiva', <sup>8</sup>]}
          textList={[
            'wirken, indem sie die Aktivität des Immunsystems unterdrücken und damit auch die mit der Neurodermitis verbundenen Entzündungsprozesse hemmen.',
            [
              'sind zur Behandlung von Neurodermitis meist orale Medikamente (Tabletten zum Einnehmen).',
              <sup>6</sup>,
            ],
            'werden in der Regel nur für kurze Zeit eingesetzt, bevor längerfristig auf andere Medikamente umgestellt wird, da sonst Nebenwirkungen drohen.',
            'können wirksam den Juckreiz lindern und die Haut heilen lassen.',
          ]}></Bulletlist>

        <ImageWithText
          style={
            isMobile
              ? {
                  position: 'relative',
                  top: '-70px',
                  zIndex: '1',
                }
              : { marginTop: 80 }
          }
          imgName="innerliche_therapien_img_6"
          altImage="Ein Laptop, auf dem Bildschirm ist eine junge Ärtztin zu sehen">
          <p>
            Es gibt viele verschiedene systemische Immunsuppressiva, die jeweils
            ihr eigenes Anwendungsschema und Nebenwirkungsprofil haben.
            <sup>2,8</sup> Dein Dermatologe wird Dir helfen zu entscheiden, ob
            ein solches Medikament das richtige für Dich ist.
          </p>
        </ImageWithText>

        <h3 style={isMobile ? { marginTop: '-30px' } : {}}>
          Wie wirkt Kortison zum Einnehmen?<sup>8</sup>
        </h3>
        <p>
          Auch Kortison-Tabletten zum Einnehmen sind eine Art von
          Immunsuppressiva. Sie werden in der Regel nur kurzzeitig (für wenige
          Wochen) eingesetzt, um einen Neurodermitis-Schub unter Kontrolle zu
          bringen, da eine längerfristige Anwendung das Risiko für
          Nebenwirkungen erhöhen kann. Vorsorgeuntersuchungen und Kontrollen bei
          Deinem Dermatologen können erforderlich sein, um zu überwachen, wie
          die Behandlung wirkt, und um sicherzustellen, dass sie keine Probleme
          verursacht.<sup>2</sup>
        </p>

        <h3>Nebenwirkungen von Immunsuppressiva</h3>

        <p>
          Immunsuppressiva unterdrücken das Immunsystem. Dieses ist allerdings
          für uns wichtig, deshalb sollten diese Präparate nur kurz angewendet
          werden. Zudem können sie schwerwiegende Nebenwirkungen haben.
          <sup>8</sup>
        </p>

        <h3 style={{ marginBottom: isMobile ? '-70px' : '0px' }}>
          Mögliche Nebenwirkungen oraler Glukokortikoide (Kortison)<sup>9</sup>
        </h3>

        <ImageWithText
          imageOnRight={true}
          style={
            isMobile
              ? {
                  position: 'relative',
                  top: '70px',
                  zIndex: '1',
                }
              : {}
          }
          imgName="innerliche_therapien_img_7"
          altImage="Eine Frau liegt wach im Bett">
          <p>
            Glukokortikoide werden bei einem akuten Schub in der Regel äußerlich
            angewendet, um Entzündungen der Haut zu behandeln. Manchmal kommen
            sie allerdings auch innerlich als Tabletten zum Einsatz. Das kann zu
            stärkeren Nebenwirkungen führen als bei der{' '}
            <Link to="/aeusserliche-therapien">äußerlichen Anwendung</Link>. Die
            Gefahr von Nebenwirkungen steigt mit der Behandlungsdauer und der
            Stärke der Kortisonpräparate.
          </p>
        </ImageWithText>

        <Bulletlist
          className="violet"
          heading="Mögliche Nebenwirkungen sind unter anderem:"
          textList={[
            'Gewichtszunahme',
            'Bluthochdruck',
            'Schlafstörungen',
            'Akne',
            'Osteoporose',
            'Grüner Star',
            'Magengeschwüre',
          ]}></Bulletlist>

        <p>
          Kortison sollte möglichst nicht über einen längeren Zeitraum
          eingenommen werden. Falls doch mal eine etwas längere Behandlung
          notwendig ist, darfst Du die Präparate nicht abrupt absetzen, sonst
          droht ein sogenannter Rebound-Effekt, bei dem Symptome verstärkt
          wieder auftreten können. Sprich Dich dazu mit Deinem Dermatologen ab.
          <sup>9</sup>
        </p>

        <h3>
          Mögliche Nebenwirkungen von Ciclosporin<sup>2, 10, 11</sup>
        </h3>

        <ImageWithText
          className="image-position-left"
          imgName="innerliche_therapien_img_8"
          altImage="Eine Frau, die gedankenversunken im Bett sitzt">
          <p>
            Ciclosporin ist ein Immunsuppressivum, das als innerliche Therapie
            für die Behandlung von Kindern und Jugendlichen nicht zugelassen ist
            und nur in sehr schweren Fällen von Ärzten verordnet wird.
            <sup>2, 10</sup> Während der Behandlung mit Ciclosporin müssen die
            Nieren- und Blutdruckwerte regelmäßig kontrolliert werden, da die
            Behandlung die Nieren belasten und den Blutdruck erhöhen kann.
            <sup>2, 10</sup>
          </p>
        </ImageWithText>

        <Bulletlist
          className="violet"
          heading={[
            'Häufige und sehr häufige Nebenwirkungen sind beispielsweise:',
            <sup>11</sup>,
          ]}
          textList={[
            'Bluthochdruck',
            'Müdigkeit',
            'Zittern (Tremor)',
            'Magen-Darm-Beschwerden',
            'Krämpfe',
            'Gewichtszunahme',
            'Anstieg des Blutzuckerspiegels',
            'Allergische Reaktionen',
            'Blutarmut (Anämie)',
            'Schwellungen im Gesicht',
            'Gicht',
          ]}></Bulletlist>

        <p>
          Bei der Anwendung von Immunsuppressiva solltest Du Dich immer an die
          Angaben Deines Dermatologen halten und bei Unklarheiten Fragen
          stellen.
        </p>
      </div>
      <div className="havas-container dark" data-summary="Fazit">
        <h2>Fazit</h2>
        <p>
          Die Neurodermitis tritt zwar in Schüben auf, aber auch in Phasen, in
          denen die Erkrankung weniger sichtbar ist, brodelt die Entzündung
          unter der Hautoberfläche oft weiter. Die Behandlung der Symptome
          bringt nicht immer den gewünschten Erfolg und nicht wenige Menschen
          mit mittelschwerer bis schwerer Neurodermitis sind nicht ausreichend
          versorgt.<sup>12</sup> Sprich mit einem Hautarzt, wenn Deine
          Neurodermitis mit Deiner aktuellen Therapie nicht unter Kontrolle zu
          bringen ist.{' '}
          <a href="/selbsttest">
            Hier kannst Du über einen Test herausfinden, wie kontrolliert Deine
            Neurodermitis ist
          </a>
          . Gemeinsam könnt Ihr überprüfen, ob Dir moderne
          Behandlungsmöglichkeiten mehr Freiraum geben können. Falls Du Dich
          vorher umfassend informieren möchtest, kannst Du eine Zusammenfassung
          aller verfügbaren Therapien{' '}
          <a href="https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Behandlungsmoeglichkeiten.pdf?la=de-DE">
            hier
          </a>{' '}
          herunterladen und mit zu Deinem Dermatologen nehmen.
        </p>
        <ArztefinderTeaser />
        <div className="bottom-text">
          <p>
            <sup>1 </sup>Apotheken-Umschau.de. Fortschritte in der
            Neurodermitis-Therapie. Abrufbar unter:
            https://www.apotheken-umschau.de/krankheiten-symptome/hautkrankheiten/so-helfen-biologika-bei-neurodermitis-881273.html
            (abgerufen im Oktober 2022).
          </p>

          <p>
            <sup>2 </sup>Wollenberg A et al. J Eur Acad Dermatol Venereol 2018;
            32:850–878.
          </p>

          <p>
            <sup>3 </sup>Boguniewicz M et al. Ann Allergy Asthma Immunol 2018;
            120(1):10–22.
          </p>

          <p>
            <sup>4 </sup>Rodrigues MA and Torres T. Eur Ann Allergy Clin Immunol
            2020; 52:45–48.
          </p>

          <p>
            <sup>5 </sup>Mein Allergie-Portal. JAK-Inhibitoren. Abrufbar unter:
            mein-allergie-portal.com/allergie-wiki/3197-jak-inhibitoren.html
            (abgerufen im Oktober 2022).
          </p>

          <p>
            <sup>6 </sup>Gesundheitsstadt Berlin GmbH. Zwei neue
            Behandlungsansätze bei schwerer Neurodermitis. Abrufbar unter
            https://www.gesundheitsstadt-berlin.de/zwei-neue-behandlungsansaetze-bei-schwerer-neurodermitis-15465/
            (abgerufen im September 2022).
          </p>

          <p>
            <sup>7 </sup>Klein B et al. J Dtsch Dermatol Ges. 2022;
            20(1):19-25.4
          </p>

          <p>
            <sup>8 </sup>National Eczema Association. Precription oral. 2020.
            Abrufbar unter:
            nationaleczema.org/eczema/treatment/immunosuppressants/ (abgerufen
            im September 2022).
          </p>

          <p>
            <sup>9 </sup>Gesundheitsinformation.de. Kortison richtig anwenden
            und Nebenwirkungen vermeiden. Abrufbar unter:
            https://www.gesundheitsinformation.de/kortison-richtig-anwenden-und-nebenwirkungen-vermeiden.html
            (abgerufen im Oktober 2022).
          </p>

          <p>
            <sup>10 </sup>Gesundheitsinformation.de. Lichttherapie, Tabletten
            und Spritzen. Abrufbar
            unter:https://www.gesundheitsinformation.de/lichttherapie-tabletten-und-spritzen.html
            (abgerufen im Oktober 2022).
          </p>

          <p>
            <sup>11 </sup>Gelbe-Liste.de Cyclosporin. Abrufbar unter:
            https://www.gelbe-liste.de/wirkstoffe/Ciclosporin_2707 (abgerufen im
            Oktober 2022).
          </p>

          <p>
            <sup>12 </sup>Langenbruch A et al. Hautarzt 2021; 72(12):079–1089.
          </p>
        </div>
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Innerliche Behandlung"
    metaTitle="Neurodermitis – innerliche Therapien"
    description="Neurodermitis ist bisher nicht heilbar, aber moderne innerliche (systemische) Therapien, können hartnäckigen Juckreiz und Entzündungen langfristig lindern."
    trustKeyWords="Neurodermitis Therapie, Biologika Neurodermitis, Therapie Neurodermitis, Neurodermitis Tabletten, Neurodermitis Spritzen, innere Therapie, Neurodermitis neue Therapie, atopische Dermatitis Therapie, Neurodermitis Therapie neu, Tabletten gegen Juckreiz Neurodermitis, Antikörper Therapie Neurodermitis, Therapie bei Neurodermitis, System Therapie Neurodermitis, systemische Therapie Neurodermitis, Neurodermitis Behandlungsmöglichkeiten"
  />
);
