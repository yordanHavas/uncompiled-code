import * as React from 'react';
import { useContext } from 'react';
import Hero from '../components/Hero';
import CardBig from '../components/CardBig';
import { ArztefinderTeaser } from '../components/Teaser';
import InfoAndClickableText from '../components/InfoAndClickableText';
import iconInstagram from '../assets/icons/startseite_icon_1.svg';
import iconYouTube from '../assets/icons/startseite_icon_2.svg';
import iconPodcast from '../assets/icons/startseite_icon_3.svg';
import iconBlog from '../assets/icons/startseite_icon_4.svg';
import Button from '../components/Button';
import Slider from '../components/Slider';
import IconsRow from '../components/IconsRow';
import { MobileContext } from '../components/Layout';
import { Link } from 'gatsby';
import { SEO } from '../components/seo';

const IndexPage = () => {
  const isMobile = useContext(MobileContext);

  return (
    <div className="startseite">
      <div className="havas-container light">
        <Hero
          className="l-height-desktop startHero"
          headingTextList={['Verändere DEIN', 'LEBEN MIT', 'NEURODERMITIS']}
          coloredWordsArr={['Verändere']}
          imgName="startseite_img_1"
          color="yellow"
          altImg="Ein junge Frau mit wehenden Haaren im Wind."
          text="Weltweit leiden Millionen Menschen an Neurodermitis. Bist auch Du betroffen? Erfahre hier, was bei dieser komplexen chronisch-entzündlichen Hauterkrankung unter der Haut passiert, wie Du lernen kannst, damit umzugehen, und welche Behandlungsmöglichkeiten es gibt."
        />

        <div className="card-container var1 violet">
          <CardBig
            image="verstehen_img_1"
            altImg="Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt."
            subTitle="Was genau ist Neurodermitis?"
            title="Verstehen"
            text="Hier erfährst Du alles, was Du über Neurodermitis wissen musst. Denn je besser Du die Erkrankung kennst, umso besser kannst Du lernen, damit umzugehen."
            buttonText="Neurodermitis verstehen"
            buttonLink="/verstehen"
          />
          <CardBig
            image="uberblick_img_1"
            altImg="Ein junges Mädchen lächelt in die Kamera, im Hintergrund ist eine Ärztin zu sehen."
            subTitle="Wie wird Neurodermitis behandelt?"
            title="Behandeln"
            text={[
              'Informiere Dich hier über die verschiedenen Behandlungs-möglichkeiten bei Neurodermitis. Dank medizinischer Forschung stehen heute auch langfristige Therapieoptionen zur Verfügung.',
            ]}
            buttonText="Behandlungsmöglichkeiten"
            buttonLink="/ueberblick"
          />

          <CardBig
            noMarginBottom={true}
            image="startseite_img_2"
            altImg="Junge Frau sitzt am Laptop"
            subTitle="Hast Du Deine Neurodermitis unter Kontrolle?"
            title="Kontrollieren"
            text="Mit dem Selbsttest kannst Du anhand Deines ADCT-Scores herausfinden, ob Deine Neurodermitis ausreichend kontrolliert ist oder ob Deine Be-handlung optimiert werden sollte."
            buttonText="Zum Selbsttest"
            buttonLink="/selbsttest"
            linkType={'a'}
          />
        </div>
      </div>

      {isMobile ? (
        <div className="havas-container dark">
          <ArztefinderTeaser />
        </div>
      ) : (
        <ArztefinderTeaser />
      )}

      <div className="havas-container light">
        <h2>Social-Media & Podcast</h2>
        <p>
          Folge uns auch auf Social Media und bleib auf dem Laufenden oder hör
          doch mal in unseren Podcast rein!
        </p>

        <p className="quote">Unsere Kanäle:</p>
        <IconsRow className="icons" />
      </div>

      <div className="havas-container dark">
        <h2>Lernplattform Neurodermitis</h2>
        <p style={{ textAlign: `${isMobile ? 'left' : 'center'}` }}>
          Auf unserer Lernplattform vermitteln wir aktuelle Informationen und
          Anregungen zu allen Bereichen der chronischen Hauterkrankung.
        </p>
        <Button
          style={{ alignSelf: `${isMobile ? 'start' : 'center'}`, height: 61 }}
          type="primary"
          to="/lernplattform"
          text="Zur Lernplattform"
          linkType={'a'}
        />

        <Slider
          minHeightDesktop="355.81px"
          special={true}
          items={[
            {
              imgText: [
                <span>Neurodermitis</span>,
                <br />,
                <span>allgemein</span>,
              ],
              imgTextColor: 'yellow',
              altImage:
                'Eine junge Frau blick verträumt in die Ferne, während sie ein Handy hält',
              imgName: 'startseite_slider_1',
              heading: 'Was ist Neurodermitis (atopische Dermatitis)?',
              text: [
                'Weltweit leiden Millionen Menschen an Neurodermitis – mit steigender Tendenz. Doch was ist das für eine Erkrankung und wie wirkt sie sich aus? Erfahre hier mehr zu Definition, Ursachen und Symptomen von Neurodermitis.',
                <br />,
                <br />,
                <Button
                  text="Mehr erfahren"
                  to={'/lernplattform/elearning-1'}
                  linkType="a"
                  type="tertiary"
                />,
              ],
              subHeading: ['Lernmodul 1'],
            },
            {
              imgText: [
                <span>Umgang mit</span>,
                <br />,
                <span>Neurodermitis</span>,
              ],
              imgTextColor: 'blue',
              altImage:
                'Frau blickt in den Himmel und streckt die Arme in die Luft',
              imgName: 'startseite_slider_2',
              heading: [
                'Tipps für eine bessere Stressbewältigung',
                <br />,
                <br />,
              ],
              text: [
                'Stress und Neurodermitis bilden einen Teufelskreis. Stress kann einen Schub auslösen und ein Schub kann Stress verursachen. In diesem Lernmodul erfährst Du, wie Du Stressbewältigung zu Deiner täglichen Routine machen kannst.',
                <br />,
                <br />,
                <Button
                  text="Mehr erfahren"
                  to={'/lernplattform/elearning-6'}
                  type="tertiary"
                  linkType={'a'}
                />,
              ],
              subHeading: ['Lernmodul 6'],
            },
            {
              imgText: [
                <span>Neurodermitis</span>,
                <br />,
                <span>allgemein</span>,
              ],
              imgTextColor: 'yellow',
              altImage:
                'Ein Mann mit Bart trägt ein kleines Mädchen auf den Schultern. Seine Augen sind von Sonnenblumen verdeckt, das Mädchen lacht.',
              imgName: 'startseite_slider_3',
              heading: 'Therapiemöglichkeiten bei Neurodermitis',
              text: [
                'Die Behandlung der Neurodermitis ist von vielen Faktoren abhängig und erfordert individuell abgestimmte Maßnahmen. Erfahre hier, welche Therapieoptionen Dir zur Verfügung stehen.',
                <br />,
                <br />,
                <Button
                  text="Mehr erfahren"
                  to={'/lernplattform/elearning-10'}
                  linkType="a"
                  type="tertiary"
                />,
              ],
              subHeading: 'Lernmodul 10',
            },
          ]}
        />
      </div>

      <div className="havas-container light">
        <h2>Unser Neurodermitis-Begleiter-Team ist für Dich da</h2>
        <p>
          Neurodermitis kann für Betroffene und Angehörige eine große
          Herausforderung sein. Zögere daher nicht, Deinen Dermatologen oder das
          Behandlungsteam anzusprechen.
        </p>
        <p>
          Hast Du akut Fragen zu Therapieoptionen oder benötigst Du Tipps für
          den Alltag mit Neurodermitis, stehen Dir unsere medizinisch
          ausgebildeten Ansprechpartnerinnen Karin, Anna und Claudia beratend
          zur Verfügung. Dieser Service ist für Dich kostenfrei.
        </p>

        <InfoAndClickableText />
      </div>
    </div>
  );
};

export default IndexPage;

export const Head = () => (
  <SEO>
    <script src="https://cdn.podlove.org/web-player/5.x/embed.js"></script>
  </SEO>
);
