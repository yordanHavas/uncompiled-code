import React from 'react';
import { useContext } from 'react';
import Hero from '../components/Hero';
import { MobileContext } from '../components/Layout';
import { DescktopThemenübersicht } from '../components/Themenübersicht';
import AkkordeonImage from '../components/AkkodeonImage';
import { Link } from 'gatsby';
import { SEO } from '../components/seo';
export default function Anlaufstellen() {
  const isMobile = useContext(MobileContext);

  const accordeonList = [
    {
      altTag: 'BITTE BERÜHREN Logo',
      header: '„Bitte berühren – Hand in Hand gegen Neurodermitis“',
      imgName: 'anlaufstellen_img_2',
      link: 'https://neurodermitis.bitteberuehren.de',
      buttonText: 'Zu „Bitte berühren“',
      text: [
        {
          previousText:
            'Mit „Bitte berühren – Hand in Hand gegen Neurodermitis“ hat der Berufsverband der Deutschen Dermatologen e.V. (BVDD) eine Kampagne ins Leben gerufen, die Menschen mit Neurodermitis dabei unterstützt, ihr Leben und ihre Erkrankung aktiv in die Hand zu nehmen. Auf der Website ',
          link: 'https://neurodermitis.bitteberuehren.de',
          website: 'www.neurodermitis.bitteberuehren.de',
          followingText:
            ' werden Betroffenen Wege zur Hilfe und Selbsthilfe aufgezeigt und es wird Orientierung in Bezug auf die zahlreichen Möglichkeiten der Selbstversorgung und der medikamentösen Therapien gegeben.',
        },
        'Der BVDD ist der Zusammenschluss der in Deutschland niedergelassenen Hautärzte. Der Verband setzt sich aktiv für verbesserte Rahmenbedingungen bei der Betreuung und Behandlung von Patienten ein und beteiligt sich an den großen gesundheitspolitischen Diskussionen.',
      ],
      imgWidth: '250px',
      imgHeight: '53px',
    },
    {
      altTag: 'daab Logo',
      header: 'Deutscher Allergie- und Asthmabund (DAAB)',
      imgName: 'anlaufstellen_img_3',
      link: 'https://daab.de',
      buttonText: 'Zum DAAB',
      text: [
        'Beim Deutschen Allergie- und Asthmabund e.V. (DAAB) engagieren sich viele tausend Menschen. Die Mitarbeiter des DAAB sind in den Bereichen Ernährung, Biologie, Chemie, Pädagogik, sowie als Anaphylaxie- oder Neurodermitis-Trainer aktiv. Sie beraten Mitglieder individuell und stehen ihnen in ihrem Alltag zur Seite. Zahlreiche Service- und Informationsmaterialien bieten Kindern und Erwachsenen mit Allergien, Asthma, Neurodermitis und Urtikaria fundiertes Wissen rund um ihre Erkrankung sowie praktische Unterstützung. Der DAAB ist zudem in vielen Forschungsprojekten aktiv, führt Marktchecks durch und ist weltweit vernetzt. Das Ziel des DAAB ist es, Betroffenen die bestmögliche Diagnose und Therapie sowie ein Maximum an Lebensqualität zu ermöglichen.',
      ],
      imgWidth: '157px',
      imgHeight: '86px',
    },
    {
      altTag: 'Deutsche Haut- und Allergiehilfe e.V. Logo',
      header: 'Deutsche Haut- und Allergiehilfe (DHA)',
      imgName: 'anlaufstellen_img_4',
      link: 'https://dha-allergien.de',
      buttonText: 'Zur DHA',
      text: [
        'Mitte der 80er Jahre gründeten Ärzte zusammen mit Patienten die Deutsche Haut- und Allergiehilfe (DHA). Seither vertritt der gemeinnützige Verein erfolgreich die Interessen von Hautpatienten und Allergikern und macht auf die alarmierende Zunahme von Allergien aufmerksam. Der DHA setzt sich unermüdlich für eine Verbesserung der Versorgung und der Lebensqualität von Betroffenen ein. Ein weiteres Schwerpunktthema des Vereins ist, Betroffenen ein besseres Verständnis für ihre Erkrankung zu vermitteln und Behandlungsoptionen zu erläutern. Um Vorurteile abzubauen, sensibilisiert der DHA immer wieder die Öffentlichkeit für Haut- und Allergiethemen und engagiert sich dafür, dass Hauterkrankungen sowie Allergien nicht bagatellisiert, sondern als ernsthafte Erkrankungen wahrgenommen werden.',
      ],
      imgWidth: '240px',
      imgHeight: '85.3px',
    },
    {
      altTag: 'DNB Logo',
      header: 'Deutscher Neurodermitis Bund (DNB)',
      imgName: 'anlaufstellen_img_5',
      link: 'https://neurodermitis-bund.de',
      buttonText: 'Zum DNB',
      text: [
        'Der Deutsche Neurodermitis Bund e.V. (DNB) setzt sich seit 1986 unermüdlich für die Belange von Menschen mit Neurodermitis ein. Ein wichtiges Ziel dabei ist, Erfahrungen und Informationen über die Ursachen, Erscheinungsformen, Behandlungsmethoden und Folgen von Neurodermitis und artverwandten Erkrankungen zu sammeln und diese sowohl der Forschung als auch Betroffenen zugänglich zu machen. Zudem fördert der DNB den Austausch von Betroffenen, medizinischem Fachpersonal und der Wissenschaft. Der gemeinnützige Verein unterstützt Menschen, insbesondere Kinder mit Neurodermitis und deren Familien beim eigenverantwortlichen Umgang mit der Erkrankung. Um Vorurteile nachhaltig abzubauen, klärt der DNB über Neurodermitis und die Folgen auf. Außerdem vertritt er die Interessen der Betroffenen auf gesellschaftspolitischer, rechtlicher und steuerlicher Ebene.',
        {
          previousText:
            'Der DNB hat eine Petition über change.org zur Kostenübernahme von therapeutischen Hautpflegeprodukten für chronisch Hautkranke durch Krankenkassen ins Leben gerufen: ',
          link: 'https://t1p.de/tbao',
          website: 'https://t1p.de/tbao',
        },
      ],
      imgWidth: '143.2px',
      imgHeight: '90px',
    },
    {
      altTag: 'Mein Allergie Portal Logo',
      header: 'MeinAllergieportal',
      imgName: 'anlaufstellen_img_6',
      link: 'https://www.mein-allergie-portal.com/neurodermitis',
      buttonText: 'Zu MeinAllergieportal',
      text: [
        'MeinAllergiePortal ist eine unabhängige Informationsplattform. Betroffene und Interessierte finden hier vielfältige und medizinisch validierte Informationen zu Ursachen und Symptomen sowie zur Diagnose und Therapie von Allergien und Unverträglichkeiten. In den DigiPat Live-Webinaren, den digitalen Patiententagen, bietet Dir MeinAllergiePortal die Möglichkeit, Dich aus erster Hand zu informieren und direkt Fragen zu stellen. Das Online-Portal will die Gesundheitskompetenz von Menschen mit Allergien und Unverträglichkeiten stärken und sie besser über die Ursachen sowie die Diagnose- und Therapiemöglichkeiten zu ihren Beschwerden aufklären. Zudem will MeinAllergiePortal Betroffenen helfen, ihr Leben aktiv zu gestalten und zielgerichtet zu handeln. Das unabhängige Portal wurde 2013 von Sabine Jossé und Dr. Harald Jossé gegründet, die selbst von Allergien und Unverträglichkeiten betroffen sind.',
      ],
      imgWidth: '130px',
      imgHeight: '130px',
    },
    {
      altTag: 'NIK e.V. Logo',
      header: 'Netzwerk Autoimmun-erkrankter (NiK e.V.)',
      imgName: 'anlaufstellen_img_7',
      link: 'https://nik-ev.de',
      buttonText: 'Zum NIK e.V.',
      text: [
        'Das Netzwerk Autoimmunerkrankter (NIK e.V.) wurde von der Betroffenen Tanja Renner gegründet. NIK beschäftigt sich vor allem mit Themen rund um MS, Rheuma, Psoriasis, Neurodermitis und chronisch-entzündliche Darmerkrankungen. Das Netzwerk unterstützt Menschen mit Autoimmunkrankheiten dabei, ihre Erkrankung besser zu verstehen und hilft ihnen, die richtigen Informationen im Netz sowie die richtigen Spezialisten zu finden. NIK unterstützt Betroffene praxisnah und bietet die Möglichkeit zum Austausch. Authentische Geschichten von anderen Patienten machen Mut und digitale Veranstaltungen mit Ärzten und Experten vermitteln fundierte Fakten und alltagstaugliche Tipps.',
      ],
      imgWidth: '202px',
      imgHeight: '82px',
    },
  ];

  return (
    <>
      <DescktopThemenübersicht />
      <div className="havas-container light">
        <Hero
          headingTextList={[
            'Anlaufstellen',
            'für Menschen',
            'mit Neurodermitis',
          ]}
          coloredWordsArr={['Anlaufstellen']}
          imgName="anlaufstellen_img_1"
          altImg="Drei Generationen von Frauen"
          color="#60e2e3"
          className="l-height-desktop"
          text={[
            'Auch wenn Du Dich manchmal allein mit Deiner Erkrankung fühlst, gibt es viele Menschen, die in einer ähnlichen Situation sind, denn weltweit leiden Millionen Menschen an Neurodermitis. Wer Unterstützung sucht, findet zahlreiche Anlaufstellen, die sich auf die Anliegen von Menschen mit Neurodermitis oder anderen atopischen Erkrankungen spezialisiert haben.',
          ]}
        />
        <p>
          Egal, ob Du Dich austauschen möchtest oder spezielle Informationen
          suchst, es gibt für fast jedes Anliegen passende
          Unterstützungsangebote. Wir haben eine Liste mit wichtigen Adressen
          für Dich zusammengestellt. Darin findest Du Selbsthilfeorganisationen,
          Expertenportale, Anbieter von Schulungen und vieles mehr.
        </p>
      </div>

      {accordeonList.map((el, index) => {
        const theme = index % 2 === 0 ? 'dark' : 'light';
        if (index % 3 === 0) {
        }
        if (isMobile) {
          return (
            <div key={index} className={`havas-container ${theme}`}>
              <AkkordeonImage
                altTag={el.altTag}
                header={el.header}
                imgName={el.imgName}
                link={el.link}
                text={el.text}
                buttonText={el.buttonText}
                imgHeight={el.imgHeight}
                imgWidth={el.imgWidth}></AkkordeonImage>
            </div>
          );
        } else {
          if (index % 3 === 0) {
            // const violet = index % 2 === 0 ? false : true;
            return (
              <div key={index} className={`havas-container ${theme}`}>
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    width: '100%',
                    placeContent: 'space-between',
                  }}>
                  <AkkordeonImage
                    altTag={accordeonList[index].altTag}
                    header={accordeonList[index].header}
                    imgName={accordeonList[index].imgName}
                    buttonText={accordeonList[index].buttonText}
                    link={accordeonList[index].link}
                    text={accordeonList[index].text}
                    imgHeight={accordeonList[index].imgHeight}
                    imgWidth={accordeonList[index].imgWidth}
                    // violet={violet}
                  ></AkkordeonImage>
                  <AkkordeonImage
                    altTag={accordeonList[index + 1].altTag}
                    header={accordeonList[index + 1].header}
                    imgName={accordeonList[index + 1].imgName}
                    buttonText={accordeonList[index + 1].buttonText}
                    link={accordeonList[index + 1].link}
                    text={accordeonList[index + 1].text}
                    imgHeight={accordeonList[index + 1].imgHeight}
                    imgWidth={accordeonList[index + 1].imgWidth}
                    // violet={violet}
                  ></AkkordeonImage>
                  <AkkordeonImage
                    altTag={accordeonList[index + 2].altTag}
                    header={accordeonList[index + 2].header}
                    imgName={accordeonList[index + 2].imgName}
                    buttonText={accordeonList[index + 2].buttonText}
                    link={accordeonList[index + 2].link}
                    text={accordeonList[index + 2].text}
                    imgHeight={accordeonList[index + 2].imgHeight}
                    imgWidth={accordeonList[index + 2].imgWidth}
                    // violet={violet}
                  ></AkkordeonImage>
                </div>
              </div>
            );
          }
        }
      })}

      <div className="havas-container light">
        <p>
          Wenn Du Dich mit anderen austauschen möchtest, kannst Du auch unseren{' '}
          <a href="https://www.instagram.com/leben_mit_neurodermitis.info/">
            Instagram-Kanal
          </a>{' '}
          besuchen. Dort findest Du zahlreiche Tipps für den Alltag mit
          Neurodermitis.
        </p>
        <p>
          Für Fragen zum Leben mit Neurodermitis stehen Dir auch unsere
          persönlichen{' '}
          <Link to="/neurodermitis-berater-kontaktieren#threeWomen">
            Ansprechpartnerinnen des Neurodermitis-Begleiter-Teams
          </Link>{' '}
          telefonisch zur Verfügung.
        </p>
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Anlaufstellen"
    metaTitle="Wichtige Anlaufstellen für Menschen mit Neurodermitis"
    description="Wir stellen wichtige Anlaufstellen vor, die sich auf die Anliegen von Menschen mit Neurodermitis oder anderen atopischen Erkrankungen spezialisiert haben."
  />
);
