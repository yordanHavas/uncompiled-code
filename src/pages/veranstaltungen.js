import React, { useContext } from 'react';
import CardBig from '../components/CardBig';
import FilteredSlider from '../components/FilteredSlider';
import Hero from '../components/Hero';
import { SEO } from '../components/seo';
import { MobileContext } from '../components/Layout';
import { FilterSeminar } from '../helpers/helpers';
import VeranAkkordeon from '../components/VeranAkkordeon';

export default function Veranstaltungen() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      <div className="havas-container light">
        <Hero
          headingTextList={['Aktuelle', 'Veranstaltungen']}
          coloredWordsArr={['Veranstaltungen']}
          imgName="veranstaltungen"
          altImg="Ein Laptop, auf dem Bildschirm ist eine junge Ärtztin zu sehen."
          text="Ob Informationsveranstaltungen für Betroffene und Interessierte, Webinare von Experten oder Live-Talks und Fragerunden, hier kündigen wir Dir spannende Events an."
        />

        <h2>NEURODERMITIS-VERANSTALTUNGEN</h2>
        <p>
          Hier kündigen wir spannende Events,
          Patienteninformationsveranstaltungen und Webinare von Experten für
          Betroffene an.
        </p>

        <VeranAkkordeon seminarArray={FilterSeminar(seminarArray, false)} />
      </div>

      <div className="havas-container dark">
        <h2>Vergangene Veranstaltungen</h2>
        <p>Hier findest du einen Überblick über vergangene Veranstaltungen.</p>
        <FilteredSlider cardsL={FilterSeminar(seminarArray, true)} />
      </div>
    </>
  );
}
export const Head = () => <SEO />;

export const seminarArray = [
  // {
  //   subTitle: 'Instagram Live',
  //   title: 'It‘s Winter time – Hautpflege bei Neurodermitis an kalten Tagen',
  //   text: 'In unserem Instagram Live-Talk rund um das Thema „It‘s Winter time – Hautpflege bei Neurodermitis an kalten Tagen“ geben die Dermatologin Dr. Natalia Kirsten und die Kosmetikwissenschaftlerin Dr. Meike Streker Tipps für die optimale Hautpflege bei Neurodermitis in der kalten Jahreszeit. Du hast den Live-Talk verpasst? Dann schau doch gerne hier direkt rein.',
  //   date: 'Dienstag, 13.12.2022',
  //   buttonText: 'Jetzt nochmal ansehen',
  //   buttonLink: 'https://www.instagram.com/reel/CmHt9sLhrXr',
  //   className: 'eventDisplay titleLarge',
  //   image: 'Insta-Live_06_Hautpflege bei ND_Veranstaltungshinweis_GCI',
  //   altImg:
  //     'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
  // },

  // {
  //   subTitle: 'Online-Seminar',
  //   title: 'Umgang mit Juckreiz und Stress bei Neurodermitis',
  //   text: 'Online-Seminar mit Diplom Psychologin und Asthma- und Neurodermitis-Trainerin Michaela Rünger',
  //   displayDate: 'Dienstag, 03.04 2023',
  //   date: '2023/04/03', // Used for akkordeon logic!!!
  //   buttonText: 'Hier kostenlos anmelden',
  //   buttonLink: '/verstehen',
  //   className: 'eventDisplay titleLarge',
  //   image: 'BG',
  //   altImg:
  //     'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
  // },

  // {
  //   image: 'EventPic',
  //   altImg:
  //     'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
  //   subTitle: 'Veranstaltungs-Typ',
  //   title: 'Veranstaltungsname',
  //   text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem',
  //   buttonText: 'Hier geht’s zum Online-Stream',
  //   buttonLink: '/verstehen',
  //   category: 'webinar',
  // },
  {
    subTitle: 'Instagram Live',
    title: 'Family first – gemeinsam den Familienalltag mit Neurodermitis meistern',
    text: 'In unserem Instagram Live-Talk rund um das Thema „Family first – gemeinsam den Familienalltag mit Neurodermitis meistern“ geben der Dermatologe Dr. Max Tischler und die Kinderärztin Dr. Andrea Jobst Tipps für einen besseren Umgang mit Neurodermitis bei Babys und Kleinkindern. Du möchtest Dir den Live-Talk ansehen? Dann schau auf unserem Instagram-Kanal vorbei.',
    displayDate: ['Dienstag, 23.05.2023',<br/>,'19:00 – 20:00 Uhr'],
    date: '2023/05/23', // Used for akkordeon logic!!!
    buttonText: 'Zum Instagram-Kanal',
    buttonLink: 'https://www.instagram.com/leben_mit_neurodermitis.info/',
    className: 'eventDisplay titleLarge calendarManipulation',
    image: 'MicrosoftTeams-image',
    altImg:
      'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
    cardTitleMinHeight: '110.34px',
    category: 'instagram',
  },
  {
    subTitle: 'Instagram Live',
    title: 'It‘s Winter time – Hautpflege bei Neurodermitis an kalten Tagen',
    text: 'In unserem Instagram Live-Talk rund um das Thema „It‘s Winter time – Hautpflege bei Neurodermitis an kalten Tagen“ geben die Dermatologin Dr. Natalia Kirsten und die Kosmetikwissenschaftlerin Dr. Meike Streker Tipps für die optimale Hautpflege bei Neurodermitis in der kalten Jahreszeit. Du hast den Live-Talk verpasst? Dann schau doch gerne hier direkt rein.',
    displayDate: 'Dienstag, 13.12.2022',
    date: '2022/12/13', // Used for akkordeon logic!!!
    buttonText: 'Jetzt nochmal ansehen',
    buttonLink: 'https://www.instagram.com/reel/CmHt9sLhrXr',
    className: 'eventDisplay titleLarge',
    image: 'Insta-Live_06_Hautpflege bei ND_Veranstaltungshinweis_GCI',
    altImg:
      'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
    cardTitleMinHeight: '110.34px',
    category: 'instagram',
  },

  // {
  //   subTitle: 'Online-Seminar',
  //   title: 'Umgang mit Juckreiz und Stress bei Neurodermitis',
  //   text: 'Online-Seminar mit Diplom Psychologin und Asthma- und Neurodermitis-Trainerin Michaela Rünger',
  //   date: 'Dienstag, 25.10 2022',
  //   buttonText: 'Hier kostenlos anmelden',
  //   buttonLink: '/verstehen',
  //   className: 'eventDisplay titleLarge',
  //   image: 'BG',
  //   altImg:
  //     'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
  // },

  {
    subTitle: 'Instagram Live',
    title: 'Hau(p)tsache selbstbewusst?! Deiner Neurodermitis aktiv begegnen',
    text: 'In unserem Instagram Live-Talk rund um das Thema „Hau(p)tsache selbstbewusst?! Deiner Neurodermitis aktiv begegnen“ geben der Dermatologe Dr. Peter Weisenseel, die Coachin Kathleen Friedrich und die Betroffene Clara Tipps für einen selbstbewussten Umgang mit Neurodermitis. Du hast den Live-Talk verpasst? Dann schau doch gerne hier direkt rein.',
    displayDate: 'Mittwoch, 14.09.2022',
    date: '2022/09/14', // Used for akkordeon logic!!!
    buttonText: 'Jetzt nochmal ansehen',
    buttonLink: 'https://www.instagram.com/reel/CifyKv4hOGg',
    className: 'eventDisplay titleLarge',
    image: 'Insta-Live_05_Selbstbewusstsein mit ND_Veranstaltungshinweis_GCI',
    altImg:
      'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
    cardTitleMinHeight: '110.34px',
    category: 'instagram',
  },
  {
    subTitle: 'Instagram Live',
    title: '#FemaleHealth – Neurodermitis bei Frauen',
    text: 'In unserem Instagram Live-Talk mit dem Thema „#FemaleHealth – Neurodermitis bei Frauen“ geben die Dermatologin Dr. Natalia Kirsten, die Gynäkologin Dr. Mandy Mangler und die Betroffene und Bloggerin Julia Tipps rund um die Gesundheit der Frau mit Neurodermitis. Du hast den Live-Talk verpasst? Dann schau doch gerne hier direkt rein. ',
    displayDate: 'Montag, 23.05.2022',
    date: '2022/05/23', // Used for akkordeon logic!!!
    buttonText: 'Jetzt nochmal ansehen',
    buttonLink: 'https://www.instagram.com/reel/Cd6QsZxBDK9',
    className: 'eventDisplay titleMid',
    image: 'Insta-Live_04_Frauengesundheit bei ND_Veranstaltungshinweis_GCI',
    altImg:
      'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
    cardTitleMinHeight: '110.34px',
    category: 'instagram',
  },
  {
    subTitle: 'Instagram Live',
    title: 'Fakt oder Fake? Mythen und Vorurteile bei Neurodermitis',
    text: 'In unserem Instagram Live-Talk rund um das Thema „Fakt oder Fake? Mythen und Vorurteile bei Neurodermitis“ klären die Dermatologin Dr. Alice Martin, die Psychologin und 2. Vorsitzende des Deutschen Neurodermitis Bundes e.V. und die Betroffene und Bloggerin Medine über die häufigsten Mythen bei Neurodermitis auf. Du hast den Live-Talk verpasst? Dann schau doch gerne hier direkt rein.',
    displayDate: 'Mittwoch, 09.03.2022',
    date: '2022/03/09', // Used for akkordeon logic!!!
    buttonText: 'Jetzt nochmal ansehen',
    buttonLink: 'https://www.instagram.com/reel/Ca5PnT9BIOY',
    className: 'eventDisplay titleMid',
    image: 'Insta-Live_03_Mythen & Vorurteile bei ND_Veranstaltungshinweis_GCI',
    altImg:
      'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
    titleSize: 'mid',
    cardTitleMinHeight: '110.34px',
    category: 'instagram',
  },
  {
    subTitle: 'Instagram Live',
    title: 'Neurodermitis und Ernährung - Beziehungsstatus: „kompliziert“',
    text: 'In unserem Instagram Live-Talk rund um das Thema „Neurodermitis und Ernährung - Beziehungsstatus: ‚kompliziert‘“ geben die Ernährungsexpertin Merrit Arndt und die beiden Betroffenen und Bloggerinnen Laura und Aylin Tipps für die optimale Ernährung bei Neurodermitis. Du hast den Live-Talk verpasst? Dann schau doch gerne hier direkt rein.',
    displayDate: 'Mittwoch, 01.12.2021',
    date: '2021/12/01', // Used for akkordeon logic!!!
    buttonText: 'Jetzt nochmal ansehen ',
    buttonLink: 'https://www.instagram.com/reel/CW851Jth5Dz/',
    className: 'eventDisplay titleMid',
    image: 'Insta-Live_02_Ernährung bei ND_Veranstaltungshinweis_GCI',
    altImg:
      'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
    titleSize: 'mid',
    cardTitleMinHeight: '110.34px',
    category: 'instagram',
  },
  {
    subTitle: 'Instagram Live',
    title: 'Neurodermitis. Familiensache - lasst uns darüber reden.',
    text: 'In unserem Instagram Live-Talk rund um das Thema „Neurodermitis. Familiensache - lasst uns darüber reden.“ geben der Psychotherapeut Dr. Christian Lüdke, der Dermatologe Dr. Ralph von Kiedrowski und die Betroffene und Bloggerin Anja mit ihrem Partner Philipp Tipps für einen besseren Umgang bei Neurodermitis in der Familie. Du hast den Live-Talk verpasst? Dann schau doch gerne hier direkt rein. ',
    displayDate: 'Dienstag, 13.09.2021',
    date: '2021/09/13', // Used for akkordeon logic!!!
    buttonText: 'Jetzt nochmal ansehen ',
    buttonLink: 'https://www.instagram.com/reel/CTxXxC8IKVC',
    className: 'eventDisplay titleMid',
    image: 'Insta-Live_01_Familie & ND_Veranstaltungshinweis_GCI',
    altImg:
      'Junge Frau mit Brille, die einen Bleistift zwischen Lippen und Nase klemmt.',
    titleSize: 'mid',
    cardTitleMinHeight: '110.34px',
    category: 'instagram',
  },
];
