import { Link } from 'gatsby';
import React, { useContext } from 'react';
import Akkordeon from '../components/Akkordeon';
import Bulletlist from '../components/Bulletlist';
import Button from '../components/Button';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';

export default function Jugendlichen() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}
      <div className="havas-container light">
        <Hero
          className="widerHero"
          color="yellow"
          headingTextList={['Neurodermitis bei', 'Jugendlichen ']}
          coloredWordsArr={['Jugendlichen']}
          imgName="jugendliche_img_1"
          altImg="Eine Gruppe von Jugendlichen springt in einen See"
          text="Der Übergang vom Kind zum Erwachsenen ist eine besondere Phase im Leben. Jugendliche übernehmen mehr und mehr Verantwortung und treffen eigene Entscheidungen. Das gilt auch für junge Menschen mit Neurodermitis. Sie wollen sich abgrenzen und brauchen doch immer wieder Unterstützung. Fundiertes Wissen und Verständnis für die Anliegen betroffener Teenager können die ganze Familie beim Umgang mit der chronisch-entzündlichen Hautkrankheit unterstützen."
        />

        <MobileThemenübersicht className="dropdown-1" />
        <p>
          Wenn Kinder mit Neurodermitis erwachsen werden, ist oft ein Wechsel
          von der Kinder- und Jugendmedizin in die Erwachsenenmedizin
          erforderlich. Dann ist es gut, wenn betroffene Teenager bereits mit
          den medizinischen Zusammenhängen ihrer Erkrankung vertraut sind und
          einen selbstbewussten Umgang damit gefunden haben.
        </p>
        <p>
          Das Basiswissen über Neurodermitis hat Euer Kind wahrscheinlich schon,
          denn es kommt selten vor, dass Neurodermitis bei Jugendlichen erstmals
          auftritt. Doch jede Lebensphase bringt neue Herausforderungen mit
          sich. Bleibt im Austausch, auch wenn sich Euer Kind langsam auf eigene
          Füße stellt, könnt Ihr weiterhin da sein, ihm Mut machen und Kraft
          geben.
        </p>
      </div>
      <div
        data-summary="Wie sieht Neurodermitis bei Jugendlichen aus?"
        className="havas-container dark">
        <h2>Wie sieht Neurodermitis bei Jugendlichen aus?</h2>
        <p>
          Die typischen Symptome der Neurodermitis unterscheiden sich bei
          Kindern, Jugendlichen und Erwachsenen im Grunde nicht. Dazu gehören
          entzündete, teilweise blutende Hautstellen (Ekzeme) und starker
          Juckreiz. Auch Hautrötungen (Erytheme), Hautverdickungen
          (Lichenifikationen), feine Hauteinrisse und trockene Haut sind typisch
          für Neurodermitis, genauso wie trockene Haut. Die Ausprägung der
          Symptome und die subjektive Beeinträchtigung unterscheiden sich
          allerdings von Mensch zu Mensch und das Krankheitsbild kann sich im
          Verlauf der Erkrankung verändern.
        </p>
        <ImageWithText
          noTilt={true}
          className="combinedImageWithText"
          imgName="Jugendlichen_page_img_2"
          altImage="Von Neurodermitis betroffene Stellen bei Jugendlichen und jungen Erwachsenen">
          <p>
            Bei Jugendlichen und jungen Erwachsenen treten die Hautveränderungen
            vornehmlich an Hals und Augenlidern sowie an Ellenbeugen und in
            Kniekehlen auf. Auch Ekzeme an Händen und Füßen sind nicht selten.
          </p>
          <p>
            Typisch für Neurodermitis ist, dass sie sich in Zeiten großer
            Hormonumstellungen verändert – beispielsweise in der Pubertät.
          </p>
        </ImageWithText>
        <h3>Kann Neurodermitis in der Pubertät verschwinden?</h3>
        <p>
          Neurodermitis-Schübe klingen bei vielen Jugendlichen mit der Pubertät
          ab oder werden mit zunehmendem Alter seltener und schwächer. Die Haut
          bleibt allerdings empfindlich, trocken und pflegeintensiv. Auch eine
          erhöhte Ekzembereitschaft und die zugrundeliegende Neigung zu einer
          Überreaktion des Immunsystems bleiben bestehen und es kann vorkommen,
          dass die typischen Hautveränderungen später wieder auftauchen.
        </p>
      </div>
      <div
        data-summary="Was tun bei Neurodermitis bei Jugendlichen?"
        className="havas-container light">
        <h2>Was tun bei Neurodermitis bei Jugendlichen?</h2>
        <ImageWithText
          imgName="GettyImages-539669603_gk"
          altImage="Eine junge Patientin lächelt in die Kamera, im Hintergrund steht eine Ärztin.">
          <p>
            Leben heißt Veränderung und die Bedürfnisse Eures Kindes sind mit 6
            anders als mit 16 Jahren. Deshalb ist es wichtig, dass Ihr
            regelmäßig mit dem Arzt Eures Kindes über Veränderungen und
            Bedürfnisse sprecht und auch gemeinsam überprüft, ob die aktuellen
            Therapiemaßnahmen die Erkrankung ausreichend kontrollieren. So kann
            die Behandlung bei Bedarf angepasst werden.
          </p>
        </ImageWithText>
        <SelbsttestTeaser
          customeText={[
            'WIE SEHR HABT IHR',
            'DIE NEURODERMITIS',
            'UNTER KONTROLLE?',
          ]}
        />
        <h3>Die Basispflege – ein fester Bestandteil im Alltag</h3>
        <ImageWithText
          altImage="Frau, die sich die Hände eincremt"
          imgName="ausserliche-therapien_img_1">
          <p>
            Eine konsequente Hautpflege ist in jedem Alter unerlässlich, auch in
            Phasen, in denen die Haut gesund wirkt. Die Basistherapie kann dabei
            helfen, dass Symptome sich nicht verschlimmern, und sie kann neuen
            Krankheitsschüben entgegenwirken. Die Hautpflege sollte sich immer
            am aktuellen Zustand der Haut orientieren, d. h. fette
            Salbengrundlagen auf trockener Haut, Feuchtigkeitscremes bei leicht
            trockener Haut bzw. bei entzündeter oder nässender Haut.
          </p>

          <Link to="/aeusserliche-therapien">
            Weitere Informationen zur Basispflege kannst du hier nachlesen.
          </Link>
        </ImageWithText>
        <p>
          Je älter und eigenständiger Euer Kind wird, umso selbstständiger wird
          es auch bei der Basispflege. Jetzt heißt es darauf vertrauen, dass
          Euer Kind sich selbstverantwortlich um die eigene Gesundheit kümmert.
          Es schadet jedoch sicher nicht, ein Auge darauf zu haben, dass es
          diese grundlegende Therapie auch wirklich konsequent durchführt. Eine
          ausgewogene Balance zwischen Beobachtung, Unterstützung, Erinnern und
          Vertrauen ist jetzt besonders wichtig, damit Euer Kind lernt seine
          Therapie mehr und mehr in die eigenen Hände zu nehmen.
        </p>
        <h3>Wie wird Neurodermitis bei Jugendlichen medikamentös behandelt?</h3>
        <p>
          Gegen starken Juckreiz und Entzündungen helfen oft nur Medikamente.
          Nicht alle Medikamente, die bei Erwachsenen eingesetzt werden, sind
          für Heranwachsende zugelassen. Die Forschung entwickelt sich aber
          stetig weiter und inzwischen stehen einige anerkannte Arzneimittel für
          Jugendliche und Kleinkinder zur Verfügung, die entweder äußerlich
          (topisch) oder innerlich (systemisch) angewandt werden und gute
          Erfolge erzielen können.
        </p>
        <ImageWithText
          imgName="newPicture1"
          altImage="Ein Vater, trägt Creme auf die Wange seiner Tochter auf">
          <h3>Behandlung von Neurodermitis bei Kindern und Jugendlichen</h3>
          <p style={{ textAlign: `${isMobile ? 'left' : 'center'}` }}>
            Ausführliche Informationen zur medikamentösen Behandlung bei Kindern
            und Jugendlichen könnt Ihr hier nachlesen.
          </p>
          <Button text="Mehr erfahren" type="primary" to="/kinder-behandlung" />
        </ImageWithText>
        <h3>Wann zu welchem Arzt?</h3>
        <p>
          Neurodermitis ist nicht gleich Neurodermitis und das Krankheitsbild
          kann sich von Patient zu Patient deutlich unterscheiden. Das Spektrum
          reicht von milden, symptomarmen Formen bis hin zu schweren
          Verlaufsformen, bei denen eine intensive Therapie notwendig ist. Bei
          Jugendlichen mit schwerer Neurodermitis kann es sinnvoll sein, einen
          Dermatologen aufzusuchen, statt die Behandlung von einem Kinderarzt
          oder einer Kinderärztin betreuen zu lassen.
        </p>
        <h3>Tipps für das Arztgespräch</h3>
        <p>
          Jugendlichen fällt es nicht immer leicht, sich auf ihre Gesundheit zu
          fokussieren oder Arzttermine einzuhalten. Dennoch möchte Euer Kind
          wahrscheinlich irgendwann allein zu seinem Arzttermin gehen. In der
          Erwachsenenmedizin ist oft weniger Zeit für Fragen und es wird viel
          Selbstständigkeit von den jungen Patienten verlangt. Erste
          selbstständige Besuche könnt Ihr deshalb gemeinsam vorbereiten.
        </p>
        <Bulletlist
          textList={[
            'Macht gemeinsam vor dem Gespräch Notizen: Schreibt alle Fragen auf, die auftauchen. Alle Fragen sind erlaubt und gleichermaßen wichtig.',
            'Auch Einleitungssätze können hilfreich sein, um auf ein Thema zu kommen. Vor allem wenn Euer Kind Themen ansprechen möchte, die ihm vielleicht peinlich oder unangenehm sind, können solche „Konversationsstarter“ helfen.',
            'Aufzeichnungen, wie ein Neurodermitis-Tagebuch, sollte Euer Kind mitnehmen, um es im Arztgespräch durchzugehen.',
            'Ermutigt Euer Kind, nachzufragen, wenn es eine Antwort nicht verstanden hat.',
          ]}
        />
        <ArztefinderTeaser />
        <ClickableBox
          title="Gut zu wissen"
          header="Wissen mit Wirkung - Neurodermitis-Schulungen">
          <p>
            Vor allem bei Kindern und Jugendlichen mit Neurodermitis können
            spezielle Schulungen den selbstbestimmten Umgang mit der Erkrankung
            fördern. Ein besseres Verständnis der Zusammenhänge kann dabei
            helfen, den Verlauf der Neurodermitis positiv zu beeinflussen. Je
            mehr betroffene Jugendliche und ihre Eltern über medizinische
            Hintergründe, Strategien zur Juckreizlinderung und zum Umgang mit
            psychosozialen Belastungen wissen, desto leichter fällt es ihnen in
            der Regel, das Leben mit der Erkrankung aktiv zu gestalten. Die
            gesetzlichen Krankenkassen übernehmen meist die Kosten der
            Schulungen für Eltern und Kind. Erkundigt Euch!
          </p>
        </ClickableBox>
      </div>
      <div
        data-summary="Alltag mit Neurodermitis - eine besondere Herausforderung für Jugendliche"
        className="havas-container dark">
        <h2>
          Alltag mit Neurodermitis – eine besondere Herausforderung für
          Jugendliche
        </h2>
        <ImageWithText
          imgName="Jugendlichen_page_img_3"
          altImage="Ein Jugendlicher sitzt etwas verzweifelt im Unterricht und greift sich an den Kopf.">
          <p>
            Der Alltag mit Neurodermitis steckt voller Höhen und Tiefen. Der
            ständige Juckreiz lässt Betroffene manchmal förmlich aus der Haut
            fahren, die Schlafstörungen behindern Leistungsfähigkeit und
            Konzentration und sichtbare Ekzeme belasten den Umgang mit anderen,
            und das alles in der ohnehin schon turbulenten Zeit des
            Erwachsenwerdens. Aufwachsen mit Neurodermitis kann ganz schön
            herausfordernd sein. Wir haben einige Themen zusammengestellt, die
            für Jugendliche mit Neurodermitis oft eine Rolle spielen.
          </p>
        </ImageWithText>
        <Akkordeon heading="Neurodermitis in der Schule" open={true}>
          <p>
            Bei Neurodermitis macht der Juckreiz nur selten eine Pause und quält
            Betroffene auch nachts. Viele Jugendliche kratzen sich dann
            unbewusst und das permanente Jucken macht einen erholsamen Schlaf
            fast unmöglich. Das führt nicht selten zu Konzentrations- und
            Leistungseinbußen am Tag. Jugendliche mit Neurodermitis können
            deshalb im Unterricht oft nicht ihr volles Potenzial ausschöpfen.
            Sie sind auch manchmal gereizt und unausgeglichen; damit ecken sie
            auch manchmal an. Es kann auch passieren, dass Euer Kind wegen
            seiner Erkrankung vermehrt Fehltage hat. Die folgenden Tipps können
            helfen, verpassten Lernstoff nachzuarbeiten und den Anschluss nicht
            zu verlieren.
          </p>
          <Bulletlist
            insideAkkordeon={true}
            textList={[
              'Bittet Lehrer Euch Übungen und Aufgaben zu geben, die Ihr zu Hause bearbeiten könnt.',
              'Auch Mitschüler können Euch Hausaufgaben und Mitschriften vorbeibringen und diese mit Eurem Kind durchgehen.',
              'Nach einer langen Fehlzeit kann der zuständige Vertrauenslehrer Eurem Kind helfen, wieder in den Schulalltag zurückzufinden.',
              'Manchmal kann auch Nachhilfeunterricht hilfreich sein oder Lerngruppen mit Mitschülern.',
            ]}></Bulletlist>
          <p>
            Bezieht die Lehrer in der Schule mit ein und informiert sie über die
            Herausforderung der Neurodermitis. Auch Freunde und Mitschüler
            können jetzt eine Unterstützung sein. Erklärt ihnen, wie die
            Krankheit verläuft und welche Auslöser es gibt. Wenn Euer Kind
            selbst mit den Lehrern oder Mitschülern sprechen möchte, könnt Ihr
            es bei der Vorbereitung der Gespräche unterstützen. Manchmal kann es
            allerdings sinnvoll sein, von alldem nichts zu tun und Euer Kind
            selbst machen zu lassen. Ein Patentrezept, was für Euer Kind am
            besten ist, gibt es nicht. Im Zweifel solltet Ihr mit Eurem Kind
            sprechen und einfach nachfragen, welche Unterstützung es sich
            wünscht und was es lieber allein angeht.
          </p>
          <p>
            Meist hilft es jedoch, wenn das Umfeld gut informiert ist. Dann
            können alle verständnisvoller mit der Erkrankung Eures Kindes
            umgehen.
          </p>
        </Akkordeon>
        <Akkordeon heading="Jugendliche mit Neurodermitis und Mobbing">
          <p>
            Euer Kind wird eventuell wegen der sichtbaren Symptome der
            Neurodermitis gemobbt. So etwas sollte nie ignoriert werden. Je
            besser Euer Kind über seine Erkrankung Bescheid weiß, desto
            erfolgreicher kann es sich meist gegen Mobbing wehren. Sprecht mit
            ihm darüber, dass die Krankheit nicht ansteckend ist und dass viele
            Menschen Neurodermitis haben.
          </p>
          <p>
            Es ist wichtig, dass Euer Kind erkennt, dass es nicht seine Schuld
            ist, wenn es von anderen gemobbt wird, und dass es das nicht
            hinnehmen muss. Erklärt auch, dass es nicht allein da durch muss.
            Überlegt Euch, andere Vertrauenspersonen zu informieren – zum
            Beispiel einen Lehrer oder einen Trainer.
          </p>
          <p>
            Gebt Eurem Kind Zeit und Gelegenheit, von sich aus über Probleme und
            Ängste zu sprechen. Fällt es ihm schwer, seine Gefühle mitzuteilen,
            und reagiert es nicht gut auf direkte Fragen? Dann ist es vielleicht
            besser, ihm ganz allgemeine Fragen über seinen Tag zu stellen und zu
            beobachten, ob sich sein Verhalten ändert.
          </p>
        </Akkordeon>
        <Akkordeon heading="Spieglein, Spieglein – Aussehen und Image">
          <p>
            In der Pubertät kann schon ein Pickel zur Katastrophe werden.
            Aussehen und Image sind jetzt besonders wichtig. Viele junge
            Patienten mit Neurodermitis gehen sozialen Kontakten deshalb aus dem
            Weg – aus Angst, darauf angesprochen, abgelehnt oder gar ausgelacht
            zu werden. Die Neurodermitis kratzt nicht selten auch am
            Selbstbewusstsein Eures Kindes und die psychische Belastung kann
            enorm sein.
          </p>
          <p>
            Sollte sie überhandnehmen, kann professionelle Unterstützung
            sinnvoll sein. Sie hilft Eurem Kind, eine seelische „Rüstung”
            aufzubauen, damit es innere Stärke und Stabilität entwickeln kann.
            Externe Unterstützung kann für Euer Miteinander eine große Hilfe
            sein. Da Jugendliche unterschiedlich mit der Erkrankung und ihren
            Begleiterscheinungen umgehen, kann es notwendig sein, verschiedene
            Maßnahmen auszuprobieren, um herauszufinden, was Eurem Kind am
            besten hilft.
          </p>
        </Akkordeon>
        <Akkordeon heading="Das geht unter die Haut – Nähe und Intimität">
          <p>
            In der Pubertät bauen Jugendliche wichtige Beziehungen mit Menschen
            auf, die nicht zur Familie gehören, und der Freundeskreis spielt
            eine große Rolle. Jugendliche mit Neurodermitis haben es dabei oft
            schwer. Aus Scham und wegen der Reaktion ihres Umfeldes entwickeln
            sie nicht selten Berührungsängste und versuchen, ihre Haut zu
            verstecken. Nähe und Intimität sind meist schwierige Themen. Hier
            kann Euer Kind vor allem von Menschen unterstützt werden, die in
            einer ähnlichen Situation sind.
          </p>
        </Akkordeon>
        <Akkordeon heading="Weitere Unterstützung und Austausch in einer Community">
          <p>
            Es ist wichtig, dass Ihr Eurem Kind zeigt, dass es nicht allein ist
            und dass es Gleichaltrige findet, die eine ähnliche Situation
            teilen. Selbsthilfegruppen können ein guter Weg sein, um mit
            Menschen zusammenzukommen, die mit den gleichen Problemen kämpfen.
          </p>
          <p>
            Auch das Internet und die sozialen Medien können Eurem Kind Zugang
            zu einer seriösen und informativen Betroffenen-Community bieten.
            Dabei solltet Ihr allerdings beachten, dass es die digitalen Medien
            nicht als Zuflucht nutzt, um realen Kontakten aus dem Weg zu gehen.
          </p>
          <p>
            Auf unserem{' '}
            <a
              href="https://www.instagram.com/leben_mit_neurodermitis.info/"
              target="_blank">
              Instagram-Kanal
            </a>{' '}
            findet Ihr regelmäßig hilfreiche Tipps und könnt Euch mit anderen
            Betroffenen austauschen.
          </p>
        </Akkordeon>
      </div>
      <div data-summary="Fazit" className="havas-container light">
        <h2>Fazit</h2>
        <p>
          Wenn Kinder langsam zu Erwachsenen werden, entwickeln sie ein Gefühl
          dafür, wer sie sind und was sie vom Leben erwarten. Diese Lebensphase
          steckt voller Höhen und Tiefen und fordert die ganze Familie heraus.
          Für Jugendliche mit Neurodermitis kommen krankheitsbedingte
          Herausforderungen hinzu, denn die entzündliche Hauterkrankung geht
          neben körperlichen auch mit seelischen Belastungen einher. Lasst Euch
          von Eurem Dermatologen beraten, um gemeinsam einen Weg zu finden, wie
          die Neurodermitis Eures Kindes am besten kontrolliert werden kann,
          damit es sein Leben so unbeschwert wie möglich genießen kann.
        </p>
        <ArztefinderTeaser />
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Jugendliche"
    metaTitle="Neurodermitis bei Jugendlichen"
    description="Bei Jugendlichen mit Neurodermitis treten die Ekzeme vornehmlich an Hals, Augenlidern, Ellenbeugen, Kniekehlen, Händen und Füßen auf."
    trustKeyWords="Neurodermitis bei Teenagern, Neurodermitis in der Pubertät "
  />
);
