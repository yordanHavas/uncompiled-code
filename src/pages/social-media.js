import * as React from 'react';
import { useContext } from 'react';
import Hero from '../components/Hero';
import Instagram from '../components/Instagram';
import { MobileContext } from '../components/Layout';
import Podcast from '../components/Podcast';
import { SEO } from '../components/seo';
import YouTube from '../components/YouTube';
import IconsRow from '../components/IconsRow';

const SocialMedia = () => {
  const isMobile = useContext(MobileContext);

  return (
    <>
      <div className={`havas-container ${isMobile ? 'dark' : 'light'}`}>
        <Hero
          className="l-height-desktop"
          headingTextList={['Social media &', 'Podcast']}
          coloredWordsArr={['Podcast']}
          imgName="social-media_img_1"
          altImg="Eine junge Frau mit Handy"
          text={[
            <IconsRow style={{ marginBottom: '30px' }} />,
            'Let’s get social! Gefällt Dir? Dann folge uns auch auf Social Media und bleib auf dem Laufenden. Hier findest Du einen Überblick, auf welchen Kanälen wir vertreten sind. Wir freuen uns auf Dich!',
          ]}
        />
      </div>
      <div className="havas-container dark">
        <Instagram />
      </div>

      <div className="havas-container light">
        <YouTube />
      </div>
      <div className="havas-container dark">
        <Podcast />
      </div>
    </>
  );
};
export default SocialMedia;
export const Head = () => <SEO />;
