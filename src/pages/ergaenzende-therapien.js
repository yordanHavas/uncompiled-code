import { Link } from 'gatsby';
import React, { useContext } from 'react';
import AchtungBox from '../components/AchtungBox';
import Bulletlist from '../components/Bulletlist';
import ClickableBox from '../components/ClickableBox';
import Hero from '../components/Hero';
import ImageWithText from '../components/ImageWithText';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import ZitatBox from '../components/ZitatBox';

export default function ErganzendeTherapien() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={[
            'Neurodermitis-',
            'weitere',
            'Behandlungs-',
            'möglichkeiten',
          ]}
          className="l-height-desktop"
          coloredWordsArr={['weitere']}
          imgName="ergänzende_img_1"
          altImg="Eine junge Frau, die auf dem Rücken liegt und die Arme über den Kopf streckt"
          color="yellow"
          text="Neurodermitis ist nicht bei jedem 
          gleich und das Krankheitsbild kann 
          sich deutlich unterscheiden. Einige 
          Menschen haben nur milde 
          Beschwerden, andere leiden unter 
          schweren Symptomen, bei denen 
          eine intensive Therapie notwendig 
          ist. Neben der Basistherapie und 
          der Behandlung mit wirkstoffhaltigen Arzneien gibt es auch einige ergänzende Maßnahmen, die Dich dabei unterstützen können, Deine Neurodermitis in Schach zu halten."
        />

        <MobileThemenübersicht className="dropdown-1" />

        <p>
          Heutzutage gibt es eine Fülle an Behandlungsmöglichkeiten für Menschen
          mit Neurodermitis und häufig werden verschiedene Maßnahmen miteinander
          kombiniert, um die Therapie an Dein Krankheitsbild und Deine
          individuellen Bedürfnisse anzupassen. Neben den
          <Link to="/aeusserliche-therapien"> äußerlichen </Link>
          (topischen) und den{' '}
          <Link to="/innerliche-therapien">innerlichen</Link> (systemischen)
          Therapien können ergänzende Therapien zum Einsatz kommen.{' '}
        </p>
        <p>
          Zur begleitenden Ekzem-Therapie zählt zum Beispiel die Phototherapie.
          Psychotherapeutische Maßnahmen oder Schulungen können beim
          alltäglichen Umgang mit der chronischen Erkrankung helfen und selbst
          Entspannungs-techniken zum Stressabbau können die Behandlung der
          atopischen Dermatitis sinnvoll ergänzen.
        </p>
      </div>
      <div
        data-summary="Phototherapie bei Neurodermitis"
        className="havas-container dark">
        <h2>Phototherapie bei Neurodermitis</h2>
        <ImageWithText
          imageOnRight={true}
          altImage="Frau erhält eine Therapiemit UV-Licht im Gesicht"
          imgName="ergänzende_img_2">
          <p>
            Die Phototherapie wird oft auch UV-Licht-Therapie genannt und
            manchmal wird sie auch als Bestrahlung der Neurodermitis bezeichnet.
            Dabei wird ultraviolettes Licht (UV-Licht) verschiedener
            Wellenlängen eingesetzt, um den Juckreiz und die Entzündung zu
            verringern, die mit Neurodermitis einhergehen.<sup>1</sup>
          </p>
        </ImageWithText>
        <p>
          Eine Phototherapie (UV-Licht-Therapie) kann eine sinnvolle Option für
          Menschen mit langwierigen Hautkrankheiten sein, bei denen die
          betroffenen Hautstellen verdickt sind und jucken.<sup>1</sup> Die Haut
          wird hier mit künstlichem UV-Licht bestrahlt.
        </p>
        <AchtungBox
          iconName="exclamation_icon"
          text={[
            'Eine Phototherapie solltest Du nicht zu Hause mit irgendwelchen UV-Lampen ausprobieren. Damit kannst Du Deiner Haut eventuell sogar schaden. Eine Phototherapie muss von einer medizinischen Fachkraft durchgeführt werden.',
            <sup>1,2</sup>,
          ]}
        />
        <ClickableBox
          className="overBullet"
          title="Gut zu wissen"
          header="Balneophototherapie">
          <p>
            Eine Spezialform der Phototherapie ist die Balneophototherapie.
            Hierbei werden Wannenbäder mit verschiedenen Zusätzen und eine
            UV-Licht-Therapie kombiniert.
          </p>
        </ClickableBox>

        <Bulletlist
          firstColumnSize={3}
          bigHeading={['Wichtige Fakten im Überblick:', <sup>1,2</sup>]}
          textList={[
            'Die Phototherapie setzt verschiedene sogenannte photobiologische Prozesse in der Haut in Gang und beeinflusst dadurch Haut und Immunzellen. Sie hat eine entzündungshemmende und immunsuppressive bzw. immunmodulierende Wirkung. Deshalb ist sie eine sinnvolle Maßnahme für manche Menschen mit Neurodermitis.',
            'Eine Phototherapie kann zusammen mit anderen Behandlungsmaßnahmen verordnet werden, wie z. B. topischen Glukokortikoiden oder entsprechenden Salben.',
            'Wie groß der Nutzen der Behandlung ist, ist von Patient zu Patient unterschiedlich. Bei manchen kann sie die Neurodermitis-Symptome verbessern und mögliche Infektionen der Haut reduzieren.',
            'Eine Phototherapie wird meist regelmäßig zur Behandlung von chronischem Juckreiz und Hautverhärtungen angewendet, nicht nur punktuell zur Behandlung von Schüben.',
            'Für die Behandlungssitzungen musst Du gegebenenfalls regelmäßig einen Arzt oder eine Klinik besuchen, der oder die diese Therapieform anbietet. Die Dauer der Behandlung ist je nach Einzelfall unterschiedlich.',
            'Diese Behandlung wird Dir nicht verordnet, wenn Dein Zustand sich in natürlichem Sonnenlicht zu verschlechtern scheint.',
            'Zu den häufigsten Nebenwirkungen zählen Sonnenbrand und Schmerzempfindlichkeit der Haut.',
          ]}
        />
        <ZitatBox
          isLoudspeaker={false}
          text={[
            '„Alle UV-Behandlungen bergen theoretisch ein langfristiges Risiko für die Beschleunigung der Haltalterung und Entwicklung von Hautkrebs.“',
            <sup>1</sup>,
          ]}></ZitatBox>
        <ClickableBox
          title="Gut zu wissen"
          header={[
            'Ein reizendes Klima: Klimatherapie bei Neurodermitis',
            <sup>3</sup>,
          ]}>
          <p>
            Wind, Wasser, Sonne und klare Luft, das sind unter anderem die
            Komponenten einer Klimatherapie. Deren Wirkung ist mittlerweile bei
            vielen Krankheiten wissenschaftlich belegt. Reizklimazonen wie die
            Nord- und Ostseeküste oder Hochgebirge können auch einen positiven
            Effekt auf atopische Erkrankungen wie die Neurodermitis haben.
            Längere Aufenthalte wirken sich im Allgemeinen positiv auf die
            Erkrankung aus.
          </p>
        </ClickableBox>
      </div>
      <div
        data-summary="Psychologische Behandlung"
        className="havas-container light">
        <h2>Psychologische Behandlung</h2>
        <p>
          Neurodermitis ist mehr als eine Hautkrankheit und ein Leben mit der
          chronisch-entzündlichen Erkrankung kann eine Herausforderung sein.
          Deshalb leidet oft auch die Psyche. Wenn der Leidensdruck durch Deine
          Erkrankung sehr hoch ist, können psychotherapeutische Verfahren
          eventuell eine sinnvolle Ergänzung Deiner Therapie sein. Denke daran:
          Eine stabile Psyche kann sich positiv auf Deine Haut auswirken und
          Stress gilt als schubfördernd.
        </p>
        <p>
          Im Lernmodul{' '}
          <a href="/lernplattform/elearning-11">„Positives Denken lernen“</a>{' '}
          haben wir Tipps für Dich zusammengestellt, die Dir helfen können, mit
          mehr Freude durch Dein Leben zu gehen.
        </p>
        <SelbsttestTeaser />
      </div>
      <div
        data-summary="Neurodermitis-Schulungen"
        className="havas-container dark">
        <h2>Neurodermitis-Schulungen</h2>
        <ImageWithText
          imgName="ergänzende_img_3"
          altImage="Frau macht sich Notizen während einer Online-Schulung">
          <p>
            Vor allem bei Kindern und Jugendlichen mit Neurodermitis können
            spezielle Schulungen das Krankheitsverständnis und den
            selbstbestimmten Umgang mit der Erkrankung fördern. Wer gut
            informiert ist und viel über seine Erkrankung weiß, kann den Verlauf
            der Neurodermitis aktiv positiv beeinflussen. Viele Schulungen sind
            speziell für Eltern oder auch für Eltern und Kinder gemeinsam
            vorgesehen. Je mehr Du über z. B. medizinische Hintergründe,
            Strategien zur Juckreizlinderung und zum Umgang mit psychosozialen
            Belastungen weißt, desto leichter fällt Dir das Leben mit der
            Erkrankung. Die gesetzlichen Krankenkassen übernehmen in der Regel
            die Kosten der Schulungen für Eltern und Kinder.{' '}
          </p>
          <p>
            Da sich Schulungen auch auf ältere Patienten positiv auswirken
            können, gibt es auch Angebote für Erwachsene.
          </p>
        </ImageWithText>
      </div>
      <div
        data-summary="Vermeiden von Triggern"
        className="havas-container light">
        <h2>Vermeiden von Triggern</h2>
        <p>
          Was einen Schub auslöst, ist nicht bei allen Menschen mit
          Neurodermitis gleich. Was bei dem einen zu Beschwerden führt, bleibt
          bei dem anderen folgenlos. Für alle Betroffenen ist es jedoch wichtig,
          die eigenen Trigger zu kennen und diese, soweit das eben möglich ist,
          zu vermeiden. Ein{' '}
          <Link to="/broschueren-und-downloads">Tagebuch</Link> kann Dir dabei
          helfen herauszufinden, was einen Schub bei Dir auslöst. Sprich auch
          mit Deinem Dermatologen, denn es gibt verschiedene Tests, die zeigen
          können, ob Du auf bestimmte Stoffe allergisch reagierst.
        </p>
        <h3>Ist das gesund? Neurodermitis und Eliminationsdiät</h3>
        <p>
          Auch Lebensmittel können zu den Triggern gehören. Es kann daher
          sinnvoll sein, bestimmte Nahrungsmittel aus Deinem Speiseplan zu
          streichen, wodurch sich die Ekzemintensität eventuell verringern kann.
          Eine sogenannte Eliminationsdiät ist aber nur dann sinnvoll, wenn
          tatsächlich eine Allergie vorliegt. Außerdem solltest Du Dir darüber
          im Klaren sein, dass Diäten zu einer Mangel- bzw. Fehlernährung und so
          zu einer Verschlechterung des Gesundheitszustandes führen können.
          Liegen tatsächlich Nahrungsmittelallergien vor, kannst Du in Absprache
          mit einem Arzt oder einem Ernährungsberater ggf. Deinen Ernährungsplan
          anpassen.
        </p>
        <p>
          Allgemeine Regeln für eine ausgewogene und abwechslungsreiche
          Ernährung findest Du im Lernmodul
          <a href="/lernplattform/elearning-12" target="_blank">
            {' '}
            „Ernährung bei Neurodermitis“
          </a>
          .
        </p>
        <h3>Entspannungstechniken bei Neurodermitis</h3>
        <ImageWithText
          imgName="ergänzende_img_422"
          altImage="Eine junge Frau mit geschlossenen Augen im Meditationssitz">
          <p>
            Wie Dein Körper braucht auch Deine Psyche hin und wieder Zuwendung.
            Vor allem Stress solltest Du vermeiden, denn der zählt zu den
            Auslösern für Neurodermitis. Leichter gesagt als getan. Stress lässt
            sich nicht immer umgehen. Deshalb ist es wichtig, Dir in Deinem
            Alltag Ruhephasen zu gönnen. Stress ist vor allem dann
            problematisch, wenn Du keinen Ausgleich schaffst. Es gibt zahlreiche
            Entspannungstechniken von A wie autogenem Training bis zu Z wie
            Zen-Meditation, da wird sicher jeder fündig. Vielleicht findest Du
            nicht auf Anhieb die Technik, die am besten in Dein Leben passt. Es
            lohnt sich aber dranzubleiben, denn wenn Du eine Technik für Dich
            gefunden hast, kannst Du dem Stress etwas entgegensetzen.
          </p>
          <p>
            Wie Du mit Stress umgehen kannst, kannst Du auch im Lernmodul
            <a href="/lernplattform/elearning-6" target="_blank">
              {' '}
              „Tipps für eine bessere Stressbewältigung“
            </a>{' '}
            nachlesen.
          </p>
        </ImageWithText>
      </div>
      <div data-summary="Fazit" className="havas-container dark">
        <h2>Fazit</h2>
        <p>
          Die Auswirkungen der Neurodermitis beschränken sich oft nicht nur auf
          Deine Haut. So zahlreich wie die Auswirkungen der
          chronisch-entzündlichen Erkrankung auf Dein Leben sein können, so
          zahlreich sind auch die{' '}
          <Link to="/ueberblick">Behandlungsoptionen</Link>, die Dir zur
          Verfügung stehen. Lass Dich von Deiner Neurodermitis nicht
          unterkriegen, werde aktiv und lass Dich beraten.
        </p>
        <ArztefinderTeaser />
      </div>
      <div className={`havas-container`}>
        <div className="bottom-text">
          <p>
            <sup>1</sup>Wollenberg A et al. J Eur Acad Dermatol Venereol 2018;
            32:657–682.
          </p>
          <p>
            <sup>2</sup>
            National Eczema Association. Prescription topicals. Abrufbar unter:
            nationaleczema.org/eczema/treatment/topicals/ (Abgerufen im
            September 2022).
          </p>
          <p>
            <sup>3</sup>Thieme. Klimatherapie: Was Gebirgsluft und Meeresklima
            leisten. Abrufbar unter:
            https://www.thieme.de/de/presse/fzm-Klimatherapie-28939.htm.
            (Abgerufen im September 2022).
          </p>
        </div>
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Begleitende Therapien"
    metaTitle="Neurodermitis – weitere Behandlungsmöglichkeiten"
    description="Neben der Basispflege, den medikamentösen äußerlichen und innerlichen Therapien, gibt es zahlreiche ergänzende Behandlungsoptionen für Menschen mit Neurodermitis."
    trustKeyWords="Lichttherapie Neurodermitis, Neurodermitis Lichttherapie, Neurodermitis Lichttherapie zu Hause, Lichttherapie Ekzem, Biologie Neurodermitis, Neurodermitis neue Therapie, UV Therapie Neurodermitis, Ekzem Therapie, Phototherapie Neurodermitis, Balneuphototherapie Neurodermitis, Antikörpertherapie Neurodermitis, Systemtherapie Neurodermitis, Neurodermitis systematische Therapie, Blaulichttherapie Neurodermitis, Bestrahlung Neurodermitis, Stufentherapie Neurodermitis, Neurodermitis Therapie Spritze"
  />
);
