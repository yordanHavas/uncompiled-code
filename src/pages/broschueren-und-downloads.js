import React from 'react';
import Hero from '../components/Hero';
import { DescktopThemenübersicht } from '../components/Themenübersicht';
import CrossTeaser from '../components/CrossTeaser';
import { SEO } from '../components/seo';
import pdfDok from '../../static/resources/brochure/Familienratgeber.pdf';
import DownloadIcon from '../assets/icons/download_icon.svg';

export default function BroschürenUndDownloads() {
  const teaserList = [
    {
      imgName: 'broschüren_img_2',
      altImage:
        'Abbildung der Broschüre zu Behandlungsmöglichkeiten bei Neurodermitis',
      description:
        'Eine Übersicht über die verschiedenen Behandlungsmöglichkeiten: Hiermit kannst Du gut gerüstet in das Gespräch mit Deinem Dermatologen gehen, damit Ihr gemeinsam einen für Dich passenden Weg findet, Deine Neurodermitis unter Kontrolle zu bringen.',
      headText: 'Behandlungsmöglichkeiten bei Neurodermitis',
      btnText: 'Jetzt herunterladen',
      link: 'https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Behandlungsmoeglichkeiten.pdf?la=de-DE',
    },

    {
      imgName: 'newBroshure',
      altImage: 'Abbildung einer Informationsbroschüre für Eltern',
      description:
        'Mit dieser Broschüre speziell für Eltern von Kindern und Jugendlichen mit Neurodermitis möchten wir helfen, die Hintergründe und Behandlungsoptionen der Erkrankung zu verstehen.',
      headText: 'Eine Informationsbroschüre für Eltern',
      btnText: 'Jetzt herunterladen',
      link: pdfDok,
      customButton: (
        <a href={pdfDok} className="havas-button button-anchor">
          <button className={'primary-button'}>
            Jetzt herunterladen
            <img src={DownloadIcon} />
          </button>
        </a>
      ),
    },

    {
      imgName: 'broschüren_img_4',
      altImage:
        'Eine Abbildung des Ratgebers für das Arztgespräch bei Neurodermitis',
      description:
        'Ein offenes und ehrliches Gespräch mit Deinem Dermatologen zu führen, ist wichtig. Nur dann, wenn er gut über Deine Situation informiert  ist, kann Dir gezielt geholfen und Deine Therapie entsprechend angepasst werden. Unser Ratgeber für das Arztgespräch kann Dir helfen, Dich optimal auf Deinen nächsten Termin vorzubereiten.',
      headText: 'Ratgeber für das Arztgespräch bei Neurodermitis',
      btnText: 'Jetzt herunterladen',
      link: 'https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Ratgeber_Arztgespraech.pdf?la=de-DE',
    },

    {
      imgName: 'broschüren_img_5',
      altImage: 'Abbildung der Printversion des Neurodermitis Tagebuchs',
      description:
        'Um Deine Neurodermitis wirksam behandeln zu können, ist es wichtig, herauszufinden, was die Auslöser (Trigger) für die Krankheitsschübe sind. Hierbei kann ein Neurodermitis-Tagebuch behilflich sein.',
      headText: 'Dein Neurodermitis Tagebuch zum Download',
      btnText: 'Jetzt herunterladen',
      link: 'https://www.leben-mit-neurodermitis.info/media/EMS/Conditions/Dermatology/Brands/Ad-Revealed-DE/Updates-2020/Aktuelles/Neurodermitis_Patiententagebuch_interaktiv.pdf?la=de-DE',
    },
  ];
  return (
    <>
      <DescktopThemenübersicht />
      <div className="havas-container light">
        <Hero
          className="l-height-desktop heroComp"
          headingTextList={['Broschüren', 'Und', 'Downloads']}
          coloredWordsArr={['Downloads']}
          imgName="broschüren_img_1"
          altImg="Eine junge Frau mit Handy"
          color="#60e2e3"
          text={[
            'Alles auf einen Blick! Hier findest Du interessante Infomaterialien ganz einfach zum Herunterladen.',
            <br />,
            <br />,
            'Du kannst unsere Download-Materialien auch gerne als Broschüren kostenlos unter ',
            <a href="tel:0800 40 500 20">0800 40 500 20</a>,
            ' (gebührenfrei) bestellen.',
          ]}
        />
        <div className="crossContainer">
          {teaserList.map((el, index) => {
            return (
              <CrossTeaser
                imgName={el.imgName}
                altImage={el.altImage}
                description={el.description}
                headText={el.headText}
                btnText={el.btnText}
                link={el.link}
                customButton={el.customButton ? el.customButton : false}
              />
            );
          })}
        </div>
        <p>
          Du kannst unsere Download-Materialien auch gerne als Broschüren
          kostenlos unter <a href="tel:0800 40 500 20">0800 40 500 20</a>{' '}
          (gebührenfrei) bestellen.
        </p>
      </div>
    </>
  );
}
export const Head = () => <SEO />;
