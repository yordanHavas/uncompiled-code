import React, { useContext } from 'react';
import Hero from '../components/Hero';
import Bulletlist from '../components/Bulletlist';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import { ArztefinderTeaser } from '../components/Teaser';
import TippsBox from '../components/TippsBox';
import Akkordeon from '../components/Akkordeon';
import { MobileContext } from '../components/Layout';
import ImageWithText from '../components/ImageWithText';
import ClickableBox from '../components/ClickableBox';
import ZitatBox from '../components/ZitatBox';
import { SEO } from '../components/seo';

export default function Behandlung() {
  const isMobile = useContext(MobileContext);

  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          className="image-position-left"
          headingTextList={[
            'Behandlung von',
            'Neurodermitis',
            'bei Babys und',
            'Kindern',
          ]}
          coloredWordsArr={['Babys und', 'Kindern']}
          color="#60e2e3"
          imgName="behandlung_img_1"
          altImg="Ein Vater, trägt Creme auf die Wange seiner Tochter auf"
          text={[
            'Neurodermitis ist die häufigste chronische Erkrankung im Kindesalter. In Deutschland sind etwa 9,4 Prozent der Kinder bis 15 Jahre betroffen.',
            <sup>1</sup>,
            ' Zwar nehmen die Symptome mit zunehmendem Alter häufig ab, allerdings ist Neurodermitis bisher nicht heilbar. Die Beschwerden der Erkrankung können jedoch durch unterschiedliche Therapiemaßnahmen meist unter Kontrolle gebracht werden und mit etwas Unterstützung können betroffene Kinder ein weitgehend normales Leben führen.',
          ]}
        />
        <MobileThemenübersicht />
        <p>
          Neurodermitis ist nicht gleich Neurodermitis und das Krankheitsbild
          unterscheidet sich von Kind zu Kind. Deshalb ist es zunächst wichtig,
          dass die Schwere der Erkrankung festgestellt wird. Auf dieser Basis
          wird die Therapie individuell an die Bedürfnisse Eures Kindes
          angepasst. Für die Behandlung der Neurodermitis ist meist viel Geduld
          nötig. Heute steht jedoch eine Vielzahl von anerkannten Arzneimitteln
          zur Verfügung, die gute Erfolge erzielen können.
        </p>
      </div>

      <div
        className="havas-container dark"
        data-summary="Basistherapie: die tägliche Hautpflege">
        <h2 className="w-100">Basistherapie: die tägliche Hautpflege</h2>
        <div
          className="anchorOffset"
          style={{ top: `${isMobile ? '-200px' : '-300px'}` }}
          id="basistherapieAnchor"></div>
        <ImageWithText
          imgName="behandlung_img_2_test"
          altImage="Eine Mutter, trägt Creme auf die Wange ihrer Tochter auf">
          <p>
            Bei der Basistherapie kommen Produkte zum Einsatz, die keine
            medizinischen Wirkstoffe enthalten und äußerlich (topisch)
            angewendet werden, sogenannte Emollienzien. Das sind Salben, Cremes,
            Lotionen oder Badezusätze, die die Haut vor dem Austrocknen
            schützen, indem sie eine Barriere bilden, die Feuchtigkeit
            zurückhält. So lindern sie Beschwerden bei juckender, trockener Haut
            und können das Infektionsrisiko verringern.<sup>2,3</sup>
          </p>
        </ImageWithText>

        <div className="picture-text">
          <div className="item" style={{ margin: 'auto' }}>
            <p>
              Diese grundlegende Hautpflege hilft dabei, dass Symptome sich
              nicht verschlimmern, und sie kann neuen Krankheitsschüben
              entgegenwirken. Die Basispflege sollte sich immer am aktuellen
              Hautzustand orientieren, d. h. fette Salbengrundlagen auf
              trockener Haut, Feuchtigkeitscremes bei leicht trockener Haut bzw.
              bei entzündeter oder nässender Haut.
            </p>

            <br></br>
            <p>
              Ob Euer Kind bestimmte Pflegemittel verträgt und welche Salbe oder
              Creme am besten geeignet ist, könnt Ihr im Halbseitenversuch
              testen. Dabei werden die beiden Körperhälften Eures Kindes mit
              unterschiedlichen Pflegemitteln behandelt und nach ein paar Tagen
              miteinander verglichen.
            </p>
          </div>

          <div className="item">
            <ZitatBox
              isLoudspeaker={true}
              text="Vorsicht ist bei Harnstoff geboten - er wird bei Säuglingen nicht empfohlen."
            />
          </div>
        </div>

        <p>
          Gegen starken Juckreiz und Entzündungen helfen oft nur Medikamente.
          Babys und Kinder haben allerdings eine wesentlich empfindlichere Haut
          als Erwachsene und reagieren anders auf Wirkstoffe. Deshalb sind
          manche Medikamente für Kinder nicht zugelassen. Es stehen dennoch
          verschiedene anerkannte Arzneimittel für Kinder zur Verfügung, die
          entweder äußerlich (topisch) oder innerlich (systemisch) angewendet
          werden.
        </p>
      </div>

      <div
        className="havas-container light"
        data-summary="Äußerliche (topische) Therapien mit wirkstoffhaltigen Präparaten">
        <h2 className="w-100">
          Äußerliche (topische) Therapien mit wirkstoffhaltigen Präparaten
        </h2>

        <p>
          Bei der topischen Therapie werden Arzneimittel direkt auf entzündete
          Hautareale aufgetragen. In der Regel handelt es sich um
          antientzündliche, juckreizstillende Cremes und Salben.
        </p>

        <Bulletlist
          circleClassName="large-backgroud textDown"
          className="violet"
          heading="Dabei kommen in der Regel 2 Wirkstoffgruppen zum Einsatz:"
          textList={['Glukokortikoide (Kortison)', 'Calcineurin-Hemmer']}
        />

        <Akkordeon
          heading="Topische
Glukokortikoide (Kortison)">
          <p style={{ lineHeight: '1.44' }}>
            Topische Glukokortikoide, besser bekannt als Kortison bzw. Cortison,
            sind eine Gruppe von Medikamenten, die in verschiedenen Formen
            erhältlich sind, z. B. als Cremes und Salben. Es gibt sie in
            unterschiedlichen Stärken, sodass die Behandlung auf die Schwere der
            Neurodermitis zugeschnitten werden kann.<sup>4</sup>
          </p>
          <p>
            <strong>
              Gerade bei Baby- und Kinderhaut ist bei der Behandlung besondere
              Sorgfalt geboten, denn ihre Hautbarriere ist noch nicht
              vollständig ausgebildet.
            </strong>
          </p>
          <p>
            Die häufigste Nebenwirkung bei einer äußerlichen Anwendung von
            Kortison ist ein leichtes Brennen beim Auftragen, das aber meist
            weniger wird, je mehr sich die Haut Eures Kindes an die Behandlung
            gewöhnt. Weitere mögliche Nebenwirkungen sind Hautverdünnung
            (Atrophie) und Pigmentstörungen auf der Haut.<sup>5</sup>
          </p>
          <p>
            Bei richtiger kurzzeitiger Anwendung sind diese Nebenwirkungen
            selten, jedoch werden topische Glukokortikoide nicht zur
            Langzeitanwendung empfohlen. Sie sollten zudem auf sensiblen
            Hautarealen zurückhaltend eingesetzt werden. So sollte Kortison bei
            Babys und Kindern nur in Ausnahmefällen im Gesicht angewendet
            werden.<sup>4</sup>
          </p>
        </Akkordeon>
        <Akkordeon
          heading="Topische 
          Calcineurin-Hemmer">
          <p style={{ lineHeight: '1.44' }}>
            An besonders empfindlichen Hautstellen, wie im Gesicht und
            Genitalbereich, können Salben bzw. Cremes mit Calcineurin-Hemmern
            aufgetragen werden. Sie haben ebenfalls eine antientzündliche
            Wirkung und gehören zur Gruppe der Immunsuppressiva, d. h. sie
            drosseln die Aktivität des Immunsystems.<sup>6</sup>
          </p>
          <p>
            Bei Kleinkindern können sie ab einem Alter von zwei Jahren
            angewendet werden. Sie eignen sich auch zur Behandlung von
            empfindlichen Hautstellen und können über einen längeren Zeitraum
            mit Unterbrechungen angewendet werden.
          </p>
          <p>
            Calcineurin-Hemmer sind im Allgemeinen relativ gut verträglich.
            <sup>7</sup> Eine häufige Nebenwirkung ist ein kribbelndes oder
            brennendes Gefühl auf der Haut, dass etwa eine Stunde andauern kann.
            <sup>2,6</sup> Diese Nebenwirkung ist meist vorübergehend und tritt
            nach wenigen Tagen der Anwendung in der Regel nicht mehr auf. Starke
            Sonneneinstrahlung auf eingecremte Körperstellen sollte allerdings
            vermieden werde, da die Wirkstoffe die Empfindlichkeit gegenüber
            UV-Strahlen erhöhen.<sup>2</sup>
          </p>
        </Akkordeon>
      </div>

      <div
        className="havas-container dark"
        data-summary="Innerliche (systemische) Therapie">
        <h2 className="w-100">Innerliche (systemische) Therapie</h2>

        <ImageWithText
          imgName="behandlung_img_3"
          altImage="Eine lächelnde junge Frau bei Wind und Wetter in der Natur">
          <p>
            Wenn äußerliche Therapien allein die Neurodermitis-Symptome bei
            Eurem Kind nicht unter Kontrolle bringen und die Beschwerden seine
            Lebensqualität stark beeinträchtigen, wird Euer Dermatologe
            möglicherweise eine systemische (innere) Therapie vorschlagen. Ihr
            könnt auch proaktiv danach fragen, ob eine solche Therapie für Euer
            Kind in Frage kommt. Die Forschung entwickelt sich stetig weiter und
            es gibt seit einigen Jahren moderne systemische Therapien, die
            bereits für Kinder ab 6 Monaten zugelassen sind. Diese haben ein
            etabliertes Sicherheitsprofil.
          </p>
        </ImageWithText>

        <h3>Moderne systemische Therapien</h3>

        <p>
          Zu den modernen Therapiemöglichkeiten, die auch langfristig bei
          Kindern bereits ab 6 Monaten eingesetzt werden können, zählen
          sogenannte Biologika. JAK-Hemmer können dahingegen ab 12 Jahren
          eingesetzt werden (Stand April 2023).
        </p>
        <p>
          Systemische Therapien werden innerlich angewendet. Einige erfolgen
          durch die Einnahme von Tabletten, andere durch subkutane Spritzen
          unter die Haut. Die Wirkstoffe in den Medikamenten verteilen sich dann
          über den Blutkreislauf im ganzen Körper – also im ganzen System. So
          gelangen sie an den Ort, an dem sie wirken sollen.
        </p>
        <p>
          Diese Therapieformen bekämpfen gezielt von innen die Ursache der
          Neurodermitis. Man unterscheidet dabei klassische Behandlungsformen
          von modernen Therapieansätzen.
        </p>

        <h3>Was sind Biologika?</h3>

        <ImageWithText
          className="image-position-80-50"
          imgName="behandlung_img_4"
          altImage="Eine Ärztin spricht mit einem kleinen Jungen im Behandlungsraum.">
          <p>
            Die Behandlung mit Biologika ist bereits ab 6 Monaten möglich.
            Biologika sind eine Gruppe von Medikamenten, die biotechnologisch
            hergestellt werden. Die Therapie mit Biologika ist ein
            verhältnismäßig neuer Ansatz, der die Behandlung vieler Erkrankungen
            revolutioniert hat. So kommen sie auch bereits bei Kindern bei
            verschiedenen chronisch-entzündlichen Erkrankungen zum Einsatz. 2017
            wurde das erste Biologikum zur Therapie der Neurodermitis
            zugelassen.<sup>8</sup>
          </p>
        </ImageWithText>

        <p>
          Biologika wirken gezielt gegen die Entzündungsprozesse, die den
          Neurodermitis-Symptomen zugrunde liegen. So helfen sie Entzündungen
          langfristig einzudämmen, den Juckreiz zu lindern und das Hautbild zu
          verbessern. Sie können entweder allein oder in Kombination mit
          topischen Therapien (Cremes oder Salben) eingesetzt werden.
          <sup>2</sup>
        </p>
        <p>
          Wie alle Medikamente, können auch Biologika unterschiedliche
          Nebenwirkungen haben, wie z. B. Reaktionen an der Einstichstelle,
          erkältungsähnliche Beschwerden, Kopfschmerzen oder
          Bindehautentzündungen.<sup>9</sup>
        </p>

        <TippsBox
          heading="Unser tipp"
          text="Besprecht mit Eurem Dermatologen, welche Medikamente für Kinder zugelassen sind und welche Nebenwirkungen auftreten können."></TippsBox>

        <h3>Eine besondere Art der Biologika - monoklonale Antikörper</h3>

        <p>
          Bei den Biologika, die derzeit (Stand November 2022) bei atopischer
          Dermatitis eingesetzt werden, handelt es sich um sogenannte
          monoklonale Antikörper. Das sind künstlich hergestellte Eiweiße, die
          gezielt in körpereigene Mechanismen eingreifen können und so das
          weitere Fortschreiten krankhafter Vorgänge im Körper verhindern.
        </p>
        <p>
          Die effektive Wirkung von monoklonalen Antikörpern beruht auf ihrer
          Genauigkeit: Sobald sie sich im Körper befinden, begeben sie sich auf
          die Suche nach ihrem Ziel. Das Ziel kann zum Beispiel die Andockstelle
          von Botenstoffen auf einer Zelle sein – der sogenannte Rezeptor – aber
          auch der Botenstoff selbst kann blockiert werden. Findet der
          Antikörper sein Ziel, hält er sich daran fest und blockiert seine
          Funktion. Das bedeutet konkret: Wenn der Antikörper einen wichtigen
          Bestandteil einer Entzündungsreaktion zum Ziel hat und sich daran
          bindet, wird die schädigende, entzündungsfördernde Wirkung eingedämmt.
          Monoklonale Antikörper sind also in der Lage, sehr gezielt in die
          Entzündungsprozesse der Neurodermitis einzugreifen und direkt eine der
          Ursachen einzudämmen. Auch das Risiko von Nebenwirkungen kann aufgrund
          der spezifischen Wirkweise gesenkt werden.<sup>9</sup>
        </p>
        <p>
          Da Antikörper Eiweißstoffe sind und Eiweiße im Magen verdaut werden,
          können sie nicht in Tablettenform eingenommen werden, sondern werden
          unter die Haut gespritzt. Das nennt man eine subkutane Injektion.
          <sup>9</sup> Durch diese Darreichungsform, werden die empfindlichen
          Wirkstoffe nicht im Magen abgebaut, sondern können direkt das
          Immunsystem erreichen.
        </p>

        <ImageWithText
          imgName="behandlung_img_5"
          altImage="Eine Ärztin mit Maske spricht mit einem kleinen Jungen mit Teddaybär">
          <p>
            Sollte Euer Kind ein Biologikum bekommen, erhaltet Ihr von Eurem
            Dermatologen Unterstützung und ein Training für die Injektion des
            Medikaments, um diese, falls gewünscht, auch zu Hause durchführen zu
            können.
          </p>
        </ImageWithText>

        <h3>Klassische systemische Therapien</h3>

        <p>
          Bis vor einigen Jahren wurden bei einer systemischen Therapie vor
          allem Immunsuppressiva wie Kortison (Glukokortikoide) und Ciclosporin
          eingesetzt. Anders als moderne Systemtherapien unterdrücken
          Immunsuppressiva das gesamte Immunsystem.<sup>10</sup> Deshalb sollten
          diese Präparate nur kurz angewendet werden.<sup>11</sup>
        </p>

        <ClickableBox title="Gut zu wissen" header="Ciclosporin Zulassung">
          <p>
            Ciclosporin ist für die Behandlung von Kindern unter 16 Jahren nicht
            zugelassen.
          </p>
        </ClickableBox>

        <p>
          Systemische Glukokortikoide kommen in der Regel nur bei besonders
          schweren Fällen zum Einsatz. Auch Vorsorgeuntersuchungen und
          Kontrollen durch Euren Arzt können bei einer Kortisonbehandlung
          erforderlich sein, um zu überwachen, wie die Behandlung wirkt und um
          sicherzustellen, dass sie keine Probleme verursacht.<sup>2,10</sup>{' '}
          Zudem steigt die Gefahr von Nebenwirkungen mit der Behandlungsdauer
          und der Stärke der Kortisonpräparate.
        </p>

        <ClickableBox title="Gut zu wissen" header="Absetzen von Kortison">
          <p>
            Falls doch einmal eine längere systemische Behandlung mit Kortison
            (Glukokortikoide) notwendig war, dürfen die Präparate nicht abrupt
            abgesetzt werden, sonst droht ein sogenannter Rebound-Effekt, bei
            dem Symptome verstärkt wieder auftreten können. Sprecht Euch deshalb
            immer mit Eurem Dermatologen ab.<sup>10</sup>
          </p>
        </ClickableBox>
      </div>

      <div className="havas-container light" data-summary="Fazit">
        <h2 className="w-100">Fazit</h2>

        <p>
          Mit Neurodermitis zu leben erfordert Kraft, denn der Alltag mit einer
          chronischen Erkrankung kann ganz schön anstrengend sein. Moderne
          Therapien können Euch bei der Krankheitsbewältigung jedoch
          unterstützen und betroffenen Kindern ein weitgehend normales Leben
          ermöglichen. Sprecht mit einem Dermatologen, welche Behandlung für
          Euer Kind geeignet ist und welche ergänzenden Maßnahmen eventuell
          sinnvoll sind. Denkt auch daran, dass sich die Neurodermitis verändern
          kann. Deshalb ist es wichtig regelmäßig zu überprüfen, ob die Therapie
          noch optimal auf die Bedürfnisse Eures Kindes abgestimmt ist.
        </p>

        <ArztefinderTeaser />

        <div className="bottom-text">
          <p>
            <sup>1</sup>{' '}
            https://www.tk.de/resource/blob/2099726/179615dc18521208dce8c3c1992e776a/neurodermitisreport-2021-langfassung-data.pdf.
          </p>
          <p>
            <sup>2</sup> Wollenberg A et al. J Eur Acad Dermatol Venereol 2018;
            32:657–68.
          </p>
          <p>
            <sup>3</sup> National Eczema Society. Emollients factsheet. 2018.
            Abrufbar unter:
            eczema.org/wp-content/uploads/Emollients-Oct-18-1.pdf (Abgerufen im
            September 2022).
          </p>
          <p>
            <sup>4</sup> Eichenfield LF et al. J Am Acad Dermatol 2014;
            71:116–132.
          </p>
          <p>
            <sup>5</sup> NHS. Topical corticosteroids. 2020. Abrufbar unter:
            nhs.uk/conditions/topical-steroids/ (Abgerufen im September 2022).
          </p>
          <p>
            <sup>6</sup> National Eczema Association. Prescription topicals.
            Abrufbar unter: nationaleczema.org/eczema/treatment/topicals/
            (Abgerufen im September 2022).
          </p>
          <p>
            <sup>7</sup>
            https://www.allergieinformationsdienst.de/therapie/medikamente/calcineurin-hemmer.html
          </p>
          <p>
            <sup>8</sup> Apotheken-Umschau.de. Fortschritte in der
            Neurodermitis-Therapie. Abrufbar unter:
            https://www.apotheken-umschau.de/krankheiten-symptome/hautkrankheiten/so-helfen-biologika-bei-neurodermitis-881273.html
            (Abgerufen Oktober 2022).
          </p>
          <p>
            <sup>9</sup> Boguniewicz M et al. Ann Allergy Asthma Immunol 2018;
            120(1):10–22.
          </p>
          <p>
            <sup>10</sup> National Eczema Association. Precription oral. 2020.
            Abrufbar unter:
            nationaleczema.org/eczema/treatment/immunosuppressants/ (Abgerufen
            im Sempember 2022).
          </p>
          <p>
            <sup>11</sup> Gesundheitsinformation.de. Kortison richtig anwenden
            und Nebenwirkungen vermeiden. Abrufbar unter:
            https://www.gesundheitsinformation.de/kortison-richtig-anwenden-und-nebenwirkungen-vermeiden.html
            (Abgerufen Oktober 2022).
          </p>
        </div>
      </div>
    </>
  );
}
export const Head = () => (
  <SEO
    title="Behandlung"
    metaTitle="Neurodermitis behandeln"
    description="Neurodermitis ist eine chronisch-entzündliche Erkrankung mit unterschiedlichen Facetten, die nicht ansteckend ist und sich überwiegend an der Haut zeigt."
    trustKeyWords="Ekzem behandeln, 
    Neurodermitis Therapie, 
    Gesichtspflege Neurodermitis (Gesichtspflege bei Neurodermitis), 
    Atopische Dermatitis Behandlung, 
    Atopisches Ekzem Behandlung, 
    Neurodermitis bei Baby natürlich behandeln (evtl. für unseren Kinder-Bereich vormerken), 
    Neurodermitis am Auge behandeln, 
    Neurodermitis Kopfhaut Behandlung, 
    Quelle: Answerthepublic.com, 
    Neurodermitis Behandlung Erwachsene, 
    Neurodermitis Behandlungsmöglichkeiten, 
    Neurodermitis Behandlungsmethoden, 
    Welcher Arzt behandelt Neurodermitis?, 
    Wie wird Neurodermitis behandelt?, 
    Neurodermitis Behandlung Hausmittel, 
    Neurodermitis Behandlung ohne Cortison, 
    Neurodermitis Behandlung Spritzen "
  />
);
