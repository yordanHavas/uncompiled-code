import React, { useContext } from 'react';
import Hero from '../components/Hero';
import Bulletlist from '../components/Bulletlist';
import CardMedium from '../components/CardMedium';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import { SelbsttestTeaser, ArztefinderTeaser } from '../components/Teaser';
import TippsBox from '../components/TippsBox';
import Akkordeon from '../components/Akkordeon';
import AchtungBox from '../components/AchtungBox';
import { MobileContext } from '../components/Layout';
import ImageWithText from '../components/ImageWithText';
import { Link } from 'gatsby';
import { SEO } from '../components/seo';
import ClickableBox from '../components/ClickableBox';

export default function Gesicht() {
  const isMobile = useContext(MobileContext);

  return (
    <>
      {<DescktopThemenübersicht />}

      <div className="havas-container light">
        <Hero
          headingTextList={['Neurodermitis', 'im Gesicht']}
          coloredWordsArr={['Gesicht']}
          color="yellow"
          imgName="gesicht_img_1"
          altImg="Frau kratzt sich im Gesicht"
          text="Manchen Menschen steht ihre Neurodermitis ganz wörtlich ins Gesicht geschrieben. Sie leiden dann meist nicht nur körperlich, die sichtbaren Ekzeme im Gesicht sind auch eine seelische Belastung. In diesem Artikel kannst Du lesen, wie sich Neurodermitis im Gesicht zeigt, welchen Einfluss die  
          chronisch-entzündliche Erkrankung auf die Hautbarriere im Gesicht hat, was  die typischen Ekzeme im Gesicht auslösen kann und vieles mehr.  "
        />
        <p style={{ marginBottom: `${isMobile ? '' : '20px'}` }}>
          Es juckt unerträglich, die Haut ist rot, trocken, schuppig und rissig.
          Im Gesicht ist Neurodermitis besonders belastend, denn die typischen
          Ekzeme sind nicht nur schmerzhaft, sie sind auch für jeden sichtbar.
          Betroffene leiden daher doppelt: Sie werden von den Symptomen geplagt
          und oft reagiert das Umfeld unsensibel oder sogar abweisend.
        </p>
        <MobileThemenübersicht />
      </div>
      <div
        className="havas-container dark"
        data-summary="Wie äußert sich Neurodermitis im Gesicht?">
        <h2 className="w-100">Wie äußert sich Neurodermitis im Gesicht?</h2>

        <p>
          Neurodermitis ist eine chronische Erkrankung, die sich vor allem durch
          Entzündungen der Haut zeigt. Typischerweise treten die Gesichtsekzeme
          in Schüben auf. Sie betreffen dann meist die Wangen und die Stirn. Es
          können auch Augenpartie und Mundregion betroffen sein.
        </p>

        <p>
          Menschen mit Neurodermitis haben aber nicht nur mit Krankheitsschüben
          zu kämpfen, selbst in schubfreien Zeiten ist ihre Gesichtshaut sehr
          trocken und sensibel. Auch die <Link to="/kopfhaut">Kopfhaut</Link>{' '}
          kann extrem trocken und schuppig sein.
        </p>

        <Bulletlist
          heading="Typische Anzeichen einer Neurodermitis im Gesicht"
          textList={[
            'Gerötete und entzündete Hautstellen, vor allem auf den Wangen und auf der Stirn',
            'Eingerissene Lippen sowie trockene, schuppige Haut um den Mund',
            'Geschwollene Lider und gerötete, trockene Haut um die Augen',
            'Trockene und schuppige Haut hinter den Ohren und eingerissene Ohrläppchen',
            'Verdickte und grobe Haut (Lichenifikation)',
            'Trockene und schuppende Kopfhaut',
          ]}
        />
        <ClickableBox title="Gut zu wissen">
          <p>
            Die Symptome der Neurodermitis können sich bei Betroffenen in ihrer
            Dauer und Schwere stark unterscheiden. Falls Du typische Symptome
            einer Neurodermitis im Gesicht bemerkst, solltest Du einen
            Dermatologen aufsuchen, um eine passende Behandlung zu bekommen.
          </p>
        </ClickableBox>

        <p>
          Ein besonders belastendes Symptom der Neurodermitis ist der starke
          Juckreiz. Kratzen hilft dagegen allerdings wenig, dadurch wird die
          Haut nur weiter geschädigt und der Juckreiz verstärkt sich. Statt zu
          kratzen kannst Du Deine Haut kneten, klopfen oder streicheln. Wenn das
          nichts nutzt, hilft Dir eventuell Kälte. Befeuchte ein Tuch (oder
          einen Waschlappen) mit kaltem Wasser und leg es auf Dein Gesicht. Der
          Kältereiz kann den Juckreiz überlagern und so für etwas Linderung
          sorgen.
        </p>

        <a href="/lernplattform/elearning-9">
          Wie Du Dich vom Juckreiz ablenken kannst, kannst Du im Lernmodul
          „Ablenkungstechniken bei Juckreiz“ nachlesen.
        </a>
      </div>

      <div
        className="havas-container light"
        data-summary="Welchen Einfluss hat Neurodermitis auf die Haut im Gesicht?">
        <h2 className="w-100">
          Welchen Einfluss hat Neurodermitis auf die Haut im Gesicht?
        </h2>

        <p>
          Bei Neurodermitis ist der Aufbau der Haut verändert. Die Hautbarriere
          besteht aus Hornzellen, die übereinander-geschichtet sind. Diese
          Zellen werden normalerweise durch Hornfette zusammengehalten. Bei
          Menschen mit Neurodermitis werden nicht ausreichend Fettstoffe
          produziert. Der Haut mangelt es an Feuchthaltefaktoren und sie kann
          schlecht Wasser binden. Der Verbund der Hautzellen wird durchlässig
          und die Haut ist sehr trocken und empfindlich.
        </p>

        <div className="card-container" style={{ justifyContent: 'center' }}>
          <CardMedium
            imageStyle={{ maxWidth: 'none' }}
            image="MOD1"
            title="Intakte Hautbarriere"
            altImg="Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis"
            text="Die intakte Haut stellt eine natürliche und geschlossene Barriere zwischen der Innen- und Außenwelt dar. Für Fremdstoffe ist es fast unmöglich, sie im gesunden Zustand zu überwinden."
          />

          <CardMedium
            image="MOD2"
            title="Beeinträchtigte Hautbarriere"
            altImg="Darstellung der Hautbarriere im Falle von intakter Haut und bei Neurodermitis"
            text="Die Hautbarriere bei Neurodermitis ist beeinträchtigt. Ihr fehlen wichtige Bestandteile, die zur Aufrechterhaltung der Schutzfunktion nötig sind (z.B. Feuchtigkeit). Fremdstoffe können eindringen und weitere Entzündungen hervorrufen."
          />
        </div>
      </div>

      <div
        className="havas-container dark"
        data-summary="Wie kommt es zu Neurodermitis im Gesicht?">
        <h2 className="w-100">Wie kommt es zu Neurodermitis im Gesicht?</h2>

        <p>
          Neurodermitis wird auch atopische Dermatitis oder atopisches Ekzem
          genannt. Unter einer Atopie versteht man die Neigung des Körpers zu
          einer übersteigerten Reaktion des Immunsystems auf normalerweise
          harmlose Substanzen oder Umweltreize.
        </p>

        <p>
          Was genau passieren muss, damit aus der Neigung die
          chronisch-entzündliche Erkrankung wird, ist bis heute nicht
          vollständig geklärt. Man weiß jedoch, dass meist verschiedene Faktoren
          zusammenkommen müssen, damit Neurodermitis ausbricht.
        </p>

        <Bulletlist
          circleClassName="biggerC"
          textList={[
            'Erbliche Vorbelastungen',
            'Eine gestörte Barrierefunktion der Haut',
            'Eine Überempfindlichkeit des Immunsystems',
            'Verschiedene Umweltfaktoren',
          ]}>
          <Link to="/ursachen" style={{ marginBottom: '60px' }}>
            Detaillierte Informationen zu den Ursachen einer Neurodermitis
            kannst Du hier nachlesen.
          </Link>
        </Bulletlist>
        <SelbsttestTeaser />
      </div>

      <div
        className="havas-container light"
        data-summary="Was löst Neurodermitis im Gesicht aus?">
        <h2 className="w-100">Was löst Neurodermitis im Gesicht aus?</h2>

        <p>
          Wie unsere Hände ist das Gesicht vielen Umwelteinflüssen ausgesetzt:
          Kälte, Hitze, Sonne, Wind, Regen, Pollen … Jeden Tag muss Deine
          Gesichtshaut unterschiedliche Herausforderungen meistern. Zudem
          greifen wir uns häufig ganz unbewusst ins Gesicht, was die ohnehin
          gereizte Haut weiter belasten kann. Auch Make-up oder dekorative
          Kosmetik, wie Wimperntusche, Lidschatten, Lippenstift und Co., können
          Deine Gesichtshaut reizen.
        </p>

        <ImageWithText
          imgName="gesicht_img_2"
          altImage="Eine lächelnde junge Frau bei Wind und Wetter in der Natur">
          <p>
            Einige dieser Einflüsse können das Krankheitsgeschehen negativ
            beeinflussen. Im Fachjargon spricht man dann von
            Provokationsfaktoren oder Triggern. Auf welche Trigger jemand
            reagiert, ist von Mensch zu Mensch verschieden.
          </p>
        </ImageWithText>

        <Bulletlist
          className="violet"
          heading="Typische Trigger für eine Neurodermitis im Gesicht:"
          textList={[
            'Falsche oder übermäßige Reinigung des Gesichts',
            'Reinigungs- und Pflegeprodukte sowie dekorative Kosmetik mit Duft- und Konservierungsstoffen',
            'Hautreizende Reinigungsprodukte wie Peelings',
            'Zigarettenrauch und Umweltschadstoffe, z. B. Abgase',
            'Klima, z. B. geringe Luftfeuchtigkeit in beheizten Räumen',
            'Stress, z. B. Lärm, Leistungsdruck',
            'Allergene, z. B. Pflanzenpollen, Tierhaare oder einige Nahrungsmittel',
          ]}></Bulletlist>
      </div>

      <div
        className="havas-container dark"
        data-summary="Wie behandle ich Neurodermitis im Gesicht?">
        <h2 className="w-100">Wie behandle ich Neurodermitis im Gesicht?</h2>

        <p>
          Neurodermitis im Gesicht tritt meist in Schüben auf und es wird immer
          wieder Phasen geben, in denen Du kaum Beschwerden hast. Allerdings
          gilt auch dann: Pflege Deine Haut konsequent und bleib an
          langfristigen Therapiemaßnahmen dran.
        </p>

        <h3 style={{ textAlign: `${isMobile ? 'left' : 'center'}` }}>
          Die Basispflege – das solltest Du bei der täglichen Gesichtspflege
          beachten
        </h3>

        <p>
          Das A und O der Behandlung der Neurodermitis ist die Basistherapie.
          Die tägliche Pflege Deiner Gesichtshaut sollte ein fester Bestandteil
          Deines Alltags sein.
        </p>

        <p>
          Bei der Wahl der richtigen Basispflege gilt: Während eines akuten
          Schubs braucht Deine Haut Produkte auf Wasserbasis, die Feuchtigkeit
          spenden. Ansonsten kannst Du fettreichere Cremes nutzen, vor allem im
          Winter, wenn Deine Haut besonders beansprucht wird. Aber Achtung: Im
          Sommer solltest Du Deine Haut nicht mit zu fetten Cremes „abdichten“,
          damit der Schweiß abtransportiert werden kann und nicht zu Juckreiz
          führt.
        </p>

        <Link to="/ueberblick">
          Weitere Informationen zur Basistherapie findest Du hier.
        </Link>

        <TippsBox
          heading="Unser tipp"
          text="Bewahre Pflegeprodukte für Neurodermitis im Kühlschrank auf – so kühlen sie beim Auftragen die juckende Haut."></TippsBox>

        <Akkordeon heading="Lippenpflege" open={true}>
          <p style={{ lineHeight: '1.44' }}>
            Rissige Mundwinkel und trockene Lippen kannst Du mit
            Lippenpflegestiften, -balsams oder
            <br /> -cremes behandeln, die für Neurodermitis-Haut geeignet sind.
            Diese spenden nicht nur Fett und Feuchtigkeit, sondern enthalten oft
            auch entzündungshemmende Inhaltsstoffe.
          </p>
        </Akkordeon>

        <Akkordeon heading="Augenpflege">
          <p style={{ lineHeight: '1.44' }}>
            Gegen Trockenheit und Juckreiz helfen Gele oder Tränenersatzmittel
            ohne Konservierungsstoffe. Sie spenden Feuchtigkeit und können
            manchmal sogar Pollen auswaschen.
          </p>
        </Akkordeon>

        <AchtungBox
          heading="Naturkosmetika"
          iconName="exclamation_icon"
          text="Bei Naturkosmetika solltest Du vorsichtig sein. Laut Deutschem Allergie- und Asthmabund (DAAB) bergen einige Inhaltsstoffe, die in Naturkosmetik eingesetzt werden, hohes Allergiepotenzial. Dazu gehören: ätherische Öle, Wollwachsalkohole und Wollwachs (Lanolin) und Propolis (Bienenwachs). Auf Naturkosmetik musst Du deshalb nicht verzichten, aber Du solltest die Inhaltsstoffe genau lesen und die Pflegeprodukte an einer kleinen Körperstelle testen, bevor Du sie großzügig einsetzt."
        />

        <h2>Medikamentöse Therapien</h2>

        <ImageWithText
          imageOnRight={true}
          imgName="gesicht_img_3"
          altImage="Apotheker öffnet Schublade, in der sich Medikamente befinden.">
          <p>
            Für Neurodermitis gibt es nicht diese eine Therapie, die für alle
            Betroffenen gleichermaßen angewendet werden kann. Jeder muss
            individuell behandelt werden. Sprich mit Deinem Dermatologen und
            schildere Deine Beschwerden möglichst genau. Es ist außerdem
            hilfreich, die Symptome und Hautzustände regelmäßig zu
            dokumentieren. Nur wenn Dein Arzt Deine Situation kennt, könnt Ihr
            gemeinsam ein passendes Therapiekonzept für Dich finden.
          </p>
        </ImageWithText>

        <h3>Topische Therapiemaßnahmen</h3>

        <p>
          Äußerliche Anwendungen auf der Hautoberfläche werden als topische
          Therapien bezeichnet. Bei einem Schub, wenn die Haut entzündet ist und
          juckt, wird Dir Dein Dermatologe in der Regel wirkstoffhaltige Salben
          oder Cremes verschreiben. Diese solltest Du nur auf die entzündeten
          Stellen auftragen und nicht dauerhaft anwenden.
        </p>

        <h3>Systemische Therapiemaßnahmen</h3>

        <p>
          Wenn topische (äußerliche) Präparate nicht ausreichen, können
          systemische Medikamente zum Einsatz kommen, die gezielt von innen
          wirken. Sie können als Tablette eingenommen oder unter die Haut
          gespritzt werden. Systemische Medikamente gelangen über das Blutsystem
          in den Körper und bekämpfen somit die Ursache der Neurodermitis.
          Systemische Therapien werden in der Regel langfristig angewendet.
        </p>

        <Link to="/innerliche-therapien">
          Mehr zu den Behandlungsoptionen bei Neurodermitis findest Du hier.
        </Link>

        <p>
          Für einige Menschen mit Neurodermitis kann eine Photo- oder
          UV-Licht-Therapie eine sinnvolle Ergänzung sein.
        </p>

        <ArztefinderTeaser />
        <ClickableBox
          title="Gut zu wissen"
          header="Was hilft schnell bei Neurodermitis? – Hausmittel aus dem Kühlschrank">
          <p>
            Joghurt, Quark oder Milch sind gute Hausmittel für Neurodermitis im
            Gesicht. Die darin enthaltenen Milchsäuren spenden der Haut
            Feuchtigkeit und fetten sie nachhaltig. Eine kalte Gesichtsmaske aus
            Joghurt oder Quark lindert zudem den Juckreiz und wirkt
            entzündungshemmend.
          </p>
        </ClickableBox>
      </div>

      <div
        className="havas-container light"
        data-summary="Wie kann ich Schübe vermeiden?">
        <h2 className="w-100">Wie kann ich Schübe vermeiden?</h2>

        <p>
          Um Schüben vorbeugen, ist es wichtig, Deine Trigger gezielt zu
          vermeiden. Dazu musst Du sie aber erst mal kennen. Ein Tagebuch kann
          dabei helfen. Verzichte nicht wahllos auf alles, von dem Du gehört
          oder gelesen hast, dass es einen Schub triggern könnte. Du solltest
          Deine Neurodermitis kennenlernen und im Auge behalten, denn Trigger
          können sich auch ändern.
        </p>

        <p>
          Mache Deine Erkrankung dabei nicht zu Deinem Lebensmittelpunkt. Richte
          einen positiven Blick auf Dich und Deine Haut. Sprich auch mit Deinem
          Dermatologen, denn es gibt auch Tests, die zeigen können, ob Du auf
          bestimmte Stoffe allergisch reagierst.{' '}
        </p>

        <Link to="/broschueren-und-downloads">
          Eine Vorlage für ein Tagebuch kannst Du übrigens hier herunterladen.
        </Link>

        <p>
          Du solltest zudem versuchen Stress zu vermeiden, denn der zählt zu den
          häufigsten Auslösern für Neurodermitis-Schübe. Stress lässt sich aber
          nicht immer umgehen. Deshalb ist es wichtig, Dir in Deinem Alltag
          Ruhepausen zu gönnen. So kannst Du einen Ausgleich schaffen und die
          Auswirkungen von Stress verringern.
        </p>
      </div>

      <div
        className="havas-container dark"
        data-summary="Neurodermitis im Gesicht ist auch belastend für die Psyche">
        <h2>Neurodermitis im Gesicht ist auch belastend für die Psyche</h2>

        <ImageWithText
          imgName="gesicht_img_4"
          altImage="Frau schlägt Hände vors Gesicht">
          <p>
            Beim ersten Eindruck spielt das Gesicht unseres Gegenübers meist
            eine entscheidende Rolle. Es geschieht ganz unbewusst und wir
            entscheiden sekundenschnell, ob wir eine Person mögen oder nicht.
            Deshalb leiden Menschen mit Neurodermitis im Gesicht meist doppelt.
            Sie werden nicht nur von den Symptomen ihrer Erkrankung geplagt, oft
            kommt eine soziale Ausgrenzung hinzu.{' '}
          </p>
        </ImageWithText>

        <p>
          Sprich mit Deinem Arzt, wenn Du über einen längeren Zeitraum
          niedergeschlagen bist oder Dich isoliert fühlst. Eventuell können
          psychotherapeutische Verfahren eine sinnvolle Ergänzung Deiner
          Therapie sein. Im Lernmodul{' '}
          <a href="/lernplattform/elearning-11">„Positives Denken lernen“</a>{' '}
          haben wir Tipps für Dich zusammengestellt, die Dir helfen können, mit
          mehr Freude durch Dein Leben zu gehen.{' '}
        </p>

        <SelbsttestTeaser />
      </div>

      <div
        className="havas-container light"
        data-summary="Wie reinige ich mein Gesicht bei Neurodermitis?">
        <h2 className="w-100">
          Wie reinige ich mein Gesicht bei Neurodermitis?
        </h2>

        <p>
          Für die schonende Reinigung Deines Gesichts reicht klares Wasser meist
          aus. Dessen Temperatur sollte kühl bis lauwarm sein. Zu kaltes oder
          heißes Wasser kann Deine Haut nämlich zusätzlich austrocknen. Falls Du
          Reinigungsprodukte – wie eine Reinigungsmilch benutzt, um zum Beispiel
          Make-up zu entfernen, sollten diese für Neurodermitis-Haut geeignet
          sein und weder Duft- noch Konservierungsstoffe enthalten. Im besten
          Fall haben Deine Reinigungsprodukte sogar hautberuhigende
          Eigenschaften.
        </p>

        <p>
          Peelings und Gesichtsmasken sind bei Neurodermitis nicht zu empfehlen.
          Sie strapazieren die Haut nur zusätzlich. Auch auf Hilfsmittel zur
          mechanischen Reinigung wie Schwämme, Tücher oder Waschlappen solltest
          Du verzichten.
        </p>
      </div>

      <div
        className="havas-container dark"
        data-summary="Neurodermitis und Make-up">
        <h2 className="w-100">Neurodermitis und Make-up</h2>

        <ImageWithText
          imageOnRight={true}
          imgName="gesicht_img_5"
          altImage="Frau trägt Make-up im Gesicht auf">
          <p>
            Darf ich mich schminken oder doch lieber nicht? Diese Frage stellen
            sich sicher viele Menschen mit Neurodermitis. Generell musst Du
            nicht auf Make-up verzichten. Allerdings solltest Du darauf achten,
            dass es Deiner Haut gerade gut geht und Du keinen akuten Schub hast.
            Ist Deine Haut gerötet, trocken, rissig oder nässt sie, solltest Du
            darauf verzichten.
          </p>
        </ImageWithText>

        <TippsBox
          heading="unser tipp"
          text="Creme Dein Gesicht immer mit Deiner Basispflege ein, bevor Du Make-up aufträgst."></TippsBox>

        <p>
          Wenn Du dekorative Kosmetik wie Foundation, Puder, Lidschatten,
          Lippenstift und Co. einsetzen willst, solltest Du Produkte nutzen, die
          für sensible Haut geeignet und frei von Duft- und Konservierunsstoffen
          sind. Je weniger Inhaltsstoffe Kosmetikprodukte haben, desto größer
          ist die Chance, dass Du sie auch gut verträgst.{' '}
        </p>

        <ImageWithText
          imgName="gesicht_img_6"
          altImage="Nahaufnahme einer jungen Frau, die Lippenpflege benutzt"
          imageOnRight={true}>
          <p>
            Besondere Vorsicht ist bei Lidschatten und Wimperntusche geboten.
            Sie enthalten oft hautreizende Inhaltsstoffe. Auch andere Produkte
            fürs Augen-Make-up solltest Du zunächst an einer anderen
            Körperstelle auf Verträglichkeit testen, denn die Haut an den Augen
            ist besonders dünn und empfindlich.{' '}
          </p>

          <p>
            Das Gleiche gilt für Deine Lippen. Auch Lippenstifte können Stoffe
            enthalten, die Deine Haut reizen. Deshalb kann es sinnvoll sein,
            erst eine fettende Lippenpflege und anschließend den Lippenstift
            aufzutragen.
          </p>
        </ImageWithText>

        <TippsBox
          heading="unser tipp"
          text="Du kannst die Neurodermitis zwar nicht wegschminken, aber manchmal kann Make-up Dir helfen, Dich attraktiver zu fühlen."></TippsBox>

        <p>
          Falls Du dekorative Kosmetikprodukte verwendest, solltest Du Deine
          Haut am Abend gründlich reinigen und danach mit Deiner Basispflege
          eincremen. Wenn eine Reinigung mit Wasser nicht ausreicht, um das
          Make-up zu entfernen, solltest Du Reinigungsprodukte nutzen, die für
          empfindliche Haut geeignet sind und keine reizenden Inhaltsstoffe
          enthalten.
        </p>
      </div>

      <div
        className="havas-container light"
        data-summary="Neurodermitis und Rasur">
        <h2 className="w-100">Neurodermitis und Rasur</h2>

        <ImageWithText
          imageOnRight={true}
          imgName="gesicht_img_7"
          altImage="Mann rasiert sich im Gesicht">
          <p>
            Männer mit Neurodermitis im Gesicht stehen vor einer besonderen
            Herausforderung. Wer nicht auf einen Bart steht, wird sich rasieren
            wollen, und selbst für Bartträger ist die Frage nach der richtigen
            Bartpflege nicht einfach zu beantworten.
          </p>
        </ImageWithText>

        <Bulletlist
          heading="Das sollest Du beim Rasieren beachten:"
          textList={[
            'Eine Trockenrasur ist in der Regel schonender und reizt die Haut weniger.',
            'Wer unbedingt nass rasieren will, sollte eine Rasiercreme verwenden, die keine Hautirritationen hervorruft.',
            'Außerdem solltest Du eine scharfe Rasierklinge verwenden. Mit stumpfer Klinge brauchst Du mehr Druck und der reizt die Haut.',
            'Egal ob trocken oder nass, nach der Rasur solltest Du Deine Haut mit einer rückfettenden Gesichtscreme pflegen.',
          ]}
        />

        <p>
          Produkte zur Bartpflege sollten, genau wie alle anderen
          Kosmetikprodukte, frei von Duft- und Konservierungsstoffen und für
          Menschen mit Neurodermitis geeignet sein. Einen Erfahrungsbericht zum
          Thema Bartpflege von Patrick kannst Du{' '}
          <a href="/blog">in unserem Blog nachlesen</a>.
        </p>
      </div>

      <div className="havas-container dark" data-summary="Fazit">
        <h2 className="w-100">Fazit</h2>

        <p>
          Für Menschen mit Neurodermitis ist der Blick in den Spiegel manchmal
          eine Herausforderung. Sie stehen ihrer Neurodermitis dann von
          Angesicht zu Angesicht gegenüber und alles, was sie sehen, ist
          gerötete, schuppige und verdickte Haut in ihrem Gesicht. Denk daran:
          Du bleibst Du, egal wie fremd Dir Dein Spiegelbild vielleicht manchmal
          ist. Wende Dich Dir mit Selbstliebe und Achtsamkeit zu.
        </p>

        <p>
          Frust und Stress verstärken den Druck und sind keine guten Begleiter
          für Menschen mit einer chronischen Erkrankung. Es ist sicher nicht
          immer leicht, aber es lohnt sich immer, den Blick auf das Positive zu
          lenken. Es gibt heutzutage viele Möglichkeiten, die Dich im Umgang mit
          Deiner Erkrankung unterstützen können. Werde aktiv und lass Dich
          beraten.{' '}
        </p>

        <ArztefinderTeaser />
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Gesicht"
    metaTitle="Neurodermitis im Gesicht"
    description="Manchen Menschen steht ihre Neurodermitis ins Gesicht geschrieben. Sie leiden dann meist körperlich und seelisch. Erfahre mehr über Neurodermitis im Gesicht."
    trustKeyWords="Neurodermitis, Gesicht, Juckreiz, Trockene Haut, Gesichtshaut, Symptome, Hautpflege, Make-up, Trockene, Ekzem, Behandlung, Schüben"
  />
);
