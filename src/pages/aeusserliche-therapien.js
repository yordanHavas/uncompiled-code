import React from 'react';
import { useContext } from 'react';
import Hero from '../components/Hero';
import { MobileContext } from '../components/Layout';
import {
  DescktopThemenübersicht,
  MobileThemenübersicht,
} from '../components/Themenübersicht';
import { ArztefinderTeaser, SelbsttestTeaser } from '../components/Teaser';
import ClickableBox from '../components/ClickableBox';
import ImageWithText from '../components/ImageWithText';
import Bulletlist from '../components/Bulletlist';
import { Link } from 'gatsby';
import TippsBox from '../components/TippsBox';
import Button from '../components/Button';
import Akkordeon from '../components/Akkordeon';
import { SEO } from '../components/seo';

export default function äußerlicheTherapien() {
  const isMobile = useContext(MobileContext);

  return (
    <>
      <DescktopThemenübersicht />
      <div className="havas-container light">
        <Hero
          className="l-height-desktop"
          headingTextList={[
            'Neurodermitis-',
            'Behandlung –',
            'ÄUßERLICHE THERAPIEN',
          ]}
          coloredWordsArr={['ÄUßERLICHE']}
          imgName="ausserliche-therapien_img_1"
          altImg="Frau, die sich die Hände eincremt"
          color="yellow"
          text="Neurodermitis ist zwar bisher nicht heilbar, aber die Symptome der Erkrankung können durch unterschiedliche Therapiemaßnahmen meist unter Kontrolle gebracht werden. Mit einer maßgeschneiderten Behandlung und etwas Unterstützung kannst Du in der Regel ein weitgehend normales Leben führen. Die Symptome akuter Schübe werden dabei meist mit äußerlichen (topischen) Therapien behandelt."
        />
        <MobileThemenübersicht />
        <p>
          Neurodermitis ist nicht gleich Neurodermitis. Das Krankheitsbild
          unterscheidet sich von Mensch zu Mensch, sowohl in der Ausprägung der
          Symptome als auch in der subjektiven Beeinträchtigung. Deshalb ist es
          zunächst wichtig, dass die Schwere der Erkrankung festgestellt wird.
          {!isMobile && (
            <>
              <br />
              <br />
            </>
          )}
          Auf dieser Basis wird die Therapie individuell an Deine Bedürfnisse
          angepasst. Heutzutage steht eine Vielzahl von Arzneimitteln zur
          Verfügung, die entweder äußerlich (topisch) oder innerlich
          (systemisch) angewandt werden. In diesem Artikel wollen wir uns die
          äußerlichen Therapien anschauen, die vor allem zur Behandlung eines
          akuten Schubs eingesetzt werden.
        </p>
        <SelbsttestTeaser />
      </div>

      <div
        className="havas-container dark"
        data-summary="Die Basistherapie – Wie pflege ich meine Haut richtig?">
        <h2>Die Basistherapie – Wie pflege ich meine Haut richtig?</h2>
        <p>
          Unter dem Begriff Basistherapie versteht man die tägliche Hautpflege,
          die aus einer topischen (äußerlichen), nichtmedikamentösen Behandlung
          Deiner Haut besteht. Die entsprechenden Präparate werden direkt auf
          die Haut aufgetragen, um diese mit Feuchtigkeit zu versorgen.
        </p>
        <p>
          Dazu werden in der Regel Emollienzien eingesetzt. Das sind
          feuchtigkeitsspendende Substanzen wie Salben, Cremes, Lotionen oder
          Badezusätze. Sie schützen die Haut vor dem Austrocknen, indem sie eine
          Barriere bilden, die die Feuchtigkeit zurückhält.<sup>1,2</sup> So
          lindern sie juckende, trockene Haut und verringern das
          Infektionsrisiko.<sup>2</sup> Sie können für alle Menschen mit
          Neurodermitis hilfreich sein, unabhängig vom Schweregrad der
          Erkrankung und auch begleitend zu anderen Behandlungsmaßnahmen.
          <sup>3,4</sup>
        </p>
        <ClickableBox title="Gut zu wissen" header="Konsequente Pflege">
          <p>
            Durch die konsequente Pflege Deiner Haut kannst Du die
            beschwerdefreien Phasen möglicherweise verlängern und
            Krankheitsschübe hinauszögern, abmildern oder sogar verhindern.
          </p>
        </ClickableBox>
        <p>
          Emollienzien können die Haut konstant mit Feuchtigkeit versorgen, wenn
          sie mindestens zweimal täglich aufgetragen werden. Sie wirken am
          besten auf nicht entzündeter Haut. Daher ist es wichtig, dass Du Deine
          Haut auch dann eincremst und pflegst, wenn Du gerade keine sichtbare
          Entzündung hast.<sup>1,4</sup>
        </p>
        <p>
          Emollienzien behandeln allerdings lediglich Symptome der Neurodermitis
          (z. B. trockene, schuppige, juckende Haut), nicht aber die zugrunde
          liegende Entzündung.<sup>1,2</sup>
        </p>
        <h3>Welche äußeren Faktoren haben Einfluss auf Neurodermitis?</h3>
        <ImageWithText
          imgName="ausserliche-therapien_img_2"
          altImage="Frau schlägt Hände vors Gesicht">
          <p>
            Es gibt einige Umweltfaktoren oder äußere Einflüsse, die sich
            negativ auf den Verlauf der Neurodermitis auswirken können. Einige
            Ärzte zählen die Vermeidung dieser sogenannten Trigger oder
            Provokationsfaktoren auch zur Basistherapie.
          </p>
        </ImageWithText>

        <Bulletlist
          firstColumnSize={3}
          heading="Häufige Trigger sind:"
          textList={[
            'Falsche oder übermäßige Hautreinigung (z. B. häufiges Duschen bzw. Gebrauch von Seifen, Pflegeprodukten mit Duft- und Konservierungsstoffen)',
            'Zigarettenrauch und Umweltschadstoffe (z. B. Abgase)',
            'Kratzende Kleidung (z. B. aus Wollfasern oder Synthetikstoffen)',
            'Klima (z. B. geringe Luftfeuchtigkeit in Räumen durch Heizungsluft)',
            'Stress (z. B. Lärm, Leistungsdruck)',
            'Allergene (z. B. Pflanzenpollen, Tierhaare, bestimmte Nahrungsmittel)',
            'Infektionen (z. B. durch Viren, Bakterien oder Pilze)',
          ]}
        />

        <p>
          Wenn Du Deine Trigger vermeiden möchtest, musst Du sie erst mal
          kennen. Ein Tagebuch kann dabei helfen.{' '}
          <Link to="/broschueren-und-downloads">
            Eine Vorlage dafür kannst Du hier herunterladen.
          </Link>
        </p>
        <Link to="/ursachen">
          Weitere Infos zu den Ursachen der Neurodermitis kannst Du hier
          nachlesen.
        </Link>

        <SelbsttestTeaser
          style={
            isMobile ? { marginTop: -40, zIndex: 20 } : { marginBottom: 0 }
          }
        />
      </div>

      <div
        className="havas-container light"
        data-summary="Was tun bei akutem Neurodermitis-Schub?">
        <h2>Was tun bei akutem Neurodermitis-Schub?</h2>
        <ImageWithText
          imgName="ausserliche-therapien_img_3"
          altImage="Eine Dose Creme von oben">
          <p>
            Bei einem akuten Schub mit Entzündungen wird Dir Dein Dermatologe
            wirkstoffhaltige Präparate verschreiben. Das sind meist Cremes,
            Salben oder Gele, die direkt auf die betroffenen Stellen aufgetragen
            werden. Auf der <Link to="/kopfhaut">Kopfhaut</Link> können auch
            wirkstoffhaltige Shampoos zum Einsatz kommen und bei Beschwerden an
            den Augen werden manchmal Augentropfen eingesetzt.
          </p>
        </ImageWithText>
        <Bulletlist
          className="short-bulletlist"
          heading="Dabei kommen in der Regel 2 Wirkstoffgruppen in Frage:"
          textList={['Glukokortikoide (Kortison)', 'Calcineurin-Hemmer']}
        />
      </div>

      <div
        style={isMobile ? { marginTop: -300 } : {}}
        className="havas-container dark"
        data-summary="Topische Glukokortikoide (Kortison)">
        <h2>Topische Glukokortikoide (Kortison)</h2>
        <p>
          Topische Glukokortikoide sind eine Gruppe von Medikamenten, die in
          verschiedenen Formen, z. B. als Cremes und Salben, erhältlich sind und
          direkt auf die Haut aufgetragen werden, um die mit Neurodermitis
          verbundenen Entzündungen und den Juckreiz zu verringern.<sup>5</sup>{' '}
          Sie sind besser bekannt als Kortison bzw. Cortison und werden oft als
          eine der ersten Behandlungsmaßnahmen verschrieben, wenn eine
          Neurodermitis neu diagnostiziert wird.<sup>1,6</sup> Es gibt topische
          Glukokortikoide (Kortison) in unterschiedlichen Stärken, sodass die
          Behandlung auf die Schwere Deiner Neurodermitis zugeschnitten werden
          kann.<sup>6</sup>
        </p>
        <p>
          Schwach wirksame Kortisonsalben mit dem Wirkstoff Hydrocortison (bzw.
          Hydrocortisonacetat) sind sogar rezeptfrei in der Apotheke erhältlich.
          Sie können bei leichten Ekzemen oder allergischen Hautreaktionen
          eingesetzt werden. Zur Behandlung eines Neurodermitis-Schubs reichen
          sie in der Regel nicht aus.
        </p>
        <TippsBox
          heading="Unser tipp"
          text="Sprich mit einem Dermatologen, welche Behandlung für Dich geeignet ist."
        />
        <Bulletlist
          bigHeading="Wichtige Fakten im Überblick:"
          heading={['Topische Glukokortikoide', <sup>6</sup>, ':']}
          textList={[
            'zählen zu den entzündungshemmenden Behandlungen für Erwachsene und Kinder mit Neurodermitis. Sie werden seit 60 Jahren eingesetzt, um entzündliche Hauterkrankungen und Juckreiz zu lindern.',
            'werden auf die Hautstellen aufgetragen, die von Neurodermitis betroffen sind, und helfen, die Entzündung in diesen Bereichen zu verringern.',
            'werden nicht für die Langzeitanwendung und die Anwendung auf sensiblen Hautarealen empfohlen; bei kurzzeitiger richtiger Anwendung sind Nebenwirkungen selten.',
          ]}
        />
        <h3>
          Kortison – so schlecht wie sein Ruf? – Nebenwirkungen topischer
          Glukokortikoide
        </h3>
        <ImageWithText
          imgName="ausserliche-therapien_img_4"
          altImage="Spiegelbild einer jungen Frau, die sich die über die Stirn streicht">
          <p>
            Wenn der Begriff Kortison fällt, bekommen viele Menschen Angst, denn
            sie verbinden damit zahlreiche Nebenwirkungen wie Gewichtszunahme
            oder Knochenbrüche durch Abnahme der Knochendichte. Aber Kortison
            ist besser als sein Ruf. Vor allem wenn Glukokortikoide äußerlich
            und nur über kurze Zeit angewendet werden, ist die Gefahr von
            Nebenwirkungen gering.
          </p>
          <p>
            Die häufigste Nebenwirkung ist ein leichtes Brennen beim Auftragen,
            das aber bei den meisten Menschen weniger wird, je mehr die Haut
            sich an die Behandlung gewöhnt.<sup>5</sup> Mögliche Nebenwirkungen,
            die bei der Anwendung von topischen Glukokortikoiden (Kortison)
            ebenfalls auftreten können, sind:
          </p>
        </ImageWithText>
        <Bulletlist
          style={isMobile ? { paddingTop: 20, paddingBottom: 20 } : {}}
          textList={[
            'Hautverdünnung (atrophische Haut)',
            'Haarwurzelentzündungen',
            'Pigmentstörungen auf der Haut',
            'Vermehrte Bildung von Dehnungsstreifen',
          ]}
        />
        <p>
          Um Nebenwirkungen zu vermeiden, sollte Kortison nicht über einen
          längeren Zeitraum eingesetzt werden, auf empfindlichen Körperbereichen
          sogar nicht länger als einige Tage. Zudem muss die Stärke des
          Präparates an die Dicke und Empfindlichkeit der betroffenen Hautstelle
          angepasst werden. An dünnen, empfindlichen Hautstellen wirkt Kortison
          nämlich stärker, insbesondere im Gesicht, in Gelenkbeugen, Achseln und
          an den Oberarmen sowie an den Augenlidern und Geschlechtsorganen.
        </p>
        <p>
          Du solltest nicht zu viel Creme oder Salbe auftragen. Verwende
          topische Glukokortikoide immer in Absprache mit Deinem Dermatologen;
          befolge seine Angaben.
        </p>
        <ArztefinderTeaser />
        <ClickableBox
          title="Gut zu wissen"
          header="Verschiedene Hautbereiche reagieren unterschiedlich">
          <p>
            Nicht alle Regionen unserer Haut sind Glukokortikoiden (Kortison)
            gegenüber gleichermaßen empfindlich. Wer also eine Kortisonsalbe
            verschrieben bekommen hat, zum Beispiel für die Hände oder den
            Bauch, sollte nicht bedenkenlos dasselbe Präparat verwenden, wenn
            Symptome zum Beispiel im Gesicht auftreten.
          </p>
        </ClickableBox>
        <p>
          Bei der innerlichen Anwendung von Kortison können auch wesentlich
          stärkere Nebenwirkungen auftreten als bei Cremes und Salben. Dazu
          gehören zum Beispiel Gewichtszunahme, Bluthochdruck, Schlafstörungen,
          Akne, Osteoporose, Morbus Cushing, grauer und grüner Star oder
          Magengeschwür.
        </p>
        <Link to="/innerliche-therapien">
          Mehr dazu kannst Du auf der Seite der innerlichen Therapien nachlesen.
        </Link>
      </div>
      <div
        className="havas-container light"
        data-summary="Topische Calcineurin-Hemmer">
        <h2>Topische Calcineurin-Hemmer</h2>

        <ImageWithText
          imageOnRight={true}
          imgName="ausserliche-therapien_img_5"
          altImage="Einem jungen Mädchen, das lächelt, streicht eine Frau Creme auf die Nase">
          <p>
            Neben topischen Glukokortikoiden kommen zur Behandlung eines akuten
            Neurodermitis-Schubs auch topische Calcineurin-Hemmer zum Einsatz.
            Diese Medikamente wirken entzündungshemmend und immunsuppressiv. Das
            tun sie, indem sie in die Produktion von bestimmten an der
            Hautentzündung beteiligten Substanzen eingreifen, die durch die
            Neurodermitis hervorgerufen wird.<sup>7</sup>
          </p>
        </ImageWithText>
        <Bulletlist
          bigHeading="Wichtige Fakten im Überblick:"
          heading={['Topische Calcineurin-Hemmer', <sup>8</sup>]}
          textList={[
            'sind nichtsteroidale Medikamente, die zur Verringerung von Neurodermitis-Symptomen und -Schüben eingesetzt werden und als Salben und Cremes erhältlich sind.',
            'können auf betroffene Hautstellen in allen Körperregionen aufgetragen werden, einschließlich des Gesichts und der Genitalien, wo topische Glukokortikoide nicht das bevorzugte Mittel sind.',
            'werden bei Bedarf direkt auf die Haut aufgetragen, um das Entzündungsgeschehen zu verringern.',
          ]}
        />
        <h3>Welche Nebenwirkungen haben topische Calcineurin-Hemmer?</h3>
        <p>
          Calcineurin-Hemmer sind im Allgemeinen relativ gut verträglich.
          <sup>9</sup> Eine häufige Nebenwirkung ist ein kribbelndes oder
          brennendes Gefühl auf der Haut, das etwa eine Stunde andauern kann.
          <sup>1,8</sup> Diese Nebenwirkung ist meist vorübergehend und tritt
          nach wenigen Tagen der Anwendung in der Regel nicht mehr auf.
          <sup>1</sup>
        </p>
        <p>
          Starke Sonneneinstrahlung auf eingecremte Körperstellen solltest Du
          allerdings vermeiden, da die Wirkstoffe die Empfindlichkeit gegenüber
          UV-Strahlen erhöhen. Wenn Du topische Calcineurin-Hemmer anwendest,
          solltest Du Sonnenschutzmittel nutzen, um Deine Haut vor UV-Strahlung
          zu schützen.<sup>1</sup>
        </p>
      </div>
      <div className="havas-container dark" data-summary="Fazit">
        <h2>Fazit</h2>
        <p>
          {' '}
          Du weißt selbst nur zu gut, wie stark Neurodermitis den Alltag und das
          Leben beeinträchtigen kann und wie verzweifelt man sich manchmal
          fühlen kann, wenn sich die Erkrankung einfach nicht unter Kontrolle
          bringen lässt. Es gibt eine Fülle an Therapiemöglichkeiten auf dem
          Markt und Deine Behandlung richtet sich immer nach dem Schweregrad
          Deiner Symptome. Wenn Deine Neurodermitis nicht mit äußerlichen
          Therapien kontrolliert werden kann, ist eine{' '}
          <Link to="/innerliche-therapien">
            systemische (innerliche) Therapie
          </Link>{' '}
          vielleicht eine Option für Dich. Manchmal ist einfach etwas Geduld
          gefragt, bis Du die optimale Therapie für Dich gefunden hast. Bleib
          dran und sprich mit einem Dermatologen.
        </p>

        <ImageWithText
          style={isMobile ? {} : { marginTop: 80 }}
          imgName="ausserliche-therapien_img_6"
          altImage="Apotheker öffnet Schublade, in der sich Medikamente befinden.">
          <h3>Innerliche Therapien</h3>
          <p>
            Innerliche Therapien können zum Einsatz kommen, wenn äußerliche
            Behandlungsmethoden nicht ausreichen. Einige Medikamente werden als
            Tablette eingenommen, andere über eine Spritze injiziert. So werden
            beispielsweise Biologika unter die Haut gespritzt und gelangen dann
            über das Blutsystem in den Körper. Diese Therapieformen bekämpfen
            gezielt von innen die Ursache der Neurodermitis und werden in der
            Regel langfristig angewendet.
          </p>
          <Button
            to="/innerliche-therapien"
            text="Mehr erfahren"
            type="primary"
          />
        </ImageWithText>

        <ArztefinderTeaser
          style={isMobile ? { marginTop: 80, marginBottom: 40 } : {}}
        />
        <h3>
          Das könnte Dich auch interessieren: Was beruhigt die Haut bei
          Neurodermitis?
        </h3>

        <ImageWithText
          imgName="ausserliche-therapien_img_7"
          altImage="Eine junge Frau mit roten Hautstellen im Gesicht- und Halsbereich">
          <p>
            Bei Neurodermitis hast Du nicht immer mit Ekzemen zu tun, der
            Zustand Deiner Haut ist mal besser, mal schlechter. Wenn Du gerade
            keinen Schub hast, ist Deine Haut wahrscheinlich dennoch manchmal
            „in Aufruhr“, denn Neurodermitis-Haut ist meist sehr trocken und
            sensibel. Deshalb ist die konsequente Basistherapie wichtig. Selbst
            wenn Dir das regelmäßige Eincremen mal lästig ist, Deine Haut
            braucht Zuwendung, damit sie sich „beruhigen“ kann.
          </p>
          <p>
            Wenn es dann mal wirklich ,,brennt'' und der Juckreiz Dich in den
            Wahnsinn treibt, können diese 3 Tipps vielleicht helfen:
          </p>
        </ImageWithText>

        <Akkordeon heading="Kühlen, kühlen, kühlen">
          <p>
            Kälte ist meist eine wirksame Methode, Juckreiz zu unterdrücken. Um
            juckende Stellen zu kühlen, kannst Du in kaltem Wasser getränkte
            Waschlappen, Umschläge oder Kühlkompressen nutzen.
          </p>
          <p>
            <strong>Unser Tipp:</strong> Bewahre immer eine Kompresse im
            Kühlschrank auf. Die kannst Du dann bei Bedarf in ein Tuch wickeln
            und die juckende Stelle damit maximal 15 Minuten kühlen.
          </p>
        </Akkordeon>

        <Akkordeon heading="Drücken, klopfen, kneifen">
          <p>
            Als Alternative zum Kratzen solltest Du Deine juckende Haut besser
            drücken, streicheln, reiben oder leicht kneifen. Manchmal helfen
            auch sogenannte Kratzklötzchen, die vom Juckreiz ablenken und keine
            Hautverletzungen verursachen.
          </p>
        </Akkordeon>

        <Akkordeon heading="Sprühen, cremen, tupfen">
          <p>
            Anti-Juck-Sprays oder -Lotionen können den Juckreiz ebenfalls
            lindern. Auch Schwarztee kann helfen. Dazu kannst Du 2 Beutel
            schwarzen, nicht aromatisierten Tee aufbrühen und ca. 10-15 Minuten
            ziehen lassen. Lass den Tee abkühlen und betupfe die betroffene
            Hautstelle oder mache einen Umschlag damit. Du solltest danach Deine
            Haut eincremen, da schwarzer Tee diese austrocknen kann.
          </p>
        </Akkordeon>
      </div>
      <div className={`havas-container`}>
        <div className="bottom-text">
          <p>
            <sup>1</sup> Wollenberg A et al. J Eur Acad Dermatol Venereol 2018;
            32:657–68.
          </p>
          <p>
            <sup>2</sup> National Eczema Society. Emollients factsheet. 2018.
            Abrufbar unter:
            eczema.org/wp-content/uploads/Emollients-Oct-18-1.pdf (Abgerufen im
            September 2022).
          </p>
          <p>
            <sup>3</sup> Ring J et al. J Eur Acad Dermatol Venereol 2012;
            26:1045–1060.
          </p>
          <p>
            <sup>4</sup> Wollenberg A et al. J Eur Acad Dermatol Venereol 2018;
            32:850–878.
          </p>
          <p>
            <sup>5</sup> NHS. Topical corticosteroids. 2020. Abrufbar unter:
            nhs.uk/conditions/topical-steroids/ (Abgerufen im September 2022).
          </p>
          <p>
            <sup>6</sup> Eichenfield LF et al. J Am Acad Dermatol 2014;
            71:116–132.
          </p>
          <p>
            <sup>7</sup> RCHSD. Topical calcineurin inhibitors. 2021. Abrufbar
            unter:
            https://www.rchsd.org/programs-services/dermatology/eczema-and-inflammatory-skin-disease-center/treatment/topical-calcineurin-inhibitors/
            (Abgerufen im September 2022).
          </p>
          <p>
            <sup>8</sup> National Eczema Association. Prescription topicals.
            Abrufbar unter: nationaleczema.org/eczema/treatment/topicals/
            (Abgerufen im September 2022).
          </p>
          <p>
            <sup>9</sup>
            https://www.allergieinformationsdienst.de/therapie/medikamente/calcineurin-hemmer.html
          </p>
        </div>
      </div>
    </>
  );
}

export const Head = () => (
  <SEO
    title="Äußere Therapien"
    metaTitle="Neurodermitis – äußere Therapien"
    description="Neurodermitis ist bisher nicht heilbar, akute Schübe können durch äußerliche Therapiemaßnahmen in der Regel unter Kontrolle gebracht werden."
    trustKeyWords="Gesichtspflege bei Neurodermitis, Kortison Salbe, Kortison Nebenwirkungen, Kortison Salbe Rezeptfrei, atopisches Ekzem Behandlung, Therapie Neurodermitis, Neurodermitis Behandlung, chronische Ekzeme Behandlung, Haut Ekzem Behandlung, Lid Ekzem Behandlung, Neurodermitis Kopfhaut Behandlung, Neurodermitis Creme, Neurodermitis Salbe, Behandlung Neurodermitis, Neurodermitis Therapie, Neurodermitis am Auge behandeln"
  />
);
