import { Link } from 'gatsby';
import React, { useContext } from 'react';
import HeadingWithColored from '../components/HeadingWithColored';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';

export default function Symptome() {
  const isMobile = useContext(MobileContext);
  return (
    <>
      <div className="havas-container dark">
        <div className="heading">
          <HeadingWithColored
            textList={['Impressum']}
            coloredWordsArr={['Impressum']}
            variant={`${isMobile ? 'h2' : 'h1'}`}
          />
        </div>
        <p style={{ marginTop: '60px' }}>
          Sanofi-Aventis Deutschland GmbH{' '}
          <a href="https://www.sanofi.de">http://www.sanofi.de</a>
        </p>

        <h2>Herausgeber</h2>
        <p>
          Sanofi-Aventis Deutschland GmbH Industriepark Hoechst, K703
          Brüningstr. 50 65926 Frankfurt am Main
        </p>
        <h2>Kontakt</h2>
        <p className="contactHeader">E-Mail</p>
        <a className="contactText" href="mailto:medinfo.de@sanofi.com">
          medinfo.de@sanofi.com
        </a>
        <p className="contactHeader">Zuständige Aufsichtsbehörde</p>
        <p className="contactText">Regierungspräsidium Darmstadt</p>

        <p className="contactHeader">Handelsregister-Nr.</p>
        <p className="contactText">Frankfurt am Main, Abt. B Nr. 40661</p>

        <p className="contactHeader">Umsatzsteuer-ID</p>
        <p className="contactText">DE 812134551</p>

        <p className="contactHeader">Haftungshinweis</p>
        <p className="contactText">
          Alle Angaben unseres Internetangebotes wurden sorgfältig geprüft. Wir
          bemühen uns, dieses Informationsangebot stetig zu erweitern und zu
          aktualisieren. Die bereitgestellten Informationen erheben jedoch weder
          einen Anspruch auf Aktualität und Richtigkeit noch auf
          Vollständigkeit. Die Benutzung erfolgt auf eigene Gefahr. Bitte
          beachten Sie dazu auch unsere Nutzungsbedingungen.
        </p>
        <p style={{ marginTop: '30px' }}>
          Der Anbieter erklärt, dass zum Zeitpunkt der Verlinkung auf fremde
          Inhalte im World Wide Web (Links und über Frames eingebundene Inhalte)
          diese frei von illegalen Inhalten waren und er keinerlei Einfluss auf
          die aktuelle und zukünftige Gestaltung der verlinkten Inhalte hat. Der
          Anbieter distanziert sich ausdrücklich von den Inhalten von
          verknüpften/verlinkten Seiten, die nach der Linksetzung verändert
          wurden, ganz gleich, ob diese Links vom Anbieter selbst oder von
          Nutzern bereitgestellt wurden. Inhalte von Dritten, die über Frames
          eingebunden sind, sind entsprechend als solche gekennzeichnet.
        </p>
        <p style={{ marginTop: '30px' }}>
          Urheberrechtlicher Hinweis: Inhalt und Struktur der Internet-Seiten
          von Sanofi sind urheber- und ggf. kennzeichenrechtlich geschützt.
          Jegliche Vervielfältigung von Informationen oder Daten, insbesondere
          die Verwendung von Texten, Textteilen, Marken und Kennzeichen,
          Bildmaterial oder sonstigen Inhalten bedarf der vorherigen
          schriftlichen Zustimmung durch den Anbieter bzw. Rechteinhaber. Auch
          insoweit verweisen wir auf unsere Nutzungsbedingungen.
        </p>
        <p style={{ marginTop: '30px' }}>
          Die Sanofi-Aventis Deutschland GmbH ist zur Teilnahme an einem
          Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle
          weder bereit noch dazu verpflichtet.
        </p>
        <p style={{ marginTop: '30px' }}>
          Für unsere Webseiten wurden Bilder von Getty Images, Shutterstock und
          Unsplash verwendet.
        </p>
        <p className="dateText">Aktualisiert: 08.04.2022</p>
        <h2 style={{ marginTop: '80px' }}>Geschäftsführer</h2>
        <p>
          Dr. Fabrizio Guidi (Vorsitzender), Anne Reuschenbach, Oliver
          Coenenberg, Marcus Lueger, Prof. Dr. Jochen Maas
        </p>
        <h2>Vorsitzender des Aufsichtsrats</h2>
        <p style={{ marginRight: 'auto' }}>Brendan O’Callaghan</p>
        <h2 style={{ marginTop: '80px' }}>
          Ansprechpartner innerhalb der deutschsprachigen Länderorganisationen
        </h2>
        <p className="headingText">Deutschland</p>
        <p className="parentText">
          Miriam Henn – Leitung der Kommunikation Deutschland
        </p>

        <p className="headingText">Österreich</p>
        <p className="parentText">
          Juliane Pamme - Leitung der Kommunikation Österreich
        </p>
        <p className="headingText">Schweiz</p>
        <p className="parentText">
          Jacques-Henri Weidmann – Leitung der Kommunikation Schweiz
        </p>
      </div>
    </>
  );
}
export const Head = () => <SEO />;
