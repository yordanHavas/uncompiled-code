import React, { useContext } from 'react';
import HeadingWithColored from '../components/HeadingWithColored';
import InfoAndClickableText from '../components/InfoAndClickableText';
import { MobileContext } from '../components/Layout';
import ResponsiveImage from '../components/ResponsiveImage';
import { SEO } from '../components/seo';

export default function BeraterKontaktieren() {
  const heroMobile = (
    <>
      <div className={`hero`}>
        <HeadingWithColored
          style={{ width: '110%' }}
          textList={[
            'Unser',
            'Neurodermitis',
            'Begleiter Team',
            'ist für Dich da',
          ]}
          col
          coloredWordsArr={['Begleiter Team']}
          variant="h1"
        />
        <ResponsiveImage
          className="image image-br image-tilt"
          imgName="Berater_img_1"
          altImg="Eine junge Frau blick verträumt in die Ferne, während sie ein Handy hält"
        />
      </div>
      <p style={{ marginTop: '40px' }}>
        Neurodermitis kann für Betroffene und Angehörige eine große
        Herausforderung sein. Zögere daher nicht, Deinen Dermatologen oder das
        Behandlungsteam anzusprechen.
      </p>
      <p>
        Hast Du akut Fragen zu Therapieoptionen oder benötigst Du Tipps für den
        Alltag mit Neurodermitis, stehen Dir unsere medizinisch ausgebildeten
        Ansprechpartnerinnen Karin, Anna und Claudia beratend zur Verfügung.
        Diese Service ist für Dich kostenfrei.
      </p>
    </>
  );

  const heroDesktop = (
    <div className={`hero`}>
      <div>
        <HeadingWithColored
          textList={[
            'Unser',
            'Neurodermitis',
            'Begleiter Team',
            'ist für Dich da',
          ]}
          coloredWordsArr={['Begleiter Team']}
          variant="h1"
          className="l-height-desktop-heading"
        />
        <div style={{ margin: '5em', marginTop: '4em' }}>
          <p style={{ margin: 0 }}>
            Neurodermitis kann für Betroffene und Angehörige eine große
            Herausforderung sein. Zögere daher nicht, Deinen Dermatologen oder
            das Behandlungsteam anzusprechen.
          </p>
          <p style={{ margin: 0, marginTop: '40px' }}>
            Hast Du akut Fragen zu Therapieoptionen oder benötigst Tipps für den
            Alltag mit Neurodermitis, stehen Dir unsere medizinisch
            ausgebildeten Ansprechpartnerinnen Karin, Anna und Claudia beratend
            zur Verfügung. Diese Service ist für Dich kostenfrei.
          </p>
        </div>
      </div>

      <ResponsiveImage
        imgName="Berater_img_1"
        altImg="Eine junge Frau blick verträumt in die Ferne, während sie ein Handy hält"
        className="image image-br image-tilt"
      />
    </div>
  );
  const isMobile = useContext(MobileContext);
  return (
    <>
      <div className="havas-container light">
        {isMobile ? heroMobile : heroDesktop}
        <div
          className="anchorOffset"
          style={{ top: `${isMobile ? '-70px' : '-250px'}` }}
          id="threeWomen"></div>
        <InfoAndClickableText
          style={{ marginTop: `${isMobile ? '0' : '-100px'}` }}
        />
      </div>
    </>
  );
}
export const Head = () => <SEO />;
