import React, { useContext } from 'react';
import GoogleMap from '../components/GoogleMap';
import Hero from '../components/Hero';
import { MobileContext } from '../components/Layout';
import { SEO } from '../components/seo';
import TeledermatologieComponent from '../components/TeledermatologieComponent';
const items = [
  {
    img: 'BVDD-Image',
    altImage: 'BVDD Arztsuche',
    width: '249px',
    height: '77px',
    header: 'BVDD Arztsuche',
    headerText:
      'Der Berufsverband der Deutschen Dermatologen (BVDD) unterstützt Dich dabei, einen Hautarzt in Deiner Nähe zu finden. Die Suchfunktion bietet mehr Optionen, als eine unspezifische Internet-Recherche. Hier kannst Du einen konkreten Namen eingeben oder Deine Umgebung per Postleitzahl nach einem Facharzt für Hautkrankheiten absuchen.',
    buttonText: 'Zur BVVD-Arztsuche',
    buttonLink: 'https://www.bvdd.de/fuer-patienten/hautarztsuche/',
  },
  {
    img: 'daad_wegweiser',
    altImage: 'daad_wegweiser Logo',
    width: '199px',
    height: '77px',
    header: 'Allergie-Wegweiser',
    headerText:
      'Menschen mit Neurodermitis leiden nicht selten auch unter Allergien. Falls Du Ärzte und Kliniken suchst, die sich mit Allergien auskennen bist Du beim „Allergie-Wegweiser“ genau richtig. Hier kannst Du unter anderem auch nach allergologisch versierten Ernährungsfachkräften und Schulungen zum Umgang mit Allergien im Alltag suchen. Über den „Allergie-Wegweiser“ findest Du in Deiner Umgebung Experten, Kliniken und Einrichtungen, die sich auf Allergien, Atemwegs- und Hauterkrankungen spezialisiert haben.',
    buttonText: 'Zum Allergie-Wegweiser',
    buttonLink: 'https://www.daab.de/',
  },
];
export default function Arztefinder() {
  const isMobile = useContext(MobileContext);

  return (
    <>
      <div className="havas-container light">
        <Hero
          headingTextList={['ÄRZTEFINDER']}
          className="centerHeding"
          coloredWordsArr={['FINDER']}
          imgName="kopfhaur_img_5"
          altImg="Eine Ärztin im Gespräch mit einer Patientin"
          text="Kann ich einem Neurodermitis-Schub vorbeugen? Welche Behandlungs-möglichkeiten gibt es, neben der Therapie eines akuten Schubs? Was genau bedeutet systemische Therapie? Wie kann ich den ständigen Juckreiz kontrollieren? Du hast Fragen und suchst nach einem Spezialisten in Deiner Nähe? Wir stellen Dir Online-Ärztefinder vor."
        />
        <h2>Finde einen Neurodermitis-Spezialisten in Deiner Nähe!</h2>
        <p>
          In dieser Suche sind Dermatologen aufgelistet, die über die
          teledermatologische Plattform OnlineDoctor angesprochen werden können
          und moderne Therapien anwenden.
        </p>
        <GoogleMap />
      </div>
      <TeledermatologieComponent items={items} />
    </>
  );
}
export const Head = () => <SEO />;
