export default {
  activeTab: null,
  'subscribe-button': {
    feed: 'https://feeds.podlovers.org/mp3',
    clients: [
      {
        id: 'antenna-pod',
        service: null,
      },
      {
        id: 'apple-podcasts',
        service: 'id1523714548',
      },
      {
        id: 'beyond-pod',
        service: null,
      },
      {
        id: 'castbox',
        service: 'id3117097',
      },
      {
        id: 'castro',
        service: 'id1523714548',
      },
      {
        id: 'clementine',
        service: null,
      },
      {
        id: 'downcast',
        service: null,
      },
      {
        id: 'google-podcasts',
        service: 'https://feeds.podlovers.org/mp3',
      },
      {
        id: 'gpodder',
        service: null,
      },
      {
        id: 'i-catcher',
        service: null,
      },
      {
        id: 'instacast',
        service: null,
      },
      {
        id: 'overcast',
        service: null,
      },
      {
        id: 'pocket-casts',
        service: '194w03r3',
      },
      {
        id: 'podcat',
        service: null,
      },
      {
        id: 'podcast-addict',
        service: null,
      },
      {
        id: 'podcast-republic',
        service: null,
      },
      {
        id: 'pod-grasp',
        service: null,
      },
      {
        id: 'podscout',
        service: null,
      },
      {
        id: 'player-fm',
        service: '2938422',
      },
      {
        id: 'rss',
        service: 'https://feeds.podlovers.org/mp3',
      },
      {
        id: 'rss-radio',
        service: null,
      },
    ],
  },
  share: {
    channels: [
      'facebook',
      'twitter',
      'whats-app',
      'linkedin',
      'pinterest',
      'xing',
      'mail',
      'link',
    ],
    outlet: 'https://cdn.podlove.org/web-player/5.x/share.html',
    sharePlaytime: true,
  },
  'related-episodes': {
    source: 'podcast',
    value: 25,
  },
  version: 5,
  playlist:
    'https://backend.podlovers.org/wp-json/podlove-web-player/shortcode/podcast',
  theme: {
    tokens: {
      brand: '#E64415',
      brandDark: '#633bb0',
      brandDarkest: '#453080',
      brandLightest: 'white',
      shadeDark: '#807E7C',
      shadeBase: '#807E7C',
      contrast: '#41337B',
      alt: '#fff',
    },
    fonts: {
      ci: {
        name: 'ci',
        family: [
          '-apple-system',
          'BlinkMacSystemFont',
          'Segoe UI',
          'Roboto',
          'Helvetica',
          'Arial',
          'sans-serif',
          'Apple Color Emoji',
          'Segoe UI Emoji", "Segoe UI Symbol',
        ],
        src: [],
        weight: 800,
      },
      regular: {
        name: 'regular',
        family: [
          '-apple-system',
          'BlinkMacSystemFont',
          'Segoe UI',
          'Roboto',
          'Helvetica',
          'Arial',
          'sans-serif',
          'Apple Color Emoji',
          'Segoe UI Emoji", "Segoe UI Symbol',
        ],
        src: [],
        weight: 300,
      },
      bold: {
        name: 'bold',
        family: [
          'MuseoSans',
          '-apple-system',
          'BlinkMacSystemFont',
          'Segoe UI',
          'Roboto',
          'Helvetica',
          'Arial',
          'sans-serif',
          'Apple Color Emoji',
          'Segoe UI Emoji", "Segoe UI Symbol',
        ],
        src: [],
        weight: 700,
      },
    },
  },
  base: 'https://cdn.podlove.org/web-player/5.x/',
};
